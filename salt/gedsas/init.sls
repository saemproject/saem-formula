{% from "gedsas/alfresco/map.jinja" import alfresco with context %}
{% if salt['file.directory_exists']('/opt/alfresco-5.0.d/tomcat/webapps') %}
include:
    - gedsas.proxy
    - gedsas.alfresco.alfresco_war
    - gedsas.alfresco.update   
{% else %}
include:
  - gedsas.proxy
  - gedsas.imagemagick
  - gedsas.libreoffice
  - gedsas.ghostscript
  - gedsas.swftools
  - gedsas.sun-java
  - gedsas.fido
  - gedsas.alfresco.alfresco
  - gedsas.alfresco.tomcat
  - gedsas.alfresco.config
  - gedsas.alfresco.clean-deploy
{% endif %} 
