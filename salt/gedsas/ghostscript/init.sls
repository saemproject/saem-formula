{% from "gedsas/ghostscript/map.jinja" import gs with context %}

{%- set gs_locations = salt['cmd.run']("whereis gs").split(' ')  %}

{%- if gs_locations|length <  2 %}
 tmp_dir_gs:
  file.directory:
   - name: /tmp
 tar:
  pkg:
   - installed

 untar_gs:
  archive.extracted:
   - name: /tmp
   - source: {{ gs.url }}
   - source_hash: sha1={{ gs.sha1 }}
   - archive_format: tar
   - keep: True
   - if_missing: /tmp/{{ gs.dir }}
   - require:
     - pkg: tar

 gcc.packages_gs:
   pkg.installed:
     - pkgs:
       - gcc
       - gcc-c++
       - automake
       - zlib-devel
       - libjpeg-turbo-devel
       - giflib-devel
       - freetype-devel

 compile_gs:
  cmd.run:
   - name: cd  /tmp/{{ gs.dir }} && ./configure && make && make install
   - require:
     - archive: untar_gs
     - pkg: gcc.packages_gs

 /tmp/{{ gs.dir }}:
  file.absent:
   - require:
     - cmd: compile_gs
{%- endif %}
