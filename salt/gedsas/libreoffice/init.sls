{% from "gedsas/libreoffice/map.jinja" import libreoffice with context %}

{%- set libreoffice_locations = salt['cmd.run']("whereis libreoffice").split(' ')  %}

{%- if libreoffice_locations|length <  2 %}
 tar:
  pkg:
    - installed

 untar_libreoffice:
  archive.extracted:
   - name: /tmp
   - source: {{ libreoffice.url }}
   - source_hash: sha1={{ libreoffice.sha1 }}
   - archive_format: tar
   - keep: True
   - if_missing: /tmp/{{ libreoffice.dir }}
   - require:
     - pkg: tar

 libreoffice.dependencies:
   pkg.installed:
     - pkgs:
       - gnome-vfs2-devel 

 install_libreoffice:
  cmd.run:
   - name:
      cd  /tmp/{{ libreoffice.dir }}/RPMS && yum -y localinstall *.rpm
   - require:
     - archive: untar_libreoffice
 /tmp/{{ libreoffice.dir }}:
  file.absent:
   - require:
     - pkg: libreoffice.dependencies
{%- endif %}






