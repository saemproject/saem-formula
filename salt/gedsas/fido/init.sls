# -*- coding: utf-8 -*-
# vim: ft=sls
{%- from 'asalae/fido/map.jinja' import fido with context %}

python-setuptools:
  pkg:
    - installed

fido_dist:
  pkg.installed:
     - name: unzip
fido_extract:
  archive.extracted:
    - name: /opt
    - source: {{ fido.dist.url }}
    - archive_format: zip
    - source_hash: {{ fido.dist.hash }}
    - keep: True
    - if_missing: /opt/fido-1.3.2-81
    - require:
      - pkg: unzip
      - pkg: python-setuptools

fido_compil:
  cmd.run:
    - cwd: /opt/fido-1.3.2-81
    - name: /usr/bin/python setup.py install
