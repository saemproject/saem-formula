{%- from 'gedsas/sun-java/settings.sls' import java with context %}

{#- require a filename - there is no default java tar.gz filename #}

{%- if java.filename is defined %}

java-install-dir:
  file.directory:
    - name: {{ java.prefix }}
    - user: root
    - group: root
    - mode: 755
    - makedirs: True

{{ java.prefix }}/{{ java.filename }}:
  file.managed:
   - user: root
   - group: root
   - source: salt://gedsas/sun-java/files/{{ java.filename }}
   - template: jinja
   - require:
     - file: java-install-dir

unpack-jdk-tarball:
  archive.extracted:
    - name: {{ java.prefix }}
    - source: file://{{ java.prefix }}/{{ java.filename }}
    - archive_format: tar
    - options: x
    - user: root
    - group: root
    - if_missing: {{ java.java_real_home }}

create-java-home:
  alternatives.install:
    - name: java-home
    - link: {{ java.java_home }}
    - path: {{ java.java_real_home }}
    - priority: 30
    - onlyif: test -d {{ java.java_real_home }} && test ! -L {{ java.java_home }}
    - require:
      - archive: unpack-jdk-tarball

remove-jdk-tarball:
  file.absent:
    - name: {{ java.prefix }}/{{ java.filename }}

{%- endif %}
