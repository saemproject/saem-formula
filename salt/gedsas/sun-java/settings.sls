{% set p  = salt['pillar.get']('java', {}) %}
{% set g  = salt['grains.get']('java', {}) %}

{%- set java_home            = salt['grains.get']('java_home', salt['pillar.get']('java_home', '/opt/java')) %}

{%- set default_version_name = 'jre1.8.0_77' %}
{%- set default_prefix       = '/opt/java' %}
{%- set default_filename   = 'jre-8u77-linux-x64.tar.gz' %}
{%- set default_source_hash  = 'edd9db13e81913d8758263eb5e923cbe854d24c1d47a61a7d62064a7dba5255f' %}
{%- set default_jce_url      = 'http://download.oracle.com/otn-pub/java/jce/8/jce_policy-8.zip' %}
{%- set default_jce_hash     = 'f3020a3922efd6626c2fff45695d527f34a8020e938a49292561f18ad1320b59' %}
{%- set default_dl_opts      = '-b oraclelicense=accept-securebackup-cookie -L -s' %}

{%- set version_name         = g.get('version_name', p.get('version_name', default_version_name)) %}
{%- set filename           = g.get('filename', p.get('filename', default_filename)) %}

{%- set jce_url              = g.get('jce_url', p.get('jce_url', default_jce_url)) %}

{%- if jce_url == default_jce_url %}
  {%- set jce_hash           = default_jce_hash %}
{%- else %}
  {%- set jce_hash           = g.get('jce_hash', p.get('jce_hash', '')) %}
{%- endif %}

{%- set dl_opts              = g.get('dl_opts', p.get('dl_opts', default_dl_opts)) %}
{%- set prefix               = g.get('prefix', p.get('prefix', default_prefix)) %}
{%- set java_real_home       = prefix + '/' + version_name %}
{%- set jre_lib_sec          = java_real_home + '/jre/lib/security' %}

{%- set java = {} %}
{%- do java.update( { 'version_name'   : version_name,
                      'filename'      : filename,
                      'jce_url'        : jce_url,
                      'jce_hash'       : jce_hash,
                      'dl_opts'        : dl_opts,
                      'java_home'      : java_home,
                      'prefix'         : prefix,
                      'java_real_home' : java_real_home,
                      'jre_lib_sec'    : jre_lib_sec,
                    } ) %}
