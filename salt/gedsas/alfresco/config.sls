{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

{{ alfresco.tomcat_home }}/lib/postgresql-9.4.1212.jar:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/postgresql-9.4.1212.jar


{{ alfresco.tomcat_home }}/bin/setenv.sh:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/setenv.sh
   - template: jinja
   - mode: 755


{{ alfresco.tomcat_home }}/conf/catalina.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/catalina.properties


{{ alfresco.tomcat_home }}/shared/classes/alfresco-global.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/alfresco-global.properties
   - makedirs: True
   - template: jinja
   
{{ alfresco.tomcat_home }}/shared/classes/saem-pes-v2.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/saem-pes-v2.properties
   - makedirs: True
   - template: jinja

{{ alfresco.tomcat_home }}/shared/classes/saem.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/saem.properties
   - makedirs: True
   - template: jinja

{{ alfresco.tomcat_home }}/conf/Catalina/localhost/solr4.xml:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/solr/solr4.xml
   - makedirs: True
   - template: jinja

{{ alfresco.home }}/solr4/archive-SpacesStore/conf/solrcore.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/solr/archive/solrcore.properties
   - makedirs: True
   - template: jinja

{{ alfresco.home }}/solr4/workspace-SpacesStore/conf/solrcore.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/solr/workspace/solrcore.properties
   - makedirs: True
   - template: jinja





