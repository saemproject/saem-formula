{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

{{ alfresco.tomcat_home }}/shared/classes/alfresco-global.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/alfresco-global.properties
   - makedirs: True
   - template: jinja

{{ alfresco.tomcat_home }}/shared/classes/alfresco/extension/subsystems/Authentication/ldap/ldap1/ldap-authentication.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/ldap-authentication.properties
   - makedirs: True
   - template: jinja

{{ alfresco.tomcat_home }}/shared/classes/alfresco/web-extension/site-webscripts/org/alfresco/share/header/share-header.get.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/share-header.get.properties
   - makedirs: True
   - template: jinja
{{ alfresco.tomcat_home }}/shared/classes/alfresco/web-extension/messages/saemMessages_fr.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/saemMessages_fr.properties
   - makedirs: True
   - template: jinja
   
{{ alfresco.tomcat_home }}/shared/classes/saem-pes-v2.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/saem-pes-v2.properties
   - makedirs: True
   - template: jinja

{{ alfresco.tomcat_home }}/shared/classes/saem.properties:
  file.managed:
   - user: {{ alfresco.user }}
   - group: {{ alfresco.user }}
   - source: salt://gedsas/alfresco/files/saem.properties
   - makedirs: True
   - template: jinja

start_service_alfresco:
 cmd.run:
  - name: service alfresco start
