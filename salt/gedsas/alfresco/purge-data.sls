{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

stop_service_alfresco:
 cmd.run:
  - name: service alfresco stop
clean_data:
 cmd.run:
  - name: |
     rm -rf {{ alfresco.data }}/solr4/content/_DEFAULT_/*
     rm -rf {{ alfresco.data }}/archive/SpaceStore/index/*
     rm -rf {{ alfresco.data }}/workspace/SpaceStore/index/*
start_service_alfresco:
 cmd.run:
  - name: service alfresco start