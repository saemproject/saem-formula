{% from "gedsas/alfresco/map.jinja" import alfresco with context %}JAVA_HOME={{ alfresco.java.home }}
JRE_HOME=$JAVA_HOME
JAVA_OPTS="-XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -Djava.awt.headless=true -Dalfresco.home={{ alfresco.home }} -Dcom.sun.management.jmxremote -XX:ReservedCodeCacheSize=128m $JAVA_OPTS "
JAVA_OPTS="-Xms512M -Xmx7026M $JAVA_OPTS " # java-memory-settings
export JAVA_HOME
export JRE_HOME
export JAVA_OPTS
			    
