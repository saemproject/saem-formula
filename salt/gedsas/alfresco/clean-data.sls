{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

clean_data:
 cmd.run:
  - name: |
     rm -rf {{ alfresco.data }}/content*
     rm -rf {{ alfresco.data }}/solr4*
     rm -rf {{ alfresco.data }}/archive*
     rm -rf {{ alfresco.data }}/workspace*
