{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

clean_tomcat:
 cmd.run:
  - name: rm -rf {{ alfresco.tomcat_home }}/work && rm -rf {{ alfresco.tomcat_home }}/temp/* && rm -rf {{ alfresco.tomcat_home }}/webapps/*

{{ alfresco.tomcat_home }}/webapps/solr4.war:
 file.managed:
  - user: {{ alfresco.user }}
  - group: {{ alfresco.user }}
  - source: salt://gedsas/alfresco/files/solr4.war
  - keep: True
  - force: true
  - require:
    - cmd: clean_tomcat
{{ alfresco.tomcat_home }}/webapps/share.war:
 file.managed:
  - user: {{ alfresco.user }}
  - group: {{ alfresco.user }}
  - source: salt://gedsas/alfresco/files/share.war
  - keep: True
  - force: true
  - require:
    - cmd: clean_tomcat
  
{{ alfresco.tomcat_home }}/webapps/alfresco.war:
 file.managed:
  - user: {{ alfresco.user }}
  - group: {{ alfresco.user }}
  - source: salt://gedsas/alfresco/files/alfresco.war
  - keep: True
  - force: true
  - require:
    - cmd: clean_tomcat
