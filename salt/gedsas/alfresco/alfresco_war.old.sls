{% from "gedsas/alfresco/map.jinja" import alfresco with context %}
git_install:
 pkg.installed:
  - name: git
git_init:
 cmd.run:
  - name: git init
  - cwd: /tmp
ssh-key-generate:
 cmd.run:
  - name: ssh-keygen -q -N '' -f /root/.ssh/id_rsa
  - runas: root
  - unless: test -f /root/.ssh/id_rsa
alfresco_wars:
  git.latest:
    - name: https://framagit.org/saemproject/alfresco-war.git
    - identity: "/root/.ssh/id_rsa"
    - target: /tmp/gedsasWars
    - rev: {{ alfresco.branch }}
    - force_fetch: True
    - force_reset: True
    - force_checkout: True
stop_service_alfresco:
 cmd.run:
  - name: service alfresco stop
  - require:
     - git: alfresco_wars
clean_directory:
 cmd.run:
  - name: |
     rm -rf {{ alfresco.tomcat_home }}/webapps/share/
     rm -rf {{ alfresco.tomcat_home }}/webapps/alfresco/
  - require:
     - cmd: stop_service_alfresco
clean_war_files:
 cmd.run:
  - name: |
     rm -rf {{ alfresco.tomcat_home }}/webapps/share.war
     rm -rf {{ alfresco.tomcat_home }}/webapps/alfresco.war
  - require:
     - cmd: stop_service_alfresco
war_files_in_webapps:
 cmd.run:
  - name: |
     mv /tmp/gedsasWars/alfresco.war {{ alfresco.tomcat_home }}/webapps/
     mv /tmp/gedsasWars/share.war {{ alfresco.tomcat_home }}/webapps/
  - require:
     - git: alfresco_wars
nettoyage:
 cmd.run:
  - name: |
     rm -rf /tmp/gedsasWars
  - require:
     - git: alfresco_wars
