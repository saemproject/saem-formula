{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

unzip:
 pkg:
   - installed

convmv:
 pkg:
   - installed

tmp_dir:
 file.directory:
  - name: /tmp

opt_dir:
 file.directory:
  - name: /opt
  - user: root
  - group: root
  - recurse:
    - user
    - group

get_alfresco_zip:
 file.managed:
  - source: salt://gedsas/alfresco/files/alfresco-community-5.0.d.zip
  - source_hash: {{ alfresco.zip.source_hash }}
  - name: /tmp/{{ alfresco.zip.name }}.zip
  - keep: True
  - require:
    - file: tmp_dir
  - unless: ls {{ alfresco.home }}

unzip_alfresco:
 cmd.run:
  - name:  unzip /tmp/{{ alfresco.zip.name }}.zip -d {{ alfresco.install.dir }}
  - require:
    - pkg: unzip
    - file: opt_dir
    - file: get_alfresco_zip
  - unless: ls {{ alfresco.home }}

rename_alfresco_dir:
 file.rename:
 - name : {{ alfresco.home }}
 - source : {{ alfresco.install.dir }}/{{ alfresco.zip.name }}
 - makedirs : True
 - require:
   - cmd: unzip_alfresco
 - unless: ls {{ alfresco.home }}

clean_alfresco_dir:
 cmd.run:
  - name: rm -rf {{ alfresco.home }}/web-server && rm -rf {{ alfresco.home }}/bin && rm -rf {{ alfresco.home }}/amps
  - require:
    - file: rename_alfresco_dir
