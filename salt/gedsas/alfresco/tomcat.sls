{% from "gedsas/alfresco/map.jinja" import alfresco with context %}

get_tomcat_zip:
 file.managed:
  - source: {{ alfresco.tomcat.source }}
  - source_hash: {{ alfresco.tomcat.source_hash }}
  - name: /tmp/{{ alfresco.tomcat.name }}.zip
  - keep: True

unzip_tomcat:
 cmd.run:
  - name: |
     unzip /tmp/{{ alfresco.tomcat.name }}.zip -d {{ alfresco.home }} 
     rm -rf {{ alfresco.home }}/{{ alfresco.tomcat.name }}/webapps/*
  - require:
    - file: get_tomcat_zip
  - unless: ls {{ alfresco.home }}/{{ alfresco.tomcat.name }}

{{ alfresco.user }}:
 user.present

tomcat_symlink:
 file.symlink:
  - name: {{ alfresco.home }}/tomcat
  - target: {{ alfresco.home }}/{{ alfresco.tomcat.name }}
  - require:
    - cmd: unzip_tomcat

{{ alfresco.home }}:
 file.directory:
  - user: {{ alfresco.user }}
  - group: {{ alfresco.user }}
  - recurse:
     - user 
     - group
  - require:
    - file: tomcat_symlink

chmod_bin_sh:
 cmd.run:
  - name: |
     cd {{ alfresco.home }}/tomcat/bin
      chmod +x *.sh
  - require: 
    - file: tomcat_symlink

alfresco_initd:
  file.managed:
   - source: salt://gedsas/alfresco/files/alfresco
   - name: /etc/init.d/alfresco 
   - template: jinja
   - defaults:
      tomcat_user: {{ alfresco.user }}
      alfresco_home: {{ alfresco.home }}
   - require: 
     - cmd: chmod_bin_sh


install_service:
 cmd.run:
  - name: |
     chmod +x /etc/init.d/alfresco
     /sbin/chkconfig --add alfresco
     /sbin/chkconfig  alfresco on
  - require: 
    - file: alfresco_initd




