{% from "gedsas/alfresco/map.jinja" import alfresco with context %}
unzip_install_if_not_exist:
 pkg.installed:
  - name: unzip
recup_war:
 cmd.run:
  - name: wget -P /tmp 'https://framagit.org/saemproject/alfresco-war/-/archive/{{ alfresco.branch }}/alfresco-war-{{ alfresco.branch }}.zip'
/tmp:
  archive.extracted:
    - source: /tmp/alfresco-war-{{ alfresco.branch }}.zip
    - use_cmd_unzip: True
    - skip_verify: True
    - if_missing: /tmp/alfresco-war-{{ alfresco.branch }}
stop_service_alfresco:
 cmd.run:
  - name: service alfresco stop
  - require:
     - archive: /tmp
clean_directory:
 cmd.run:
  - name: |
     rm -rf {{ alfresco.tomcat_home }}/webapps/share/
     rm -rf {{ alfresco.tomcat_home }}/webapps/alfresco/
     rm -rf {{ alfresco.tomcat_home }}/webapps/share.war.backup
     rm -rf {{ alfresco.tomcat_home }}/webapps/alfresco.war.backup
  - require:
     - cmd: stop_service_alfresco
backup_last_war_files:
 cmd.run:
  - name: |
     mv {{ alfresco.tomcat_home }}/webapps/share.war {{ alfresco.tomcat_home }}/webapps/share.war.backup
     mv {{ alfresco.tomcat_home }}/webapps/alfresco.war {{ alfresco.tomcat_home }}/webapps/alfresco.war.backup
  - require:
     - archive: /tmp
     - cmd: stop_service_alfresco
war_files_in_webapps:
 cmd.run:
  - name: |
     mv /tmp/alfresco-war-{{ alfresco.branch }}/alfresco.war {{ alfresco.tomcat_home }}/webapps/
     mv /tmp/alfresco-war-{{ alfresco.branch }}/share.war {{ alfresco.tomcat_home }}/webapps/
  - require:
     - archive: /tmp
     - cmd: stop_service_alfresco
nettoyage:
 cmd.run:
  - name: |
     rm -rf /tmp/alfresco-war-{{ alfresco.branch }}
     rm -rf /tmp/alfresco-war-{{ alfresco.branch }}.zip