{% from "gedsas/swftools/map.jinja" import swftools with context %}

swf_tools:
 archive.extracted:
  - name: /tmp
  - source: {{ swftools.url }}
  - source_hash: sha1={{ swftools.sha1 }}
  - archive_format: tar
  - keep: True
  - if_missing: /tmp/{{ swftools.dir }}

gcc.packages:
  pkg.installed:
    - pkgs:
      - gcc
      - gcc-c++
      - automake
      - zlib-devel
      - libjpeg-turbo-devel
      - giflib-devel
      - freetype-devel

compile_swf_tools:
 cmd.run:
  - name: cd  /tmp/{{ swftools.dir }} && ./configure && make && make install
  - require:
    - archive: swf_tools
    - pkg: gcc.packages

/tmp/{{ swftools.dir }}:
 file.absent:
  - require:
    - cmd: compile_swf_tools
