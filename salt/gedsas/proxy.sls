{% from "gedsas/alfresco/map.jinja" import alfresco with context %}
http_proxy_env:
 environ.setenv:
  - name : http_proxy
  - value : {{ alfresco.proxy }}
  - update_minion: True
https_proxy_env:
 environ.setenv:
  - name : https_proxy
  - value : {{ alfresco.proxy }}
  - update_minion: True
