{% from "saemref/map.jinja" import saemref with context %}
http_proxy_env:
 environ.setenv:
  - name : http_proxy
  - value : {{ saemref.instance.proxy }}
  - update_minion: True
https_proxy_env:
 environ.setenv:
  - name : https_proxy
  - value : {{ saemref.instance.proxy }}
  - update_minion: True
