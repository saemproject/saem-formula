{% from "saemref/map.jinja" import saemref with context %}

vocabs:
  cmd.run:
   - name: |
      curl -o /home/saemref/RefAction.xml http://data.culture.fr/thesaurus/data/ark:/67717/T2?includeSchemes=true
      curl -o /home/saemref/RefContexteHist.xml http://data.culture.fr/thesaurus/data/ark:/67717/T4?includeSchemes=true
      curl -o /home/saemref/RefTypologie.xml http://data.culture.fr/thesaurus/data/ark:/67717/T3?includeSchemes=true
      curl -o /home/saemref/ThesaurusMatiere.xml http://data.culture.fr/thesaurus/data/ark:/67717/Matiere?includeSchemes=true
      curl -o /home/saemref/RefActivite.xml http://data.culture.fr/thesaurus/data/ark:/67717/51232822-adac-4a33-aa14-29e2c701a5ee?includeSchemes=true
      curl -o /home/saemref/RefDomaine.xml http://data.culture.fr/thesaurus/data/ark:/67717/f14e8183-5885-46d6-8fc9-17ebd8f3c27e?includeSchemes=true
import-vocabs:
  cmd.run:
   - name: |
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl skos-import saemref /home/saemref/RefAction.xml
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl skos-import saemref /home/saemref/RefContexteHist.xml
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl skos-import saemref /home/saemref/RefTypologie.xml
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl skos-import saemref /home/saemref/ThesaurusMatiere.xml
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl skos-import saemref /home/saemref/RefActivite.xml
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl skos-import saemref /home/saemref/RefDomaine.xml
      /home/{{ saemref.instance.user }}/venv/bin/cubicweb-ctl list
   - user: {{ saemref.instance.user }}
   - env:
        CW_MODE: user

