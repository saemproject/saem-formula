{% if salt['file.file_exists']('/home/saemref/etc/cubicweb.d/saemref/all-in-one.conf') %}
include:
  - saemref.proxy
  - saemref.upgrade
{% else %}
include:
  - saemref.proxy
  - saemref.install
  - saemref.client
  - saemref.config
  - saemref.db-create
  - saemref.import-vocab
  - saemref.supervisor
{% endif %}
