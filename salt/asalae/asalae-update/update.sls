{% from "asalae/asalae/map.jinja" import asalae with context %}
{% from "asalae/apache/map.jinja" import apache with context %}

httpd-service:
 service.running:
  - name: {{ apache.service }}
  - enable: False
rename_dir:
 cmd.run:
  - name: mv /opt/asalae1.* /opt/{{ asalae.dist.name }}
/var/www/asalae:
  file.symlink:
    - target: /opt/{{ asalae.dist.name }}
backup-app:
 cmd.run:
  - name: |
     cp -r /opt/{{ asalae.dist.name }}/app /tmp/maj-asalae
     tar cvf /opt/app_du_$(date +%d-%m-%Y-%H-%M).tar.gz /opt/{{ asalae.dist.name }}/app
copy_asalae_old_app_dir:
 cmd.run:
  - name: cp -a /opt/{{ asalae.dist.name }}/app /opt/{{ asalae.dist.name }}/app.old

tmp_dir:
 file.directory:
  - name: /tmp 
get_asalae_zip:
 file.managed:
  - source: salt://asalae/files/{{ asalae.dist.name }}.zip
  - name: /tmp/{{ asalae.dist.name }}.zip
  - keep: True
  - require:
     - file: tmp_dir

unzip_asalae:
 cmd.run:
  - name:  unzip /tmp/{{ asalae.dist.name }}.zip -d /tmp/{{ asalae.dist.name }}
  - require:
    - file: get_asalae_zip

rep_app_copied:
 cmd.run:
  - name: cp -r /tmp/{{ asalae.dist.name }}/{{ asalae.dist.name }}/app /opt/{{ asalae.dist.name }}
  - require:
    - file: get_asalae_zip
droit_app:
 cmd.run:
  - name: chmod -R 757 /opt/{{ asalae.dist.name }}/app
/var/www/asalae/app/webroot/check/.htpasswd:
  file.managed:
    - source: salt://asalae/asalae/htpasswd
models:
 cmd.run:
  - name: cd /opt/{{ asalae.dist.name }}/app/webroot/files/models && rename _default.odt .odt *.odt
/data/echanges/entree:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
{% for cfg in ('asalae.ini.php', 'database.php', 'types_connecteurs.ini.php') %}
/var/www/asalae/app/config/{{ cfg }}:
 file.managed:
   - source: salt://asalae/asalae/{{ cfg }}
   - template: jinja
{% endfor %}
{% if (asalae.version.cible - asalae.version.actuelle == 1) %}
majschema:
 cmd.run:
  - name: cake/console/cake majschema{{ asalae.version.actuelle }}_{{ asalae.version.cible }}
  - cwd : /var/www/asalae/
{% else %}
{% for number in range(asalae.version.actuelle + 1, asalae.version.cible + 1,1)%}
majschema-{{number}}:
 cmd.run:
  - name: cake/console/cake majschema{{number}}_{{number}} + 1
{% endfor %}
  - cwd : /var/www/asalae/
{% endif %}
/var/www/asalae/app/vendors/as@lae/compteursexternes/drivers/CptExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_compteurs_externes/CptExtDriverSaemcd33.php
  - template: jinja

/var/www/asalae/app/vendors/as@lae/profilsexternes/drivers/ProfilExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_profils_externes/ProfilExtDriverSaemcd33.php

/var/www/asalae/app/vendors/as@lae/servicesexternes/drivers/ServiceExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_services_externes/ServiceExtDriverSaemcd33.php
  
/var/www/asalae/app/vendors/as@lae/servicesexternes/drivers/ServiceExtDriverSaemcd33Notices.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_services_externes/ServiceExtDriverSaemcd33Notices.php

/var/www/asalae/app/vendors/as@lae/vocabulairesexternes/drivers/VocabulaireExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_vocabulaires_externes/VocabulaireExtDriverSaemcd33.php

/var/www/asalae/app/vendors/as@lae/collectivitesexternes/drivers/CollectiviteExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_collectivites_externes/CollectiviteExtDriverSaemcd33.php
apache-reload:
 module.wait:
  - name: service.reload
  - m_name: {{ apache.service }}
apache-restart:
 module.wait:
  - name: service.restart
  - m_name: {{ apache.service }}
clean_asalae_dir:
 cmd.run:
  - name: rm -rf /tmp/{{ asalae.dist.name }}
clean_asalae_zip:
 cmd.run:
  - name: rm -rf /tmp/{{ asalae.dist.name }}.zip
