{% from "asalae/asalae/map.jinja" import asalae with context %}
{% from "asalae/apache/map.jinja" import apache with context %}

httpd-service:
 service.running:
  - name: {{ apache.service }}
  - enable: False
backup-app:
 cmd.run:
  - name: |
     cp -r /opt/{{ asalae.dist.name }}/app /tmp/maj-asalae
     tar cvf /opt/app_du_$(date +%d-%m-%Y-%H-%M).tar.gz /opt/{{ asalae.dist.name }}/app
/var/www/asalae/app/vendors/as@lae/compteursexternes/drivers/CptExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_compteurs_externes/CptExtDriverSaemcd33.php
  - template: jinja

/var/www/asalae/app/vendors/as@lae/profilsexternes/drivers/ProfilExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_profils_externes/ProfilExtDriverSaemcd33.php

/var/www/asalae/app/vendors/as@lae/servicesexternes/drivers/ServiceExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_services_externes/ServiceExtDriverSaemcd33.php

/var/www/asalae/app/vendors/as@lae/servicesexternes/drivers/ServiceExtDriverSaemcd33Notices.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_services_externes/ServiceExtDriverSaemcd33Notices.php

/var/www/asalae/app/vendors/as@lae/vocabulairesexternes/drivers/VocabulaireExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_vocabulaires_externes/VocabulaireExtDriverSaemcd33.php

/var/www/asalae/app/vendors/as@lae/collectivitesexternes/drivers/CollectiviteExtDriverSaemcd33.php:
 file.managed:
  - source: salt://asalae/files/asalae_drivers/as@lae_collectivites_externes/CollectiviteExtDriverSaemcd33.php
apache-reload:
 module.wait:
  - name: service.reload
  - m_name: {{ apache.service }}
apache-restart:
 module.wait:
  - name: service.restart
  - m_name: {{ apache.service }}