<?php
class CollectiviteExtDriverSaemcd33 implements CollectiviteExterneInterface {

    /**
     * retourne la description du driver qui sera affiché dans la vue détaillée
     * @return string description du driver
     */
    public function description() {
        return 'Référentiel des collectivités du SAEMCD33 développé par Logilab';
    }

    /**
     * retourne la liste des paramètres et de leurs définitions utilisés par le driver
     * @return string descriptiondu compteur
     */
    public function parametersDefinition() {
        return array(
            'url' => array(
                'label' => 'Url du référentiel des collectivités',
                'title' => 'veuillez saisir l\'url du référentiel logilab du projet SAEMCD33',
                'required' => true),
            'identifiantCollectiviteRattachement' => array(
                'label' => 'Identifiant de la collectivité de rattachement',
                'title' => 'veuillez saisir l\'identifiant de la collectivité de rattachement pour les droits et les rôles utilisateurs',
                'required' => true)
        );
    }

    /**
     * retourne le formatage des paramètre pour l'affichage
     * @return array liste des paramètres a afficher sous la forme :
     *      ['paramName', ....]
     */
    public function parametersToStringDefinition() {
        return array('url');
    }

    /**
     * retourne true en cas de connexion réussie avec le référentiel et false dans le cas contraire
     * @param array $parametersValues tableau des valeurs des paramètres
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function ping($parametersValues) {
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        $client = new RESTClient($parametersValues['url']);
        try {
            $response = $client->get('?verb=ListSets');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * retourne true si le driver se charge de faire l'update et false si l'update est fait par as@lae
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function autoUpdate() {
        return false;
    }

    /**
     * retourne la liste des identifiants des collectivités du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $dateFrom date-heure pour la mise à jour différentielle
     * @param string $scope NEW_AND_UPDATED pour lire les nouveaux éléments et ceux mis à jour ou DELETED pour lire les éléments supprimés
     * @return array liste des identifiants des collectivités du référentiel
     */
    public function getCollectiviteIdentifiers($parametersValues, $dateFrom='', $scope='NEW_AND_UPDATED') {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        $resumptionToken = '';
        $collectiviteIdentifiers = array();
        $headerStatus = ($scope == 'DELETED')?'deleted':'';

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $set = '&set='.'organization';
        $from = empty($dateFrom)?'':'&from='.AppOaipmh::encodeDate($dateFrom);
        $metadataPrefix = '&metadataPrefix='.'rdf';

        // lecture de la liste des collectivités
        $verb = '?verb=ListIdentifiers';
        do {
            if (empty($resumptionToken)) {
                $response = $client->get($verb.$set.$from.$metadataPrefix);
            } else {
                $resumptionToken = '&resumptionToken='.$resumptionToken;
                $response = $client->get($verb.$set.$resumptionToken.$metadataPrefix);
            }
            $collectiviteIdentifiers = array_merge($collectiviteIdentifiers, AppOaipmh::getHeadersIdentifiersFromOaiPmhResponse($response, $headerStatus));
            $resumptionToken = AppOaipmh::getResumptionTokenOaiPmhResponse($response);
        } while (!empty($resumptionToken));

        return $collectiviteIdentifiers;
    }

    /**
     * retourne les informations d'une collectivité du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $collectiviteIdentifier identifiant de la collectivité à mettre à jour
     * @return array valeurs des collectivités sous la forme :
     *      ['nom'=>string,
     *      ']
     */
    public function getCollectiviteData($parametersValues, $collectiviteIdentifier) {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        require_once(APP.DS.'libs'.DS.'xmlxsd.php');
        $ret = array();

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $metadataPrefix = '&metadataPrefix='.'rdf';

        // lecture des données de la collectivité
        $verb = '?verb=GetRecord';
        $identifier='&identifier='.$collectiviteIdentifier;
        $response = $client->get($verb.$identifier.$metadataPrefix);
        $xmlCollectiviteContent = AppOaipmh::getPathContentOaiPmhResponse('GetRecord/record/metadata', $response);
        $xmlCollectiviteContent = '<?xml version="1.0"?>'."\n".str_replace(array('<metadata>', '</metadata>'), '', $xmlCollectiviteContent);
        $ret = AppXmlrdffoafCollectivite::xmlRdfFoafCollectivite($xmlCollectiviteContent, $collectiviteIdentifier);

        // lecture de la collectivité de rattachement
        App::import('Model', 'Collectivite');
        $this->Collectivite = new Collectivite();
        $ret['collectiviteRattachementId'] = $this->Collectivite->field('id', array('identifiant' => $parametersValues['identifiantCollectiviteRattachement']));
        if (empty($ret['collectiviteRattachementId']))
            throw new Exception('collectivité de rattachement non trouvée en base de données : '.$parametersValues['identifiantCollectiviteRattachement']);

        return $ret;
    }

    /**
     * retourne les informations d'affichage d'une collectivité du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $collectiviteIdentifier identifiant de la collectivité à mettre à jour
     * @return array valeur d'affichage de la collectivité sous la forme :
     *      ['message'=>string, 'nblignes'=>integer]
     */
    public function getCollectiviteAffichageData($parametersValues, $collectiviteIdentifier) {
        return array(
            'message'=>'',
            'nblignes'=>null);
    }

    /**
     * procède à la mise à jour ou la création d'une collectivité, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $collectiviteModel modele des Collectivites
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $collectiviteIdentifier identifiant de la collectivité à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doCollectiviteCreateUpdate($collectiviteModel, $refExterieurId, $parametersValues, $collectiviteIdentifier) {
    }

    /**
     * procède à la suppression d'une collectivité, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $collectiviteModel modele des Collectivites
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $collectiviteIdentifier identifiant de la collectivité à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doCollectiviteDelete($collectiviteModel, $refExterieurId, $parametersValues, $collectiviteIdentifier) {
    }

    /**
     * fonction appelée juste avant la sauvegarde en base de données, permet d'ajuster, de compléter les données à sauvegarder
     * @param array $collectiviteDataToSave données modifiables à sauvegarder en base de données
     * @param string $refextCollectiviteIdentifier identifiant de la collectivité dans le référentiel extérieur
     * @param array $refextCollectiviteData données de la collectivité dans le référentiel extérieur
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $refExterieurParametersValues tableau des valeurs des paramètres
     * @throws Exception en cas d'erreur
     */
    public function beforeCollectiviteSave(&$collectiviteDataToSave, $refextCollectiviteIdentifier, $refextCollectiviteData, $refExterieurId, $refExterieurParametersValues) {
    }

    /**
     * fonction appelée juste après la sauvegarde en base de données, permet d'effectuer des actions supplémentaires
     * @param array $collectiviteSavedData données sauvegardée
     * @param string $refextCollectiviteIdentifier identifiant de la collectivité dans le référentiel extérieur
     * @param array $refextCollectiviteData données de la collectivité dans le référentiel extérieur
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $refExterieurParametersValues tableau des valeurs des paramètres
     * @throws Exception en cas d'erreur
     */
    public function afterCollectiviteSave($collectiviteSavedData, $refextCollectiviteIdentifier, $refextCollectiviteData, $refExterieurId, $refExterieurParametersValues) {
    }
}

class AppXmlrdffoafCollectivite {

    /**
     * retourne la collectivité d'une chaîne xmlrdffoaf
     * @param string $xmlRdfFoafContent contenu du xml rdf
     * @param string $collectiviteIdentifier identifiant du sujet (organization ou person)
     * @return array liste des organisations (services)
     */
    public function xmlRdfFoafCollectivite($xmlRdfFoafContent, $collectiviteIdentifier) {
        // initialisations
        $ret = array();

        // parsing du xml
        try {
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xmlRdfFoafContent);
            $desciptions = $domDoc->getElementsByTagName('Description');
            if ($desciptions->length == 0) return $ret;

            // recherche du noeud description de la collectivité
            $collectiviteDescriptionNode = null;
            foreach($desciptions as $desciption) {
                $id = AppXmlrdffoafCollectivite::getDescriptionValue($desciption, 'identifier');
                if ($id == $collectiviteIdentifier) {
                    $collectiviteDescriptionNode = $desciption;
                    break;
                }
            }
            if ($collectiviteDescriptionNode == null) return $ret;

            // extraction des informations
            $ret['nom'] = AppXmlrdffoafCollectivite::getDescriptionValue($collectiviteDescriptionNode, 'title');
        } catch (Exception $e) {
            $ret = array();
        }

        return $ret;
    }

    function _parseUrl($url) {
        return (substr($url, strrpos($url, '/')+1, strlen($url)));
    }

    function getDescriptionType(&$entity) {
        return AppXmlrdffoafCollectivite::_parseUrl(AppXmlrdffoafCollectivite::getDescriptionAttribute($entity, 'type', 'rdf:resource'));
    }

    function getDescriptionValue(&$entity, $tagName) {
        if ($entity == null) return '';
        $domNodes = $entity->getElementsByTagName($tagName);
        if ($domNodes->length == 0) return '';
        return $domNodes->item(0)->nodeValue;
    }

    function getDescriptionValues(&$entity, $tagName) {
        $ret = array();
        if ($entity == null) return $ret;
        $domNodes = $entity->getElementsByTagName($tagName);
        if ($domNodes->length == 0) return $ret;
        foreach ($domNodes as $domNode)
            $ret[] = $domNode->nodeValue;
        return $ret;
    }

    function getDescriptionAttribute(&$entity, $tagName, $attributeName) {
        if ($entity == null) return '';
        $domNodes = $entity->getElementsByTagName($tagName);
        if ($domNodes->length == 0) return '';
        return $domNodes->item(0)->getAttribute($attributeName);
    }
}

