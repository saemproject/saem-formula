<?php
class ServiceExtDriverSaemcd33Notices implements ServiceExterneInterface {

    /**
     * retourne la description du driver qui sera affiché dans la vue détaillée
     * @return string description du driver
     */
    public function description() {
        return 'Référentiel des notices d\'autorité du SAEMCD33 développé par Logilab';
    }

    /**
     * retourne la liste des paramètres et de leurs définitions utilisés par le driver
     * @return string descriptiondu compteur
     */
    public function parametersDefinition() {
        return array(
            'url' => array(
                'label' => 'url du référentiel des services',
                'title' => 'veuillez saisir ici l\'url du référentiel logilab du projet SAEMCD33',
                'required' => true));
    }

    /**
     * retourne le formatage des paramètre pour l'affichage
     * @return array liste des paramètres a afficher sous la forme :
     *      ['paramName', ....]
     */
    public function parametersToStringDefinition() {
        return array('url');
    }

    /**
     * retourne true en cas de connexion réussie avec le référentiel et false dans le cas contraire
     * @param array $parametersValues tableau des valeurs des paramètres
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function ping($parametersValues) {
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        $client = new RESTClient($parametersValues['url']);
        try {
            $response = $client->get('?verb=ListSets');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * retourne true si le driver se charge de faire l'update et false si l'update est fait par as@lae
     * @return boolean
     */
    public function autoUpdate() {
        return false;
    }

    /**
     * retourne la liste des identifiants des services du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $dateFrom date-heure pour la mise à jour différentielle
     * @param string $scope NEW_AND_UPDATED pour lire les nouveaux éléments et ceux mis à jour ou DELETED pour lire les éléments supprimés
     * @return array liste des identifiants des services du référentiel
     */
    public function getServiceIdentifiers($parametersValues, $dateFrom='', $scope='NEW_AND_UPDATED') {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        $resumptionToken = '';
        $serviceIdentifiers = array();
        $headerStatus = ($scope == 'DELETED')?'deleted':'';

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $set = '&set='.'authorityrecord';
        $from = empty($dateFrom)?'':'&from='.AppOaipmh::encodeDate($dateFrom);
        $metadataPrefix = '&metadataPrefix='.'eac';

        // lecture de la liste des services
        $verb = '?verb=ListIdentifiers';
        do {
            if (empty($resumptionToken)) {
                $response = $client->get($verb.$set.$from.$metadataPrefix);
            } else {
                $resumptionToken = '&resumptionToken='.$resumptionToken;
                $response = $client->get($verb.$set.$resumptionToken.$metadataPrefix);
            }
            $serviceIdentifiers = array_merge($serviceIdentifiers, AppOaipmh::getHeadersIdentifiersFromOaiPmhResponse($response, $headerStatus));
            $resumptionToken = AppOaipmh::getResumptionTokenOaiPmhResponse($response);
        } while (!empty($resumptionToken));

        return $serviceIdentifiers;
    }

    /**
     * retourne les informations d'un service du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $serviceIdentifier identifiant du service à mettre à jour
     * @return array valeurs des services sous la forme :
     *      ['collectiviteIdentifiant => string,
     *      'name' => string,
     *      'description' => string,
     *      'startDate' => string,
     *      'endDate' => string,
     *      'roles' => ['archival'|'producer'|'deposit'|'control'|'enquirer', ...],
     *      'modifiable' => boolean,
     *      'Address' => [
     *          0 => ['StreetName'=>string,
     *              'Postcode'=>string,
     *              'CityName'=>string,
     *              'Country'=>'TwoLettersCountryCode']],
     *      'Contact' => [
     *          0 => ['PersonName'=>string,
     *              'Identification'=>string]]]
     */
    public function getServiceData($parametersValues, $serviceIdentifier) {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $metadataPrefix = '&metadataPrefix='.'eac';

        // lecture des données du service
        $verb = '?verb=GetRecord';
        $identifier='&identifier='.$serviceIdentifier;
        $response = $client->get($verb.$identifier.$metadataPrefix);
        $xmlNoticeContent = AppOaipmh::getPathContentOaiPmhResponse('GetRecord/record/metadata', $response);
        $xmlNoticeContent = '<?xml version="1.0"?>'."\n".str_replace(array('<metadata>', '</metadata>'), '', $xmlNoticeContent);
        $noticeData = $this->EacToOrganization($xmlNoticeContent);

        // informations principales
        $ret['collectiviteIdentifiant'] = $this->getCollectiviteIdentifiant($parametersValues, $serviceIdentifier);
        $ret['name'] = $noticeData['name'];
        $ret['description'] = '';
        $ret['startDate'] = $noticeData['fromDate'];
        $ret['endDate'] = $noticeData['toDate'];
        $ret['roles'] = array('producer');
        $ret['modifiable'] = false;

        return $ret;
    }

    private function EacToOrganization($xmlNoticeEacContent){
        // initialisations
        $ret = array();

        // parsing du xml
        try {
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xmlNoticeEacContent);
            $cpfDesciption = $domDoc->getElementsByTagName('cpfDescription');
            if ($cpfDesciption->length == 0) return $ret;

            // recherche du nom
            $names = array();
            $nameEntries = $domDoc->getElementsByTagName('nameEntry');
            foreach($nameEntries as $nameEntry) {
                $parts = $nameEntry->getElementsByTagName('part');
                foreach($parts as $part) {
                    $names[] = $part->nodeValue;
                }
            }

            // recherche des dates d'existance
            $fromDate = '';
            $fromDates = $domDoc->getElementsByTagName('fromDate');
            if ($fromDates->length > 0) {
                $fromDate = $fromDates->item(0)->nodeValue;
            }
            $toDate = '';
            $toDates = $domDoc->getElementsByTagName('toDate');
            if ($toDates->length > 0) {
                $toDate = $toDates->item(0)->nodeValue;
            }

            // extraction des informations
            $ret['name'] = implode(', ', $names);
            $ret['fromDate'] = $fromDate;
            $ret['toDate'] = $toDate;
        } catch (Exception $e) {
            $ret = array();
        }

        return $ret;
    }

    private function getCollectiviteIdentifiant($parametersValues, $serviceIdentifier) {
        // initialisations
        $found = false;

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);

        // lecture des set authorityrecord:used_by
        $authorityRecordUsedBySets = array();
        $verb = '?verb=ListSets';
        $identifier='&identifier='.$serviceIdentifier;
        $response = $client->get($verb.$identifier);

        $domDoc = new DOMDocument();
        $domDoc->loadXML($response);
        $setSpecs = $domDoc->getElementsByTagName('setSpec');
        foreach($setSpecs as $setSpec) {
            if (strpos($setSpec->nodeValue, 'authorityrecord:used_by:') !== false) {
                $authorityRecordUsedBySets[] = $setSpec->nodeValue;
            }
        }
        unset($domDoc);

        // recherche du service d'archive utilisant la notice $serviceIdentifier
        $verb = '?verb=ListIdentifiers';
        $metadataPrefix = '&metadataPrefix='.'eac';
        foreach($authorityRecordUsedBySets as $authorityRecordUsedBySet) {
            // lecture des notices utilisées par le service d'Archives
            $set='&set='.$authorityRecordUsedBySet;
            $response = $client->get($verb.$set.$metadataPrefix);
            $domDoc = new DOMDocument();
            $domDoc->loadXML($response);
            if ($domDoc->getElementsByTagName('error')->length>0) {
                unset($domDoc);
                continue;
            }
            $noticeIdentifiers = $domDoc->getElementsByTagName('identifier');
            foreach($noticeIdentifiers as $noticeIdentifier) {
                if ($serviceIdentifier == $noticeIdentifier->nodeValue) {
                    $found = true;
                    break;
                }
            }
            if ($found) {
                break;
            }
        }
        if (!$found) {
            throw new Exception('erreur lors de la recherche du service d\'archive correspondant à la notice '.$serviceIdentifier);
        }
        // lecture de l'acteur SEDA correspondant au set trouvé
        App::import('Model', 'Organization');
        $this->Organization = new Organization();
        $organizationIdentifier = 'ark:/'.substr($authorityRecordUsedBySet, strrpos($authorityRecordUsedBySet, ':')+1);
        $organization = $this->Organization->find('first', array(
            'recursive' => -1,
            'fields' => array('id', 'collectivite_id'),
            'conditions' => array('identification' => $organizationIdentifier)
        ));
        if (empty($organization)) {
            throw new Exception('erreur acteur SEDA '.$organizationIdentifier.' non trouvé en base de données');
        }
        // lecture de la collectivité
        App::import('Model', 'Collectivite');
        $this->Collectivite = new Collectivite();
        $collectivite = $this->Collectivite->find('first', array(
            'recursive' => -1,
            'fields' => array('id', 'identifiant'),
            'conditions' => array('id' => $organization['Organization']['collectivite_id'])
        ));
        if (empty($collectivite)) {
            throw new Exception('erreur collectivité id:'.$organization['Organization']['collectivite_id'].' non trouvée en base de données');
        }
        return $collectivite['Collectivite']['identifiant'];
    }

    /**
     * procède à la mise à jour ou la création d'un service, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $serviceModel modele des Services
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $serviceIdentifier identifiant du service à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doServiceCreateUpdate($serviceModel, $refExterieurId, $parametersValues, $serviceIdentifier) {
    }

    /**
     * procède à la suppression d'un service, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $serviceModel modele des Services
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $serviceIdentifier identifiant du service à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doServiceDelete($serviceModel, $refExterieurId, $parametersValues, $serviceIdentifier) {
    }

    /**
     * fonction appelée juste avant la sauvegarde en base de données, permet d'ajuster, de compléter les données à sauvegarder
     * @param array $organizationData données à sauvegarder et modifiables
     * @param string $refextServiceIdentifier identifiant du service dans le référentiel extérieur
     * @throws Exception en cas d'erreur
     */
    public function beforeServiceSave(&$organizationDataToSave, $refextServiceIdentifier, $refextServiceData, $refExterieurId, $refExterieurParametersValues) {
        // ajout du premier connecteur LOCAL
        App::import('Model', 'Connecteur');
        $this->Connecteur = new Connecteur();
        $connecteurLocal = $this->Connecteur->find('first', array(
            'recursive' => -1,
            'fields' => array('id'),
            'conditions' => array('type' => 'LOCAL'),
            'order' => array('id')
        ));
        if (!empty($connecteurLocal)) {
            $organizationDataToSave['Connecteur']['Connecteur'][0] = $connecteurLocal['Connecteur']['id'];
        }
    }

    /**
     * fonction appelée juste après la sauvegarde en base de données, permet d'effectuer des actions supplémentaires
     * @param array $organizationData données sauvegardée
     * @param string $refextServiceIdentifier identifiant du service dans le référentiel extérieur
     * @throws Exception en cas d'erreur
     */
    public function afterServiceSave($organizationSavedData, $refextServiceIdentifier, $refextServiceData, $refExterieurId, $refExterieurParametersValues) {
    }

}
