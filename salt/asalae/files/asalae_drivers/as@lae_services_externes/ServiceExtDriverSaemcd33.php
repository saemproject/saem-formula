<?php
class ServiceExtDriverSaemcd33 implements ServiceExterneInterface {

    /**
     * retourne la description du driver qui sera affiché dans la vue détaillée
     * @return string description du driver
     */
    public function description() {
        return 'Référentiel des services du SAEMCD33 développé par Logilab';
    }

    /**
     * retourne la liste des paramètres et de leurs définitions utilisés par le driver
     * @return string descriptiondu compteur
     */
    public function parametersDefinition() {
        return array(
            'url' => array(
                'label' => 'url du référentiel des services',
                'title' => 'veuillez saisir ici l\'url du référentiel logilab du projet SAEMCD33',
                'required' => true),
            'mappingContactPointRoles' => array(
                'label' => 'mapping du rôle utilisateur du contact à créer avec les rôles des services du référentiel : archival, deposit, producer, control',
                'title' => 'veuillez saisir par ordre d\'importance la correspondance entre les rôles des services dans le référentiel et les rôles utilisateurs dans as@ae ex:archival:Archiviste|producer:Producteur. Format : [role_service_referentiel(archival|deposit|producer|control)]:[Nom_rôle_utilisateur_as@lae]|[...]',
                'required' => true));
    }

    /**
     * retourne le formatage des paramètre pour l'affichage
     * @return array liste des paramètres a afficher sous la forme :
     *      ['paramName', ....]
     */
    public function parametersToStringDefinition() {
        return array('url');
    }

    /**
     * retourne true en cas de connexion réussie avec le référentiel et false dans le cas contraire
     * @param array $parametersValues tableau des valeurs des paramètres
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function ping($parametersValues) {
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        $client = new RESTClient($parametersValues['url']);
        try {
            $response = $client->get('?verb=ListSets');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * retourne true si le driver se charge de faire l'update et false si l'update est fait par as@lae
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function autoUpdate() {
        return false;
    }

    /**
     * retourne la liste des identifiants des services du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $dateFrom date-heure pour la mise à jour différentielle
     * @param string $scope NEW_AND_UPDATED pour lire les nouveaux éléments et ceux mis à jour ou DELETED pour lire les éléments supprimés
     * @return array liste des identifiants des services du référentiel
     */
    public function getServiceIdentifiers($parametersValues, $dateFrom='', $scope='NEW_AND_UPDATED') {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        $resumptionToken = '';
        $serviceIdentifiers = array();
        $headerStatus = ($scope == 'DELETED')?'deleted':'';

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $set = '&set='.'organizationunit';
        $from = empty($dateFrom)?'':'&from='.AppOaipmh::encodeDate($dateFrom);
        $metadataPrefix = '&metadataPrefix='.'rdf';

        // lecture de la liste des services
        $verb = '?verb=ListIdentifiers';
        do {
            if (empty($resumptionToken)) {
                $response = $client->get($verb.$set.$from.$metadataPrefix);
            } else {
                $resumptionToken = '&resumptionToken='.$resumptionToken;
                $response = $client->get($verb.$set.$resumptionToken.$metadataPrefix);
            }
            $serviceIdentifiers = array_merge($serviceIdentifiers, AppOaipmh::getHeadersIdentifiersFromOaiPmhResponse($response, $headerStatus));
            $resumptionToken = AppOaipmh::getResumptionTokenOaiPmhResponse($response);
        } while (!empty($resumptionToken));

        return $serviceIdentifiers;
    }

    /**
     * retourne les informations d'un service du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $serviceIdentifier identifiant du service à mettre à jour
     * @return array valeurs des services sous la forme :
     *      ['collectiviteIdentifiant => string,
     *      'name' => string,
     *      'description' => string,
     *      'startDate' => string,
     *      'endDate' => string,
     *      'roles' => ['archival'|'producer'|'deposit'|'control'|'enquirer', ...],
     *      'modifiable' => boolean,
     *      'Address' => [
     *          0 => ['StreetName'=>string,
     *              'Postcode'=>string,
     *              'CityName'=>string,
     *              'Country'=>'TwoLettersCountryCode']],
     *      'Contact' => [
     *          0 => ['PersonName'=>string,
     *              'Identification'=>string]]]
     */
    public function getServiceData($parametersValues, $serviceIdentifier) {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $metadataPrefix = '&metadataPrefix='.'rdf';

        // lecture des données du service
        $verb = '?verb=GetRecord';
        $identifier='&identifier='.$serviceIdentifier;
        $response = $client->get($verb.$identifier.$metadataPrefix);
        $xmlServiceContent = AppOaipmh::getPathContentOaiPmhResponse('GetRecord/record/metadata', $response);
        $xmlServiceContent = '<?xml version="1.0"?>'."\n".str_replace(array('<metadata>', '</metadata>'), '', $xmlServiceContent);
        $organizationData = AppXmlrdffoaf::xmlRdfFoafOrganization($xmlServiceContent, $serviceIdentifier);

        // informations principales
        $ret['collectiviteIdentifiant'] = $organizationData['administrativeAuthorityIdentifier'];
        $ret['name'] = $organizationData['title'];
        $ret['description'] = '';
        $ret['startDate'] = $organizationData['startDate'];
        $ret['endDate'] = $organizationData['endDate'];
        $ret['roles'] = $organizationData['roles'];
        $ret['modifiable'] = false;

        // adresses
        $refAdresses = AppXmlrdffoaf::xmlRdfFoafAdresses($xmlServiceContent);
        foreach($refAdresses as $key=>$refAdresse) {
            $ret['Address'][$key]['StreetName'] = $refAdresse['street-address'];
            $ret['Address'][$key]['Postcode'] = $refAdresse['postal-code'];
            $ret['Address'][$key]['CityName'] = $refAdresse['locality'];
            if (!empty($refAdresse['country-name'])) {
                $fileUri = WWW_ROOT . 'files' . DS . 'sedacodes' . DS . '0.2' . DS . 'ISOTwoletterCountryCodeIdentifierContentType';
                if (!is_file($fileUri)) {
                    require_once(APP.'libs'.DS.'SedaSchemaParser.php');
                    SedaSchemaParser::schemaToTree('0.2', 'Organization');
                }
                $listeName = json_decode(file_get_contents($fileUri), true);
                $Countrylist = array();
                foreach($listeName as $countryCode=>$value) $Countrylist[$countryCode]=$value['Name'];
                $countryName = strtoupper($refAdresse['country-name']);
                if (in_array($countryName, $Countrylist)) {
                    $ret['Address'][$key]['Country'] = array(
                        '@value' => array_search($countryName, $Countrylist),
                        '@attributes' => array('listVersionID'=>'second edition 2006'));
                }
            }
        }
        if (!empty($organizationData['contactPoint'])) {
            $refContactPoint = AppXmlrdffoaf::xmlRdfFoafContactPoint($xmlServiceContent, $organizationData['contactPoint']);
            $ret['Contact'][0]['Identification'] = $refContactPoint['identifier'];
            $ret['Contact'][0]['PersonName'] = $refContactPoint['name'];
        }

        return $ret;
    }

    /**
     * procède à la mise à jour ou la création d'un service, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $serviceModel modele des Services
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $serviceIdentifier identifiant du service à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doServiceCreateUpdate($serviceModel, $refExterieurId, $parametersValues, $serviceIdentifier) {
    }

    /**
     * procède à la suppression d'un service, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $serviceModel modele des Services
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $serviceIdentifier identifiant du service à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doServiceDelete($serviceModel, $refExterieurId, $parametersValues, $serviceIdentifier) {
    }

    /**
     * fonction appelée juste avant la sauvegarde en base de données, permet d'ajuster, de compléter les données à sauvegarder
     * @param array $organizationData données à sauvegarder et modifiables
     * @param string $refextServiceIdentifier identifiant du service dans le référentiel extérieur
     * @throws Exception en cas d'erreur
     */
    public function beforeServiceSave(&$organizationDataToSave, $refextServiceIdentifier, $refextServiceData, $refExterieurId, $refExterieurParametersValues) {
        // ajout du premier connecteur LOCAL
        App::import('Model', 'Connecteur');
        $this->Connecteur = new Connecteur();
        $connecteurLocal = $this->Connecteur->find('first', array(
            'recursive' => -1,
            'fields' => array('id'),
            'conditions' => array('type' => 'LOCAL'),
            'order' => array('id')
        ));
        if (!empty($connecteurLocal)) {
            $organizationDataToSave['Connecteur']['Connecteur'][0] = $connecteurLocal['Connecteur']['id'];
        }
    }

    /**
     * fonction appelée juste après la sauvegarde en base de données, permet d'effectuer des actions supplémentaires
     * @param array $organizationData données sauvegardée
     * @param string $refextServiceIdentifier identifiant du service dans le référentiel extérieur
     * @throws Exception en cas d'erreur
     */
    public function afterServiceSave($organizationSavedData, $refextServiceIdentifier, $refextServiceData, $refExterieurId, $refExterieurParametersValues) {
        // initialisations
        App::import('Model', 'Service');
        $this->Service = new Service();

        // création ou mise à jour du service lié
        $service = $this->Service->find('first', array(
            'recursive' => -1,
            'conditions' => array('acteur_seda'=>true, 'organization_id'=>$organizationSavedData['Organization']['id'])));
        if (empty($service)) {
            $service = $this->Service->create();
            $service['Service'] = array(
                'collectivite_id' => $organizationSavedData['Organization']['collectivite_id'],
                'nom' => $organizationSavedData['Organization']['nom'],
                'organization_id' => $organizationSavedData['Organization']['id'],
                'acteur_seda' => true,
                'contact_user_id' => null,
                'actif' => true,
                'parent_id' => null,
                'created_user_id' => 1,
                'modified_user_id' => 1);
        } else {
            $service['Service']['nom'] = $organizationSavedData['Organization']['nom'];
        }
        if (!$this->Service->save($service, false))
            throw new Exception('erreur lors de la sauvegarde du service '.$organizationSavedData['Organization']['nom'].' ('.$refextServiceIdentifier.') en base de données');
        $service['Service']['id'] = $this->Service->id;

        // utilisateur référent du service
        if (empty($refextServiceData['Contact'][0]['PersonName']))
            return;
        App::import('Model', 'User');
        $this->User = new User();
        App::import('Model', 'Role');
        $this->Role = new Role();
        App::import('Model', 'Aro');
        $this->Aro = new Aro();
        App::import('Model', 'Composition');
        $this->Composition = new Composition();

        // détermination du rôle utilisateur
        $mappingRoles = array();
        $mappingContactPointRoles = explode('|', $refExterieurParametersValues['mappingContactPointRoles']);
        foreach($mappingContactPointRoles as $mappingContactPointRole) {
            $contactPointRole = explode(':', $mappingContactPointRole);
            $mappingRoles[$contactPointRole[0]] = $contactPointRole[1];
        }
        $roleNameFound = '';
        foreach($mappingRoles as $roleRefExtName => $roleName) {
            if (in_array($roleRefExtName, $refextServiceData['roles'])) {
                $roleNameFound = $roleName;
                break;
            }
        }
        if (empty($roleNameFound))
            return;
        // recherche de l'id rôle en base de données
        $roleId = $this->Role->field('id', array(
            'collectivite_id' => $organizationSavedData['Organization']['collectivite_id'],
            'nom' => $roleNameFound));
        if (empty($roleId))
            throw new Exception('erreur lors de la sauvegarde de l\'utilisateur '.$refextServiceData['Contact'][0]['PersonName'].' ('.$refextServiceIdentifier.') : rôle '.$roleNameFound.' non trouvé en base de données');

        // recherche de l'utilisateur en base de données par son identifiant ark
        $user = $this->User->find('first', array(
            'recursive' => -1,
            'fields' => array('id'),
            'conditions' => array('cert_subject'=>$refextServiceData['Contact'][0]['Identification'])));
        $userName = $this->_calculUsername($refextServiceData['Contact'][0]['Identification'], $refextServiceData['Contact'][0]['PersonName']);
        $personNameTab = explode(' ', $refextServiceData['Contact'][0]['PersonName']);
        if (count($personNameTab)<2) {
            $firstName = $personNameTab[0];
            $lastName = $personNameTab[0];
        } else {
            $firstName = $personNameTab[0];
            $lastName = $personNameTab[1];
        }
        if (!empty($user)) {
            // mise à jour de l'utilisateur
        } else {
            // nouvel utilisateur
            $user = $this->User->create();
            $user['User'] = array(
                'collectivite_id' => $organizationSavedData['Organization']['collectivite_id'],
                'service_id' => $service['Service']['id'],
                'role_id' => $roleId,
                'username' => $userName,
                'password' => Security::hash($userName, null, true),
                'cert_subject' => $refextServiceData['Contact'][0]['Identification'],
                'need_cert' => false,
                'civilite' => '',
                'nom' => $lastName,
                'prenom' => $firstName,
                'titre' => '',
                'mail' => '',
                'user_delegue_id' => null,
                'actif' => true,
                'affichage_id' => 0,
                'password_sha256' => hash('sha256', $userName),
                'created_user_id' => 1,
                'modified_user_id' => 1);
            $user['ListeService']['ListeService'][] = $service['Service']['id'];
            $user['Affichage'] = array(
                'message' => '',
                'nblignes' => null);
            if (!$this->User->save($user, false))
                throw new Exception('erreur lors de la sauvegarde de l\'utilisateur '.$userName.' ('.$refextServiceIdentifier.') en base de données');
            $user['User']['id'] = $this->User->id;
            // ajout des droits du rôle
            $parentAroId = $this->Aro->field('id', array('model' => 'Role', 'foreign_key' => $roleId));
            $aro['Aro'] = array(
                'parent_id' => $parentAroId,
                'model' => 'User',
                'foreign_key' => $user['User']['id'],
                'alias' => $userName);
            if (!$this->Aro->save($aro, false))
                throw new Exception('erreur lors de la sauvegarde des droits de l\'utilisateur '.$userName.' ('.$refextServiceIdentifier.') en base de données');
            // ajout de l'utilisateur au traitement de l'acteur SEDA
            $composition['Composition'] = array(
                'type_validation' => 'V',
                'created_user_id' => 1,
                'modified_user_id' => 1,
                'type' => 'USER',
                'mail_contact' => null,
                'mail' => null,
                'model' => 'Organization',
                'foreign_key' => $organizationSavedData['Organization']['id'],
                'traiteur_model' => 'User',
                'user_id' => $user['User']['id']);
            if (!$this->Composition->save($composition, false))
                throw new Exception('erreur lors de la sauvegarde de la composition du service ('.$refextServiceIdentifier.') en base de données');
        }
    }

    private function _calculUsername($identifiaction, $personName) {
        $userName = '';
        $i=0;
        $personNameTab = explode(' ', $personName);
        if (count($personNameTab)<2) {
            $firstName = $personNameTab[0];
            $lastName = substr($personNameTab[0],1);
        } else {
            $firstName = $personNameTab[0];
            $lastName = $personNameTab[1];
        }

        while($i<10000) {
            $userName = mb_strtolower($firstName[0].$lastName, 'UTF-8').($i>0?$i:'');
            $user = $this->User->find('first', array(
                'recursive' => -1,
                'fields' => array('id'),
                'conditions' => array(
                    'username' => $userName,
                    'cert_subject !=' => $identifiaction)));
            if (empty($user))
                break;
            $i++;
        }
        return $userName;
    }
}

class AppXmlrdffoaf {

    /**
     * retourne l'organization d'une chaîne xmlrdffoaf
     * @param string $xmlRdfFoafContent contenu du xml rdf
     * @param string $serviceIdentifier identifiant du sujet (organization ou person)
     * @return array liste des organisations (services)
     */
    public function xmlRdfFoafOrganization($xmlRdfFoafContent, $serviceIdentifier) {
        // initialisations
        $ret = array();

        // parsing du xml
        try {
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xmlRdfFoafContent);
            $desciptions = $domDoc->getElementsByTagName('Description');
            if ($desciptions->length == 0) return $ret;

            // recherche du noeud description de l'autorité administrative
            $serviceDescriptionNode = null;
            foreach($desciptions as $desciption) {
                $type = AppXmlrdffoaf::_parseUrl(AppXmlrdffoaf::getDescriptionAttribute($desciption, 'type', 'rdf:resource'));
                if ($type == 'org#Organization') {
                    $serviceDescriptionNode = $desciption;
                    break;
                }
            }
            if ($serviceDescriptionNode == null) return $ret;
            $ret['administrativeAuthorityIdentifier'] = AppXmlrdffoaf::getDescriptionValue($serviceDescriptionNode, 'identifier');

            // recherche du noeud description du service
            $serviceDescriptionNode = null;
            foreach($desciptions as $desciption) {
                $identifier = AppXmlrdffoaf::getDescriptionValue($desciption, 'identifier');
                if ($identifier == $serviceIdentifier) {
                    $serviceDescriptionNode = $desciption;
                    break;
                }
            }
            if ($serviceDescriptionNode == null) return $ret;

            // extraction des informations
            $ret['title'] = AppXmlrdffoaf::getDescriptionValue($serviceDescriptionNode, 'title');
            $ret['startDate'] = AppXmlrdffoaf::getDescriptionValue($serviceDescriptionNode, 'startDate');
            $ret['endDate'] = AppXmlrdffoaf::getDescriptionValue($serviceDescriptionNode, 'endDate');
            $ret['roles'] = AppXmlrdffoaf::getDescriptionValues($serviceDescriptionNode, 'role');
            if (empty($ret['roles']))
                $ret['roles'][0] = 'producer';
            $ret['contactPoint'] = AppXmlrdffoaf::getDescriptionAttribute($serviceDescriptionNode, 'contactPoint', 'rdf:resource');
        } catch (Exception $e) {
            $ret = array();
        }

        return $ret;
    }

    /**
     * retourne les adresses d'une organisation d'une chaîne xmlrdffoaf
     * @param string $xmlRdfFoafContent contenu du xml rdf
     * @return array liste des adresses
     */
    public function xmlRdfFoafAdresses($xmlRdfFoafContent) {
        // initialisations
        $ret = array();

        // parsing du xml
        try {
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xmlRdfFoafContent);
            $desciptions = $domDoc->getElementsByTagName('Description');
            if ($desciptions->length < 2) return $ret;

            // parcours des descriptions et extraction des adresses
            foreach($desciptions as $desciption) {
                if(AppXmlrdffoaf::getDescriptionType($desciption) != 'ns#Location')
                    continue;
                $adr = array();
                $adr['street-address'] = AppXmlrdffoaf::getDescriptionValue($desciption, 'street-address');
                $adr['postal-code'] = AppXmlrdffoaf::getDescriptionValue($desciption, 'postal-code');
                $adr['locality'] = AppXmlrdffoaf::getDescriptionValue($desciption, 'locality');
                $adr['country-name'] = AppXmlrdffoaf::getDescriptionValue($desciption, 'country-name');
                $ret[] = $adr;
            }

        } catch (Exception $e) {
            $ret = array();
        }

        return $ret;
    }

    /**
     * retourne les adresses d'une organisation d'une chaîne xmlrdffoaf
     * @param string $xmlRdfFoafContent contenu du xml rdf
     * @param string $descriptionAbout attribut about de la description du point de contact
     * @return array point de contact
     */
    public function xmlRdfFoafContactPoint($xmlRdfFoafContent, $descriptionAbout) {
        // initialisations
        $ret = array();

        // parsing du xml
        try {
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xmlRdfFoafContent);
            $desciptions = $domDoc->getElementsByTagName('Description');
            if ($desciptions->length < 2) return $ret;

            // parcours des descriptions et extraction du point de contact
            foreach($desciptions as $desciption) {
                if($desciption->getAttribute('rdf:about') != $descriptionAbout)
                    continue;
                $ret['name'] = AppXmlrdffoaf::getDescriptionValue($desciption, 'name');
                $ret['identifier'] = AppXmlrdffoaf::getDescriptionValue($desciption, 'identifier');
                return $ret;
            }
        } catch (Exception $e) {
            $ret = array();
        }

        return $ret;
    }

    function _parseUrl($url) {
        return (substr($url, strrpos($url, '/')+1, strlen($url)));
    }

    function getDescriptionType(&$entity) {
        return AppXmlrdffoaf::_parseUrl(AppXmlrdffoaf::getDescriptionAttribute($entity, 'type', 'rdf:resource'));
    }

    function getDescriptionValue(&$entity, $tagName) {
        if ($entity == null) return '';
        $domNodes = $entity->getElementsByTagName($tagName);
        if ($domNodes->length == 0) return '';
        return $domNodes->item(0)->nodeValue;
    }

    function getDescriptionValues(&$entity, $tagName) {
        $ret = array();
        if ($entity == null) return $ret;
        $domNodes = $entity->getElementsByTagName($tagName);
        if ($domNodes->length == 0) return $ret;
        foreach ($domNodes as $domNode)
            $ret[] = $domNode->nodeValue;
        return $ret;
    }

    function getDescriptionAttribute(&$entity, $tagName, $attributeName) {
        if ($entity == null) return '';
        $domNodes = $entity->getElementsByTagName($tagName);
        if ($domNodes->length == 0) return '';
        return $domNodes->item(0)->getAttribute($attributeName);
    }
}
