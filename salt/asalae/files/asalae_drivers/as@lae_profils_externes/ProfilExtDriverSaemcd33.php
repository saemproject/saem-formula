<?php
class ProfilExtDriverSaemcd33 implements ProfilExterneInterface {

    /**
     * retourne la description du driver qui sera affiché dans la vue détaillée
     * @return string description du driver
     */
    public function description() {
        return 'Référentiel des profils d\'archive du SAEMCD33 développé par Logilab';
    }

    /**
     * retourne la liste des paramètres et de leurs définitions utilisés par le driver
     * @return string descriptiondu compteur
     */
    public function parametersDefinition() {
        return array(
            'url' => array(
                'label' => 'Url du référentiel des profils d\'archives',
                'title' => 'veuillez saisir ici l\'url du référentiel logilab du projet SAEMCD33',
                'required' => true),
            'metadataPrefix' => array(
                'label' => 'Format et version du SEDA du schéma',
                'title' => 'veuillez sélectionner le format et version du SEDA du schéma',
                'required' => true,
                'options' => array(
                    'seda02rng'=>'Schéma RelaxNg, SEDA 0.2',
                    'seda1rng'=>'Schéma RelaxNg, SEDA 1.0',
                    'seda02xsd'=>'Schéma XSD, SEDA 0.2'))
            );
    }

    /**
     * retourne le formatage des paramètre pour l'affichage
     * @return array liste des paramètres a afficher sous la forme :
     *      ['paramName', ....]
     */
    public function parametersToStringDefinition() {
        return array('url');
    }

    /**
     * retourne true en cas de connexion réussie avec le référentiel et false dans le cas contraire
     * @param array $parametersValues tableau des valeurs des paramètres
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function ping($parametersValues) {
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        $client = new RESTClient($parametersValues['url']);
        try {
            $response = $client->get('?verb=Identify');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * retourne true si le driver se charge de faire l'update et false si l'update est fait par as@lae
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function autoUpdate() {
        return false;
    }

    /**
     * retourne la liste des identifiants des profils du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $dateFrom date-heure pour la mise à jour différentielle
     * @param string $scope NEW_AND_UPDATED pour lire les nouveaux éléments et ceux mis à jour ou DELETED pour lire les éléments supprimés
     * @return array liste des identifiants des profils du référentiel
     */
    public function getProfilIdentifiers($parametersValues, $dateFrom='', $scope='NEW_AND_UPDATED') {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        $resumptionToken = '';
        $profileIdentifiers = array();
        $headerStatus = ($scope == 'DELETED')?'deleted':'';

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $set = '&set='.'profile';
        $from = empty($dateFrom)?'':'&from='.AppOaipmh::encodeDate($dateFrom);
        $metadataPrefix = '&metadataPrefix='.$parametersValues['metadataPrefix'];

        // lecture de la liste des profils
        $verb = '?verb=ListIdentifiers';
        do {
            if (empty($resumptionToken)) {
                $response = $client->get($verb.$set.$from.$metadataPrefix);
            } else {
                $resumptionToken = '&resumptionToken='.$resumptionToken;
                $response = $client->get($verb.$set.$resumptionToken.$metadataPrefix);
            }
            $profileIdentifiers = array_merge($profileIdentifiers, AppOaipmh::getHeadersIdentifiersFromOaiPmhResponse($response, $headerStatus));
            $resumptionToken = AppOaipmh::getResumptionTokenOaiPmhResponse($response);
        } while (!empty($resumptionToken));

        return $profileIdentifiers;
    }

    /**
     * retourne les informations d'un profil du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $profilIdentifier identifiant du profil à mettre à jour
     * @return array valeurs des profils sous la forme :
     *      ['nom'=>string,
     *      'description'=>string,
     *      'modifiable'=>boolean,
     *      'xmlSchemas' => [
     *          0 => ['filename'=>string,
     *              'description'=>string,
     *              'pronom_format'=>string,
     *              'mime_code'=>string,
     *              'filecontent'=>string]]]
     */
    public function getProfilData($parametersValues, $profilIdentifier) {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        require_once(APP.DS.'libs'.DS.'xmlxsd.php');
        $ret = array();

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $metadataPrefix = '&metadataPrefix='.$parametersValues['metadataPrefix'];

        // lecture des données du profil
        $verb = '?verb=GetRecord';
        $identifier='&identifier='.$profilIdentifier;
        $response = $client->get($verb.$identifier.$metadataPrefix);
        $xmlProfileContent = AppOaipmh::getPathContentOaiPmhResponse('GetRecord/record/metadata', $response);
        $xmlProfileContent = '<?xml version="1.0"?>'."\n".str_replace(array('<metadata>', '</metadata>'), '', $xmlProfileContent);

        // selon le type du profil à importer
        switch($parametersValues['metadataPrefix']) {
            case 'seda02rng':
            case 'seda1rng':
                // lecture du nom du profil
                $profilNom = AppXmlxsd::xmlXsdPathValue($xmlProfileContent, '//rng:element[@name="ArchiveTransfer"]/xsd:annotation/xsd:documentation');
                $ret['nom'] = empty($profilNom)?$profilIdentifier:substr($profilNom, 0, 255);
                // lecture de la description du profil
                $profilDescription = AppXmlxsd::xmlXsdPathAttribute($xmlProfileContent, '//rng:element[@name="ArchiveTransfer"]/xsd:element[@name="Comment"]', 'fixed');
                $ret['description'] = substr($profilDescription, 0, 255);
                // modifiable ?
                $ret['modifiable'] = false;
                // fichier du schéma
                $ret['xmlSchemas'][0]['filename'] = $profilIdentifier.'.rng';
                $ret['xmlSchemas'][0]['description'] = 'schémas RelaxNG des profils';
                $ret['xmlSchemas'][0]['pronom_format'] = 'fmt/101';
                $ret['xmlSchemas'][0]['mime_code'] = 'application/xml';
                $ret['xmlSchemas'][0]['filecontent'] = $xmlProfileContent;
                break;
            case 'seda02xsd':
                // lecture du nom du profil
                $profilNom = AppXmlxsd::xmlXsdPathValue($xmlProfileContent, '//xsd:element[@name="ArchiveTransfer"]/xsd:annotation/xsd:documentation');
                $ret['nom'] = empty($profilNom)?$profilIdentifier:substr($profilNom, 0, 255);
                // lecture de la description du profil
                $profilDescription = AppXmlxsd::xmlXsdPathAttribute($xmlProfileContent, '//xsd:element[@name="ArchiveTransfer"]/xsd:element[@name="Comment"]', 'fixed');
                $ret['description'] = substr($profilDescription, 0, 255);
                // modifiable ?
                $ret['modifiable'] = false;
                // fichier du schéma
                $ret['xmlSchemas'][0]['filename'] = $profilIdentifier.'.xsd';
                $ret['xmlSchemas'][0]['description'] = 'schémas xsd des profils';
                $ret['xmlSchemas'][0]['pronom_format'] = 'fmt/101';
                $ret['xmlSchemas'][0]['mime_code'] = 'application/xml';
                $ret['xmlSchemas'][0]['filecontent'] = $this->_addImportNamespaceSedaToXsd($xmlProfileContent);
                break;
        }
        return $ret;
    }

    /**
     * procède à la mise à jour ou la création d'un profil d'archive, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $profilModel modele des Profils
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $profilIdentifier identifiant du profil à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doProfilCreateUpdate($profilModel, $refExterieurId, $parametersValues, $profilIdentifier) {
    }

    /**
     * procède à la suppression d'un profil d'archive, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $profilModel modele des Profils
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $profilIdentifier identifiant du profil à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doProfilDelete($profilModel, $refExterieurId, $parametersValues, $profilIdentifier) {
    }

    /**
     * ajoute les déclaration d'import des schémas des namespaces
     * @param string $xsdContent xml du schéma
     * @return string contenu du schéma avec les imports des namespaces
     */
    function _addImportNamespaceSedaToXsd($xsdContent) {
        $seda02SchemasLocation = WWW_ROOT.'schema_xml'.DS.'seda_v0-2'.DS.'v02'.DS;
        $seda10SchemasLocation = WWW_ROOT.'schema_xml'.DS.'seda_v1-0'.DS.'v10'.DS;
        $sedaSchemaNamespacesLocations = array(
            '0.2' => array(
                'fr:gouv:ae:archive:draft:standard_echange_v0.2:QualifiedDataType:1' => $seda02SchemasLocation.'archives_echanges_v0-2_QualifiedDataType.xsd',
                'urn:un:unece:uncefact:data:standard:UnqualifiedDataType:6' => $seda02SchemasLocation.'UnqualifiedDataType_6p0.xsd',
                'http://uri.etsi.org/01903/v1.4.1#' => $seda02SchemasLocation.'XAdESv141.xsd',
                'http://uri.etsi.org/01903/v1.3.2#' => $seda02SchemasLocation.'xades.xsd',
                'http://www.w3.org/2000/09/xmldsig#' => $seda02SchemasLocation.'xmldsig-core-schema.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:accessRestrictionCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_accessrestriction_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:appraisalCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_appraisal_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:descriptionLevelCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_descriptionlevel_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:documentTypeCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_documenttype_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:fileTypeCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_filetype_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:keywordTypeCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_keywordtype_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:languageCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_language_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:replyCode:2009-08-18' => $seda02SchemasLocation.'codes/archives_echanges_v0-2_reply_code.xsd',
                'urn:un:unece:uncefact:codelist:standard:IANA:CharacterSetCode:2007-05-14' => $seda02SchemasLocation.'codes/IANA_CharacterSetCode_20070514.xsd',
                'urn:un:unece:uncefact:codelist:standard:IANA:MIMEMediaType:2008-11-12' => $seda02SchemasLocation.'codes/IANA_MIMEMediaType_20081112.xsd',
                'urn:un:unece:uncefact:codelist:standard:5:ISO42173A:2008-11-12' => $seda02SchemasLocation.'codes/ISO_ISO3AlphaCurrencyCode_20081112.xsd',
                'urn:un:unece:uncefact:identifierlist:standard:5:ISO316612A:SecondEdition2006VI-3' => $seda02SchemasLocation.'codes/ISO_ISOTwoletterCountryCode_SecondEdition2006VI-3.xsd',
                'urn:un:unece:uncefact:codelist:standard:6:3055:D08A' => $seda02SchemasLocation.'codes/UNECE_AgencyIdentificationCode_D08A.xsd',
                'urn:un:unece:uncefact:codelist:standard:6:0133:40106' => $seda02SchemasLocation.'codes/UNECE_CharacterSetEncodingCode_40106.xsd',
                'urn:un:unece:uncefact:codelist:standard:6:Recommendation20:5' => $seda02SchemasLocation.'codes/UNECE_MeasurementUnitCommonCode_5.xsd'
            ),
            '1.0' => array(
                'fr:gouv:culture:archivesdefrance:seda:v1.0:QualifiedDataType:1' => $seda10SchemasLocation.'seda_v1-0_QualifiedDataType.xsd',
                'urn:un:unece:uncefact:data:standard:UnqualifiedDataType:10' => $seda10SchemasLocation.'UnqualifiedDataType_10p0.xsd',
                'http://uri.etsi.org/01903/v1.4.1#' => $seda10SchemasLocation.'XAdESv141.xsd',
                'http://uri.etsi.org/01903/v1.3.2#' => $seda10SchemasLocation.'xades.xsd',
                'http://www.w3.org/2000/09/xmldsig#' => $seda10SchemasLocation.'xmldsig-core-schema.xsd',
                'urn:un:unece:uncefact:codelist:standard:IANA:CharacterSetCode:2010-11-04' => $seda10SchemasLocation.'codes/IANA_CharacterSetCode_20101104.xsd',
                'urn:un:unece:uncefact:codelist:standard:IANA:MIMEMediaType:2011-02-16' => $seda10SchemasLocation.'codes/IANA_MIMEMediaType_20110216.xsd',
                'urn:un:unece:uncefact:codelist:standard:5:ISO42173A:2011-02-18' => $seda10SchemasLocation.'codes/ISO_ISO3AlphaCurrencyCode_20110218.xsd',
                'urn:un:unece:uncefact:identifierlist:standard:5:ISO316612A:SecondEdition2006VI-8' => $seda10SchemasLocation.'codes/ISO_ISOTwoletterCountryCode_SecondEdition2006VI-8.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:accessRestrictionCode:2009-08-18' => $seda10SchemasLocation.'codes/seda_v1-0_accessrestriction_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:appraisalCode:2009-08-18' => $seda10SchemasLocation.'codes/seda_v1-0_appraisal_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:SIAF:authorisationReasonCode:2012-07-05' => $seda10SchemasLocation.'codes/seda_v1-0_authorisationReason_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:descriptionLevelCode:2009-08-18' => $seda10SchemasLocation.'codes/seda_v1-0_descriptionlevel_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:documentTypeCode:2009-08-18' => $seda10SchemasLocation.'codes/seda_v1-0_documenttype_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:fileTypeCode:2011-11-03' => $seda10SchemasLocation.'codes/seda_v1-0_filetype_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:keywordTypeCode:2009-08-18' => $seda10SchemasLocation.'codes/seda_v1-0_keywordtype_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:DAF:languageCode:2011-10-07' => $seda10SchemasLocation.'codes/seda_v1-0_language_code.xsd',
                'urn:un:unece:uncefact:codelist:draft:SIAF:unitIdentifier:2011-12-15' => $seda10SchemasLocation.'codes/seda_v1-0_unitidentifier_code.xsd',
                'urn:un:unece:uncefact:codelist:standard:6:3055:D10A' => $seda10SchemasLocation.'codes/UNECE_AgencyIdentificationCode_D10A.xsd',
                'urn:un:unece:uncefact:codelist:standard:6:0133:40106' => $seda10SchemasLocation.'codes/UNECE_CharacterSetEncodingCode_40106.xsd',
                'urn:un:unece:uncefact:codelist:standard:UNECE:CommunicationMeansTypeCode:D10A' => $seda10SchemasLocation.'codes/UNECE_CommunicationMeansTypeCode_D10A.xsd',
                'urn:un:unece:uncefact:codelist:standard:6:Recommendation20:7' => $seda10SchemasLocation.'codes/UNECE_MeasurementUnitCommonCode_7.xsd'
            )
        );

        // liste des namespaces déclarés
        $xsd = new SimpleXMLElement($xsdContent);
        $namespaces = $xsd->getDocNamespaces();
        unset($xsd);

        // version du SEDA du schéma
        if (!empty($namespaces[''])) {
            $targetNameSpace = $namespaces[''];
            unset($namespaces['']);
        } else {
            $targetNameSpace = '';
        }
        if ($targetNameSpace == 'fr:gouv:culture:archivesdefrance:seda:v1.0')
            $sedaVersion = VERSION_SEDA_V1_0;
        elseif ($targetNameSpace == 'fr:gouv:ae:archive:draft:standard_echange_v0.2')
            $sedaVersion = VERSION_SEDA_V0_2;
        else
            $sedaVersion = VERSION_SEDA_V1_0;

        // ajout des imports
        $xsd = new DomDocument;
        $xsd->preserveWhiteSpace = false;
        $xsd->formatOutput = true;
        $xsd->loadXML($xsdContent);
        $xsd->documentElement->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xsd', 'http://www.w3.org/2001/XMLSchema');
        foreach($xsd->documentElement->childNodes as $firstNode) {
            if ($firstNode->nodeType == XML_ELEMENT_NODE)
                break;
        }
        foreach($namespaces as $namespace) {
            if (!empty($sedaSchemaNamespacesLocations[$sedaVersion][$namespace])) {
                $domEle = $xsd->createElementNS('http://www.w3.org/2001/XMLSchema', 'xsd:import');
                $domEle->setAttribute('namespace', $namespace);
                $domEle->setAttribute('schemaLocation', $sedaSchemaNamespacesLocations[$sedaVersion][$namespace]);
                $xsd->documentElement->insertBefore($domEle, $firstNode);
            }
        }

        return $xsd->saveXML();
    }
}
