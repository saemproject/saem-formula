<?php
class CptExtDriverSaemcd33 implements CompteurExterneInterface {

    /**
     * Paramètres d'authentification au référentiel
     *
     */
    private $url = 'http://saem-demo.cloudapp.net/referentiel/ark';
    private $tokenId = 'admindev';
    private $secret = "9743bd6bff094c3da8cb97047122ada90a4b8388202e425baf953088d2065d98f1532eba274f422cb89389fdbcd184feacb1d2e2413c4e4ca1d9c75af1d2925c";

    /**
     * variables de la classe
     */
    private $version = '1.0.1';

    /**
     * retourne la version du compteur qui sera affichée dans la vue détaillée des compteurs
     * @return string descriptiondu compteur
     */
    public function version() {
        return $this->version;
    }

    /**
     * retourne la description du compteur qui sera sera affichée dans la colonne 'définition' de la liste des compteurs
     * @return string descriptiondu compteur
     */
    public function description() {
        return 'Identifiant ARK SAEM CD33';
    }

    /**
     * retourne la valeur d'un compteur
     * @param string $compteurName nom du compteur
     * @param array $options tableau de valeurs optionnelles
     * @return string valeur du compteur
     */
    public function generateCounterValue($counterName, $options=array()) {
        // détermination de l'identifiant de l'autorité administrative correspondante dans le référentiel
        $organizationRefIdentifier = $this->_getRefAutoriteAdministrativeIdenfifierFromCollectivite($options);
        $url = $this->url.'?organization='.$organizationRefIdentifier;

        // initialisation des données de l'entête
        $saveLcTime = setlocale(LC_TIME, 0);
        if (!setlocale(LC_TIME, 'en_US'))
            setlocale(LC_TIME, 'en_US.utf8');
        $date = gmstrftime("%a, %d %b %Y %H:%M:%S GMT", time());
        setlocale(LC_TIME, $saveLcTime);
        $content = '';

        // calcul du token d'authentification hmac
        $tokenSubject = 'POST'.$url.hash('md5', $content).$date;
        $authToken = hash_hmac('md5', $tokenSubject, $this->secret);

        // initialisation et exécution du client curl
        $curlOpts = array(
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(),
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HEADER => false,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Cubicweb '.$this->tokenId.':'.$authToken,
                'Content-MD5: '.hash('md5', $content),
                'Date: '.$date,
                'Accept: application/json'
            )
        );
        $ch = curl_init();
        curl_setopt_array($ch, $curlOpts);
        $jsonResponse = curl_exec($ch);

        // recherche des erreurs
        if ($jsonResponse === false)
            throw new Exception('Erreur compteur externe '.$this->description().' : CURL code '.curl_error($ch));
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_status >= 400)
            throw new Exception('Erreur compteur externe '.$this->description().' : HTTP code '.$http_status);

        curl_close($ch);

        // analyse de la réponse
        $result = json_decode($jsonResponse, true);
        if (empty($result[0]))
            throw new Exception('Erreur compteur externe '.$this->description().' : réponse incomplète : '.$jsonResponse);
        if (!empty($result[0]['error']))
            throw new Exception('Erreur compteur externe '.$this->description().' : le serveur a répondu '.$result[0]['error']);
        if (empty($result[0]['ark']))
            throw new Exception('Erreur compteur externe '.$this->description().' : réponse incomplète : '.$jsonResponse);

        return 'ark:/'.$result[0]['ark'];
    }

    /**
     * permet de lire l'identiant de l'autorité administrative du référentiel stocké dans le champ description de la collectivité
     * @param array $options valeurs permettant de détarminer l'identifiant de l'organization du référentiel
     * @return string identifiant de l'organisation du référentiel
     * @throws Exception si l'identifiant n'est pas trouvé
     */
    private function _getRefAutoriteAdministrativeIdenfifierFromCollectivite($options) {
        // intialisations
        $collectiviteId = null;
        if (!class_exists('Collectivite'))
            App::import('Model','Collectivite');
        $this->Collectivite = new Collectivite();
        if (!class_exists('Organization'))
            App::import('Model','Organization');
        $this->Organization = new Organization();

        // détermination de l'id de la collectivité par le service d'archives
        if (!empty($options['archivalAgencyId'])) {
            $collectiviteId = $this->Organization->field('collectivite_id', array('id'=>$options['archivalAgencyId']));
            if (empty($collectiviteId))
                throw new Exception('Erreur compteur externe '.$this->description().' : acteur SEDA id:'.$options['archivalAgencyId'].' non trouvé');
        } elseif (!empty($options['valeursVariables']['ArchivalAgencyArchiveIdentifier'])) {
            if (!class_exists('Archive'))
                App::import('Model','Archive');
            $this->Archive = new Archive();
            $archivalAgencyId = $this->Archive->field('archival_agency_id', array('archival_agency_archive_identifier' => $options['valeursVariables']['ArchivalAgencyArchiveIdentifier']));
            if (empty($archivalAgencyId))
                throw new Exception('Erreur compteur externe '.$this->description().' : archive cote:'.$options['valeursVariables']['ArchivalAgencyArchiveIdentifier'].' non trouvée');
            $collectiviteId = $this->Organization->field('collectivite_id', array('id'=>$archivalAgencyId));
            if (empty($collectiviteId))
                throw new Exception('Erreur compteur externe '.$this->description().' : acteur SEDA id:'.$archivalAgencyId.' non trouvé');
        }

        // lecture de la collectivité du service d'archive
        $collectivite = $this->Collectivite->find('first', array(
            'recursive' => -1,
            'fields' => array('id', 'nom', 'description'),
            'conditions' => array('id'=>$collectiviteId)));
        if (empty($collectivite))
            throw new Exception('Erreur compteur externe '.$this->description().' : collectivité id:'.$collectiviteId.' non trouvée');
        // recherche de l'identifiant de l'autorité administrative
        $autoriteAdministrativeIdenfifier = '';
        $idAutAdminTag = '#IdAutoriteAdminRef#';
        if (!empty($collectivite['Collectivite']['description'])) {
            if (strpos($collectivite['Collectivite']['description'], $idAutAdminTag) !== false) {
                $pos1 = strpos($collectivite['Collectivite']['description'], $idAutAdminTag);
                $pos2 = strpos($collectivite['Collectivite']['description'], $idAutAdminTag, $pos1+1);
                if ($pos2 !== false) {
                    $autoriteAdministrativeIdenfifier = substr($collectivite['Collectivite']['description'], $pos1+strlen($idAutAdminTag), $pos2-$pos1-strlen($idAutAdminTag));
                }
            }
        }
        if (empty($autoriteAdministrativeIdenfifier)) {
            $autoriteAdministrativeIdenfifier = $this->_getRefAutoriteAdministrativeIdenfifierFromReferentiel($collectiviteId);
            if (!empty($autoriteAdministrativeIdenfifier)) {
                $collectivite['Collectivite']['description'] .= ($collectivite['Collectivite']['description']?' ':'').$idAutAdminTag.$autoriteAdministrativeIdenfifier.$idAutAdminTag;
                $this->Collectivite->Save($collectivite, false);
            }
        }
        if (empty($autoriteAdministrativeIdenfifier))
            throw new Exception('Erreur compteur externe '.$this->description().' : identifiant de l\'autorité administrative non trouvé pour la collectivité '.$collectivite['Collectivite']['nom']);
        return $autoriteAdministrativeIdenfifier;
    }

    /**
     * permet de lire l'identiant de l'autorité administrative du référentiel stocké dans le champ description de la collectivité
     * @param array $options valeurs permettant de détarminer l'identifiant de l'organization du référentiel
     * @return string identifiant de l'organisation du référentiel
     * @throws Exception si l'identifiant n'est pas trouvé
     */
    private function _getRefAutoriteAdministrativeIdenfifierFromReferentiel($collectiviteId) {
        $ret = null;
        try {
            // lecture de la collectivité
            $collectivite = $this->Collectivite->find('first', array(
                'recursive' => -1,
                'fields' => array('id', 'identifiant', 'nom', 'description'),
                'conditions' => array('id'=>$collectiviteId)));

            $url = str_replace('/ark', '', $this->url).'/'.$collectivite['Collectivite']['identifiant'].'?vid=rdf';

            // initialisation et exécution du client curl
            $curlOpts = array(
                CURLOPT_HTTPGET => true,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HEADER => false,
            );
            $ch = curl_init();
            curl_setopt_array($ch, $curlOpts);
            $response = curl_exec($ch);

            // recherche des erreurs
            if ($response === false)
                throw new Exception('Erreur compteur externe '.$this->description().' : CURL code '.curl_error($ch));
            $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($http_status >= 400)
                throw new Exception('Erreur compteur externe '.$this->description().' : HTTP code '.$http_status);

            // traitement de la réponse rdf
            $domDoc = new DOMDocument();
            $domDoc->loadXML($response);
            $identifiers = $domDoc->getElementsByTagName('identifier');
            if ($identifiers->length == 0)
                throw new Exception('Identifiant non trouvé dans le rdf');
            return $identifiers->item(0)->nodeValue;
        } catch (Exception $e) {
            return $ret;
        }
    }
}
