<?php
class VocabulaireExtDriverSaemcd33 implements VocabulaireExterneInterface {

    /**
     * retourne la description du driver qui sera affiché dans la vue détaillée
     * @return string description du driver
     */
    public function description() {
        return 'Référentiel des vocabulaires contrôlés du SAEMCD33 développé par Logilab';
    }

    /**
     * retourne la liste des paramètres et de leurs définitions utilisés par le driver
     * @return string descriptiondu compteur
     */
    public function parametersDefinition() {
        return array(
            'url' => array(
                'label' => 'url du référentiel des vocabulaires contrôlés',
                'title' => 'veuillez saisir ici l\'url du référentiel logilab du projet SAEMCD33',
                'required' => true));
    }

    /**
     * retourne le formatage des paramètre pour l'affichage
     * @return array liste des paramètres a afficher sous la forme :
     *      ['paramName', ....]
     */
    public function parametersToStringDefinition() {
        return array('url');
    }

    /**
     * retourne true en cas de connexion réussie avec le référentiel et false dans le cas contraire
     * @param array $parametersValues tableau des valeurs des paramètres
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function ping($parametersValues) {
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        $client = new RESTClient($parametersValues['url']);
        try {
            $response = $client->get('?verb=Identify');
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * retourne true si le driver se charge de faire l'update et false si l'update est fait par as@lae
     * @return boolean true si la connexion avec le référentiel est réussie et false dans le cas contraire
     */
    public function autoUpdate() {
        return true;
    }

    /**
     * retourne la liste des identifiants des vocabulaires contrôlés du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $dateFrom date-heure pour la mise à jour différentielle
     * @param string $scope NEW_AND_UPDATED pour lire les nouveaux éléments et ceux mis à jour ou DELETED pour lire les éléments supprimés
     * @return array liste des identifiants des vocabulaires contrôlés du référentiel
     */
    public function getVocabulaireIdentifiers($parametersValues, $dateFrom='', $scope='NEW_AND_UPDATED') {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');
        $resumptionToken = '';
        $vocabulaireIdentifiers = array();
        $headerStatus = ($scope == 'DELETED')?'deleted':'';

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $set = '&set='.'conceptscheme';
        $from = empty($dateFrom)?'':'&from='.AppOaipmh::encodeDate($dateFrom);
        $metadataPrefix = '&metadataPrefix='.'rdf';

        // lecture de la liste des vocabulaires contrôlés
        $verb = '?verb=ListIdentifiers';
        do {
            if (empty($resumptionToken)) {
                $response = $client->get($verb.$set.$from.$metadataPrefix);
            } else {
                $resumptionToken = '&resumptionToken='.$resumptionToken;
                $response = $client->get($verb.$set.$resumptionToken.$metadataPrefix);
            }
            $vocabulaireIdentifiers = array_merge($vocabulaireIdentifiers, AppOaipmh::getHeadersIdentifiersFromOaiPmhResponse($response, $headerStatus));
            $resumptionToken = AppOaipmh::getResumptionTokenOaiPmhResponse($response);
        } while (!empty($resumptionToken));

//        foreach($vocabulaireIdentifiers as &$vocabulaireIdentifier)
//            $vocabulaireIdentifier = str_replace('ark:/', '', $vocabulaireIdentifier);

        return $vocabulaireIdentifiers;
    }

    /**
     * retourne les informations d'un vocabulaire contrôlé du référentiel
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $vocabulaireIdentifier identifiant du vocabulaire contrôlé à mettre à jour
     * @return array valeurs des vocabulaires contrôlés sous la forme :
     *      ['name'=>string,
     *      'description'=>string,
     *      'modifiable'=>boolean,
     *      'keywords' => []
     */
    public function getVocabulaireData($parametersValues, $vocabulaireIdentifier) {
        // initialisations
        require_once(APP.DS.'libs'.DS.'RESTClient.php');
        require_once(APP.DS.'libs'.DS.'oaipmh.php');

        // initialisation des parametres de la requête OAI-PMH
        $client = new RESTClient($parametersValues['url']);
        $metadataPrefix = '&metadataPrefix='.'rdf';

        // lecture des données du vocabulaire contrôlé
        $verb = '?verb=GetRecord';
        $identifier='&identifier='.$vocabulaireIdentifier;
        $response = $client->get($verb.$identifier.$metadataPrefix);
        $xmlVocabulaireContent = AppOaipmh::getPathContentOaiPmhResponse('GetRecord/record/metadata', $response);
        $xmlVocabulaireContent = '<?xml version="1.0"?>'."\n".str_replace(array('<metadata>', '</metadata>'), '', $xmlVocabulaireContent);

        $domDoc = new DOMDocument();
        $domDoc->loadXML($xmlVocabulaireContent);
        $domTitles = $domDoc->getElementsByTagName('title');
        if($domTitles->length>0)
            $ret['name'] = $domTitles->item(0)->nodeValue;
        else
            $ret['name'] = $vocabulaireIdentifier;
        $ret['description'] = '';

        // modifiable ?
        $ret['modifiable'] = false;

        // suppression du terme ark:/ pour le set
        $vocabulaireIdentifier = str_replace('ark:/', '', $vocabulaireIdentifier);

        // lecture des mots clés
        $verb = '?verb=ListRecords';
        $set='&set='.'concept:in_scheme:'.$vocabulaireIdentifier;
        $resumptionToken = '';
        $ret['keywords'] = array();
        do {
            $resumptionToken = empty($resumptionToken)?'':('&resumptionToken='.$resumptionToken);
            $response = $client->get($verb.$set.$resumptionToken.$metadataPrefix);
            $ret['keywords'] = array_merge($ret['keywords'], AppXmlrdfskos::xmlRdfSkosConcepts($response));
            $resumptionToken = AppOaipmh::getResumptionTokenOaiPmhResponse($response);
        } while (!empty($resumptionToken));

        return $ret;
    }

    /**
     * procède à la mise à jour ou la création d'un vocabulaire contrôlé, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $vocabulaireModel modele des Vocabulaires
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $vocabulaireIdentifier identifiant du vocabulaire contrôlé à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doVocabulaireCreateUpdate($vocabulaireModel, $refExterieurId, $parametersValues, $vocabulaireIdentifier) {
        // initialisations
        $this->Keywordlist = &$vocabulaireModel;
        $this->Keyword = &$this->Keywordlist->Keyword;
        $this->Keyword->Behaviors->detach('Tree');
        $this->Altkeyword = &$this->Keyword->Altkeyword;
        $ret = array('report'=>'', 'record'=>array());

        // lecture des données du référentiel
        $vocabulaireData = $this->getVocabulaireData($parametersValues, $vocabulaireIdentifier);

        // ajout ou mise à jour de la liste des mots clés
        $keywordlist = $this->Keywordlist->find('first', array(
            'recursive' => -1,
            'conditions' => array('identifiant'=>$vocabulaireIdentifier)));
        if (empty($keywordlist)) {
            $action = 'create';
            $keywordlist = $this->Keywordlist->create();
            $keywordlist['Keywordlist'] = array(
                'identifiant' => $vocabulaireIdentifier,
                'version' => 1,
                'active' => true,
                'keywordtype_id' => null,
                'scheme_id' => null,
                'scheme_name' => null,
                'scheme_agency_name' => null,
                'scheme_version_id' => null,
                'scheme_data_uri' => null,
                'scheme_uri' => null,
                'created_user_id' => 1,
                'modified_user_id' => 1);
        } else {
            $action = 'update';
            // suppression des mots clés
            $keywords = $this->Keyword->find('all', array(
                'recursive' => -1,
                'fields' => array('id'),
                'conditions' => array('keywordlist_id'=>$keywordlist['Keywordlist']['id'])));
            foreach($keywords as $keyword) {
                $this->Altkeyword->deleteAll(array('keyword_id'=>$keyword['Keyword']['id']));
                $query = 'DELETE FROM "keywords_keywords" WHERE keyword_id = '.$keyword['Keyword']['id'].';';
                $this->Keyword->query($query);
            }
            $this->Keyword->deleteAll(array('keywordlist_id'=>$keywordlist['Keywordlist']['id']));
        }
        // création et mise à jour
        $keywordlist['Keywordlist']['nom'] = $vocabulaireData['name'];
        $keywordlist['Keywordlist']['description'] = $vocabulaireData['description'];
        $keywordlist['Keywordlist']['modifiable'] = $vocabulaireData['modifiable'];

        // sauvegarde en base de données
        if (!$this->Keywordlist->save($keywordlist, false))
            throw new Exception('Synchronisation du référentiel \''.$vocabulaireData['name'].'\''.' : erreur lors de la sauvegarde de la liste de mots-clés '.$vocabulaireIdentifier.' en base de données');
        $keywordlist['Keywordlist']['id'] = $this->Keywordlist->id;

        // création des mots clés
        // mots clés
        $conceptsByGenericCodes = array();
        foreach($vocabulaireData['keywords'] as &$concept) {
            if (empty($concept['genericCode'])) {
                $conceptsByGenericCodes['#SANS_PARENT_#NO_PARENT#'][] = $concept;
            } else {
                $conceptsByGenericCodes[$concept['genericCode']][] = $concept;
            }
        }
        $treeStartLeft = $this->Keyword->field('MAX(rght)')+1;
//        $treeConcepts = $this->_conceptsToTree($vocabulaireData['keywords'], $treeStartLeft);
        $treeConcepts = $this->_conceptsToTree($conceptsByGenericCodes, $treeStartLeft, '#SANS_PARENT_#NO_PARENT#');
        // sauvegarde en base de données
        $this->_saveConceptsTree($treeConcepts, null, $keywordlist['Keywordlist']['id'], 'Synchronisation du référentiel \''.$vocabulaireData['name'].'\'');
        // termes lies
        foreach ($vocabulaireData['keywords'] as &$concept) {
            foreach($concept['relatedCodes'] as $relatedKeywordCode) {
                $keywordId = $this->Keyword->field('id', array('keywordlist_id'=>$keywordlist['Keywordlist']['id'], 'code'=>$concept['code']));
                $relatedKeywordId = $this->Keyword->field('id', array('keywordlist_id'=>$keywordlist['Keywordlist']['id'], 'code'=>$relatedKeywordCode));
                $query = 'INSERT INTO "keywords_keywords" (keyword_id, related_id) VALUES ('.$keywordId.', '.$relatedKeywordId.');';
                $this->Keyword->query($query);
            }
        }

        $ret['report'] = ' - '.($action=='create'?'création':'mise à jour').' du vocabulaire contrôlé \''.$vocabulaireIdentifier.'\' réussie';
        $ret['record'] = array('action'=>$action, 'id'=>$keywordlist['Keywordlist']['id'], 'nom'=>$keywordlist['Keywordlist']['nom']);

        return $ret;
    }

    /**
     * procède à la suppression d'un vocabulaire contrôlé, appelée lorsque la méthode autoUpdate() retourne true
     * @param object $vocabulaireModel modele des Vocabulaires
     * @param integer $refExterieurId id du référentiel extérieur en cours de mise à jour
     * @param array $parametersValues tableau des valeurs des paramètres
     * @param string $vocabulaireIdentifier identifiant du vocabulaire contrôlé à mettre à jour
     * @return array tableau de rapport d'exécution formaté comme suit [['report'=>string, 'record'=>['action'=>'create'|'update'|'delete'|'deactivate', 'id', 'nom']]
     */
    public function doVocabulaireDelete($vocabulaireModel, $refExterieurId, $parametersValues, $vocabulaireIdentifier) {
        // initialisations
        $this->Keywordlist = &$vocabulaireModel;
        $this->Keyword = &$this->Keywordlist->Keyword;
        $this->Keyword->Behaviors->detach('Tree');
        $this->Altkeyword = &$this->Keyword->Altkeyword;
        $ret = array('report'=>'', 'record'=>array());

        $keywordlist = $this->Keywordlist->find('first', array(
            'recursive' => -1,
            'fields' => array('id', 'nom', 'active'),
            'conditions' => array('identifiant'=>$vocabulaireIdentifier)));
        if (empty($keywordlist))
            return $ret;
        if ($this->Keywordlist->isDeletable($keywordlist['Keywordlist']['id'])) {
            // suppression des mots clés
            $keywords = $this->Keyword->find('all', array(
                'recursive' => -1,
                'fields' => array('id'),
                'conditions' => array('keywordlist_id'=>$keywordlist['Keywordlist']['id'])));
            foreach($keywords as $keyword) {
                $this->Altkeyword->deleteAll(array('keyword_id'=>$keyword['Keyword']['id']));
                $query = 'DELETE FROM "keywords_keywords" WHERE keyword_id = '.$keyword['Keyword']['id'].';';
                $this->Keyword->query($query);
            }
            $this->Keyword->deleteAll(array('keywordlist_id'=>$keywordlist['Keywordlist']['id']));
            if (!$this->Keywordlist->del($keywordlist['Keywordlist']['id']))
                throw new Exception(__('Une erreur est survenue pendant la suppression du vocabulaire contrôlé', true).' '.$vocabulaireIdentifier);
            $ret['report'] = ' - suppression du vocabulaire contrôlé \''.$vocabulaireIdentifier.'\' réussie';
            $ret['record'] = array('action'=>'delete', 'id'=>$keywordlist['Keywordlist']['id'], 'nom'=>$keywordlist['Keywordlist']['nom']);
        } elseif ($keywordlist['Keywordlist']['active']) {
            $keywordlist['Keywordlist']['active'] = false;
            if (!$this->Keywordlist->save($keywordlist, false))
                throw new Exception('Synchronisation du référentiel \''.$keywordlist['Keywordlist']['nom'].'\''.' : erreur lors de la sauvegarde du vocabulaire contrôlé '.$vocabulaireIdentifier.' en base de données');

            $ret['report'] = ' - désactivation du vocabulaire contrôlé \''.$vocabulaireIdentifier.'\' réussie';
            $ret['record'] = array('action'=>'deactivate', 'id'=>$keywordlist['Keywordlist']['id'], 'nom'=>$keywordlist['Keywordlist']['nom']);
        }

        return $ret;
    }

    /**
     * transforme un tableau de concepts à plat en un tableau hiérarchique CakeTree
     * @param array $concepts tableau de concept à plat
     * @param integer $left valeur de l'indexe de hierarchie (CakeTree)
     * @param string $parentCode clé code du parent (recursivité)
     * @return array tableau sous forme hierarchique
     */
    private function _conceptsToTree(&$conceptsByGenericCodes, &$left, $parentCode) {
        $ret = array();
        foreach($conceptsByGenericCodes[$parentCode] as $concept) {
            $concept['left'] = $left;
            $concept['right'] = null;
            $left++;
            if (!empty($conceptsByGenericCodes[$concept['code']]))
                $concept['children'] = $this->_conceptsToTree($conceptsByGenericCodes, $left, $concept['code']);
            else
                $concept['children'] = array();
            $concept['right']=$left;
            $ret[] = $concept;
            $left++;
        }
        return $ret;
    }

    /**
     * sauvegarde en base de données un tableau de concepts sous forme hiérarchique CakeTree
     * @param array $concepts tableau de concept à plat
     * @param integer $parentId id du concept parent
     * @param integer $keywordlistId id de la liste des mots clés
     * @param string $prefixeMessages préfixe à utiliser pour les messages d'erreur
     * @return array tableau sous forme hierarchique
     */
    private function _saveConceptsTree(&$concepts, $parentId, $keywordlistId, $prefixeMessages) {
        foreach ($concepts as &$concept) {
            $keyword = $this->Keyword->create();
            $keyword['Keyword']['code'] = $concept['code'];
            $keyword['Keyword']['libelle'] = empty($concept['prefLabels'])?$concept['code']:array_shift($concept['prefLabels']);
            $keyword['Keyword']['exact_match'] = $concept['exact_match'];
            $keyword['Keyword']['change_note'] = $concept['change_note'];
            $keyword['Keyword']['keywordlist_id'] = $keywordlistId;
            $keyword['Keyword']['version'] = 1;
            $keyword['Keyword']['parent_id'] = $parentId;
            $keyword['Keyword']['lft'] = $concept['left'];
            $keyword['Keyword']['rght'] = $concept['right'];
            $keyword['Keyword']['created_user_id'] = 1;
            $keyword['Keyword']['modified_user_id'] = 1;
            if (!$this->Keyword->save($keyword, false))
                throw new Exception($prefixeMessages.' : erreur lors de la sauvegarde du mot clés '.$concept['code'].' en base de données');
            $concept['keyword_id'] = $this->Keyword->id;

            // termes alternatifs
            foreach($concept['altLabels'] as $altLabel) {
                $this->Altkeyword->create();
                $altkeyword['Altkeyword']['keyword_id'] = $concept['keyword_id'];
                $altkeyword['Altkeyword']['alt_label'] = $altLabel;
                $altkeyword['Altkeyword']['created_user_id'] = 1;
                $altkeyword['Altkeyword']['modified_user_id'] = 1;
                if (!$this->Altkeyword->save($altkeyword, false))
                    throw new Exception($prefixeMessages.' : erreur lors de la sauvegarde du mot clés alternatif'.$altkeyword['Altkeyword']['alt_label'].' en base de données');
            }

            // concepts enfants
            if (!empty($concept['children']))
                $this->_saveConceptsTree($concept['children'], $concept['keyword_id'], $keywordlistId, $prefixeMessages);
        }
    }
}

/**
 * Created on 23 sept. 2015
 *
 * Classe regroupant des fonctions de parsing des xml rdf
 *
 */

class AppXmlrdfskos {

    /**
     * retourne la liste des concepts d'une chaîne xmlrdfskos
     * @param string $xmlRdfSkosContent contenu du xml rdf
     * @return array liste des concepts (mots clés) sous la forme ['code', 'libelle')
     */
    public function xmlRdfSkosConcepts($xmlRdfSkosContent) {
        // initialisations
        $ret = array();

        // parsing du xml
        try {
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xmlRdfSkosContent);
            $concepts = $domDoc->getElementsByTagName('Description');

            // parcours des concepts
            foreach ($concepts as $concept) {
                $entity = array();
                $entity['code'] = AppXmlrdfskos::getConceptCode($concept);
                $entity['prefLabels'] = AppXmlrdfskos::getPrefLabels($concept);
                $entity['exact_match'] = AppXmlrdfskos::getExactMatchConcept($concept);
                $entity['change_note'] = AppXmlrdfskos::getChangeNoteConcept($concept);
                $entity['altLabels'] = AppXmlrdfskos::getAltLabels($concept);
                $entity['relatedCodes'] = AppXmlrdfskos::getRelatedCodes($concept);
                $entity['genericCode'] = AppXmlrdfskos::getBroaderCode($concept);
                if ($entity['genericCode'] == $entity['code']) {
                    $entity['genericCode'] = '';
                }
                if (empty($entity['prefLabels']) && empty($entity['altLabels'])) continue;
                $ret[] = $entity;
            }
        } catch (Exception $e) {
            $ret = array();
        }

        return $ret;
    }

    function _parseUrl($url) {
        return (substr($url, strrpos($url, '/')+1, strlen($url)));
    }
    function getConceptCode(&$concept) {
        if ($concept == null) return '';
        $url_rdf_about = $concept->getAttribute('rdf:about');
        return AppXmlrdfskos::_parseUrl ($url_rdf_about);
    }
    function getPrefLabels(&$concept) {
        $ret = array();
        if ($concept == null) return '';
        $prefLabels =  $concept->getElementsByTagName('prefLabel');
        if ($prefLabels->length == 0) return '';
        foreach($prefLabels as $prefLabel)
            $ret[] = $prefLabel->nodeValue;
        return $ret;
    }
    function getExactMatchConcept(&$concept) {
        if ($concept == null) return '';
        $exactMatches = $concept->getElementsByTagName('exactMatch');
        if ($exactMatches->length == 0) return '';
        return $exactMatches->item(0)->getAttribute('rdf:resource');
    }
    function getChangeNoteConcept(&$concept) {
        if ($concept == null) return '';
        $changeNotes =  $concept->getElementsByTagName('changeNote');
        if ($changeNotes->length == 0) return '';
        $changeNote = $changeNotes->item(0)->nodeValue;
        $changeNote = str_replace(array("\n", "\r", "	"), array(' ', ' ', ''), $changeNote);
        return $changeNote;
    }
    function getAltLabels(&$concept) {
        if ($concept == null) return array();
        $altLabels = $concept->getElementsByTagName('altLabel');
        if ($altLabels->length == 0) return array();
        $ret = array();
        foreach($altLabels as $altLabel)
            $ret[] = $altLabel->nodeValue;
        return $ret;
    }
    function getResourceCode(&$broader) {
        if ($broader == null) return '';
        $url_rdf = $broader->getAttribute('rdf:resource');
        return AppXmlrdfskos::_parseUrl($url_rdf);
    }
    function getBroaderCode(&$concept) {
        if ($concept == null) return '';
        $broaders =  $concept->getElementsByTagName('broader');
        if ($broaders->length == 0) return '';
        return AppXmlrdfskos::getResourceCode($broaders->item(0));
    }
    function getRelatedCodes(&$concept) {
        $ret = array();
        if ($concept == null) return array();
        // parcours de balises related de premier niveau
        foreach($concept->childNodes as $childNode) {
            if ($childNode->nodeType != XML_ELEMENT_NODE) continue;
            if (strpos($childNode->tagName, 'related')===false) continue;
            if ($childNode->childNodes->length == 0) {
                $ret[] = AppXmlrdfskos::getResourceCode($childNode);
            } else {
                $relatedConcepts = $concept->getElementsByTagName('Concept');
                if ($relatedConcepts->length == 0) continue;
                $ret[] = AppXmlrdfskos::getConceptCode($relatedConcepts->item(0));
            }
        }
        return $ret;
    }
}
