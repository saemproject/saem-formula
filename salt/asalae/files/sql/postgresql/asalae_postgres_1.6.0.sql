--
-- PostgreSQL database dump : asalae 1.6.0
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- début de la transaction
--
BEGIN;




--
-- Name: accords_filetypes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE accords_filetypes (
    id integer NOT NULL,
    accord_id integer NOT NULL,
    filetype_id integer NOT NULL
);


--
-- Name: accords_filetypes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accords_filetypes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accords_filetypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accords_filetypes_id_seq OWNED BY accords_filetypes.id;


--
-- Name: accords_versants; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE accords_versants (
    id integer NOT NULL,
    accord_id integer NOT NULL,
    organization_id integer NOT NULL
);


--
-- Name: accords_versants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE accords_versants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: accords_versants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE accords_versants_id_seq OWNED BY accords_versants.id;


--
-- Name: acos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE acos (
    id bigint NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key bigint,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer
);


--
-- Name: acos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE acos_id_seq OWNED BY acos.id;


--
-- Name: adm-accords; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-accords" (
    id integer NOT NULL,
    identifiant character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    archival_agency_id integer NOT NULL,
    profil_id integer,
    circuit_id integer,
    plan_rangement character varying(255) DEFAULT NULL::character varying,
    date_debut date,
    date_fin date,
    periodicite integer DEFAULT 0,
    unite_periode character varying(1) DEFAULT NULL::character varying,
    conversion boolean DEFAULT false,
    volume_max_unitaire integer DEFAULT 0,
    unite_vol_max_unitaire character varying(2) DEFAULT NULL::character varying,
    volume_max_total integer DEFAULT 0,
    unite_vol_max_total character varying(2) DEFAULT NULL::character varying,
    empreinte boolean DEFAULT false,
    doc_signe_verif boolean DEFAULT false,
    actif boolean DEFAULT true,
    evt_aucun_versement boolean DEFAULT false,
    evt_depasse_nb_versement boolean DEFAULT false,
    evt_depasse_vol_unitaire boolean DEFAULT false,
    evt_depasse_vol_total boolean DEFAULT false,
    evt_autres_erreurs boolean DEFAULT false,
    communication_service_id integer,
    communication_user_id integer,
    communication_email character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL,
    signature boolean DEFAULT false,
    pj_invalide_alerte boolean DEFAULT false,
    volume_identifier character varying(256),
    validation_automatique boolean DEFAULT false,
    defaut boolean DEFAULT false,
    seda02 character varying,
    seda10 character varying,
    tout_producteur boolean DEFAULT false
);


--
-- Name: adm-accords_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-accords_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-accords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-accords_id_seq" OWNED BY "adm-accords".id;


--
-- Name: adm-affichages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-affichages" (
    id integer NOT NULL,
    message text,
    nblignes integer
);


--
-- Name: adm-affichages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-affichages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-affichages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-affichages_id_seq" OWNED BY "adm-affichages".id;


--
-- Name: adm-altkeywords; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-altkeywords" (
    id integer NOT NULL,
    keyword_id integer NOT NULL,
    alt_label character varying(256) NOT NULL,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer
);


--
-- Name: adm-altkeywords_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-altkeywords_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-altkeywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-altkeywords_id_seq" OWNED BY "adm-altkeywords".id;


--
-- Name: adm-collectivites; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-collectivites" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    active boolean DEFAULT true,
    affichage_id integer NOT NULL,
    parent_id integer DEFAULT 0,
    lft integer DEFAULT 0,
    rght integer DEFAULT 0,
    created timestamp without time zone NOT NULL,
    created_user_id integer,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer
);


--
-- Name: adm-collectivites_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-collectivites_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-collectivites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-collectivites_id_seq" OWNED BY "adm-collectivites".id;


--
-- Name: adm-compteurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-compteurs" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    def_compteur character varying(255) NOT NULL,
    sequence_id integer NOT NULL,
    def_reinit character varying(255) DEFAULT NULL::character varying,
    val_reinit character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified_user_id integer NOT NULL,
    seda02 character varying,
    seda10 character varying,
    type character varying(128) DEFAULT 'interne'::character varying
);


--
-- Name: adm-compteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-compteurs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-compteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-compteurs_id_seq" OWNED BY "adm-compteurs".id;


--
-- Name: adm-connecteurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-connecteurs" (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    actif boolean DEFAULT true,
    ws_username character varying(255) DEFAULT NULL::character varying,
    ws_password character varying(255) DEFAULT NULL::character varying,
    param1 character varying(255) DEFAULT NULL::character varying,
    param2 character varying(255) DEFAULT NULL::character varying,
    param3 character varying(255) DEFAULT NULL::character varying,
    param4 character varying(255) DEFAULT NULL::character varying,
    param5 character varying(255) DEFAULT NULL::character varying,
    param6 character varying(255) DEFAULT NULL::character varying,
    param7 character varying(255) DEFAULT NULL::character varying,
    param8 character varying(255) DEFAULT NULL::character varying,
    param9 character varying(255) DEFAULT NULL::character varying,
    param10 character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    created_user_id integer,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer
);


--
-- Name: adm-connecteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-connecteurs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-connecteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-connecteurs_id_seq" OWNED BY "adm-connecteurs".id;


--
-- Name: adm-contrats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-contrats" (
    id integer NOT NULL,
    accord_id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    date_debut date,
    date_fin date,
    originating_agency_id integer NOT NULL,
    actif boolean DEFAULT true NOT NULL,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL
);


--
-- Name: adm-contrats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-contrats_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-contrats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-contrats_id_seq" OWNED BY "adm-contrats".id;


--
-- Name: adm-crons; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-crons" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    controller character varying(255) NOT NULL,
    action character varying(255) NOT NULL,
    has_params boolean DEFAULT false,
    params character varying(255) DEFAULT NULL::character varying,
    next_execution_time timestamp without time zone,
    execution_duration character varying(255),
    last_execution_start_time timestamp without time zone,
    last_execution_end_time timestamp without time zone,
    last_execution_report text,
    last_execution_status character varying(255),
    active boolean DEFAULT true,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL
);


--
-- Name: adm-crons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-crons_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-crons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-crons_id_seq" OWNED BY "adm-crons".id;


--
-- Name: adm-fichiers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-fichiers" (
    id integer NOT NULL,
    filename character varying(1024) DEFAULT NULL::character varying,
    description text,
    pronom_format character varying(256) DEFAULT NULL::character varying,
    mime_code character varying(256) DEFAULT NULL::character varying,
    size_bytes bigint DEFAULT 0,
    foreign_model character varying(256) DEFAULT NULL::character varying,
    foreign_key integer DEFAULT 0,
    foreign_type character varying(256) DEFAULT NULL::character varying,
    foreign_order integer DEFAULT 0,
    uri character varying(1024) DEFAULT NULL::character varying,
    content bytea,
    hash_type character varying(256) DEFAULT NULL::character varying,
    hash_value character varying(1024) DEFAULT NULL::character varying,
    json_encoded_metadata text,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: adm-fichiers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-fichiers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-fichiers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-fichiers_id_seq" OWNED BY "adm-fichiers".id;


--
-- Name: adm-journaux; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-journaux" (
    id integer NOT NULL,
    date timestamp without time zone NOT NULL,
    origine_type character varying(255) NOT NULL,
    origine_identifiant character varying(255) NOT NULL,
    origine_nom character varying(255) NOT NULL,
    evenement_controller character varying(255) NOT NULL,
    evenement_action character varying(255) NOT NULL,
    evenement_nom character varying(255) NOT NULL,
    message text NOT NULL,
    cycle_vie boolean DEFAULT false,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key bigint,
    hash_value character varying(255) NOT NULL,
    exporte boolean DEFAULT false,
    hash_type character varying(255) NOT NULL,
    cycle_vie_type character varying(1) DEFAULT 'N'::character varying NOT NULL
);


--
-- Name: adm-journaux_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-journaux_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-journaux_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-journaux_id_seq" OWNED BY "adm-journaux".id;


--
-- Name: adm-keywordlists; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-keywordlists" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(1024),
    version integer NOT NULL,
    active boolean NOT NULL,
    modifiable boolean DEFAULT true,
    keywordtype_id integer,
    scheme_id character varying(1024),
    scheme_name character varying(1024),
    scheme_agency_name character varying(1024),
    scheme_version_id character varying(1024),
    scheme_data_uri character varying(1024),
    scheme_uri character varying(1024),
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone,
    modified_user_id integer,
    identifiant character varying(256) DEFAULT NULL::character varying NOT NULL
);


--
-- Name: adm-keywordlists_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-keywordlists_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-keywordlists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-keywordlists_id_seq" OWNED BY "adm-keywordlists".id;


--
-- Name: adm-keywords; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-keywords" (
    id integer NOT NULL,
    keywordlist_id integer NOT NULL,
    version integer NOT NULL,
    code character varying(255) NOT NULL,
    libelle character varying(1024),
    parent_id integer,
    lft integer NOT NULL,
    rght integer NOT NULL,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone,
    modified_user_id integer,
    exact_match character varying(255) DEFAULT NULL::character varying,
    change_note text
);


--
-- Name: adm-keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-keywords_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-keywords_id_seq" OWNED BY "adm-keywords".id;


--
-- Name: adm-parapheurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-parapheurs" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    actif boolean DEFAULT true,
    wsto character varying(255) DEFAULT NULL::character varying,
    clientcert_file_uri character varying(255) DEFAULT NULL::character varying,
    passphrase character varying(255) DEFAULT NULL::character varying,
    httpauth character varying(255) DEFAULT NULL::character varying,
    httppasswd character varying(255) DEFAULT NULL::character varying,
    visibilite character varying(255) DEFAULT NULL::character varying,
    typetech character varying(255) DEFAULT NULL::character varying,
    soustype character varying(255) DEFAULT NULL::character varying,
    email_emmeteur character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL
);


--
-- Name: adm-parapheurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-parapheurs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-parapheurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-parapheurs_id_seq" OWNED BY "adm-parapheurs".id;


--
-- Name: adm-profils; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-profils" (
    id integer NOT NULL,
    identifiant character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    seda02 character varying,
    seda10 character varying,
    actif boolean DEFAULT true NOT NULL,
    modifiable boolean DEFAULT true,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL
);


--
-- Name: adm-profils_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-profils_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-profils_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-profils_id_seq" OWNED BY "adm-profils".id;


--
-- Name: adm-referentiels; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-referentiels" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description text
);


--
-- Name: adm-referentiels_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-referentiels_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-referentiels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-referentiels_id_seq" OWNED BY "adm-referentiels".id;


--
-- Name: adm-refexterieurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-refexterieurs" (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    driver_name character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    description text,
    actif boolean DEFAULT true,
    parametres text NOT NULL,
    execution_date timestamp without time zone,
    update_date timestamp without time zone,
    update_report text,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone,
    created_user_id integer,
    modified_user_id integer
);


--
-- Name: adm-refexterieurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-refexterieurs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-refexterieurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-refexterieurs_id_seq" OWNED BY "adm-refexterieurs".id;


--
-- Name: adm-roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-roles" (
    id integer NOT NULL,
    collectivite_id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    created_user_id integer,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer
);


--
-- Name: adm-roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-roles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-roles_id_seq" OWNED BY "adm-roles".id;


--
-- Name: adm-sequences; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-sequences" (
    id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    num_sequence integer NOT NULL,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL
);


--
-- Name: adm-sequences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-sequences_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-sequences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-sequences_id_seq" OWNED BY "adm-sequences".id;


--
-- Name: adm-services; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-services" (
    id integer NOT NULL,
    collectivite_id integer NOT NULL,
    nom character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    organization_id integer,
    acteur_seda boolean DEFAULT false NOT NULL,
    contact_user_id integer,
    actif boolean DEFAULT true,
    parent_id integer,
    lft integer NOT NULL,
    rght integer NOT NULL,
    created timestamp without time zone NOT NULL,
    created_user_id integer,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer
);


--
-- Name: adm-services_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-services_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-services_id_seq" OWNED BY "adm-services".id;


--
-- Name: adm-typeacteurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-typeacteurs" (
    id integer NOT NULL,
    nom character varying(255) DEFAULT NULL::character varying,
    description character varying(255) DEFAULT NULL::character varying,
    code_type character varying(1) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    created_user_id integer,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer
);


--
-- Name: adm-typeacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-typeacteurs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-typeacteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-typeacteurs_id_seq" OWNED BY "adm-typeacteurs".id;


--
-- Name: adm-typeentites; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-typeentites" (
    id integer NOT NULL,
    code character varying(255) NOT NULL,
    libelle character varying(255) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: adm-typeentites_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-typeentites_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-typeentites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-typeentites_id_seq" OWNED BY "adm-typeentites".id;


--
-- Name: adm-typemessages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-typemessages" (
    id integer NOT NULL,
    classe character varying(255) NOT NULL,
    code character varying(255) NOT NULL,
    nom character varying(255) NOT NULL,
    description text
);


--
-- Name: adm-typemessages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-typemessages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-typemessages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-typemessages_id_seq" OWNED BY "adm-typemessages".id;


--
-- Name: adm-users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-users" (
    id integer NOT NULL,
    collectivite_id integer NOT NULL,
    service_id integer NOT NULL,
    role_id integer,
    username character varying(255) DEFAULT NULL::character varying,
    password character varying(255) DEFAULT NULL::character varying,
    cert_subject bytea,
    need_cert boolean,
    civilite character varying(255) DEFAULT NULL::character varying,
    nom character varying(255) NOT NULL,
    prenom character varying(255) NOT NULL,
    titre character varying(255) DEFAULT NULL::character varying,
    mail character varying(255) DEFAULT NULL::character varying,
    user_delegue_id integer,
    actif boolean DEFAULT true NOT NULL,
    affichage_id integer,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL,
    last_login timestamp without time zone,
    last_logout timestamp without time zone,
    password_sha256 character varying(255) DEFAULT NULL::character varying
);


--
-- Name: adm-users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-users_id_seq" OWNED BY "adm-users".id;


--
-- Name: adm-verrous; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "adm-verrous" (
    id integer NOT NULL,
    nom character varying(256) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: adm-verrous_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "adm-verrous_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: adm-verrous_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "adm-verrous_id_seq" OWNED BY "adm-verrous".id;


--
-- Name: arc-accessrestrictions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-accessrestrictions" (
    id integer NOT NULL,
    code character varying(128) DEFAULT NULL::character varying,
    start_date date,
    accessrestrictioncode_id integer,
    end_date date,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-accessrestrictions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-accessrestrictions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-accessrestrictions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-accessrestrictions_id_seq" OWNED BY "arc-accessrestrictions".id;


--
-- Name: arc-appraisals; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-appraisals" (
    id integer NOT NULL,
    code character varying(128) DEFAULT NULL::character varying,
    duration character varying(128) DEFAULT NULL::character varying,
    start_date date,
    appraisalcode_id integer,
    end_date date,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-appraisals_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-appraisals_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-appraisals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-appraisals_id_seq" OWNED BY "arc-appraisals".id;


--
-- Name: arc-archivefiles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-archivefiles" (
    id integer NOT NULL,
    archive_id integer NOT NULL,
    type character varying(256) NOT NULL,
    version integer DEFAULT 1,
    identifier character varying(256) NOT NULL,
    filename character varying(1024) NOT NULL,
    pronom_unique_id character varying(256) DEFAULT NULL::character varying,
    mime_code character varying(256) DEFAULT NULL::character varying,
    size_bytes bigint NOT NULL,
    hash_type character varying(256) NOT NULL,
    hash_value character varying(1024) NOT NULL,
    integrity character(1) DEFAULT 'I'::bpchar,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-archivefiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-archivefiles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-archivefiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-archivefiles_id_seq" OWNED BY "arc-archivefiles".id;


--
-- Name: arc-archivefilestorages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-archivefilestorages" (
    id integer NOT NULL,
    archivefile_id integer NOT NULL,
    volume_identifier character varying(256) NOT NULL,
    storef character varying(1024) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-archivefilestorages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-archivefilestorages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-archivefilestorages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-archivefilestorages_id_seq" OWNED BY "arc-archivefilestorages".id;


--
-- Name: arc-archiveobjects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-archiveobjects" (
    id integer NOT NULL,
    archival_agency_object_identifier character varying(256) DEFAULT NULL::character varying,
    description_level character varying(128) DEFAULT NULL::character varying,
    name text,
    transferring_agency_object_identifier character varying(256) DEFAULT NULL::character varying,
    archive_id integer,
    contentdescription_id integer,
    accessrestriction_id integer,
    appraisal_id integer,
    descriptionlevel_id integer,
    parent_id integer,
    lft integer,
    rght integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    taille_documents_octets bigint,
    eliminated character varying(1) DEFAULT NULL::character varying,
    transfered character varying(1) DEFAULT NULL::character varying,
    restitued character varying(1) DEFAULT NULL::character varying,
    originating_agency_object_identifier character varying(256)
);


--
-- Name: arc-archiveobjects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-archiveobjects_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-archiveobjects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-archiveobjects_id_seq" OWNED BY "arc-archiveobjects".id;


--
-- Name: arc-archives; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-archives" (
    id integer NOT NULL,
    archival_agency_archive_identifier character varying(256) DEFAULT NULL::character varying,
    archival_agreement character varying(256) DEFAULT NULL::character varying,
    archival_profile character varying(256) DEFAULT NULL::character varying,
    name text,
    service_level character varying(128) DEFAULT NULL::character varying,
    transferring_agency_archive_identifier character varying(256) DEFAULT NULL::character varying,
    accord_id integer,
    profil_id integer,
    archivetransfer_id integer NOT NULL,
    transferring_agency_id integer NOT NULL,
    archival_agency_id integer NOT NULL,
    originating_agency_id integer NOT NULL,
    contentdescription_id integer NOT NULL,
    accessrestriction_id integer,
    appraisal_id integer,
    rangement character varying(1024) DEFAULT NULL::character varying,
    taille_documents_octets bigint,
    eliminated character varying(1) DEFAULT NULL::character varying,
    transfered character varying(1) DEFAULT NULL::character varying,
    volume_identifiers_json character varying(256) NOT NULL,
    fichier_archive_nom character varying(1024) DEFAULT NULL::character varying,
    fichier_archive_storef character varying(1024) DEFAULT NULL::character varying,
    fichier_cyclevie_nom character varying(1024) DEFAULT NULL::character varying,
    fichier_cyclevie_storef character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    integrite_fichiers_statut character varying(1) DEFAULT 'I'::character varying,
    integrite_fichiers_process character varying(1) DEFAULT 'P'::character varying,
    integrite_fichiers_date timestamp without time zone,
    freezed boolean DEFAULT false,
    restitued character varying(1) DEFAULT NULL::character varying,
    description_language_json character varying(1024),
    originating_agency_archive_identifier character varying(256)
);


--
-- Name: arc-archives_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-archives_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-archives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-archives_id_seq" OWNED BY "arc-archives".id;


--
-- Name: arc-contentdescriptions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-contentdescriptions" (
    id integer NOT NULL,
    custodial_history_json text,
    description text,
    file_plan_position character varying(256) DEFAULT NULL::character varying,
    format text,
    latest_date date,
    oldest_date date,
    other_descriptive_data text,
    related_object_reference character varying(256) DEFAULT NULL::character varying,
    size real,
    size_unit_code character varying(128) DEFAULT NULL::character varying,
    other_metadata_json text,
    originating_agency_id integer,
    repository_id integer,
    accessrestriction_id integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    description_level character varying(128),
    descriptionlevel_id integer,
    language_json character varying(1024)
);


--
-- Name: arc-contentdescriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-contentdescriptions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-contentdescriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-contentdescriptions_id_seq" OWNED BY "arc-contentdescriptions".id;


--
-- Name: arc-contentdescriptives; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-contentdescriptives" (
    id integer NOT NULL,
    keyword_content text,
    keyword_reference character varying(256) DEFAULT NULL::character varying,
    keyword_type character varying(128) DEFAULT NULL::character varying,
    contentdescription_id integer,
    accessrestriction_id integer,
    keywordtype_id integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-contentdescriptives_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-contentdescriptives_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-contentdescriptives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-contentdescriptives_id_seq" OWNED BY "arc-contentdescriptives".id;


--
-- Name: arc-documentfiles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-documentfiles" (
    id integer NOT NULL,
    document_id integer NOT NULL,
    document_type character varying(256) NOT NULL,
    document_order integer DEFAULT 0,
    identifier character varying(256) NOT NULL,
    filename character varying(1024) NOT NULL,
    pronom_unique_id character varying(256) DEFAULT NULL::character varying,
    mime_code character varying(256) DEFAULT NULL::character varying,
    size_bytes bigint NOT NULL,
    hash_type character varying(256) NOT NULL,
    hash_value character varying(1024) NOT NULL,
    deleted boolean NOT NULL,
    json_metadata text,
    integrity character(1) DEFAULT 'I'::bpchar,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-documentfiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-documentfiles_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-documentfiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-documentfiles_id_seq" OWNED BY "arc-documentfiles".id;


--
-- Name: arc-documentfilestorages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-documentfilestorages" (
    id integer NOT NULL,
    documentfile_id integer NOT NULL,
    volume_identifier character varying(256) NOT NULL,
    storef character varying(1024) NOT NULL,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL
);


--
-- Name: arc-documentfilestorages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-documentfilestorages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-documentfilestorages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-documentfilestorages_id_seq" OWNED BY "arc-documentfilestorages".id;


--
-- Name: arc-documents; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-documents" (
    id integer NOT NULL,
    attachment bytea,
    attachment_filename character varying(1024) DEFAULT NULL::character varying,
    attachment_format character varying(256) DEFAULT NULL::character varying,
    attachment_mime_code character varying(256) DEFAULT NULL::character varying,
    control boolean DEFAULT false,
    copy boolean DEFAULT false,
    creation timestamp without time zone,
    description text,
    identification character varying(256) DEFAULT NULL::character varying,
    issue timestamp without time zone,
    item_identifier character varying(256) DEFAULT NULL::character varying,
    purpose text,
    receipt timestamp without time zone,
    response timestamp without time zone,
    status character varying(128) DEFAULT NULL::character varying,
    submission timestamp without time zone,
    type character varying(128) DEFAULT NULL::character varying,
    other_metadata_json text,
    model character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    documenttype_id integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone NOT NULL,
    converted character varying(1) DEFAULT NULL::character varying,
    conversion_format character varying(256) DEFAULT NULL::character varying,
    archival_agency_document_identifier character varying(256) DEFAULT NULL::character varying,
    originating_agency_document_identifier character varying(256),
    transferring_agency_document_identifier character varying(256),
    language_json character varying(1024)
);


--
-- Name: arc-documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-documents_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-documents_id_seq" OWNED BY "arc-documents".id;


--
-- Name: arc-organizations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "arc-organizations" (
    id integer NOT NULL,
    identification character varying(256) DEFAULT NULL::character varying,
    nom character varying(255) NOT NULL,
    prive boolean DEFAULT false NOT NULL,
    parent_id integer,
    lft integer,
    rght integer,
    typeentite_id integer NOT NULL,
    date_existence_debut date,
    date_existence_fin date,
    seda02 character varying,
    seda10 character varying,
    active boolean DEFAULT true,
    modifiable boolean DEFAULT true,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL,
    collectivite_id integer NOT NULL
);


--
-- Name: arc-organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "arc-organizations_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: arc-organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "arc-organizations_id_seq" OWNED BY "arc-organizations".id;


--
-- Name: archivedeliveries_archiveobjects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archivedeliveries_archiveobjects (
    id integer NOT NULL,
    archiveobject_id integer NOT NULL,
    archivedelivery_id integer NOT NULL
);


--
-- Name: archivedeliveries_archiveobjects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archivedeliveries_archiveobjects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archivedeliveries_archiveobjects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archivedeliveries_archiveobjects_id_seq OWNED BY archivedeliveries_archiveobjects.id;


--
-- Name: archivedeliveries_archives; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archivedeliveries_archives (
    id integer NOT NULL,
    archive_id integer NOT NULL,
    archivedelivery_id integer NOT NULL
);


--
-- Name: archivedeliveries_archives_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archivedeliveries_archives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archivedeliveries_archives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archivedeliveries_archives_id_seq OWNED BY archivedeliveries_archives.id;


--
-- Name: archivedeliveryrequests_archives; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archivedeliveryrequests_archives (
    id integer NOT NULL,
    archive_id integer NOT NULL,
    archivedeliveryrequest_id integer NOT NULL
);


--
-- Name: archivedeliveryrequests_archives_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archivedeliveryrequests_archives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archivedeliveryrequests_archives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archivedeliveryrequests_archives_id_seq OWNED BY archivedeliveryrequests_archives.id;


--
-- Name: archivedestructionrequests_archives; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archivedestructionrequests_archives (
    id integer NOT NULL,
    archive_id integer NOT NULL,
    archivedestructionrequest_id integer NOT NULL
);


--
-- Name: archivedestructionrequests_archives_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archivedestructionrequests_archives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archivedestructionrequests_archives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archivedestructionrequests_archives_id_seq OWNED BY archivedestructionrequests_archives.id;


--
-- Name: archivedestructionrequests_arcobjs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archivedestructionrequests_arcobjs (
    id integer NOT NULL,
    archiveobject_id integer NOT NULL,
    archivedestructionrequest_id integer NOT NULL
);


--
-- Name: archivedestructionrequests_arcobjs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archivedestructionrequests_arcobjs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archivedestructionrequests_arcobjs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archivedestructionrequests_arcobjs_id_seq OWNED BY archivedestructionrequests_arcobjs.id;


--
-- Name: archiverestitutionrequests_archives; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archiverestitutionrequests_archives (
    id integer NOT NULL,
    archive_id integer NOT NULL,
    archiverestitutionrequest_id integer NOT NULL
);


--
-- Name: archiverestitutionrequests_archives_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archiverestitutionrequests_archives_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archiverestitutionrequests_archives_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archiverestitutionrequests_archives_id_seq OWNED BY archiverestitutionrequests_archives.id;


--
-- Name: archives_archivetransfers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE archives_archivetransfers (
    id integer NOT NULL,
    archive_id integer NOT NULL,
    archivetransfer_id integer NOT NULL
);


--
-- Name: archives_archivetransfers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE archives_archivetransfers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: archives_archivetransfers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE archives_archivetransfers_id_seq OWNED BY archives_archivetransfers.id;


--
-- Name: aros; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE aros (
    id bigint NOT NULL,
    parent_id integer,
    model character varying(255) DEFAULT NULL::character varying,
    foreign_key bigint,
    alias character varying(255) DEFAULT NULL::character varying,
    lft integer,
    rght integer
);


--
-- Name: aros_acos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE aros_acos (
    id bigint NOT NULL,
    aro_id bigint NOT NULL,
    aco_id bigint NOT NULL,
    _create character(2) DEFAULT '0'::bpchar NOT NULL,
    _read character(2) DEFAULT '0'::bpchar NOT NULL,
    _update character(2) DEFAULT '0'::bpchar NOT NULL,
    _delete character(2) DEFAULT '0'::bpchar NOT NULL
);


--
-- Name: aros_acos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE aros_acos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: aros_acos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE aros_acos_id_seq OWNED BY aros_acos.id;


--
-- Name: aros_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE aros_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: aros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE aros_id_seq OWNED BY aros.id;


--
-- Name: connecteurs_organizations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE connecteurs_organizations (
    id integer NOT NULL,
    connecteur_id integer NOT NULL,
    organization_id integer NOT NULL
);


--
-- Name: connecteurs_organizations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE connecteurs_organizations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: connecteurs_organizations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE connecteurs_organizations_id_seq OWNED BY connecteurs_organizations.id;


--
-- Name: io-acknowledgements; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-acknowledgements" (
    id integer NOT NULL,
    sens character varying(1),
    version_seda character varying(128),
    fichier_message character varying(1024),
    foreign_model character varying(256),
    foreign_key integer,
    comment text,
    date timestamp without time zone,
    acknowledgement_identifier character varying(256),
    acknowledgement_identifier_jsattr character varying(1024),
    message_received_identifier character varying(256),
    receiver_id integer,
    sender_id integer,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer
);


--
-- Name: io-acknowledgements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-acknowledgements_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-acknowledgements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-acknowledgements_id_seq" OWNED BY "io-acknowledgements".id;


--
-- Name: io-alertes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-alertes" (
    id integer NOT NULL,
    model character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    code character varying(255) NOT NULL,
    commentaire text,
    note character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL
);


--
-- Name: io-alertes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-alertes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-alertes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-alertes_id_seq" OWNED BY "io-alertes".id;


--
-- Name: io-archivedeliveries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivedeliveries" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    delivery_authorization_identifier character varying(256) DEFAULT NULL::character varying,
    delivery_identifier character varying(256) DEFAULT NULL::character varying,
    delivery_request_identifier character varying(256) DEFAULT NULL::character varying,
    unit_identifier character varying(256) DEFAULT NULL::character varying,
    requester_id integer,
    archival_agency_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    fichier_documents character varying(1024) DEFAULT NULL::character varying,
    statut integer,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    type_demandeur character varying(1) DEFAULT NULL::character varying,
    archivedeliveryrequest_id integer,
    comfiles_nb integer DEFAULT 0,
    comfiles_size_bytes bigint DEFAULT 0,
    version_seda character varying(128)
);


--
-- Name: io-archivedeliveries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivedeliveries_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivedeliveries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivedeliveries_id_seq" OWNED BY "io-archivedeliveries".id;


--
-- Name: io-archivedeliveryrequests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivedeliveryrequests" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    delivery_request_identifier character varying(256) DEFAULT NULL::character varying,
    derogation boolean DEFAULT false,
    unit_identifier character varying(256) DEFAULT NULL::character varying,
    access_requester_id integer,
    archival_agency_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    statut integer,
    decision character varying(1) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    version_seda character varying(128)
);


--
-- Name: io-archivedeliveryrequests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivedeliveryrequests_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivedeliveryrequests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivedeliveryrequests_id_seq" OWNED BY "io-archivedeliveryrequests".id;


--
-- Name: io-archivedestructionnotifications; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivedestructionnotifications" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    destruct_acceptance_identifier character varying(256) DEFAULT NULL::character varying,
    destruct_date timestamp without time zone,
    destruct_notif_identifier character varying(256) DEFAULT NULL::character varying,
    destruct_request_identifier character varying(256) DEFAULT NULL::character varying,
    unit_identifier character varying(256) DEFAULT NULL::character varying,
    originating_agency_id integer,
    archival_agency_id integer,
    archivedestructionrequest_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    envoi_date timestamp without time zone,
    envoi_destinataire character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    version_seda character varying(128)
);


--
-- Name: io-archivedestructionnotifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivedestructionnotifications_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivedestructionnotifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivedestructionnotifications_id_seq" OWNED BY "io-archivedestructionnotifications".id;


--
-- Name: io-archivedestructionrequestreplies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivedestructionrequestreplies" (
    id integer NOT NULL,
    sens character varying(1),
    version_seda character varying(128),
    comment text,
    date timestamp without time zone,
    des_req_identifier character varying(256) DEFAULT NULL::character varying,
    des_req_rep_identifier character varying(256) DEFAULT NULL::character varying,
    reply_code character varying(128) DEFAULT NULL::character varying,
    unit_identifiers_json text,
    archival_agency_id integer,
    originating_agency_id integer,
    archivedestructionrequest_id integer NOT NULL,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer
);


--
-- Name: io-archivedestructionrequestreplies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivedestructionrequestreplies_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivedestructionrequestreplies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivedestructionrequestreplies_id_seq" OWNED BY "io-archivedestructionrequestreplies".id;


--
-- Name: io-archivedestructionrequests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivedestructionrequests" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    destruction_request_identifier character varying(256) DEFAULT NULL::character varying,
    unit_identifier character varying(256) DEFAULT NULL::character varying,
    originating_agency_id integer,
    archival_agency_id integer,
    control_authority_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    statut integer,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    fichier_message_pdf character varying(1024) DEFAULT NULL::character varying,
    fichier_message_signe character varying(1024) DEFAULT NULL::character varying,
    fichier_message_pdf_signe character varying(1024) DEFAULT NULL::character varying,
    decision character varying(1) DEFAULT NULL::character varying,
    signature_message_pdf character varying(1024) DEFAULT NULL::character varying,
    desfiles_nb integer DEFAULT 0,
    desfiles_size_bytes bigint DEFAULT 0,
    version_seda character varying(128)
);


--
-- Name: io-archivedestructionrequests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivedestructionrequests_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivedestructionrequests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivedestructionrequests_id_seq" OWNED BY "io-archivedestructionrequests".id;


--
-- Name: io-archiverestitutionrequestreplies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archiverestitutionrequestreplies" (
    id integer NOT NULL,
    sens character varying(1),
    version_seda character varying(128),
    comment text,
    date timestamp without time zone,
    reply_code character varying(128) DEFAULT NULL::character varying,
    res_req_identifier character varying(256) DEFAULT NULL::character varying,
    res_req_rep_identifier character varying(256) DEFAULT NULL::character varying,
    unit_identifiers_json text,
    archival_agency_id integer,
    originating_agency_id integer,
    archiverestitutionrequest_id integer NOT NULL,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer
);


--
-- Name: io-archiverestitutionrequestreplies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archiverestitutionrequestreplies_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archiverestitutionrequestreplies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archiverestitutionrequestreplies_id_seq" OWNED BY "io-archiverestitutionrequestreplies".id;


--
-- Name: io-archiverestitutionrequests; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archiverestitutionrequests" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    restitution_request_identifier character varying(256) DEFAULT NULL::character varying,
    unit_identifier character varying(256) DEFAULT NULL::character varying,
    originating_agency_id integer,
    archival_agency_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    statut integer,
    decision character varying(1) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    version_seda character varying(128)
);


--
-- Name: io-archiverestitutionrequests_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archiverestitutionrequests_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archiverestitutionrequests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archiverestitutionrequests_id_seq" OWNED BY "io-archiverestitutionrequests".id;


--
-- Name: io-archiverestitutions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archiverestitutions" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    restitution_identifier character varying(256) DEFAULT NULL::character varying,
    restitution_request_reply_identifier character varying(256) DEFAULT NULL::character varying,
    originating_agency_id integer,
    archival_agency_id integer,
    archiverestitutionrequestreply_id integer,
    archiverestitutionrequest_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    fichier_documents character varying(1024) DEFAULT NULL::character varying,
    envoi_date timestamp without time zone,
    envoi_destinataire character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    statut integer NOT NULL,
    version_seda character varying(128)
);


--
-- Name: io-archiverestitutions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archiverestitutions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archiverestitutions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archiverestitutions_id_seq" OWNED BY "io-archiverestitutions".id;


--
-- Name: io-archivetransferacceptances; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivetransferacceptances" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    reply_code character varying(128) DEFAULT NULL::character varying,
    transfer_acceptance_identifier character varying(256) DEFAULT NULL::character varying,
    transfer_identifier character varying(256) DEFAULT NULL::character varying,
    archivetransfer_id integer,
    transferring_agency_id integer,
    archival_agency_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    version_seda character varying(128),
    transfer_acceptance_ident_jsattr character varying(1024)
);


--
-- Name: io-archivetransferacceptances_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivetransferacceptances_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivetransferacceptances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivetransferacceptances_id_seq" OWNED BY "io-archivetransferacceptances".id;


--
-- Name: io-archivetransferreplies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivetransferreplies" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    reply_code character varying(128) DEFAULT NULL::character varying,
    transfer_identifier character varying(256) DEFAULT NULL::character varying,
    transfer_reply_identifier character varying(256) DEFAULT NULL::character varying,
    archivetransfer_id integer,
    transferring_agency_id integer,
    archival_agency_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    version_seda character varying(128),
    grant_date timestamp without time zone
);


--
-- Name: io-archivetransferreplies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivetransferreplies_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivetransferreplies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivetransferreplies_id_seq" OWNED BY "io-archivetransferreplies".id;


--
-- Name: io-archivetransfers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-archivetransfers" (
    id integer NOT NULL,
    sens character varying(1),
    comment text,
    date timestamp without time zone,
    related_transfer_reference character varying(256) DEFAULT NULL::character varying,
    transfer_identifier character varying(256) DEFAULT NULL::character varying,
    transfer_request_reply_identifier character varying(256) DEFAULT NULL::character varying,
    transferring_agency_id integer,
    archival_agency_id integer,
    originating_agency_id integer,
    accord_id integer,
    contrat_id integer,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    repertoire_documents character varying(1024) DEFAULT NULL::character varying,
    taille_documents_octets bigint,
    statut integer,
    conforme boolean,
    decision integer,
    created timestamp without time zone,
    created_user_id integer,
    modified timestamp without time zone,
    modified_user_id integer,
    origine integer,
    fichier_documents character varying(1024) DEFAULT NULL::character varying,
    alerte boolean DEFAULT false,
    infected boolean DEFAULT false,
    fichier_message_orig character varying(1024) DEFAULT NULL::character varying,
    repondre boolean DEFAULT false,
    repondre_via character varying(255) DEFAULT NULL::character varying,
    repondre_destinataire character varying(255) DEFAULT NULL::character varying,
    version_seda character varying(128)
);


--
-- Name: io-archivetransfers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-archivetransfers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-archivetransfers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-archivetransfers_id_seq" OWNED BY "io-archivetransfers".id;


--
-- Name: io-avscans; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-avscans" (
    id integer NOT NULL,
    model character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    antivirus_type character varying(255) NOT NULL,
    date timestamp without time zone NOT NULL,
    scan_summary text,
    scan_details text
);


--
-- Name: io-avscans_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-avscans_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-avscans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-avscans_id_seq" OWNED BY "io-avscans".id;


--
-- Name: io-erreurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-erreurs" (
    id integer NOT NULL,
    model character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    code character varying(255) NOT NULL,
    commentaire text,
    note character varying(255) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL
);


--
-- Name: io-erreurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-erreurs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-erreurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-erreurs_id_seq" OWNED BY "io-erreurs".id;


--
-- Name: io-messages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-messages" (
    id integer NOT NULL,
    collectivite_id integer NOT NULL,
    service_id integer NOT NULL,
    comment text,
    date timestamp without time zone,
    typemessage_id integer NOT NULL,
    fichier_message character varying(1024) DEFAULT NULL::character varying,
    expediteur_organization_id integer,
    destinataire_organization_id integer,
    sens character varying(1) DEFAULT NULL::character varying,
    statut integer NOT NULL,
    message_id integer,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone,
    modified_user_id integer
);


--
-- Name: io-messages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-messages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-messages_id_seq" OWNED BY "io-messages".id;


--
-- Name: io-piecejointes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "io-piecejointes" (
    id integer NOT NULL,
    archivetransfer_id integer NOT NULL,
    filename character varying(1024) NOT NULL,
    taille_octets bigint NOT NULL,
    bdx_fmt character varying(256) DEFAULT NULL::character varying,
    bdx_mimecode character varying(256) DEFAULT NULL::character varying,
    val_info character varying(255) DEFAULT NULL::character varying,
    val_fmt character varying(256) DEFAULT NULL::character varying,
    val_mimecode character varying(256) DEFAULT NULL::character varying,
    val_format character varying(256) DEFAULT NULL::character varying,
    val_version character varying(256) DEFAULT NULL::character varying,
    val_bien_forme boolean DEFAULT false,
    val_valide boolean DEFAULT false,
    val_archivable boolean DEFAULT false,
    val_date timestamp without time zone,
    infected boolean DEFAULT false,
    virusname character varying(256) DEFAULT NULL::character varying,
    fmt_puid character varying(255) DEFAULT NULL::character varying,
    fmt_formatname character varying(255) DEFAULT NULL::character varying,
    fmt_version character varying(255) DEFAULT NULL::character varying,
    fmt_signaturename character varying(255) DEFAULT NULL::character varying,
    fmt_mimetype character varying(255) DEFAULT NULL::character varying,
    fmt_info character varying(255) DEFAULT NULL::character varying,
    fmt_date timestamp without time zone
);


--
-- Name: io-piecejointes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "io-piecejointes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: io-piecejointes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "io-piecejointes_id_seq" OWNED BY "io-piecejointes".id;


--
-- Name: keywords_keywords; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE keywords_keywords (
    id integer NOT NULL,
    keyword_id integer NOT NULL,
    related_id integer NOT NULL
);


--
-- Name: keywords_keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE keywords_keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keywords_keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE keywords_keywords_id_seq OWNED BY keywords_keywords.id;


--
-- Name: oaipmh-resumptiontoken_records; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "oaipmh-resumptiontoken_records" (
    id integer NOT NULL,
    resumptiontoken_id integer NOT NULL,
    archive_id integer NOT NULL,
    identifier character varying,
    datestamp character varying,
    deleted boolean DEFAULT false
);


--
-- Name: oaipmh-resumptiontoken_records_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "oaipmh-resumptiontoken_records_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oaipmh-resumptiontoken_records_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "oaipmh-resumptiontoken_records_id_seq" OWNED BY "oaipmh-resumptiontoken_records".id;


--
-- Name: oaipmh-resumptiontokens; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "oaipmh-resumptiontokens" (
    id integer NOT NULL,
    resumption_token character varying NOT NULL,
    verb character varying NOT NULL,
    metadataprefix character varying NOT NULL,
    expiration_date character varying,
    complete_list_size integer,
    cursor integer DEFAULT 0
);


--
-- Name: oaipmh-resumptiontokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "oaipmh-resumptiontokens_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oaipmh-resumptiontokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "oaipmh-resumptiontokens_id_seq" OWNED BY "oaipmh-resumptiontokens".id;


--
-- Name: organizations_typeacteurs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE organizations_typeacteurs (
    id integer NOT NULL,
    organization_id integer NOT NULL,
    typeacteur_id integer NOT NULL
);


--
-- Name: organizations_typeacteurs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE organizations_typeacteurs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: organizations_typeacteurs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE organizations_typeacteurs_id_seq OWNED BY organizations_typeacteurs.id;


--
-- Name: ref-accessrestrictioncodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-accessrestrictioncodes" (
    id integer NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(250) NOT NULL,
    description text NOT NULL
);


--
-- Name: ref-accessrestrictioncodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-accessrestrictioncodes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-accessrestrictioncodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-accessrestrictioncodes_id_seq" OWNED BY "ref-accessrestrictioncodes".id;


--
-- Name: ref-appraisalcodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-appraisalcodes" (
    id integer NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(250) NOT NULL
);


--
-- Name: ref-appraisalcodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-appraisalcodes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-appraisalcodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-appraisalcodes_id_seq" OWNED BY "ref-appraisalcodes".id;


--
-- Name: ref-descriptionlevels; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-descriptionlevels" (
    id integer NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(250) NOT NULL,
    description text NOT NULL
);


--
-- Name: ref-descriptionlevels_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-descriptionlevels_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-descriptionlevels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-descriptionlevels_id_seq" OWNED BY "ref-descriptionlevels".id;


--
-- Name: ref-documenttypes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-documenttypes" (
    id integer NOT NULL,
    code character varying(10) NOT NULL,
    name character varying(250) NOT NULL,
    description text NOT NULL
);


--
-- Name: ref-documenttypes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-documenttypes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-documenttypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-documenttypes_id_seq" OWNED BY "ref-documenttypes".id;


--
-- Name: ref-filetypes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-filetypes" (
    id integer NOT NULL,
    code character varying(20) NOT NULL,
    name character varying(250) NOT NULL,
    description text NOT NULL
);


--
-- Name: ref-filetypes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-filetypes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-filetypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-filetypes_id_seq" OWNED BY "ref-filetypes".id;


--
-- Name: ref-keywordtypes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-keywordtypes" (
    id integer NOT NULL,
    code character varying(10) NOT NULL,
    documentation character varying(250) NOT NULL
);


--
-- Name: ref-keywordtypes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-keywordtypes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-keywordtypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-keywordtypes_id_seq" OWNED BY "ref-keywordtypes".id;


--
-- Name: ref-languagecodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-languagecodes" (
    id integer NOT NULL,
    code character(2) NOT NULL,
    documentation character varying(250) NOT NULL
);


--
-- Name: ref-languagecodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-languagecodes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-languagecodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-languagecodes_id_seq" OWNED BY "ref-languagecodes".id;


--
-- Name: ref-mimecodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-mimecodes" (
    id integer NOT NULL,
    code character varying(128) NOT NULL,
    name character varying(250) NOT NULL,
    definition character varying(250) NOT NULL
);


--
-- Name: ref-mimecodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-mimecodes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-mimecodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-mimecodes_id_seq" OWNED BY "ref-mimecodes".id;


--
-- Name: ref-replycodes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "ref-replycodes" (
    id integer NOT NULL,
    code character(3) NOT NULL,
    documentation character varying(250) NOT NULL
);


--
-- Name: ref-replycodes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "ref-replycodes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ref-replycodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "ref-replycodes_id_seq" OWNED BY "ref-replycodes".id;


--
-- Name: services_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE services_users (
    id integer NOT NULL,
    service_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL
);


--
-- Name: services_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE services_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: services_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE services_users_id_seq OWNED BY services_users.id;


--
-- Name: versions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE versions (
    version character varying(255) NOT NULL,
    date timestamp without time zone NOT NULL
);


--
-- Name: wkf-circuits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-circuits" (
    id integer NOT NULL,
    nom character varying(250) NOT NULL,
    description text,
    actif boolean DEFAULT true NOT NULL,
    defaut boolean DEFAULT false NOT NULL,
    created_user_id integer NOT NULL,
    modified_user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone,
    type character varying(255) NOT NULL
);


--
-- Name: wkf-circuits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-circuits_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-circuits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-circuits_id_seq" OWNED BY "wkf-circuits".id;


--
-- Name: wkf-compositions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-compositions" (
    id integer NOT NULL,
    type_validation character varying(1) DEFAULT NULL::character varying,
    created timestamp without time zone NOT NULL,
    created_user_id integer NOT NULL,
    modified timestamp without time zone NOT NULL,
    modified_user_id integer NOT NULL,
    type character varying(255) NOT NULL,
    mail_contact character varying(255) DEFAULT NULL::character varying,
    mail character varying(255) DEFAULT NULL::character varying,
    model character varying(255) NOT NULL,
    foreign_key integer NOT NULL,
    traiteur_model character varying(255) DEFAULT NULL::character varying,
    traiteur_foreign_key integer
);


--
-- Name: wkf-compositions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-compositions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-compositions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-compositions_id_seq" OWNED BY "wkf-compositions".id;


--
-- Name: wkf-etapes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-etapes" (
    id integer NOT NULL,
    circuit_id integer NOT NULL,
    nom character varying(250) NOT NULL,
    description text,
    type integer NOT NULL,
    ordre integer NOT NULL,
    created_user_id integer NOT NULL,
    modified_user_id integer,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: wkf-etapes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-etapes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-etapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-etapes_id_seq" OWNED BY "wkf-etapes".id;


--
-- Name: wkf-signatures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-signatures" (
    id integer NOT NULL,
    type_signature character varying(100) NOT NULL,
    signature text NOT NULL,
    created timestamp without time zone NOT NULL
);


--
-- Name: wkf-signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-signatures_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-signatures_id_seq" OWNED BY "wkf-signatures".id;


--
-- Name: wkf-traitementpjs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-traitementpjs" (
    id integer NOT NULL,
    traitement_id integer NOT NULL,
    nom character varying(255) DEFAULT NULL::character varying,
    filename character varying(1024) DEFAULT NULL::character varying,
    mime_code character varying(256) DEFAULT NULL::character varying,
    size_bytes bigint DEFAULT 0,
    contents bytea,
    created timestamp without time zone NOT NULL
);


--
-- Name: wkf-traitementpjs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-traitementpjs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-traitementpjs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-traitementpjs_id_seq" OWNED BY "wkf-traitementpjs".id;


--
-- Name: wkf-traitements; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-traitements" (
    id integer NOT NULL,
    circuit_id integer NOT NULL,
    foreign_key integer NOT NULL,
    numero_traitement integer DEFAULT (1)::numeric NOT NULL,
    created timestamp without time zone NOT NULL,
    model character varying(255) NOT NULL,
    commentaire character varying(500),
    statut integer NOT NULL,
    resultat integer DEFAULT 0
);


--
-- Name: wkf-traitements_compositions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-traitements_compositions" (
    id integer NOT NULL,
    traitementetape_id integer NOT NULL,
    composition_id integer,
    traiteur_type character varying(255) NOT NULL,
    traiteur_model character varying(255) DEFAULT NULL::character varying,
    traiteur_foreign_key integer,
    action character varying(2) DEFAULT NULL::character varying,
    treated boolean DEFAULT false,
    traiteur_notified boolean DEFAULT false,
    traiteur_date_notification timestamp without time zone,
    type_validation character varying(1) DEFAULT NULL::character varying,
    mail_contact character varying(255) DEFAULT NULL::character varying,
    mail character varying(255) DEFAULT NULL::character varying,
    traite_par character varying(255) DEFAULT NULL::character varying,
    commentaire character varying(500) DEFAULT NULL::character varying,
    signature_id integer,
    created timestamp without time zone NOT NULL,
    modified timestamp without time zone
);


--
-- Name: wkf-traitements_compositions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-traitements_compositions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-traitements_compositions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-traitements_compositions_id_seq" OWNED BY "wkf-traitements_compositions".id;


--
-- Name: wkf-traitements_etapes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE "wkf-traitements_etapes" (
    id integer NOT NULL,
    traitement_id integer NOT NULL,
    etape_id integer,
    nom character varying(250) NOT NULL,
    type integer NOT NULL,
    ordre integer NOT NULL,
    treated boolean NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone
);


--
-- Name: wkf-traitements_etapes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-traitements_etapes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-traitements_etapes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-traitements_etapes_id_seq" OWNED BY "wkf-traitements_etapes".id;


--
-- Name: wkf-traitements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE "wkf-traitements_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wkf-traitements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE "wkf-traitements_id_seq" OWNED BY "wkf-traitements".id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accords_filetypes ALTER COLUMN id SET DEFAULT nextval('accords_filetypes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY accords_versants ALTER COLUMN id SET DEFAULT nextval('accords_versants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY acos ALTER COLUMN id SET DEFAULT nextval('acos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-accords" ALTER COLUMN id SET DEFAULT nextval('"adm-accords_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-affichages" ALTER COLUMN id SET DEFAULT nextval('"adm-affichages_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-altkeywords" ALTER COLUMN id SET DEFAULT nextval('"adm-altkeywords_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-collectivites" ALTER COLUMN id SET DEFAULT nextval('"adm-collectivites_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-compteurs" ALTER COLUMN id SET DEFAULT nextval('"adm-compteurs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-connecteurs" ALTER COLUMN id SET DEFAULT nextval('"adm-connecteurs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-contrats" ALTER COLUMN id SET DEFAULT nextval('"adm-contrats_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-crons" ALTER COLUMN id SET DEFAULT nextval('"adm-crons_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-fichiers" ALTER COLUMN id SET DEFAULT nextval('"adm-fichiers_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-journaux" ALTER COLUMN id SET DEFAULT nextval('"adm-journaux_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-keywordlists" ALTER COLUMN id SET DEFAULT nextval('"adm-keywordlists_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-keywords" ALTER COLUMN id SET DEFAULT nextval('"adm-keywords_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-parapheurs" ALTER COLUMN id SET DEFAULT nextval('"adm-parapheurs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-profils" ALTER COLUMN id SET DEFAULT nextval('"adm-profils_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-referentiels" ALTER COLUMN id SET DEFAULT nextval('"adm-referentiels_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-refexterieurs" ALTER COLUMN id SET DEFAULT nextval('"adm-refexterieurs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-roles" ALTER COLUMN id SET DEFAULT nextval('"adm-roles_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-sequences" ALTER COLUMN id SET DEFAULT nextval('"adm-sequences_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-services" ALTER COLUMN id SET DEFAULT nextval('"adm-services_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-typeacteurs" ALTER COLUMN id SET DEFAULT nextval('"adm-typeacteurs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-typeentites" ALTER COLUMN id SET DEFAULT nextval('"adm-typeentites_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-typemessages" ALTER COLUMN id SET DEFAULT nextval('"adm-typemessages_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-users" ALTER COLUMN id SET DEFAULT nextval('"adm-users_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "adm-verrous" ALTER COLUMN id SET DEFAULT nextval('"adm-verrous_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-accessrestrictions" ALTER COLUMN id SET DEFAULT nextval('"arc-accessrestrictions_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-appraisals" ALTER COLUMN id SET DEFAULT nextval('"arc-appraisals_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-archivefiles" ALTER COLUMN id SET DEFAULT nextval('"arc-archivefiles_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-archivefilestorages" ALTER COLUMN id SET DEFAULT nextval('"arc-archivefilestorages_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-archiveobjects" ALTER COLUMN id SET DEFAULT nextval('"arc-archiveobjects_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-archives" ALTER COLUMN id SET DEFAULT nextval('"arc-archives_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-contentdescriptions" ALTER COLUMN id SET DEFAULT nextval('"arc-contentdescriptions_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-contentdescriptives" ALTER COLUMN id SET DEFAULT nextval('"arc-contentdescriptives_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-documentfiles" ALTER COLUMN id SET DEFAULT nextval('"arc-documentfiles_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-documentfilestorages" ALTER COLUMN id SET DEFAULT nextval('"arc-documentfilestorages_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-documents" ALTER COLUMN id SET DEFAULT nextval('"arc-documents_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "arc-organizations" ALTER COLUMN id SET DEFAULT nextval('"arc-organizations_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archivedeliveries_archiveobjects ALTER COLUMN id SET DEFAULT nextval('archivedeliveries_archiveobjects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archivedeliveries_archives ALTER COLUMN id SET DEFAULT nextval('archivedeliveries_archives_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archivedeliveryrequests_archives ALTER COLUMN id SET DEFAULT nextval('archivedeliveryrequests_archives_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archivedestructionrequests_archives ALTER COLUMN id SET DEFAULT nextval('archivedestructionrequests_archives_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archivedestructionrequests_arcobjs ALTER COLUMN id SET DEFAULT nextval('archivedestructionrequests_arcobjs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archiverestitutionrequests_archives ALTER COLUMN id SET DEFAULT nextval('archiverestitutionrequests_archives_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY archives_archivetransfers ALTER COLUMN id SET DEFAULT nextval('archives_archivetransfers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY aros ALTER COLUMN id SET DEFAULT nextval('aros_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY aros_acos ALTER COLUMN id SET DEFAULT nextval('aros_acos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY connecteurs_organizations ALTER COLUMN id SET DEFAULT nextval('connecteurs_organizations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-acknowledgements" ALTER COLUMN id SET DEFAULT nextval('"io-acknowledgements_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-alertes" ALTER COLUMN id SET DEFAULT nextval('"io-alertes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivedeliveries" ALTER COLUMN id SET DEFAULT nextval('"io-archivedeliveries_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivedeliveryrequests" ALTER COLUMN id SET DEFAULT nextval('"io-archivedeliveryrequests_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivedestructionnotifications" ALTER COLUMN id SET DEFAULT nextval('"io-archivedestructionnotifications_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivedestructionrequestreplies" ALTER COLUMN id SET DEFAULT nextval('"io-archivedestructionrequestreplies_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivedestructionrequests" ALTER COLUMN id SET DEFAULT nextval('"io-archivedestructionrequests_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archiverestitutionrequestreplies" ALTER COLUMN id SET DEFAULT nextval('"io-archiverestitutionrequestreplies_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archiverestitutionrequests" ALTER COLUMN id SET DEFAULT nextval('"io-archiverestitutionrequests_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archiverestitutions" ALTER COLUMN id SET DEFAULT nextval('"io-archiverestitutions_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivetransferacceptances" ALTER COLUMN id SET DEFAULT nextval('"io-archivetransferacceptances_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivetransferreplies" ALTER COLUMN id SET DEFAULT nextval('"io-archivetransferreplies_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-archivetransfers" ALTER COLUMN id SET DEFAULT nextval('"io-archivetransfers_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-avscans" ALTER COLUMN id SET DEFAULT nextval('"io-avscans_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-erreurs" ALTER COLUMN id SET DEFAULT nextval('"io-erreurs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-messages" ALTER COLUMN id SET DEFAULT nextval('"io-messages_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "io-piecejointes" ALTER COLUMN id SET DEFAULT nextval('"io-piecejointes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY keywords_keywords ALTER COLUMN id SET DEFAULT nextval('keywords_keywords_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "oaipmh-resumptiontoken_records" ALTER COLUMN id SET DEFAULT nextval('"oaipmh-resumptiontoken_records_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "oaipmh-resumptiontokens" ALTER COLUMN id SET DEFAULT nextval('"oaipmh-resumptiontokens_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY organizations_typeacteurs ALTER COLUMN id SET DEFAULT nextval('organizations_typeacteurs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-accessrestrictioncodes" ALTER COLUMN id SET DEFAULT nextval('"ref-accessrestrictioncodes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-appraisalcodes" ALTER COLUMN id SET DEFAULT nextval('"ref-appraisalcodes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-descriptionlevels" ALTER COLUMN id SET DEFAULT nextval('"ref-descriptionlevels_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-documenttypes" ALTER COLUMN id SET DEFAULT nextval('"ref-documenttypes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-filetypes" ALTER COLUMN id SET DEFAULT nextval('"ref-filetypes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-keywordtypes" ALTER COLUMN id SET DEFAULT nextval('"ref-keywordtypes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-languagecodes" ALTER COLUMN id SET DEFAULT nextval('"ref-languagecodes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-mimecodes" ALTER COLUMN id SET DEFAULT nextval('"ref-mimecodes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "ref-replycodes" ALTER COLUMN id SET DEFAULT nextval('"ref-replycodes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY services_users ALTER COLUMN id SET DEFAULT nextval('services_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-circuits" ALTER COLUMN id SET DEFAULT nextval('"wkf-circuits_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-compositions" ALTER COLUMN id SET DEFAULT nextval('"wkf-compositions_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-etapes" ALTER COLUMN id SET DEFAULT nextval('"wkf-etapes_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-signatures" ALTER COLUMN id SET DEFAULT nextval('"wkf-signatures_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-traitementpjs" ALTER COLUMN id SET DEFAULT nextval('"wkf-traitementpjs_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-traitements" ALTER COLUMN id SET DEFAULT nextval('"wkf-traitements_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-traitements_compositions" ALTER COLUMN id SET DEFAULT nextval('"wkf-traitements_compositions_id_seq"'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY "wkf-traitements_etapes" ALTER COLUMN id SET DEFAULT nextval('"wkf-traitements_etapes_id_seq"'::regclass);


--
-- Data for Name: accords_filetypes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY accords_filetypes (id, accord_id, filetype_id) FROM stdin;
\.


--
-- Name: accords_filetypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('accords_filetypes_id_seq', 1, false);


--
-- Data for Name: accords_versants; Type: TABLE DATA; Schema: public; Owner: -
--

COPY accords_versants (id, accord_id, organization_id) FROM stdin;
\.


--
-- Name: accords_versants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('accords_versants_id_seq', 1, false);


--
-- Data for Name: acos; Type: TABLE DATA; Schema: public; Owner: -
--

COPY acos (id, parent_id, model, foreign_key, alias, lft, rght) FROM stdin;
1	\N	\N	\N	Users:accueil	1	2
3	2	\N	\N	Archivetransfers:versementFormate	4	5
4	2	\N	\N	Archivetransfers:sortants_index	6	7
5	2	\N	\N	Archivetransfers:nonconformes	8	9
6	2	\N	\N	Archivetransfers:atraiter	10	11
7	2	\N	\N	Archivetransfers:encourstraitement	12	13
2	\N	\N	\N	Pages:versement	3	16
8	2	\N	\N	Archivetransfers:index	14	15
10	9	\N	\N	Archives:index	18	19
11	9	\N	\N	Archives:navniveau	20	21
12	9	\N	\N	Archives:chercher	22	23
13	9	\N	\N	Archivetransfers:issusarchives_index	24	25
9	\N	\N	\N	Pages:archive	17	28
14	9	\N	\N	Archives:operation_masse	26	27
16	15	\N	\N	Archivedeliveryrequests:index	30	31
17	15	\N	\N	Archivedeliveryrequests:atraiter	32	33
15	\N	\N	\N	Pages:communication	29	36
18	15	\N	\N	Archivedeliveries:index	34	35
20	19	\N	\N	Archivedestructionrequests:index	38	39
19	\N	\N	\N	Pages:elimination	37	42
21	19	\N	\N	Archivedestructionrequests:atraiter	40	41
23	22	\N	\N	Archiverestitutionrequests:index	44	45
24	22	\N	\N	Archiverestitutionrequests:atraiter	46	47
25	22	\N	\N	Archiverestitutions:index	48	49
22	\N	\N	\N	Pages:restitution	43	52
26	22	\N	\N	Archiverestitutions:atraiter	50	51
28	27	\N	\N	Collectivites:index	54	55
29	27	\N	\N	Services:index	56	57
30	27	\N	\N	Roles:index	58	59
31	27	\N	\N	Users:index	60	61
27	\N	\N	\N	Pages:administration	53	64
32	27	\N	\N	Circuits:index	62	63
34	33	\N	\N	Organizations:index	66	67
35	33	\N	\N	Profils:index	68	69
36	33	\N	\N	Accords:index	70	71
33	\N	\N	\N	Pages:accord	65	74
37	33	\N	\N	Referentiels:index	72	73
39	38	\N	\N	Connecteurs:index	76	77
40	38	\N	\N	Refexterieurs:index	78	79
41	38	\N	\N	Keywordlists:index	80	81
42	38	\N	\N	Compteurs:index	82	83
43	38	\N	\N	Journaux:index	84	85
44	38	\N	\N	Crons:index	86	87
45	38	\N	\N	Volumes:index	88	89
46	38	\N	\N	Parapheurs:index	90	91
38	\N	\N	\N	Pages:technique	75	94
47	38	\N	\N	Statistiques:indicateurs	92	93
48	\N	\N	\N	Module:Collectivites	95	98
49	48	\N	\N	Collectivites:accesAToutes	96	97
51	50	\N	\N	Techniques:webServicesEchanges	100	101
50	\N	\N	\N	Module:Techniques	99	104
52	50	\N	\N	Techniques:webServicesOAIPMH	102	103
54	53	\N	\N	Archives:geler	106	107
55	53	\N	\N	Archives:destructionArchivesRestituees	108	109
56	53	\N	\N	Archives:conversionFormat	110	111
53	\N	\N	\N	Module:Archives	105	114
58	53	\N	\N	Archives:edit	112	113
\.


--
-- Name: acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('acos_id_seq', 59, false);


--
-- Data for Name: adm-accords; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-accords" (id, identifiant, nom, description, archival_agency_id, profil_id, circuit_id, plan_rangement, date_debut, date_fin, periodicite, unite_periode, conversion, volume_max_unitaire, unite_vol_max_unitaire, volume_max_total, unite_vol_max_total, empreinte, doc_signe_verif, actif, evt_aucun_versement, evt_depasse_nb_versement, evt_depasse_vol_unitaire, evt_depasse_vol_total, evt_autres_erreurs, communication_service_id, communication_user_id, communication_email, created, created_user_id, modified, modified_user_id, signature, pj_invalide_alerte, volume_identifier, validation_automatique, defaut, seda02, seda10, tout_producteur) FROM stdin;
\.


--
-- Name: adm-accords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-accords_id_seq"', 1, false);


--
-- Data for Name: adm-affichages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-affichages" (id, message, nblignes) FROM stdin;
2		\N
1		\N
\.


--
-- Name: adm-affichages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-affichages_id_seq"', 3, false);


--
-- Data for Name: adm-altkeywords; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-altkeywords" (id, keyword_id, alt_label, created, created_user_id, modified, modified_user_id) FROM stdin;
1	15	distribution	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
2	16	structure industrielle	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
3	57	religion	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
4	61	coopération décentralisée	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
5	64	grange	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
6	64	bergerie	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
7	64	ferme	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
8	64	étable	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
9	66	chambre d'hôte	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
10	71	bâtiment travaux publics	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
11	77	administrateur de biens	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
12	83	exode rural	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
13	85	friche	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
14	85	gel des terres	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1
15	90	sanction disciplinaire	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
16	90	punition	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
17	93	maison d'arrêt	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
18	93	maison centrale	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
19	93	prison	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
20	93	centre de détention	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
21	94	détenu	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
22	94	prisonnier	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
23	98	archéologue	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
24	98	science préhistorique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
25	98	fouille archéologique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
26	98	site archéologique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
27	100	invention	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
28	101	colloque	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
29	101	séminaire	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
30	102	société d'agriculture	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
31	102	académie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
32	106	scientifique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
33	106	géographe	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
34	106	historien	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
35	106	préhistorien	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
36	106	ethnologue	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
37	106	astronome	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
38	113	dessins et modèles	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
39	113	dépôt de brevet	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
40	114	vie associative	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
41	120	honoraire	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
42	120	salaire	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
43	120	traitement	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
44	123	aménagement du temps de travail	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
45	126	voirie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
46	126	ponts et chaussées	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
47	126	route	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
48	130	rue	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
49	133	chemin départemental	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
50	145	collectivité territoriale	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
51	154	espionnage	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
52	156	déserteur	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
53	159	cadre de réserve	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
54	159	affectation spéciale	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
55	165	zone inondable	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
56	165	curage	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
57	165	digue	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
58	165	surface submersible	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
59	166	zone inondable	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
60	166	curage	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
61	166	digue	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
62	166	surface submersible	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
63	169	assèchement	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
64	170	garde champêtre	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
65	174	structure agricole	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
66	176	machinisme agricole	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
67	177	comices agricoles	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
68	180	cuir	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
69	180	peau	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
70	180	ganterie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
71	180	mégisserie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
72	180	équarrissage	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
73	180	chaussure	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
74	181	pollution industrielle	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
75	182	gravière	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
76	182	sablière	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
77	182	tourbière	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
78	182	extraction de matériaux	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
79	182	marais salant	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
80	182	saline	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
81	184	habillement	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
82	184	tissage	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
83	184	textile	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
84	184	laine	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
85	184	coton	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
86	185	électronique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
87	186	ressource naturelle	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
88	188	machine outil	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
89	189	phosphate	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
90	189	parfumerie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
91	189	plastique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
92	190	fabrique	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
93	190	haut fourneau	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
94	190	usine	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
95	193	cristallerie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
96	193	arts de la table	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
97	193	porcelaine	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
98	194	sidérurgie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
99	194	aluminium	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
100	194	fonderie	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
101	194	acier	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
102	194	cuivre	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
103	195	métier d'art	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
104	196	legs	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
105	200	séquestre révolutionnaire	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
106	203	acquisition foncière	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
107	205	sépulture	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
108	205	inhumation	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
109	215	crématorium	2013-01-15 16:46:22	1	2013-01-15 16:46:22	1
110	236	instrument de mesure	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
111	241	importation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
112	241	commerce maritime	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
113	241	exportation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
114	242	marché d'intérêt national	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
115	244	foire exposition	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
116	244	foire	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
117	244	salon	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
118	244	exposition universelle	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
119	245	urbanisme commercial	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
120	245	supermarché	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
121	245	hypermarché	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
122	245	magasin de grande surface	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
123	246	représentant de commerce	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
124	246	VRP	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
125	246	commis voyageur	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
126	250	antiquaire	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
127	253	objet d'art	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
128	253	mobilier classé	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
129	254	officier public	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
130	254	charge	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
131	255	station balnéaire	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
132	256	station balnéaire	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
133	257	estran	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
134	259	phare	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
135	259	amer	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
136	259	balise	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
137	261	équitation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
138	261	ski	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
139	261	tennis	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
140	261	basket ball	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
141	261	sports d'hiver	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
142	261	tir	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
143	261	rugby	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
144	261	athlétisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
145	261	pétanque	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
146	261	sport aérien	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
147	261	patinage	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
148	261	arts martiaux	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
149	261	sport automobile	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
150	261	alpinisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
151	261	course pédestre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
152	261	cyclisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
153	261	sport nautique	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
154	261	escrime	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
155	261	football	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
156	261	voile	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
157	261	montgolfière	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
158	261	gymnastique	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
159	261	natation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
160	262	cure	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
161	262	station thermale	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
162	262	établissement thermal	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
163	265	garde côte	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
164	265	eaux territoriales	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
165	269	ambassade	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
166	269	consulat	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
167	270	coopération transfrontalière	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
168	270	coopération européenne	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
169	274	entreprise de presse	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
170	274	agence de presse	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
171	274	groupe de presse	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
172	284	monument aux morts	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
173	284	monument commémoratif	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
174	285	parrainage civil	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
175	286	décoration	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
176	286	arts et lettres	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
177	286	mérite	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
178	286	croix de guerre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
179	286	palmes académiques	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
180	286	légion d'honneur	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
181	286	médaille du travail	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
182	286	mérite agricole	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
183	286	médaille d'honneur	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
184	286	mérite maritime	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
185	289	rétablissement de l'ordre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
186	289	ordre public	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
187	290	championnat	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
188	290	course automobile	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
189	290	compétition sportive	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
190	290	manifestation aérienne	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
191	290	match	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
192	290	régate	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
193	292	voyage officiel	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
194	295	surveillance des personnes	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
195	296	police de la circulation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
196	296	police de la navigation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
197	297	moeurs	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
198	298	jeu clandestin	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
199	300	garde chasse	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
200	301	garde pêche	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
201	303	forces de l'ordre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
202	306	grève	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
203	317	marbre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
204	317	phosphates	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
205	317	talc	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
206	317	pierre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
207	317	sables et graviers	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
208	317	ardoisière	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
209	317	salpêtre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
210	317	plâtre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
211	317	pierre à faux	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
212	319	mine	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
213	320	forage	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
214	322	auxiliaire de vie	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
215	322	aide familiale	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
216	322	assistante sociale	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
217	322	assistant de service social	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
218	322	surveillant	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
219	322	conseiller d'éducation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
220	323	surveillant	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
221	323	conseiller d'éducation	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
222	353	gauchisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
223	359	antimilitarisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
224	359	pacifisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
225	361	syndicat patronal	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
226	363	chambre de commerce	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
227	363	chambre des métiers	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
228	363	chambre d'agriculture	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
229	365	randonnée	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
230	365	sentier pédestre	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
231	369	station de sports d'hiver	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
232	371	interprète	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
233	373	station verte de vacances	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
234	375	itinéraire thématique	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
235	376	syndicat d'initiative	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
236	376	office de tourisme	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
237	380	désertion	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
238	380	insoumission	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
239	381	attentat à la pudeur	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
240	381	harcèlement sexuel	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
241	381	détournement de mineur	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
242	381	viol	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
243	388	lèse-majesté	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
244	388	injures envers le chef de l'État	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
245	388	sabotage	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
246	388	violences envers agents publics	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
247	389	brigandage	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
248	390	banqueroute	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
249	390	contrefaçon	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
250	390	infraction douanière	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
251	390	détournement	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
252	394	braconnage	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
253	394	délit de chasse	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
254	394	délit de pêche	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
255	394	délit de pacage	2013-01-15 16:46:23	1	2013-01-15 16:46:23	1
256	395	proxénétisme	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
257	395	racolage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
258	397	incitation à la haine raciale	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
259	397	diffamation	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
260	397	atteinte à la vie privée	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
261	397	violation de sépulture	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
262	397	injures et menaces	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
263	397	discrimination	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
264	398	otage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
265	399	vandalisme	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
266	401	infanticide	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
267	401	homicide	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
268	401	assassinat	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
269	403	racket	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
270	407	mise à l'épreuve	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
271	407	sursis avec mise à l'épreuve	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
272	408	exécution des peines	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
273	415	centre d'aide par le travail	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
274	415	atelier protégé	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
275	416	patente	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
276	420	magasin	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
277	421	établissement bancaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
278	421	crédit	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
279	421	banque	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
280	422	centre de gestion agréé	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
281	424	patronat	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
282	424	employeur	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
283	425	succursale	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
284	425	filiale	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
285	426	établissement public industriel et commercial	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
286	426	secteur public	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
287	426	société nationalisée	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
288	431	cautionnement	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
289	431	nantissement	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
290	432	marque commerciale	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
291	435	munition	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
292	435	explosif	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
293	435	arme	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
294	435	poudre	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
295	435	port d'arme	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
296	437	disparition	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
297	437	fugue	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
298	441	vigile	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
299	441	gardiennage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
300	442	commerçant ambulant	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
301	443	agence immobilière	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
302	443	marchand de biens	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
303	444	café	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
304	444	bar	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
305	445	taxi	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
306	446	salon de coiffure	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
307	450	apatride	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
308	453	droit d'usage d'eau	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
309	453	droit de pacage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
310	453	droit de coupe de bois	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
311	453	droit de pêche	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
312	453	droit de chasse	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
313	460	trafic ferroviaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
314	460	chemin de fer	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
315	460	train à grande vitesse	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
316	460	train	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
317	461	trafic ferroviaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
318	461	chemin de fer	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
319	461	train à grande vitesse	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
320	461	train	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
321	462	trafic ferroviaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
322	462	chemin de fer	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
323	462	train à grande vitesse	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
324	462	train	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
325	466	alpage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
326	467	incendie de forêt	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
327	469	square	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
328	469	jardin public	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
329	474	sans papiers	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
330	482	salle polyvalente	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
331	484	maison de quartier	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
332	487	terrain de golf	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
333	487	terrain de sport	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
334	487	circuit automobile	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
335	487	piscine	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
336	487	équipement sportif	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
337	487	aéro club	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
338	487	patinoire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
339	487	gymnase	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
340	487	stade	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
341	487	base nautique	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
342	489	parc de stationnement	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
343	489	stationnement	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
344	489	parking	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
345	493	marché couvert	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
346	498	bains-douches	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
347	498	toilettes publiques	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
348	505	collecte sélective	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
349	507	produits d'équarrissage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
350	511	déchetterie	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
351	512	station service	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
352	512	établissement classé	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
353	516	praticien hospitalier	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
354	516	chef de clinique	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
355	517	corps préfectoral	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
356	517	agent de l'État	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
357	517	douanier	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
358	517	agent fiscal	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
359	517	représentant en mission	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
360	518	professeur des écoles	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
361	521	maître de conférence	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
362	521	adjoint d'enseignement	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
363	521	universitaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
364	521	assistant	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
365	522	querelle scolaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
366	523	groupe scolaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
367	526	cours complémentaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
368	526	enseignement primaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
369	529	brevet d'enseignement professionnel	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
370	529	brevet	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
371	529	certificat d'aptitude professionnel	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
372	529	baccalauréat	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
373	530	école libre	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
374	530	enseignement libre	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
375	531	école maternelle	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
376	531	jardin d'enfant	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
377	534	principal	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
378	534	proviseur	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
379	536	promotion sociale	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
380	536	éducation permanente	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
381	536	schéma régional de formation	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
382	536	formation continue	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
383	536	congé de conversion	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
384	536	stage	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
385	540	fonction publique	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
386	547	épargne logement	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
387	548	règlement judiciaire	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
388	550	liquidation de biens	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
389	552	pépinière d'entreprises	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
390	560	création radiophonique	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
391	561	poésie	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
392	563	art lyrique	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
393	564	photographie	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
394	564	sculpture	2013-01-15 16:46:24	1	2013-01-15 16:46:24	1
395	564	enluminure	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
396	564	dessin	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
397	564	peinture	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
398	564	gravure	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
399	572	vieillesse	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
400	572	troisième âge	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
401	574	condition féminine	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
402	575	gitan	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
403	575	tsigane	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
404	576	conseillère conjugale	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
405	577	vagabond	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
406	577	clochard	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
407	584	pauvreté	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
408	584	économiquement faible	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
409	588	secours d'urgence	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
410	589	assistance publique	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
411	590	bienfaisance	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
412	592	famille d'accueil	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
413	592	famille nourricière	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
414	593	allocation de salaire unique	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
415	593	allocation orphelin	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
416	593	allocation maternité	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
417	593	allocation d'adoption	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
418	593	allocation familiale	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
419	602	mutualité complémentaire	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
420	602	mutualité	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
421	602	secours mutuel	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
422	604	taille	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
423	604	contribution du clergé	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
424	604	cinquantième	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
425	604	capitation	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
426	604	vingtième	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
427	604	dixième	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
428	604	impôts de guerre	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
429	607	impôt indirect	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
430	608	taxe locale d'équipement	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
431	615	douane	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
432	619	fermier général	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
433	619	collecteur	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
434	621	contribution patriotique	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
435	623	pont	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
436	623	échangeur	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
437	623	écluse	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
438	623	aqueduc	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
439	623	viaduc	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
440	623	tunnel	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
441	625	centrale hydraulique	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
442	625	hydroélectricité	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
443	626	canal	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
444	628	funiculaire	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
445	629	embarcadère	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
446	632	vaccination des animaux	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
447	635	bière	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
448	635	poiré	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
449	635	cidre	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
450	635	alcool	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
451	635	bouilleur de cru	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
452	635	distillation	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
453	635	liqueur	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
454	636	arboriculture	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
455	642	voiture	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
456	643	accident de la route	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
457	643	déraillement	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
458	643	naufrage	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
459	643	catastrophe ferroviaire	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
460	643	catastrophe aérienne	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
461	644	moteur	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
462	649	métro	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
463	649	diligence	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
464	649	autobus	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
465	649	trolleybus	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
466	649	tramway	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
467	650	conducteur automobile	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
468	651	avion	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
469	651	hélicoptère	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
470	652	bac	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
471	652	barge	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
472	652	péniche	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
473	653	batellerie	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
474	653	marinier	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
475	653	navigation intérieure	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
476	654	paquebot	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
477	654	cargo	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
478	654	ferry	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
479	654	bateau à voile	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
480	654	bateau de pêche	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
481	654	bateau à vapeur	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
482	654	chalutier	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
483	656	trafic aérien	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
484	656	aviation civile	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
485	658	vélo	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
486	658	moto	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
487	661	aérogare	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
488	661	terrain d'aviation	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
489	661	aéroport	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
490	661	héliport	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
491	664	mortalité	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
492	670	juge seigneurial	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
493	670	lieutenant de sénéchaussée	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
494	670	conseiller au parlement	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
495	670	juge mage	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
496	670	juge d'appeaux	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
497	673	représentant syndical	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
498	678	développement local	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
499	678	restructuration industrielle	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
500	683	zone artisanale	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
501	685	assistance médicale gratuite	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
502	686	foyer de travailleurs immigrés	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
503	686	foyer logement	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
504	686	foyer de jeunes travailleurs	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
505	686	foyer d'hébergement	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
506	686	logement foyer	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
507	687	bureau d'aide sociale	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
508	687	bureau de bienfaisance	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
509	687	centre communal d'aide sociale	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
510	688	assistance judiciaire	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
511	689	revenu minimum d'insertion	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
512	689	allocation personnalisée d'autonomie	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
513	689	revenu de solidarité active	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
514	696	adjoint au maire	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
515	697	sénateur	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
516	697	député	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
517	697	député européen	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
518	701	consul	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
519	708	vétérinaire	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
520	710	épizootie	2013-01-15 16:46:25	1	2013-01-15 16:46:25	1
521	717	étude notariale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
522	717	hypothèque	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
523	717	propriété foncière	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
524	718	hypothèque	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
525	718	propriété foncière	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
526	722	service national	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
527	722	sursitaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
528	722	service militaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
529	722	soutien de famille	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
530	722	insoumis	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
531	722	dispensé du service militaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
532	722	tirage au sort	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
533	722	remplacement militaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
534	722	appelé	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
535	722	objecteur de conscience	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
536	724	vacances scolaires	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
537	725	patronage	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
538	725	centre aéré	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
539	726	colonie de vacances	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
540	727	emploi du temps	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
541	730	pipeline	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
542	730	gazoduc	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
543	730	oléoduc	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
544	731	transformateur électrique	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
545	732	chauffage géothermique	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
546	738	tournesol	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
547	738	olive	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
548	738	colza	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
549	738	matière grasse	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
550	739	avoine	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
551	739	orge	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
552	739	blé	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
553	739	riz	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
554	739	maïs	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
555	740	cigarette	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
556	741	betterave	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
557	741	canne à sucre	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
558	742	province ecclésiastique	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
559	742	archiprêtré	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
560	742	archidiaconé	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
561	742	paroisse	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
562	742	diocèse	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
563	745	prieuré	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
564	745	ermitage	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
565	745	couvent	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
566	745	commanderie	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
567	745	monastère	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
568	745	abbaye	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
569	756	raz de marée	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
570	756	tremblement de terre	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
571	756	éboulement	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
572	756	avalanche	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
573	756	glissement de terrain	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
574	756	tempête	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
575	756	cyclone	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
576	756	séisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
577	756	tornade	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
578	759	réanimation	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
579	760	ruisseau	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
580	760	rivière	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
581	760	fleuve	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
582	760	torrent	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
583	761	étang	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
584	761	plan d'eau	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
585	761	lac	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
586	763	climatologie	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
587	768	taxe sur la valeur ajoutée	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
588	774	communauté de villes	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
589	774	syndicat intercommunal	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
590	774	district urbain	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
591	774	communauté de communes	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
592	774	communauté d'agglomération	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
593	774	communauté urbaine	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
594	781	catéchèse	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
595	785	ordres religieux	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
596	785	évêque	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
597	785	pape	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
598	785	cardinal	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
599	785	curé	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
600	785	chapitre	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
601	785	ermite	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
602	785	archidiacre	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
603	785	prêtre	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
604	785	archevêque	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
605	785	rabbin	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
606	785	official	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
607	785	vicaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
608	785	chapelain	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
609	785	aumônier militaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
610	785	clergé	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
611	785	imam	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
612	786	scriptorium	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
613	786	mosquée	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
614	786	oratoire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
615	786	basilique	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
616	786	cathédrale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
617	786	lieu de culte	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
618	786	chapelle	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
619	786	temple	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
620	786	collégiale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
621	786	synagogue	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
622	786	église	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
623	788	valdéisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
624	788	gnosticisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
625	788	catharisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
626	788	béguinisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
627	788	manichéisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
628	788	arianisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
629	789	chorale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
630	789	orchestre	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
631	789	groupe folklorique	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
632	795	breton	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
633	795	dialecte	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
634	795	corse	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
635	795	basque	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
636	795	occitan	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
637	795	alsacien	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
638	797	natalité	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
639	799	nuptialité	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
640	802	costume traditionnel	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
641	802	danse populaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
642	802	légende	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
643	802	ethnologie	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
644	802	médecine populaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
645	802	conte	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
646	802	charivari	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
647	802	culture locale ou régionale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
648	802	proverbe	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
649	802	almanach	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
650	802	littérature populaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
651	803	costume traditionnel	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
652	803	danse populaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
653	803	légende	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
654	803	ethnologie	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
655	803	médecine populaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
656	803	conte	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
657	803	charivari	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
658	803	culture locale ou régionale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
659	803	proverbe	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
660	803	almanach	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
661	803	littérature populaire	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
662	806	révolte de détenus	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
663	811	enfant recueilli temporairement	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
664	811	enfant placé	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
665	812	légitimation	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
666	816	habitat ancien	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
667	824	village fleuri	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
668	824	ville fleurie	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
669	829	embauche	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
670	832	service public de l'emploi	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
671	832	mission locale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
672	832	permanence d'accueil d'information et d'orientation\n\t\t\t(PAIO)	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
673	833	main d'oeuvre étrangère	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
674	834	suppression d'emplois	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
675	836	travail au noir	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
676	844	musicien	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
677	844	danseur	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
678	844	profession artistique	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
679	844	acteur	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
680	845	exposition	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
681	845	représentation théâtrale	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
682	845	festival	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
683	845	concert	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
684	845	son et lumière	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
685	848	journalier	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
686	849	syndicat ouvrier	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
687	849	syndicalisme	2013-01-15 16:46:26	1	2013-01-15 16:46:26	1
688	855	dentiste	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
689	857	orthophoniste	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
690	857	profession paramédicale	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
691	857	infirmier	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
692	857	aide soignante	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
693	857	opticien lunetier	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
694	857	orthopédiste	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
695	857	infirmière	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
696	857	pédicure	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
697	857	audioprothésiste	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
698	857	kinésithérapeute	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
699	859	produit pétrolier	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
700	859	raffinage	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
701	859	gasoil	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
702	859	essence	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
703	859	fioul domestique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
704	859	carburant	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
705	860	produit pétrolier	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
706	860	raffinage	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
707	860	gasoil	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
708	860	essence	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
709	860	fioul domestique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
710	860	carburant	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
711	861	gabelle	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
712	863	déboisement	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
713	865	marais	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
714	865	zone marécageuse	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
715	868	moulin à eau	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
716	871	moulin à vent	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
717	872	charbon	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
718	874	uranium	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
719	874	bauxite	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
720	874	argent	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
721	876	réseau télématique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
722	876	minitel	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
723	877	ligne de télécommunications	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
724	877	installation de télécommunications	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
725	878	messagerie électronique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
726	878	réseau câblé	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
727	878	vidéoconférence	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
728	878	internet	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
729	878	télécommunications par satellite	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
730	879	service télégraphique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
731	879	installation télégraphique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
732	881	abonnement téléphonique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
733	881	installation téléphonique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
734	888	pouponnière	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
735	890	justice de paix	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
736	890	tribunal	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
737	890	chambre régionale des comptes	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
738	890	cour des comptes	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
739	890	tribunal d'exception	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
740	890	tribunal révolutionnaire	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
741	890	cour d'assises	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
742	890	cour d'appel	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
743	890	tribunal administratif	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
744	890	tribunal de commerce	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
745	912	réfugié politique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
746	912	droit d'asile	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
747	915	flux migratoire	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
748	920	élection au Conseil de la République	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
749	921	plébiscite	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
750	927	sectionnement électoral	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
751	933	municipalité	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
752	933	délégation spéciale	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
753	934	participation	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
754	935	hygiène du travail	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
755	938	travail intérimaire	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
756	938	travail temporaire	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
757	944	emprunt public	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
758	952	écomusée	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
759	953	vidéothèque	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
760	953	phonothèque	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
761	955	archives hospitalières	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
762	955	archives départementales	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
763	955	archives régionales	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
764	955	archives nationales	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
765	955	archives communales	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
766	955	archives militaires	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
767	958	salle de cinéma	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
768	958	music hall	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
769	958	auditorium	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
770	958	dancing	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
771	958	théâtre	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
772	958	boîte de nuit	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
773	959	lecture publique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
774	959	bibliothèque de prêt	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
775	972	amélioration génétique des espèces animales	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
776	972	monte	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
777	972	insémination artificielle	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
778	972	haras	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
779	974	salaison	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
780	980	centre commercial	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
781	981	pharmacie d'officine	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
782	983	gérance	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
783	984	auberge	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
784	984	meublé	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
785	984	garni	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
786	986	cercle de jeux	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
787	988	quête sur la voie publique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
788	992	éducation physique	2013-01-15 16:46:27	1	2013-01-15 16:46:27	1
789	997	fusillé	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
790	1007	conseil de guerre	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
791	1007	cour martiale	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
792	1011	hôpital militaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
793	1015	cavalerie	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
794	1015	infanterie	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
795	1015	artillerie	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
796	1017	patrimoine industriel	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
797	1017	collections scientifiques	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
798	1020	archives d'érudit	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
799	1020	archives d'association	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
800	1020	livre	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
801	1020	archives familiales	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
802	1020	archives d'entreprise	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
803	1020	archives religieuses	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
804	1020	ouvrage manuscrit	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
805	1020	archives personnelles	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
806	1020	document d'archives	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
807	1022	monument historique	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
808	1022	édifice classé	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
809	1022	château	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
810	1022	monument	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
811	1022	mur d'enceinte	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
812	1029	village de vacances	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
813	1041	site protégé	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
814	1041	monument naturel	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
815	1041	site pittoresque	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
816	1041	grotte	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
817	1041	paysage	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
818	1042	parc zoologique	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
819	1042	gibier	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
820	1042	zoo	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
821	1045	végétation	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
822	1049	société de chasse	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
823	1053	société de pêche	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
824	1054	commission administrative paritaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
825	1054	comité technique paritaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
826	1054	comité hygiène et sécurité	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
827	1054	conseil de quartier	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
828	1057	association parapublique	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
829	1061	administration centrale royale	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
830	1061	ministère	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
831	1065	diocèse civil	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
832	1065	intendance	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
833	1065	gouvernement provincial	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
834	1069	société civile immobilière	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
835	1069	promotion immobilière	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
836	1070	rénovation cadastrale	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
837	1073	congé de maternité	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
838	1076	nourrice agréée	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
839	1079	contrôle des naissances	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
840	1079	planning familial	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
841	1083	aide publique	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
842	1088	ligne ferroviaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
843	1095	classe verte	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
844	1095	classe de neige	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
845	1095	classe de mer	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
846	1095	classe de nature	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
847	1095	classe du patrimoine	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
848	1095	classe de ville	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
849	1095	classe culturelle	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
850	1098	santé scolaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
851	1103	garderie scolaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
852	1106	assurance scolaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
853	1110	délinquance juvénile	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
854	1114	ambulance	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
855	1114	avion sanitaire	2013-01-15 16:46:28	1	2013-01-15 16:46:28	1
856	1118	pompier	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
857	1120	concurrence	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
858	1120	pratique anti concurrentielle	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
859	1122	eau minérale	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
860	1122	nappe phréatique	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
861	1122	source	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
862	1124	assainissement	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
863	1124	déversement des eaux usées	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
864	1124	station d'épuration	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
865	1124	fosse septique	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
866	1124	égout	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
867	1124	épuration des eaux	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
868	1127	hygiène sociale	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
869	1128	maladie vénérienne	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
870	1130	maladie contagieuse	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
871	1130	peste	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
872	1137	syndrome d'immunodéficience acquis	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
873	1149	marée noire	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
874	1153	amirauté	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
875	1153	cour des aides	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
876	1153	justice de l'équivalent	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
877	1153	table de marbre	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
878	1153	maîtrise des eaux et forêts	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
879	1153	justice royale secondaire	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
880	1153	présidial	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
881	1153	chambre des comptes	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
882	1153	cour de parlement	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
883	1159	assemblée provinciale	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
884	1168	bible	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
885	1168	coran	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
886	1168	torah	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
887	1169	énergie atomique	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
888	1172	mutilé du travail	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
889	1177	ouvrier agricole	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
890	1179	fermier	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
891	1188	clinique	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
892	1190	psychiatrie	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
893	1191	centre hospitalier	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
894	1191	centre hospitalier universitaire	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
895	1191	hôpital	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
896	1195	congé de longue maladie	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
897	1195	congé longue durée	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
898	1197	herboristerie	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
899	1199	école normale	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
900	1199	institut universitaire de formation des maîtres	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
901	1201	centre d'apprentissage	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
902	1206	impôt sur les grandes fortunes	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
903	1206	impôt de solidarité sur la fortune	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
904	1208	bénéfice commercial	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
905	1208	bénéfice industriel	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
906	1208	bénéfice agricole	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
907	1211	allocation vieillesse	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
908	1211	régime de retraite	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
909	1215	hospice	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
910	1215	dispensaire	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
911	1215	centre de soins	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
912	1215	maison de retraite	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
913	1218	agent des collectivités locales	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
914	1218	agent municipal d'Ancien Régime	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
915	1219	intendant	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
916	1219	bailli	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
917	1219	sénéchal	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
918	1219	gouverneur	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
919	1219	subdélégué	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
920	1219	officier royal	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
921	1221	auxiliaire	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
922	1221	vacataire	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
923	1221	contractuel	2013-01-15 16:46:29	1	2013-01-15 16:46:29	1
924	1233	neige	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
925	1233	orage	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
926	1233	verglas	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
927	1235	dot	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
928	1235	contrat de mariage	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
929	1243	domestique	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
930	1244	métier du bâtiment	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
931	1245	gardien d'immeuble	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
932	1246	travail à domicile	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
933	1246	aide-ménagère	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
934	1247	télévision numérique	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
935	1247	télévision câblée	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
936	1247	télévision	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
937	1247	chaîne de télévision	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
938	1249	société sportive	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
939	1254	système d'information géographique	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
940	1254	informatique	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
941	1255	trafic postal	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
942	1278	ramassage scolaire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
943	1279	aide scolaire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
944	1279	prêt d'honneur	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
945	1280	restaurant universitaire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
946	1280	cantine scolaire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
947	1282	cité universitaire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
948	1283	voyageur	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
949	1289	moniteur d'auto école	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
950	1289	auto école	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
951	1295	licence	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
952	1295	doctorat	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
953	1295	maîtrise	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
954	1295	grade universitaire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
955	1296	pari	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
956	1297	tombola	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
957	1298	pari mutuel urbain	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
958	1298	champ de course	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
959	1300	machine à sous	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
960	1303	épargne	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
961	1314	travail d'intérêt général	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
962	1316	carcan	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
963	1316	torture	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
964	1318	interrogatoire	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
965	1326	mairie	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
966	1331	don du sang	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
967	1332	radiologie	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
968	1343	église réformée	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
969	1343	évangélisme	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
970	1343	calvinisme	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
971	1343	luthéranisme	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
972	1350	protection des sols	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
973	1350	forêt de protection	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
974	1350	érosion	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
975	1350	dune	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
976	1352	médicament	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
977	1352	produit pharmaceutique	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
978	1353	phylloxéra	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
979	1354	parasite	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
980	1354	piégeage	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
981	1354	nuisible	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
982	1354	dégât du gibier	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
983	1356	bail immobilier	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
984	1356	baux d'habitation	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
985	1361	engrais	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
986	1371	châtellenie	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
987	1371	prévôté	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
988	1371	viguerie	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
989	1371	vicomté	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
990	1371	bailliage	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
991	1373	subdélégation	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
992	1376	pré-professionnalisation	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
993	1380	logo	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
994	1380	charte graphique	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
995	1381	administré	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
996	1381	usager	2013-01-15 16:46:30	1	2013-01-15 16:46:30	1
997	1385	poison	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
998	1390	désamiantage	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
999	1392	bâtiment menaçant ruine	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1000	1392	bâtiment insalubre	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1001	1392	taudis	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1002	1392	insalubrité	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1003	1392	bidonville	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1004	1399	blessé de guerre	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1005	1399	mutilé de guerre	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1006	1408	divorce	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1007	1408	séparation	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1008	1408	pension alimentaire	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1009	1408	union civile	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1010	1408	droit parental	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1011	1411	crustacé	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1012	1411	ostréiculture	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1013	1411	huître	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1014	1411	coquillage	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1015	1411	mytiliculture	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1016	1411	escargot	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1017	1411	moule	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1018	1424	spoliation	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1019	1431	police de proximité	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1020	1431	police nationale	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1021	1440	traîneau	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1022	1440	carrosse	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1023	1440	calèche	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1024	1440	fiacre	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1025	1440	charrette	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1026	1441	cave coopérative	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1027	1442	fermage	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1028	1444	sica	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1029	1446	résistant	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1030	1447	collaborateur	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1031	1449	papier	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1032	1449	carton	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1033	1468	camion	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1034	1473	radio locale privée	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1035	1475	manuel scolaire	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1036	1481	emprisonnement	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1037	1489	émeute	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1038	1489	pillage	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1039	1489	attroupement	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1040	1489	cris séditieux	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1041	1489	insurrection	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1042	1493	généalogie	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1043	1496	menée antinationale	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1044	1496	propos antinational	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1045	1496	propos défaitiste	2013-01-15 16:46:31	1	2013-01-15 16:46:31	1
1046	1504	médiation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1047	1504	arbitrage	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1048	1515	armement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1049	1515	habillement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1050	1520	homologation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1051	1520	labellisation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1052	1520	habilitation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1053	1520	approbation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1054	1524	évaluation fiscale	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1055	1529	modernisation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1056	1529	réforme administrative	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1057	1530	mise à disposition	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1058	1530	nomination	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1059	1530	promotion professionnelle	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1060	1530	destitution	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1061	1530	mise en disponibilité	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1062	1530	titularisation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1063	1530	affectation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1064	1530	intégration	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1065	1530	avancement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1066	1530	licenciement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1067	1530	mise à la retraite	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1068	1530	notation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1069	1530	suspension	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1070	1530	détachement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1071	1533	métrage	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1072	1542	affiliation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1073	1542	francisation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1074	1548	abrogation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1075	1551	dotation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1076	1551	subvention	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1077	1555	extension	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1078	1555	rénovation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1079	1557	information	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1080	1561	audit	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1081	1565	dérogation	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1082	1565	dispense	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1083	1565	interdiction	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1084	1566	engagement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1085	1566	paiement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1086	1566	mandatement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1087	1566	liquidation comptable	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1088	1566	ordonnancement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1089	1566	apurement	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1
1090	1707	grand livre	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1
1091	1707	journal centralisateur	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1
1092	1707	livre journal	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1
1093	1708	bilan d'activité	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1094	1734	placet	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1095	1738	feuille de ménage	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1096	1738	document de base	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1097	1739	négatif	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1098	1739	tirage	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1099	1739	diapositive	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1100	1739	ektachrome	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1101	1740	instruction	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1102	1749	film	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1103	1749	dvd	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1104	1749	cassette vidéo	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1105	1749	cassette audio	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1106	1749	cédérom	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1107	1749	cd-audio	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1108	1753	bréviaire	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1109	1753	missel	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1110	1770	minutier	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1111	1790	testament	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1112	1793	placard	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1113	1796	avant-projet définitif	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1114	1796	avant-projet sommaire	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1115	1799	licence	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1116	1829	voeu	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1117	1843	thèse	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1118	1844	bulletin paroissial	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1119	1844	bulletin communal	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1120	1844	bulletin municipal	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1121	1844	bulletin d'école	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1122	1844	bulletin cantonal	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1123	1844	bulletin intercommunal	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1124	1845	carte de séjour	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1125	1848	arrêté ministériel	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1126	1848	traité diplomatique	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1127	1848	loi	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1128	1848	ordonnance	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1129	1849	bulletin de paie	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1130	1858	allocution	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1131	1866	document dessiné	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1132	1866	document peint	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1133	1866	estampe	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1134	1875	tableau statistique	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1135	1875	état	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1136	1884	plumitif d'audience	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1137	1888	ordre de recouvrement	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1138	1888	mandat de paiement	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1139	1888	décompte définitif	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1140	1888	ordre de reversement	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1
1141	1888	devis	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1142	1888	facture	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1143	1888	ordonnance de paiement	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1144	1888	pièce justificative de dépenses	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1145	1888	quittance	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1146	1888	quitus	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1147	1888	titre de recette	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1148	1890	jugement	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1149	1890	sentence	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1150	1890	ordonnance juridictionnelle	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1151	1934	aveu et dénombrement	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1152	1934	foi et hommage	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1153	1934	reconnaissances seigneuriales	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1154	1935	livre de raison	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
1155	1935	journal intime	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1
\.


--
-- Name: adm-altkeywords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-altkeywords_id_seq"', 1156, false);


--
-- Data for Name: adm-collectivites; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-collectivites" (id, nom, description, active, affichage_id, parent_id, lft, rght, created, created_user_id, modified, modified_user_id,identifiant) FROM stdin;
1	Conseil Général		t	1	\N	1	2	2009-06-16 13:59:43	1	2016-03-30 11:58:05	1	COLREF
\.


--
-- Name: adm-collectivites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-collectivites_id_seq"', 2, false);


--
-- Data for Name: adm-compteurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-compteurs" (id, nom, description, def_compteur, sequence_id, def_reinit, val_reinit, created, modified, created_user_id, modified_user_id, seda02, seda10, type) FROM stdin;
1	ArchivalAgencyArchiveIdentifier	Utilisé pour donner un numéro aux archives	#s#	1			2010-01-06 23:58:10	2016-03-07 11:27:04	1	1	\N	\N	interne
9	ArchivalAgencyDocumentArchiveIdentifier	Utilisé pour donner un numéro aux documents liés directement aux archives.	#RelativeIdentifier#_000_#000#	9			2012-04-11 09:32:02	2013-08-12 14:19:44	1	1	\N	\N	interne
8	ArchiveDestructionRequest	Utilisé pour générer le code ArchiveDestructionIdentifier du bordereau d'élimination d'archive.	ADR_#s#	8		\N	2011-01-27 14:22:56	2011-01-27 15:41:55	1	1	\N	\N	interne
14	ArchiveDeliveryRequest	Utilisé pour générer le code DeliveryRequestIdentifier du bordereau de demande de communication d'archives.	ADLR_#s#	14			2012-04-11 09:32:02	2012-04-11 09:32:02	1	1	\N	\N	interne
3	ArchiveTransferReply	Utilisé pour générer le code TransferReplyIdentifier ainsi que le nom du fichier d'échange XML.	ATR_#s#	3			2010-01-07 09:24:07	2011-04-01 10:01:28	1	1	\N	\N	interne
7	ArchiveDelivery	Utilisé pour générer le code DeliveryIdentifier du bordereau de communication d'archive.	ADL_#s#	7		\N	2011-01-27 14:22:56	2012-12-18 15:16:12	1	1	\N	\N	interne
16	Archiverestitutionrequestreply	Utilisé pour générer le code RestitutionRequestReplyIdentifier ainsi que le nom du fichier d'échange XML.	ARRR_#s#	16			2016-01-20 11:19:23	2016-01-20 11:19:23	1	1	\N	\N	interne
12	ArchiveRestitutionRequest	Utilisé pour générer le code RestitutionRequestIdentifier du bordereau de demande de restitution d'archives.	ARR_#s#	12			2012-04-11 09:32:02	2012-04-11 09:32:02	1	1	\N	\N	interne
13	ArchiveRestitution	Utilisé pour générer le code RestitutionIdentifier du bordereau de restitution d'archives.	AR_#s#	13			2012-04-11 09:32:02	2012-04-11 09:32:02	1	1	\N	\N	interne
11	ArchiveDestructionNotification	Utilisé pour générer le code DestructionNotificationIdentifier du bordereau de notification d'élimination d'archives.	ADN_#s#	11			2012-04-11 09:32:02	2012-04-11 09:32:02	1	1	\N	\N	interne
6	ArchiveTransfert	Utilisé pour générer le code TransferIdentifier des bordereaux de transfert.	AT_#s#	6		\N	2010-11-06 12:00:00	2010-11-06 12:00:00	1	1	\N	\N	interne
5	AcknowledgementIdentifier	Utilisé pour générer le code AcknowledgementIdentifier du message d'accusé de réception.	ACK_#s#	5		\N	2014-07-04 11:33:25	2014-07-04 11:33:25	1	1	\N	\N	interne
2	ArchivalAgencyObjectIdentifier	Utilisé pour donner un numéro aux objets d'archive. La séquence est remplacée par le nombre d'objets de l'archive.	#ArchivalAgencyArchiveIdentifier#_#000#	2			2010-04-22 17:32:02	2013-08-13 14:54:59	1	1	\N	\N	interne
10	ArchivalAgencyDocumentObjectIdentifier	Utilisé pour donner un numéro aux documents liés aux objets d'archive.	#RelativeIdentifier#_#000#	9			2012-04-11 09:32:02	2013-08-13 14:57:56	1	1	\N	\N	interne
4	ArchiveTransferAcceptance	Utilisé pour générer le code TransferAcceptanceIdentifier ainsi que le nom du fichier d'échange XML.	ATA_#s#	4			2010-01-07 11:29:34	2010-07-29 15:41:42	1	1	\N	\N	interne
15	Archivedestructionrequestreply	Utilisé pour générer le code DestructionRequestReplyIdentifier ainsi que le nom du fichier d'échange XML.	ADRR_#s#	15			2016-01-19 18:01:07	2016-01-19 18:01:07	1	1	\N	\N	interne
\.


--
-- Name: adm-compteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-compteurs_id_seq"', 17, false);


--
-- Data for Name: adm-connecteurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-connecteurs" (id, type, nom, description, actif, ws_username, ws_password, param1, param2, param3, param4, param5, param6, param7, param8, param9, param10, created, created_user_id, modified, modified_user_id) FROM stdin;
1	LOCAL	as@lae locale	Instance locale d'as@lae	t			\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2011-01-25 16:24:14	1	2012-12-20 18:19:01	1
\.


--
-- Name: adm-connecteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-connecteurs_id_seq"', 2, false);


--
-- Data for Name: adm-contrats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-contrats" (id, accord_id, nom, description, date_debut, date_fin, originating_agency_id, actif, created, created_user_id, modified, modified_user_id) FROM stdin;
\.


--
-- Name: adm-contrats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-contrats_id_seq"', 1, false);


--
-- Data for Name: adm-crons; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-crons" (id, nom, description, controller, action, has_params, params, next_execution_time, execution_duration, last_execution_start_time, last_execution_end_time, last_execution_report, last_execution_status, active, created, created_user_id, modified, modified_user_id) FROM stdin;
16	Constitution des demandes d'élimination d'archives	Sélectionne toute les archives éliminables par service producteur et constitue les demandes d'élimination qui seront automatiquement envoyées dans le circuit de traitement	archivedestructionrequests	demandeEliminationAutomatique	f		2013-10-04 15:00:00	P7D	2012-01-01 00:00:00	2012-01-01 00:00:00		SUCCES	f	2013-08-30 15:00:48	1	2013-10-06 18:10:12	1
5	Circuits de traitement : gestion des traitements extérieurs	Envoi (mail, parapheur) et lecture (parapheur) des traitements extérieurs	traitements	gererTraitementsExterieurs	f	\N	2016-01-08 18:50:00	PT1H	2016-01-08 18:35:09	2016-01-08 18:35:10		SUCCES	t	2011-09-22 08:59:27	1	2016-01-08 18:35:10	1
8	Vérification de l'intégrité des journaux	Vérifie que le chainage des empreintes des journaux est intègre.	journaux	checkIntegrityJnl	f		2016-01-09 01:20:00	P1D	2016-01-08 18:35:11	2016-01-08 18:35:12		SUCCES	t	2012-03-09 09:00:00	1	2016-01-08 18:35:12	1
19	Référentiels extérieurs : synchronisation	Création ou mise à jour des éléments liés aux référentiels extérieurs	refexterieurs	synchronisation	f	\N	2016-03-25 12:48:00	PT1H	2016-03-25 11:57:05	2016-03-25 12:00:14	Synchronisation du référentiel 'Profil SEDA referentiel'<br> - mise à jour du profil 'saemref-test/000004285' réussie<br> - mise à jour du profil 'saemref-test/000005049' réussie<br> - création du profil 'saemref-test/000005238' réussie<br> - mise à jour du profil 'saemref-test/000005340' réussie<br> - mise à jour du profil 'saemref-test/000005522' réussie<br> - mise à jour du profil 'saemref-test/000005555' réussie<br> - mise à jour du profil 'saemref-test/000005780' réussie<br> - mise à jour du profil 'saemref-test/000005865' réussie<br> - mise à jour du profil 'saemref-test/000005924' réussie<br> - mise à jour du profil 'saemref-test/000006081' réussie<br> - mise à jour du profil 'saemref-test/000006332' réussie<br> - mise à jour du profil 'saemref-test/000006868' réussie<br> - mise à jour du profil 'saemref-test/000006981' réussie<br> - mise à jour du profil 'saemref-test/000007294' réussie<br> - mise à jour du profil 'saemref-test/000007364' réussie<br> - mise à jour du profil 'saemref-test/000007682' réussie<br> - mise à jour du profil 'saemref-test/000007820' réussie<br> - mise à jour du profil 'saemref-test/000010435' réussie<br> - mise à jour du profil 'saemref-test/000010474' réussie<br> - mise à jour du profil 'saemref-test/000010631' réussie<br> - mise à jour du profil 'saemref-test/000011217' réussie<br> - mise à jour du profil 'saemref-test/000011341' réussie<br> - mise à jour du profil 'saemref-test/000011427' réussie<br> - création du profil 'saemref-test/000011443' réussie<br> - mise à jour du profil 'saemref-test/000011703' réussie<br> - mise à jour du profil 'saemref-test/000011788' réussie<br> - mise à jour du profil 'saemref-test/000011918' réussie<br> - création du profil 'saemref-test/000012029' réussie<br> - mise à jour du profil 'saemref-test/000012666' réussie<br> - mise à jour du profil 'saemref-test/000012683' réussie<br> - mise à jour du profil 'saemref-test/000012722' réussie<br> - mise à jour du profil 'saemref-test/000012946' réussie<br> - mise à jour du profil 'saemref-test/000013168' réussie<br> - mise à jour du profil 'saemref-test/000013284' réussie<br> - désactivation du profil 'saemref-test/000005581' réussie<br> - suppression du profil 'saemref-test/000012743' réussie<br>	SUCCES	t	2015-09-29 16:48:27	1	2016-03-25 12:00:14	1
17	OAI-PMH : Suppression des tokens expirés	Supprime les listes de résultats paginés des requêtes OAI-PMH expirées	oaipmh	deleteExpiredTokens	f	\N	2016-01-08 19:33:25	PT1H	2016-01-08 18:35:16	2016-01-08 18:35:17		SUCCES	t	2014-07-04 11:33:25	1	2016-01-08 18:35:17	1
6	Conversion de format des fichiers	Conversion de format des fichiers des documents sélectionnés dans les opérations de masse sur les entrées.	documents	convertirFormatMasse	t	100	2016-01-09 01:00:00	P1D	2016-01-08 18:35:10	2016-01-08 18:35:11		SUCCES	t	2011-09-22 08:59:27	1	2016-01-08 18:35:11	1
11	Circuits de traitement : fin de traitement des demandes de restitution	Traite les demandes de restitution pour lesquelles le traitement dans le circuit est terminé.	archiverestitutionrequests	traiteArchiveRestitutionRequestPostWorkFlow	f		2016-01-09 01:30:00	P1D	2016-01-08 18:35:12	2016-01-08 18:35:12		SUCCES	t	2012-03-09 09:00:00	1	2016-01-08 18:35:12	1
10	Circuits de traitement : fin de traitement des demandes d'élimination	Traite les demandes d'élimination pour lesquelles le traitement dans le circuit est terminé.	archivedestructionrequests	traiteArchiveDestructionRequestPostWorkFlow	f		2016-01-09 01:30:00	P1D	2016-01-08 18:35:12	2016-01-08 18:35:13		SUCCES	t	2012-03-09 09:00:00	1	2016-01-08 18:35:13	1
14	Circuits de traitement : fin de traitement des acquittements des restitutions	Traite acquittements des restitutions pour lesquels le traitement dans le circuit est terminé.	archiverestitutions	traiteArchiveRestitutionPostWorkFlow	f		2016-01-09 01:40:00	P1D	2016-01-08 18:35:13	2016-01-08 18:35:13		SUCCES	t	2012-03-09 09:00:00	1	2016-01-08 18:35:13	1
12	Circuits de traitement : fin de traitement des demandes de communication	Traite les demandes de communication pour lesquelles le traitement dans le circuit est terminé.	archivedeliveryrequests	traiteArchiveDeliveryRequestPostWorkFlow	f		2016-01-09 01:40:00	P1D	2016-01-08 18:35:13	2016-01-08 18:35:14		SUCCES	t	2012-03-09 09:00:00	1	2016-01-08 18:35:14	1
3	Circuits de traitement : prévenir par courriel pour le traitement	Envoi d'un courriel aux utilisateurs d'asalae pour les prévenir qu'un ou plusieurs traitements les attendent dans as@lae	traitements	genererAlerteMailTraitement	f	\N	2016-01-10 00:30:00	P7D	2016-01-08 18:35:15	2016-01-08 18:35:15		SUCCES	t	2011-08-05 11:41:00	1	2016-01-08 18:35:15	1
2	Suppression des lignes du journal des événements déjà exportées	Suppression en base de données des événements hors cycle de vie déjà éxportés.	journaux	supprimeJournal	t	P2Y	2016-02-01 00:20:00	P1M	2016-01-08 18:35:15	2016-01-08 18:35:16		SUCCES	t	2011-08-05 11:41:00	1	2016-01-08 18:35:16	1
13	Vérifications fonctionnement as@lae	Vérifications de l'installation de as@lae ainsi que des composants utilisés par l'application.	techniques	verificationServices	f		2016-01-09 01:40:00	P1D	2016-01-08 18:35:08	2016-01-08 18:35:09		SUCCES	t	2012-03-09 09:00:00	1	2016-01-08 18:35:09	1
15	Notification par courriel des entrées créées automatiquement	Liste les entrées créées automatiquement et envoie cette liste par mail à un ou plusieurs destinataires	archives	envoiParMailEntreesValidationAutomatique	t		2016-01-15 15:00:00	P7D	2016-01-08 18:35:14	2016-01-08 18:35:15		SUCCES	t	2013-08-30 15:00:48	1	2016-01-08 18:35:15	1
18	Mise à jour des transferts d'archive sortants (RESTFUL)	Met à jour le statut des transferts d'archive sortants : Acquittement et décision du SAE distant (via REST)	archivetransfers	traiteArchivetransferSortantUpdate	f	\N	2016-01-08 19:33:25	PT1H	2016-01-08 18:35:16	2016-01-08 18:35:16		SUCCES	t	2014-07-04 11:33:25	1	2016-01-08 18:35:16	1
1	Exportation du journal des événements	Création du fichier d'exportation du journal des événements sous forme d'un fichier xml. Puis transfert dans as@lae.	journaux	exportJournal	f	\N	2016-02-05 00:10:00	P1D	2016-02-04 12:42:09	2016-02-04 12:42:15	Nom du fichier du journal : export_jnl_asalae_20160121.xml. Nombre d'événements exportés : 110<br>Nom du fichier du journal : export_jnl_asalae_20160125.xml. Nombre d'événements exportés : 59<br>Nom du fichier du journal : export_jnl_asalae_20160128.xml. Nombre d'événements exportés : 22<br>Nom du fichier du journal : export_jnl_asalae_20160129.xml. Nombre d'événements exportés : 140<br>Nom du fichier du journal : export_jnl_asalae_20160201.xml. Nombre d'événements exportés : 398<br>Nom du fichier du journal : export_jnl_asalae_20160202.xml. Nombre d'événements exportés : 15<br>Nom du fichier du journal : export_jnl_asalae_20160203.xml. Nombre d'événements exportés : 122<br><br><br>	SUCCES	t	2011-08-05 10:00:00	1	2016-02-04 12:42:15	1
9	Circuits de traitement : fin de traitement des transferts	Traite les transferts pour lesquels le traitement dans le circuit est terminé.	archivetransfers	traiteArchivetransferPostWorkFlow	f		2016-03-07 11:30:00	PT1H	2016-03-07 11:27:11	2016-03-07 11:27:24	<br><br>Acceptation du transfert AT_13 : archive créée avec la cote  : 51 (id:63)<br><br><br>Acceptation du transfert HABITAT76 DOCUMENTS 181783 part 1 : archive créée avec la cote  : 52 (id:64), archive créée avec la cote  : 53 (id:65), archive créée avec la cote  : 54 (id:66), archive créée avec la cote  : 55 (id:67), archive créée avec la cote  : 56 (id:68), archive créée avec la cote  : 57 (id:69), archive créée avec la cote  : 58 (id:70)<br><br>	SUCCES	t	2012-03-09 09:00:00	1	2016-03-07 11:27:24	1
4	Prise en compte asynchrone des transferts.	Prise en compte asynchrone des transferts présents dans le répertoire dépôt, analyse asynchrone des transferts.	archivetransfers	transfertsTraitAsync	f	\N	2016-02-02 00:40:00	P1D	2016-02-01 12:18:39	2016-02-01 12:20:21	traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121815 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121815_01 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121815_02 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816_01 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816_02 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816_03 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816_04 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816_05 : effectué<br>traitement du répertoire /data/asalae16/echanges/entree/depot/20160201_121816_06 : effectué	SUCCES	t	2011-08-31 12:00:00	1	2016-02-01 12:20:21	1
7	Vérification de l'intégrité des fichiers des archives	Vérifie que les empreintes recalculées des fichiers des archives stockés sur des volumes de type FileSystem, correspondent à celles stockées en base de données. Paramètre 1, optionnel, défaut = 500, nombre d'archives traitées par exécution de la tâche. 	archives	checkIntegrityFiles	t	500	2016-03-16 01:10:00	P1D	2016-03-15 08:28:35	2016-03-15 08:28:46	Nombre d'archives traitées : 54<br/>Nombre de fichiers en erreur : 2<br/>Détail des erreurs :<br/>Archive 50 - Test de conversion de fichiers MS Office<br/>&nbsp;&nbsp;- erreur d'intégrité du fichier du document 50_000_002 : fichier PPT.ppt.pdf (fichier converti) stocké sur le volume Volume01 sous la référence /data/asalae16/archives/FRPROD001/2016/toutproducteur/50/conversions/fichier PPT.ppt.pdf : fichier non trouvé :  /data/asalae16/archives/FRPROD001/2016/toutproducteur/50/conversions/fichier PPT.ppt.pdf<br/>&nbsp;&nbsp;- erreur d'intégrité du fichier du document 50_000_002 : fichier PPT.ppt.pdf (fichier converti) stocké sur le volume Volume02 sous la référence /data/asalae16/archives2/FRPROD001/2016/toutproducteur/50/conversions/fichier PPT.ppt.pdf : fichier non trouvé :  /data/asalae16/archives2/FRPROD001/2016/toutproducteur/50/conversions/fichier PPT.ppt.pdf<br/>	WARNING	t	2012-03-09 09:00:00	1	2016-03-15 08:28:46	1
20	Rétention des transferts acceptés : suppression des fichiers	Suppression des fichiers des transferts acceptés une fois le délai de rétention passé. Paramètre 1 : [true, false], optionnel, défaut = true, les archives doivent être intègres avant de pouvoir supprimer les transferts (true).	archivetransfers	supprimerFichiersTransfertsAcceptes	t	true	2016-02-18 11:05:54	P1D	2016-02-18 11:05:54	2016-02-18 11:05:54	Création de la tâche planifiée	SUCCES	f	2016-02-18 11:05:54	1	2016-02-18 11:05:54	1
\.


--
-- Name: adm-crons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-crons_id_seq"', 21, false);


--
-- Data for Name: adm-fichiers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-fichiers" (id, filename, description, pronom_format, mime_code, size_bytes, foreign_model, foreign_key, foreign_type, foreign_order, uri, content, hash_type, hash_value, json_encoded_metadata, created, modified) FROM stdin;
\.


--
-- Name: adm-fichiers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-fichiers_id_seq"', 1, false);


--
-- Data for Name: adm-journaux; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-journaux" (id, date, origine_type, origine_identifiant, origine_nom, evenement_controller, evenement_action, evenement_nom, message, cycle_vie, model, foreign_key, hash_value, exporte, hash_type, cycle_vie_type) FROM stdin;
\.


--
-- Name: adm-journaux_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-journaux_id_seq"', 1, false);


--
-- Data for Name: adm-keywordlists; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-keywordlists" (id, nom, description, version, active, modifiable, keywordtype_id, scheme_id, scheme_name, scheme_agency_name, scheme_version_id, scheme_data_uri, scheme_uri, created, created_user_id, modified, modified_user_id, identifiant) FROM stdin;
1	Thésaurus-matières pour l'indexation des archives locales	Le Thésaurus pour la description et l'indexation des archives locales s'applique à tous les fonds d'archives locales, publiques et privées, anciennes, modernes et contemporaines. Il a valeur réglementaire pour l’ensemble des services d’archives territoriales – régionales, départementales et communales. Il se compose du thésaurus-matières, essentiellement réservé aux expressions illustrant la notion d'objet mais accueillant aussi des termes liés à des attributions essentielles des producteurs d'archives (par exemple : police, fiscalité, aide sociale), ainsi que trois listes d'autorité ("Actions", "Typologie documentaire", "Contexte historique") contenant des descripteurs qui ne sont pas par eux-mêmes des termes d'indexation mais qu'on associera à un ou plusieurs descripteurs du thésaurus, si le contexte documentaire l'exige.	1	t	t	7	THESAURUS-MATIERES	Thésaurus-matières	Service interministériel des Archives de France	2012-11-22	http://data.culture.fr/thesaurus/resource/ark:/67717/Matiere	http://data.culture.fr/thesaurus/resource/ark:/67717/Matiere	2011-05-25 10:02:54	1	2011-05-25 10:02:54	1	67717/Matiere
2	Liste d'autorité « Actions » pour l'indexation des archives locales	La liste d'autorité "Actions" réunit des descripteurs désignant des actions, des modes d'intervention ou des procédures. Ces descripteurs ne sont pas par eux-mêmes des termes d'indexation mais doivent être associés à un ou plusieurs descripteurs du thésaurus-matières, si le contexte documentaire l'exige.	1	t	t	7	LISTE D'AUTORITES « ACTIONS »	Liste d'autorités « Actions »	Service interministériel des Archives de France	2012-11-22	http://data.culture.fr/thesaurus/resource/ark:/67717/Actions	http://data.culture.fr/thesaurus/resource/ark:/67717/Actions	2011-05-25 10:06:25	1	2013-08-07 16:56:26	1	67717/Actions
3	Liste d'autorité « Contexte historique » pour l'indexation des archives locales	La liste d'autorité "Contexte historique" réunit des descripteurs désignant des événements ou des périodes historiques. Ces descripteurs ne sont pas par eux-mêmes des termes d'indexation mais doivent être associés à un ou plusieurs descripteurs du thésaurus-matières, si le contexte documentaire l'exige.	1	t	t	7	LISTE D'AUTORITE « CONTEXTE HISTORIQUE »	Liste d'autorité « Contexte historique »	Service interministériel des Archives de France	2012-11-22	http://data.culture.fr/thesaurus/resource/ark:/67717/Contexte	http://data.culture.fr/thesaurus/resource/ark:/67717/Contexte	2011-05-25 10:09:29	1	2011-05-25 10:09:29	1	67717/Contexte
4	Liste d'autorité « Typologie documentaire » pour l'indexation des archives locales	La liste d'autorité "Typologie documentaire" rassemble des termes renvoyant à des catégories facilement identifiables de documents (par exemple : chrono, registre, plan) ou des termes plus précis mais qui désignent des documents généralement versés et conservés en série (exemple : rôle d'imposition). Ces descripteurs ne sont pas par eux-mêmes des termes d'indexation mais doivent être associés à un ou plusieurs descripteurs du thésaurus-matières, si le contexte documentaire l'exige.	1	t	t	7	LISTE D'AUTORITES « TYPOLOGIE DOCUMENTAIRE »	Liste d'autorités « Typologie documentaire »	Service interministériel des Archives de France	2012-11-22	http://data.culture.fr/thesaurus/resource/ark:/67717/Typologie	http://data.culture.fr/thesaurus/resource/ark:/67717/Typologie	2011-05-25 10:13:13	1	2011-05-25 10:13:13	1	67717/Typologie
\.


--
-- Name: adm-keywordlists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-keywordlists_id_seq"', 5, false);


--
-- Data for Name: adm-keywords; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-keywords" (id, keywordlist_id, version, code, libelle, parent_id, lft, rght, created, created_user_id, modified, modified_user_id, exact_match, change_note) FROM stdin;
1386	1	1	67717/T1-728	produit cosmétique	1197	1484	1485	2013-01-15 16:46:31	1	2013-01-15 16:48:01	1		
662	1	1	67717/T1-1427	circulation urbaine	12	93	94	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb119443950	
137	1	1	67717/T1-376	finances locales	24	617	626	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb11954096q	
117	1	1	67717/T1-4	durée du travail	118	1322	1323	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb11950215g	
1192	1	1	67717/T1-894	sanatorium	800	1412	1413	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb11978600w	
1115	1	1	67717/T1-278	police secours	759	902	903	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		
493	1	1	67717/T1-960	halle	483	1856	1857	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
1210	1	1	67717/T1-1312	emploi réservé	828	1256	1257	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
779	1	1	67717/T1-923	élection cantonale	917	2782	2783	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb12247657x	
1	1	1	67717/T1-503	communications	0	1	144	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1		
13	1	1	67717/T1-915	messagerie	1	98	143	2013-01-15 16:46:21	1	2013-01-15 16:46:35	1	http://data.bnf.fr/ark:/12148/cb119776657	
14	1	1	67717/T1-527	industrie	2	146	209	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11975704t	2011-12-07: ajout du narrower term "artisanat", ancien altLabel de "artisan", suite à l'appel à commentaires d'octobre 2010-mars 2011
2	1	1	67717/T1-543	économie	0	145	422	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1		
15	1	1	67717/T1-1248	commerce	2	210	293	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11971502m	
20	1	1	67717/T1-1405	enseignement agricole	990	2144	2145	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11976564c	
1220	1	1	67717/T1-507	conseil de discipline	540	942	943	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1		
789	1	1	67717/T1-75	formation musicale	790	1600	1601	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb119753292	2011-12-07 : suppression du related term "langue régionale", suite à l'appel à commentaires d'octobre 2010-mars 2011
788	1	1	67717/T1-1369	hérésie	57	2749	2750	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb12005387c	
1209	1	1	67717/T1-1301	adulte handicapé	595	1226	1227	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		2011-12-12 : changement de broader term et suppression des narrower term "invalide de guerre" et "invalide du travail", suite à l'appel à commentaires d'octobre 2010-mars 2011
1102	1	1	67717/T1-602	absentéisme scolaire	48	2209	2210	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb13511167f	
1059	1	1	67717/T1-1109	établissement public national	776	994	995	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1	http://data.bnf.fr/ark:/12148/cb12099004v	
1068	1	1	67717/T1-132	évaluation foncière	608	814	815	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
584	1	1	67717/T1-678	indigent	583	1152	1153	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1		
302	1	1	67717/T1-863	police des cultes	25	739	740	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
1109	1	1	67717/T1-709	mouvement de jeunesse	40	1709	1710	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		
1021	1	1	67717/T1-1223	patrimoine iconographique	796	1624	1625	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
984	1	1	67717/T1-225	hôtel de tourisme	88	1498	1499	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1		
526	1	1	67717/T1-379	enseignement élémentaire	46	2105	2106	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11936354p	
495	1	1	67717/T1-1320	lavoir	483	1860	1861	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb12017858v	
919	1	1	67717/T1-624	élection présidentielle	917	2788	2789	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb11948207k	
481	1	1	67717/T1-496	racisme	476	2658	2659	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://dbpedia.org/resource/Category:Racism	
428	1	1	67717/T1-938	création d'entreprise	16	351	352	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb11935223w	
790	1	1	67717/T1-1311	entreprise de spectacle	38	1599	1608	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		
792	1	1	67717/T1-297	cirque	790	1602	1603	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb11931206b	
793	1	1	67717/T1-689	compagnie chorégraphique	790	1604	1605	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb12220743d	
794	1	1	67717/T1-1180	compagnie théâtrale	790	1606	1607	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb119459518	
513	1	1	67717/T1-457	production électrique	18	395	402	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
791	1	1	67717/T1-219	société de télédiffusion privée	1247	114	115	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		
477	1	1	67717/T1-1239	droits de l'homme	56	2661	2662	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb13318689g	
718	1	1	67717/T1-1215	publicité foncière	608	812	813	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb11974650g	
475	1	1	67717/T1-867	reconduite à la frontière	295	708	709	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb119719826	
738	1	1	67717/T1-283	oléagineux	633	538	539	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb11938307n	
204	1	1	67717/T1-616	biens régionaux	151	642	643	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
1218	1	1	67717/T1-353	fonctionnaire territorial	540	938	939	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1	http://data.bnf.fr/ark:/12148/cb11985734z	
1018	1	1	67717/T1-889	patrimoine numérique	796	1618	1619	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
511	1	1	67717/T1-1159	décharge publique	505	1954	1955	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		2011-12-07 : ajout du altLabel "déchetterie" suite à l'appel à commentaires d'octobre 2010-mars 2011
781	1	1	67717/T1-232	enseignement religieux	47	2129	2130	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb11939990x	
780	1	1	67717/T1-171	élection municipale	917	2784	2785	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb12403766q	
528	1	1	67717/T1-1306	enseignement secondaire	46	2109	2110	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb119761063	
699	1	1	67717/T1-721	conseiller général	693	2770	2771	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb12653596g	
1222	1	1	67717/T1-1071	concours administratif	540	946	947	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1	http://data.bnf.fr/ark:/12148/cb11952286g	
474	1	1	67717/T1-417	clandestin	295	706	707	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
591	1	1	67717/T1-466	assurance sociale	32	1199	1214	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
848	1	1	67717/T1-127	ouvrier	35	1337	1338	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1		
917	1	1	67717/T1-107	élection politique	58	2779	2798	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb133188593	
716	1	1	67717/T1-964	construction navale	183	166	167	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb119361575	
1064	1	1	67717/T1-1435	administration communale	776	1004	1005	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1	http://data.bnf.fr/ark:/12148/cb119340202	
535	1	1	67717/T1-810	artisan	424	334	335	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb119356997	2011-12-07 : suppression des altLabel "artisanat" et "métier d'art" et ajout du related term "artisanat" suite à l'appel à commentaires d'octobre 2010-mars 2011
815	1	1	67717/T1-1097	restructuration urbaine	813	1886	1887	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb131626960	
771	1	1	67717/T1-594	regroupement de communes	145	918	919	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		
772	1	1	67717/T1-1449	département	145	920	921	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb120986404	
773	1	1	67717/T1-199	coopération interrégionale	145	922	923	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		
774	1	1	67717/T1-1238	établissement public de coopération\n\t\t\tintercommunale	145	924	925	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		2011-12-06: ajout des altlabel "communauté d'agglomération" et "communauté urbaine" et du related term "finances intercommunales" suite à l'appel à commentaires d'octobre 2010-mars 2011
775	1	1	67717/T1-317	commune	145	926	927	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb119499021	
1227	1	1	67717/T1-598	prostitution	297	716	717	2013-01-15 16:46:29	1	2013-01-15 16:47:49	1	http://dbpedia.org/resource/Category:Prostitution	
162	1	1	67717/T1-1293	mobilisation	60	2901	2902	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb120096039	
161	1	1	67717/T1-770	milices	60	2893	2900	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb119486515	
438	1	1	67717/T1-545	transport de corps	294	698	699	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1		
982	1	1	67717/T1-596	établissement artisanal	420	314	315	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1		
983	1	1	67717/T1-844	baux commerciaux	420	316	317	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb11934079q	
248	1	1	67717/T1-516	agent de change	15	279	280	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb11934025s	
834	1	1	67717/T1-611	licenciement	33	1271	1274	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb12036222c	
50	1	1	67717/T1-892	justice civile	9	2248	2293	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
964	1	1	67717/T1-1234	label de qualité	17	387	388	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1		
1067	1	1	67717/T1-621	administration préfectorale	776	1010	1011	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
736	1	1	67717/T1-64	plante tinctoriale	633	534	535	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://dbpedia.org/resource/Category:Plant_dyes	
185	1	1	67717/T1-81	industrie électronique	14	171	174	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb11950233d	
595	1	1	67717/T1-1495	personne handicapée	32	1221	1230	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		2011-12-12: ajout du concept "personne handicapée" avec comme broader term "protection sociale" et comme narrower "adulte handicapé", "enfant handicapé", "invalide de guerre" et "invalide du travail", suite à l'appel à commentaires d'octobre 2010-mars 2011
147	1	1	67717/T1-1070	produit domanial	151	628	629	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1		
192	1	1	67717/T1-873	industrie du verre	14	201	202	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb11941231r	
194	1	1	67717/T1-913	métallurgie	14	205	206	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb11932350b	
321	1	1	67717/T1-1455	exploitation minière	182	162	163	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		2011-04-19: Suppression du terme non-descripteurs "mine" suite à une remarque de l'éditeur GAIA
1167	1	1	67717/T1-1197	théologie	782	2716	2717	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://data.bnf.fr/ark:/12148/cb11951726r	
717	1	1	67717/T1-60	notaire	254	2298	2299	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb11951013g	
1261	1	1	67717/T1-1014	victime d'infraction	52	2421	2422	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1	http://data.bnf.fr/ark:/12148/cb133188771	
1263	1	1	67717/T1-1084	qualification criminelle	52	2425	2426	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
1264	1	1	67717/T1-1381	contravention	52	2427	2428	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1	http://data.bnf.fr/ark:/12148/cb11971860m	
694	1	1	67717/T1-685	intervention d'élu	275	2520	2521	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
828	1	1	67717/T1-1027	travailleur handicapé	33	1253	1258	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb11950209j	
398	1	1	67717/T1-1436	prise d'otage	379	2396	2397	2013-01-15 16:46:24	1	2013-01-15 16:46:55	1		
399	1	1	67717/T1-992	dégradation de biens	379	2398	2399	2013-01-15 16:46:24	1	2013-01-15 16:46:55	1		
1239	1	1	67717/T1-792	instruction civique	989	2136	2137	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb119569104	
1240	1	1	67717/T1-1398	enseignement ménager	989	2138	2139	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1		
749	1	1	67717/T1-967	séminaire religieux	743	2696	2697	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
545	1	1	67717/T1-386	graphologie	542	2818	2819	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11936595t	
546	1	1	67717/T1-901	spiritisme	542	2820	2821	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb119367585	
929	1	1	67717/T1-175	assemblée nationale	703	954	955	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
547	1	1	67717/T1-153	accession à la propriété	68	1736	1737	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb120447187	
1196	1	1	67717/T1-1423	organisation sanitaire	36	1463	1476	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		
1237	1	1	67717/T1-208	éducation sexuelle	989	2132	2133	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb11935543b	
1238	1	1	67717/T1-433	langue étrangère	989	2134	2135	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1		
667	1	1	67717/T1-1061	élection professionnelle	675	2754	2755	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1		
648	1	1	67717/T1-217	bateau de plaisance	12	49	50	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1		
649	1	1	67717/T1-1273	transport en commun	12	51	54	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb11933623h	
650	1	1	67717/T1-267	automobiliste	12	55	56	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb119803141	
651	1	1	67717/T1-1406	aéronef	12	57	58	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb119756694	
1061	1	1	67717/T1-1044	administration centrale	776	998	999	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1	http://data.bnf.fr/ark:/12148/cb12264189v	
1228	1	1	67717/T1-1347	établissement interdit aux mineurs	297	718	719	2013-01-15 16:46:29	1	2013-01-15 16:47:49	1		
737	1	1	67717/T1-447	plante textile	633	536	537	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1		
665	1	1	67717/T1-176	machine dangereuse	935	1342	1343	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1		
666	1	1	67717/T1-51	prud'homme	539	2308	2309	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb11950087s	
668	1	1	67717/T1-313	juge administratif	539	2310	2311	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb11951208f	
669	1	1	67717/T1-372	juge consulaire	539	2312	2313	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb12010258h	
670	1	1	67717/T1-409	officier de justice d'Ancien Régime	539	2314	2315	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1		
1262	1	1	67717/T1-1458	délinquance	52	2423	2424	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
347	1	1	67717/T1-1339	mouvement étudiant	330	2620	2621	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
304	1	1	67717/T1-522	police des eaux	25	755	756	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
1207	1	1	67717/T1-930	impôt social	609	824	825	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
1121	1	1	67717/T1-1024	concentration	963	384	385	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1		
977	1	1	67717/T1-1389	aliment du bétail	634	562	563	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1		
1276	1	1	67717/T1-565	hygiène alimentaire	506	1378	1379	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb11937798n	
1233	1	1	67717/T1-1481	intempéries	763	1978	1979	2013-01-15 16:46:30	1	2013-01-15 16:47:49	1		2011-12-07: ajout du concept "intempéries" avec comme broader term "météorologie", suite à l'appel à commentaires d'octobre 2010-mars 2011
991	1	1	67717/T1-610	pédagogie	47	2155	2160	2013-01-15 16:46:27	1	2013-01-15 16:47:32	1	http://dbpedia.org/resource/Category:Pedagogy	
992	1	1	67717/T1-798	éducation sportive	47	2161	2162	2013-01-15 16:46:27	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb131627112	
993	1	1	67717/T1-120	bombardement	62	2939	2940	2013-01-15 16:46:27	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb11958673p	
342	1	1	67717/T1-1177	mouvement écologiste	330	2610	2611	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1		
1257	1	1	67717/T1-945	service radio électrique	13	135	142	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
1256	1	1	67717/T1-592	radiodiffusion	13	131	134	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1	http://data.bnf.fr/ark:/12148/cb119604993	
1255	1	1	67717/T1-1184	poste	13	123	130	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1	http://data.bnf.fr/ark:/12148/cb11934544f	
1014	1	1	67717/T1-455	légion étrangère	159	2876	2877	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://dbpedia.org/resource/Category:French_Foreign_Legion	
1036	1	1	67717/T1-1395	pêche maritime	641	606	607	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1		
1234	1	1	67717/T1-207	émancipation	899	1306	1307	2013-01-15 16:46:30	1	2013-01-15 16:47:49	1	http://data.bnf.fr/ark:/12148/cb120104013	
811	1	1	67717/T1-467	enfant en garde	589	1180	1181	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb12302524t	
1259	1	1	67717/T1-264	jury d'assises	52	2417	2418	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1	http://data.bnf.fr/ark:/12148/cb13734692h	
940	1	1	67717/T1-112	recette non fiscale	211	666	667	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1		
871	1	1	67717/T1-1355	énergie éolienne	867	414	415	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://dbpedia.org/resource/Category:Wind_power	
869	1	1	67717/T1-775	énergie solaire	867	410	411	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb11931977s	
560	1	1	67717/T1-330	création audiovisuelle	557	1588	1589	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1		
561	1	1	67717/T1-360	littérature	557	1590	1591	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://dbpedia.org/resource/Category:Literature	2011-12-08 : ajout du altLabel "poésie", suite à l'appel à commentaires d'octobre 2010-mars 2011
131	1	1	67717/T1-451	autoroute	126	1808	1809	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb119310012	
1108	1	1	67717/T1-1474	internat	48	2221	2222	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		2011-12-07: ajout du concept "internat", suite à l'appel à commentaires d'octobre 2010-mars 2011
1164	1	1	67717/T1-165	miracle	782	2710	2711	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://data.bnf.fr/ark:/12148/cb11934605t	
1221	1	1	67717/T1-657	agent non titulaire	540	944	945	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1	http://data.bnf.fr/ark:/12148/cb12036307r	
326	1	1	67717/T1-406	association familiale	324	1148	1149	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb11967797x	
740	1	1	67717/T1-1049	tabac	633	542	543	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb11935399d	
689	1	1	67717/T1-742	prestation d'aide sociale légale	588	1170	1171	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		2011-12-07 : modification du prefLabel de "aide sociale légale" en "prestation d'aide sociale légale" et ajout des altLabel "allocation personnalisée d'autonomie" et "revenu de solidarité active", suite à l'appel à commentaires d'octobre 2010-mars 2011
899	1	1	67717/T1-489	droit de la famille	34	1303	1312	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://dbpedia.org/resource/Category:Family_law	2011-12-12 : suppression du related term "droits de succession" et ajout du narrower term "succession", suite à l'appel à commentaires d'octobre 2010-mars 2011
1267	1	1	67717/T1-239	hommage	455	768	769	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1	http://data.bnf.fr/ark:/12148/cb123113158	
1248	1	1	67717/T1-850	handisport	305	1674	1675	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb120296716	
1249	1	1	67717/T1-1368	association sportive	305	1676	1677	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1		
1250	1	1	67717/T1-227	entreprise du bâtiment	71	1748	1749	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
1251	1	1	67717/T1-292	sondage géologique	71	1750	1751	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
1252	1	1	67717/T1-595	chantier	71	1752	1753	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
1253	1	1	67717/T1-914	participation des employeurs	71	1754	1755	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1	http://data.bnf.fr/ark:/12148/cb12130301d	
1304	1	1	67717/T1-1082	service financier de la poste	1255	124	125	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb12335702d	
1307	1	1	67717/T1-281	judaïsme	784	2724	2725	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11953375b	
1303	1	1	67717/T1-263	caisse d'épargne	421	320	321	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11934100q	
1305	1	1	67717/T1-622	prêteur	421	322	323	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1		
1306	1	1	67717/T1-1386	banque mutualiste	421	324	325	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1		
1318	1	1	67717/T1-271	garde à vue	299	724	725	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1	http://data.bnf.fr/ark:/12148/cb122720074	
1299	1	1	67717/T1-1304	jeu de cartes	1296	1718	1719	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		
1300	1	1	67717/T1-1309	appareil de jeu	1296	1720	1721	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		
1384	1	1	67717/T1-1444	éducation surveillée	598	2278	2279	2013-01-15 16:46:31	1	2013-01-15 16:48:01	1	http://data.bnf.fr/ark:/12148/cb133191253	
1382	1	1	67717/T1-1402	manufacture	190	196	197	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1	http://data.bnf.fr/ark:/12148/cb15565552s	
1383	1	1	67717/T1-1279	internement administratif	995	2948	2949	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		
1355	1	1	67717/T1-1338	lieutenant de louveterie	300	734	735	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1		
1359	1	1	67717/T1-293	écobuage	173	462	463	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1	http://data.bnf.fr/ark:/12148/cb12126869n	
1360	1	1	67717/T1-323	agriculture biologique	173	464	465	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1	http://dbpedia.org/resource/Category:Sustainable_agriculture	
1361	1	1	67717/T1-582	amélioration des sols	173	466	467	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1		
1362	1	1	67717/T1-1026	assolement	173	468	469	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1	http://data.bnf.fr/ark:/12148/cb12049583b	
1363	1	1	67717/T1-1360	battage	173	470	471	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1	http://data.bnf.fr/ark:/12148/cb12198458t	
1364	1	1	67717/T1-1370	contrebande	613	848	849	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1	http://data.bnf.fr/ark:/12148/cb13318522x	
1410	1	1	67717/T1-531	pisciculture	640	600	601	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb11933140q	
1411	1	1	67717/T1-890	conchyliculture	640	602	603	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb119317657	
1412	1	1	67717/T1-1067	poisson	641	610	611	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb11975896k	
1428	1	1	67717/T1-853	droits réservés	617	862	863	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
1458	1	1	67717/T1-846	commerçant	424	338	339	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1	http://data.bnf.fr/ark:/12148/cb119387230	
1433	1	1	67717/T1-479	servitude aéronautique	656	68	69	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1		
1435	1	1	67717/T1-859	navigation aérienne	656	70	71	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1	http://data.bnf.fr/ark:/12148/cb119326040	
1436	1	1	67717/T1-1236	ligne aérienne	656	72	73	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1	http://data.bnf.fr/ark:/12148/cb11970232g	
1437	1	1	67717/T1-482	logiciel informatique	1254	118	119	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1	http://data.bnf.fr/ark:/12148/cb133183707	
1439	1	1	67717/T1-573	animal de bât	657	76	77	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1		
1440	1	1	67717/T1-955	véhicule à traction animale	657	78	79	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1	http://data.bnf.fr/ark:/12148/cb119364001	
1408	1	1	67717/T1-533	affaire familiale	599	2282	2283	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1		
1413	1	1	67717/T1-428	élève	1099	2196	2197	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb119772712	
1449	1	1	67717/T1-845	industrie papetière	149	148	149	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1464	1	1	67717/T1-1469	archéologie du sol	98	1552	1553	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1		2011-12-07: ajout du concept "archéologie du sol", suite à l'appel à commentaires d'octobre 2010-mars 2011
1465	1	1	67717/T1-547	distribution postale	1255	126	127	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1		
1466	1	1	67717/T1-1416	tri postal	1255	128	129	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1	http://data.bnf.fr/ark:/12148/cb119540763	
1467	1	1	67717/T1-705	liaison routière internationale	659	84	85	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1		
1470	1	1	67717/T1-774	habitat périurbain	76	1778	1779	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1		
1468	1	1	67717/T1-1003	poids lourd	659	86	87	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1	http://data.bnf.fr/ark:/12148/cb11931102p	
1471	1	1	67717/T1-1230	centre ville	76	1780	1781	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb11936078v	
12	1	1	67717/T1-548	transport	1	2	97	2013-01-15 16:46:21	1	2013-01-15 16:46:35	1	http://dbpedia.org/resource/Category:Transportation	
1472	1	1	67717/T1-1065	habitation à loyer modéré	79	1792	1793	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb119481048	
1469	1	1	67717/T1-1076	signalisation routière	128	4	5	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb11950066t	
1476	1	1	67717/T1-1075	transhumance	175	484	485	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb11938338x	
1075	1	1	67717/T1-190	crèche	810	1424	1425	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1		
1494	1	1	67717/T1-1476	histoire locale	108	2048	2049	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1		2011-12-07: ajout du concept "histoire locale", suite à l'appel à commentaires d'octobre 2010-mars 2011
1488	1	1	67717/T1-1424	gitan	31	1139	1140	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1		
1473	1	1	67717/T1-1336	station de radiodiffusion privée	1256	132	133	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1		
1479	1	1	67717/T1-711	publicité mensongère	249	282	283	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1	http://data.bnf.fr/ark:/12148/cb11983387z	
1477	1	1	67717/T1-692	appareil radio électrique	1257	138	139	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb119327199	
1478	1	1	67717/T1-1445	station radio électrique privée	1257	140	141	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1		
1489	1	1	67717/T1-1153	mouvement populaire	288	2566	2567	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1		2011-12-12 : changement de broader term ("manifestation publique" au lieu de "réunion publique") et ajout du altlabel "émeute", suite à l'appel à commentaires d'octobre 2010-mars 2011
1495	1	1	67717/T1-1477	sciences auxiliaires de l'histoire	108	2050	2051	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1	http://data.bnf.fr/ark:/12148/cb133191133	2011-12-07: ajout du concept "sciences auxiliaires de l'histoire", suite à l'appel à commentaires d'octobre 2010-mars 2011
1483	1	1	67717/T1-941	marché d'intérêt national	242	264	265	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1		
1490	1	1	67717/T1-947	soldes	251	288	289	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1		
1491	1	1	67717/T1-1195	abri	1004	2988	2989	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1	http://data.bnf.fr/ark:/12148/cb125686186	
1492	1	1	67717/T1-1249	aide sociale facultative	588	1176	1177	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1		
1496	1	1	67717/T1-1490	manifestation antinationale	288	2568	2569	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1		2011-12-12: ajout du concept "manifestation antinationale" avec comme broader term "manifestation publique" et comme altLabel "menée antinationale", "propos antinational" et "propos défaitiste", suite à l'appel à commentaires d'octobre 2010-mars 2011
1498	1	1	67717/T1-1488	deuil public	273	2504	2505	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1		2011-12-12: ajout du concept "deuil public" avec comme broader term "cérémonie publique", suite à l'appel à commentaires d'octobre 2010-mars 2011
1480	1	1	67717/T1-1118	incapable majeur	601	2290	2291	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1		
1493	1	1	67717/T1-1475	histoire des familles	108	2046	2047	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1		2011-12-07: ajout du concept "histoire des familles" avec comme broader term "recherche historique" et comme related term "armoiries", suite à l'appel à commentaires d'octobre 2010-mars 2011
5	1	1	67717/T1-307	société	0	1081	1492	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb12009547n	
6	1	1	67717/T1-1359	temps libre et sociabilité	0	1493	1728	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1		
7	1	1	67717/T1-1262	équipement	0	1729	2020	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb13163006s	
8	1	1	67717/T1-1302	éducation et sciences	0	2021	2224	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb13318807v	2011-12-07 : modification du prefLabel "éducation" en "éducation et sciences"
11	1	1	67717/T1-150	extérieur	0	2833	2994	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1		
632	1	1	67717/T1-1167	protection sanitaire du cheptel	23	525	532	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1		2011-12-06: ajout du altlabel "vaccination des animaux" suite à l'appel à commentaires d'octobre 2010-mars 2011
633	1	1	67717/T1-1325	culture industrielle	23	533	546	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1		
634	1	1	67717/T1-1013	élevage	23	547	568	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb11931346v	2011-12-07: Ajout du narrower term "équidé" suite à l'appel à commentaires d'octobre 2010-mars 2011
63	1	1	67717/T1-1	habitat rural	41	1731	1732	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
1266	1	1	67717/T1-1415	maison familiale de vacances	368	1522	1523	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
763	1	1	67717/T1-663	météorologie	44	1975	1980	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb11932496x	2011-12-07: ajout des narrower terms "canicule" et "intempéries" et du related term "catastrophe naturelle" et "calamité agricole", suite à l'appel à commentaires d'octobre 2010-mars 2011
524	1	1	67717/T1-198	carte scolaire	46	2095	2096	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb15116070v	
525	1	1	67717/T1-251	enseignement supérieur	46	2097	2104	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11939974p	
914	1	1	67717/T1-521	commerçant étranger	424	336	337	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb11980271k	
580	1	1	67717/T1-1271	population urbaine	31	1133	1134	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb12540479h	
581	1	1	67717/T1-1314	démographie	31	1135	1136	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb11934148q	
582	1	1	67717/T1-1191	rapatrié	31	1137	1138	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1		
901	1	1	67717/T1-819	droit des obligations	34	1315	1318	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
1297	1	1	67717/T1-348	loterie	1296	1714	1715	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://data.bnf.fr/ark:/12148/cb12653241c	
325	1	1	67717/T1-1096	association socio-éducative	324	1146	1147	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1		
572	1	1	67717/T1-404	personne âgée	31	1117	1118	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb119329500	
944	1	1	67717/T1-993	dette publique	211	674	675	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb119719152	
805	1	1	67717/T1-1400	carnaval	802	1688	1689	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb11936675g	
645	1	1	67717/T1-1374	construction automobile	188	188	189	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb119470320	
364	1	1	67717/T1-1098	corporation	361	302	303	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb119341419	
355	1	1	67717/T1-174	bonapartisme	330	2636	2637	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb11947138b	
356	1	1	67717/T1-1218	mouvement féministe	330	2638	2639	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
357	1	1	67717/T1-803	mouvement autonomiste	330	2640	2641	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
358	1	1	67717/T1-115	mouvement homosexuel	330	2642	2643	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
366	1	1	67717/T1-37	tourisme fluvial	37	1513	1514	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb12317720d	
934	1	1	67717/T1-111	intéressement des travailleurs	35	1339	1340	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb11950051s	
295	1	1	67717/T1-1269	surveillance du territoire	25	701	710	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		2011-12-06: ajout du altlabel "surveillance des personnes" et du narrower term "police des frontières" suite à l'appel à commentaires d'octobre 2010-mars 2011
305	1	1	67717/T1-639	sport	40	1665	1678	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1	http://data.bnf.fr/ark:/12148/cb133188907	
306	1	1	67717/T1-886	conflit du travail	937	1360	1361	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1	http://data.bnf.fr/ark:/12148/cb119344832	
292	1	1	67717/T1-606	visite officielle	273	2492	2493	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb11971592d	
1313	1	1	67717/T1-758	travaux forcés	1309	2452	2453	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb13319111d	
1314	1	1	67717/T1-746	peine de substitution	1309	2454	2455	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11995925r	
158	1	1	67717/T1-167	organisation de l'armée	60	2859	2872	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb126497848	2011-12-07: ajout du narrower term "remonte militaire" et des related "cérémonie militaire" et "justice militaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
38	1	1	67717/T1-786	culture	6	1544	1647	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11931827z	
602	1	1	67717/T1-97	société mutualiste	587	1158	1159	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb11977120w	
603	1	1	67717/T1-230	organisme de sécurité sociale	587	1160	1161	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
604	1	1	67717/T1-47	fiscalité directe d'Ancien Régime	27	789	790	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
576	1	1	67717/T1-411	famille	31	1125	1126	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb119339867	
402	1	1	67717/T1-1156	faux en écriture	379	2404	2405	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1		
605	1	1	67717/T1-193	contentieux fiscal	597	2266	2267	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb119678856	
1280	1	1	67717/T1-1199	restauration scolaire	728	2174	2175	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1		2011-12-07: ajout du related term "restauration collective", suite à l'appel à commentaires d'octobre 2010-mars 2011
963	1	1	67717/T1-939	police économique	17	381	386	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1		
870	1	1	67717/T1-973	énergie de la mer	867	412	413	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1		
1008	1	1	67717/T1-744	équipement militaire	158	2862	2863	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb131627937	
1051	1	1	67717/T1-340	lot de pêche	1038	1702	1703	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
1010	1	1	67717/T1-506	état-major	158	2866	2867	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
277	1	1	67717/T1-473	personnalité	54	2531	2532	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb119330753	
324	1	1	67717/T1-200	action sociale	32	1143	1150	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb13318308t	
579	1	1	67717/T1-910	harki	31	1131	1132	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb11954158f	
1311	1	1	67717/T1-388	amende	1309	2448	2449	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11973182g	
1312	1	1	67717/T1-613	galères	1309	2450	2451	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11933049j	
922	1	1	67717/T1-401	élection au conseil d'arrondissement	917	2794	2795	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
880	1	1	67717/T1-768	télex	877	108	109	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1		
786	1	1	67717/T1-820	édifice cultuel	57	2745	2746	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		2011-12-08 : suppression du altLabel "abbaye", suppression des related "édifice classé" et "monument historique" et ajout du related "patrimoine architectural", suite à l'appel à commentaires d'octobre 2010-mars 2011
661	1	1	67717/T1-1022	aérodrome	12	91	92	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb11936051t	
663	1	1	67717/T1-1428	gare	12	95	96	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb119470471	
1127	1	1	67717/T1-1319	action sanitaire	36	1437	1462	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1		2011-12-13 : ajout du altLabel "hygiène sociale" et ajout des narrower term "maladie mentale", "toxicomanie" et "soins médicaux", suite à l'appel à commentaires d'octobre 2010-mars 2011
619	1	1	67717/T1-791	recouvrement fiscal	27	867	868	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
620	1	1	67717/T1-1387	timbre fiscal	27	869	870	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
621	1	1	67717/T1-1165	fiscalité directe révolutionnaire	27	871	872	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
622	1	1	67717/T1-1123	contribuable	27	873	874	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1		
625	1	1	67717/T1-1089	barrage hydroélectrique	513	398	399	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1		
623	1	1	67717/T1-48	ouvrage d'art	42	1825	1826	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1	http://data.bnf.fr/ark:/12148/cb12331207s	
624	1	1	67717/T1-277	infrastructure portuaire	629	16	17	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1		
629	1	1	67717/T1-231	port	12	15	32	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1	http://data.bnf.fr/ark:/12148/cb13318424b	
335	1	1	67717/T1-919	fascisme	330	2596	2597	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb13318764d	
1286	1	1	67717/T1-1006	école militaire	160	2888	2889	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb13507931w	
443	1	1	67717/T1-437	agent immobilier	75	1774	1775	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb119377541	
801	1	1	67717/T1-79	fête foraine	802	1684	1685	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb12007620d	
829	1	1	67717/T1-234	offre d'emploi	33	1259	1260	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb11961355f	
831	1	1	67717/T1-520	cumul d'emploi	33	1263	1264	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
1241	1	1	67717/T1-1260	travail manuel	989	2140	2141	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb11971535k	
872	1	1	67717/T1-94	houille	186	178	179	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb11977233h	
1330	1	1	67717/T1-1289	hôtel de la préfecture	777	1028	1029	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1		
1331	1	1	67717/T1-279	transfusion sanguine	1196	1468	1469	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb122349314	
1332	1	1	67717/T1-730	installation radiologique	1196	1470	1471	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb119582878	
1333	1	1	67717/T1-1085	vaccination	1196	1472	1473	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb11933743d	
935	1	1	67717/T1-1264	sécurité du travail	35	1341	1350	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb11933251n	
936	1	1	67717/T1-480	profession particulière	35	1351	1358	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1		
307	1	1	67717/T1-936	entrée solennelle	273	2494	2495	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
933	1	1	67717/T1-159	conseil municipal	703	960	961	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb120170904	2011-12-06: ajout du altlabel "délégation spéciale" suite à l'appel à commentaires d'octobre 2010-mars 2011
174	1	1	67717/T1-1099	exploitation agricole	21	473	482	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb11950185c	
175	1	1	67717/T1-743	vie pastorale	21	483	486	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
176	1	1	67717/T1-925	matériel agricole	21	487	488	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb11934027g	
184	1	1	67717/T1-203	industrie textile	14	169	170	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb119336078	
285	1	1	67717/T1-1124	baptême civil	54	2555	2556	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
1335	1	1	67717/T1-958	apprentissage	990	2150	2151	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb11948371z	
226	1	1	67717/T1-1119	arsenal	155	2846	2847	2013-01-15 16:46:22	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11952358h	
189	1	1	67717/T1-1145	industrie chimique	14	193	194	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb119344476	
19	1	1	67717/T1-764	population rurale	31	1083	1086	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11970930q	
21	1	1	67717/T1-1134	économie rurale	3	424	491	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1		2011-12-06: Ajout du narrower term "concours agricole" suite à l'appel à commentaires d'octobre 2010-mars 2011
169	1	1	67717/T1-211	drainage	165	454	455	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb11931899t	
172	1	1	67717/T1-1324	usages agricoles locaux	21	459	460	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
207	1	1	67717/T1-383	domaine immobilier	151	648	649	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
1039	1	1	67717/T1-425	pêche en eau douce	641	608	609	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb12466006w	
311	1	1	67717/T1-1486	cérémonie militaire	273	2502	2503	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		2011-12-07: ajout du concept "cérémonie militaire" avec comme broader term "cérémonie publique" et comme related "organisation de l'armée", suite à l'appel à commentaires d'octobre 2010-mars 2011
1291	1	1	67717/T1-519	garde nationale	161	2894	2895	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://data.bnf.fr/ark:/12148/cb12049667c	
351	1	1	67717/T1-1390	radicalisme	330	2628	2629	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb119482156	
352	1	1	67717/T1-1322	extrême droite	330	2630	2631	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb133186377	
353	1	1	67717/T1-1207	extrême gauche	330	2632	2633	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb11948045k	
168	1	1	67717/T1-133	irrigation	165	452	453	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://dbpedia.org/resource/Category:Irrigation	
1354	1	1	67717/T1-461	animal nuisible	638	594	595	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
557	1	1	67717/T1-608	art	38	1581	1598	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb11934758p	
46	1	1	67717/T1-499	organisation scolaire	8	2060	2123	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		2011-12-07: ajout du narrower term "chef d'établissement scolaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
47	1	1	67717/T1-881	enseignement	8	2124	2163	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb11934463f	
1236	1	1	67717/T1-1494	succession	899	1310	1311	2013-01-15 16:46:30	1	2013-01-15 16:47:49	1	http://data.bnf.fr/ark:/12148/cb12011895m	2011-12-12: ajout du concept "succession" avec comme broader term "droit de la famille" et comme related "droits de succession", suite à l'appel à commentaires d'octobre 2010-mars 2011
136	1	1	67717/T1-1079	région	145	916	917	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1		
216	1	1	67717/T1-1461	pompes funèbres	294	688	689	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11940090g	2011-12-06: Ajout du concept "pompes funèbres" suite à l'appel à commentaires d'octobre 2010-mars 2011
151	1	1	67717/T1-11	propriété publique	24	627	654	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1		
229	1	1	67717/T1-1137	commissaire priseur	254	2296	2297	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11991359f	2011-12-13 : ajout du related "commissaire priseur" et suppression du related "objet d'art", suite à l'appel à commentaires d'octobre 2010-mars 2011
459	1	1	67717/T1-252	droits d'usage forestiers	22	505	508	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
341	1	1	67717/T1-1278	nationalisme	330	2608	2609	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb11937474t	
344	1	1	67717/T1-526	communisme	330	2614	2615	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb133408542	
345	1	1	67717/T1-605	gaullisme	330	2616	2617	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb119315483	
349	1	1	67717/T1-391	mouvement ouvrier	330	2624	2625	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://dbpedia.org/resource/Category:Labor_movement	
350	1	1	67717/T1-266	libéralisme	330	2626	2627	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb119542174	
396	1	1	67717/T1-759	faux monnayage	379	2392	2393	2013-01-15 16:46:24	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb12360299n	
397	1	1	67717/T1-877	atteinte à la dignité des personnes	379	2394	2395	2013-01-15 16:46:24	1	2013-01-15 16:46:55	1		
313	1	1	67717/T1-18	chanson subversive	312	2576	2577	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
200	1	1	67717/T1-1051	biens nationaux	151	634	635	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb119745239	2011-12-06: ajout de l'altlabel "séquestre révolutionnaire" suite à l'appel à commentaires d'octobre 2010-mars 2011
167	1	1	67717/T1-242	barrage hydraulique	165	450	451	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb119470351	
1324	1	1	67717/T1-351	cité administrative	777	1016	1017	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1		
1325	1	1	67717/T1-356	palais de justice	777	1018	1019	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1	http://dbpedia.org/resource/Category:Courthouses	
676	1	1	67717/T1-52	politique de la montagne	678	362	363	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb12098635j	
1302	1	1	67717/T1-814	jeu-concours	40	1725	1726	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://data.bnf.fr/ark:/12148/cb12652391z	
1295	1	1	67717/T1-586	titre universitaire	525	2102	2103	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://dbpedia.org/resource/Category:Academic_degrees	
1282	1	1	67717/T1-865	résidence universitaire	728	2178	2179	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb119813028	
571	1	1	67717/T1-191	suicide	31	1115	1116	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb11946657r	
609	1	1	67717/T1-1376	fiscalité des personnes	27	821	828	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
677	1	1	67717/T1-334	montagne	44	1957	1960	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb119323641	
1348	1	1	67717/T1-1143	cartographie	44	2005	2008	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1	http://data.bnf.fr/ark:/12148/cb11936066v	
1349	1	1	67717/T1-971	travaux d'utilité publique	44	2009	2014	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
1352	1	1	67717/T1-625	spécialité pharmaceutique	1197	1480	1481	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
445	1	1	67717/T1-617	chauffeur de taxi	12	7	8	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb119316967	
541	1	1	67717/T1-39	ésotérisme	542	2812	2813	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb131626662	
542	1	1	67717/T1-172	sciences parallèles	59	2811	2822	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb13174059t	
692	1	1	67717/T1-1237	conseil régional	703	950	951	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
577	1	1	67717/T1-426	sans domicile fixe	31	1127	1128	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb133192210	
812	1	1	67717/T1-1284	enfant naturel	899	1304	1305	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb13774173p	
813	1	1	67717/T1-83	opération d'urbanisme	43	1883	1890	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1		
534	1	1	67717/T1-1468	chef d'établissement scolaire	46	2121	2122	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1		2011-12-07: ajout du concept "chef d'établissement scolaire" avec les altLabel "principal" et "proviseur", suite à l'appel à commentaires d'octobre 2010-mars 2011
1358	1	1	67717/T1-772	logement collectif	73	1766	1767	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1	http://data.bnf.fr/ark:/12148/cb11932066w	
446	1	1	67717/T1-587	coiffeur	239	246	247	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb119757144	
447	1	1	67717/T1-644	boucherie	238	236	237	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb11941361z	
448	1	1	67717/T1-733	détective privé	239	248	249	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb13340880m	
211	1	1	67717/T1-747	comptabilité publique	24	665	678	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11971989m	
1195	1	1	67717/T1-1330	assurance maladie	591	1204	1205	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		
80	1	1	67717/T1-1448	logement de fonction	41	1795	1796	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb135075115	
73	1	1	67717/T1-290	logement	41	1759	1768	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://dbpedia.org/resource/Category:Housing	
74	1	1	67717/T1-1170	amélioration de l'habitat	41	1769	1772	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
178	1	1	67717/T1-944	prise d'eau	179	1920	1921	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
293	1	1	67717/T1-796	manifestation de protestation	288	2564	2565	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		2011-12-12 : changement de broader term ("manifestation publique" au lieu de "réunion publique") et suppression du altlabel "émeute", suite à l'appel à commentaires d'octobre 2010-mars 2011
267	1	1	67717/T1-1069	colonie	61	2917	2922	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb11952088m	
298	1	1	67717/T1-253	police des jeux	25	721	722	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
171	1	1	67717/T1-584	marin pêcheur	21	457	458	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
1334	1	1	67717/T1-1265	laboratoire d'analyse	1196	1474	1475	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1		
102	1	1	67717/T1-1073	société savante	45	2033	2034	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb119458948	
36	1	1	67717/T1-344	santé	5	1374	1491	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11945860z	2011-12-13 : suppression du narrower term "hygiène sociale", suite à l'appel à commentaires d'octobre 2010-mars 2011
146	1	1	67717/T1-7	coupe de bois	148	494	495	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1		
297	1	1	67717/T1-1396	police des moeurs	25	713	720	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
30	1	1	67717/T1-1259	droit public	4	1060	1079	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb13340881z	
618	1	1	67717/T1-623	droits domaniaux d'Ancien Régime	27	865	866	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
1369	1	1	67717/T1-1043	domaine royal	778	1044	1045	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1	http://data.bnf.fr/ark:/12148/cb119742224	
1370	1	1	67717/T1-1035	agglomération urbaine	778	1046	1047	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1	http://data.bnf.fr/ark:/12148/cb119762129	
1371	1	1	67717/T1-812	sénéchaussée	778	1048	1049	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1	http://data.bnf.fr/ark:/12148/cb11956250f	
1372	1	1	67717/T1-1372	pays	778	1050	1051	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1		
1373	1	1	67717/T1-638	généralité	778	1052	1053	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1		
1374	1	1	67717/T1-708	circonscription fiscale	778	1054	1055	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1		
238	1	1	67717/T1-869	commerce alimentaire	15	233	242	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb13340877q	
417	1	1	67717/T1-1198	mécénat	38	1555	1556	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119395152	
1378	1	1	67717/T1-1292	stagiaire	536	1240	1241	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1	http://data.bnf.fr/ark:/12148/cb11975072c	
380	1	1	67717/T1-61	infraction militaire	379	2360	2361	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb126533122	
157	1	1	67717/T1-359	service civique	60	2857	2858	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb124993678	
346	1	1	67717/T1-720	mouvement régionaliste	330	2618	2619	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
296	1	1	67717/T1-102	police des transports	25	711	712	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
26	1	1	67717/T1-842	régime seigneurial	4	758	787	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1		
336	1	1	67717/T1-110	centrisme	330	2598	2599	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1		
156	1	1	67717/T1-872	recrutement militaire	60	2851	2856	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1		
426	1	1	67717/T1-691	entreprise publique	16	347	348	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb11950180n	
421	1	1	67717/T1-314	établissement de crédit	16	319	326	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb119759438	
422	1	1	67717/T1-1294	comptabilité d'entreprise	16	327	330	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1		
243	1	1	67717/T1-431	commerce de détail	15	267	268	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11938536s	
242	1	1	67717/T1-785	marché en gros	15	263	266	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11935653x	2011-12-06: transformation du prefLabel "marché agricole" en "marché en gros", transformation du narrower term "marché d'intérêt national" en altlabel, et suppression du related term "production agricole" suite à l'appel à commentaires d'octobre 2010-mars 2011
140	1	1	67717/T1-354	finances intercommunales	137	622	623	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1		2011-12-06: ajout du related term "établissement de coopération intercommunale" suite à l'appel à commentaires d'octobre 2010-mars 2011
240	1	1	67717/T1-274	marché de détail	15	255	260	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1		
334	1	1	67717/T1-70	mouvement laïque	330	2594	2595	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1		
1298	1	1	67717/T1-703	course hippique	1296	1716	1717	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		2011-12-07: Ajout du related term "équidé" suite à l'appel à commentaires d'octobre 2010-mars 2011
558	1	1	67717/T1-160	architecture	557	1584	1585	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb133183211	
241	1	1	67717/T1-456	commerce extérieur	15	261	262	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11976156n	
150	1	1	67717/T1-1112	charbon de bois	148	496	497	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb11940304w	
219	1	1	67717/T1-694	garde forestier	303	746	747	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11980721d	
198	1	1	67717/T1-763	domaine mobilier	151	630	631	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
199	1	1	67717/T1-1433	biens intercommunaux	151	632	633	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
393	1	1	67717/T1-619	crime de guerre	379	2386	2387	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb13318892x	
91	1	1	67717/T1-702	médecine pénitentiaire	49	2233	2234	2013-01-15 16:46:22	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb11932994m	
1406	1	1	67717/T1-808	indépendance	267	2920	2921	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1		
891	1	1	67717/T1-302	expulsion locative	73	1760	1761	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb12010616p	
370	1	1	67717/T1-697	agence de voyages	37	1527	1528	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb11941813g	
129	1	1	67717/T1-76	voie privée	126	1804	1805	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb12049536t	
130	1	1	67717/T1-395	voie communale	126	1806	1807	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb145201248	
368	1	1	67717/T1-1412	tourisme social	37	1517	1524	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb119523397	
392	1	1	67717/T1-588	abandon d'enfant	379	2384	2385	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb12154030j	
27	1	1	67717/T1-829	fiscalité	4	788	875	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11975821p	
1337	1	1	67717/T1-303	hindouisme	784	2726	2727	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb11939162g	
1401	1	1	67717/T1-1399	pupille de la nation	997	2954	2955	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb13755486p	
942	1	1	67717/T1-206	dépense d'investissement	211	670	671	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1		
943	1	1	67717/T1-464	dépense de fonctionnement	211	672	673	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1		
523	1	1	67717/T1-662	établissement d'enseignement	46	2075	2094	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
1268	1	1	67717/T1-553	serment de fidélité	455	770	771	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
16	1	1	67717/T1-885	entreprise	2	294	353	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11950174q	
1319	1	1	67717/T1-1054	interdit de séjour	299	726	727	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1		
527	1	1	67717/T1-954	enseignement public	46	2107	2108	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11957060f	
1193	1	1	67717/T1-560	équipement médico chirurgical	800	1414	1415	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		
37	1	1	67717/T1-632	tourisme	6	1494	1543	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://dbpedia.org/resource/Category:Tourism	
1341	1	1	67717/T1-980	religion orthodoxe	784	2734	2735	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb11952251t	
1342	1	1	67717/T1-1175	bouddhisme	784	2736	2737	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1	http://data.bnf.fr/ark:/12148/cb11938935j	
31	1	1	67717/T1-1066	population	5	1082	1141	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb119563454	2011-12-08 : suppression duy narrower term "gitan", suite à l'appel à commentaires d'octobre 2010-mars 2011
10	1	1	67717/T1-906	opinion	0	2479	2832	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb13318767f	
1403	1	1	67717/T1-969	veuve de guerre	997	2958	2959	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb11971580d	
1405	1	1	67717/T1-384	colonisation	267	2918	2919	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb119596539	
836	1	1	67717/T1-1344	travail clandestin	33	1277	1278	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1		
830	1	1	67717/T1-414	examen professionnel	33	1261	1262	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
655	1	1	67717/T1-440	transport à dos d'homme	12	65	66	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb122968636	
656	1	1	67717/T1-1329	transport aérien	12	67	74	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb119337004	
657	1	1	67717/T1-488	transport par animal	12	75	80	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1		2011-12-07: Ajout du related term "équidé" suite à l'appel à commentaires d'octobre 2010-mars 2011
54	1	1	67717/T1-1174	vie publique	10	2480	2573	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		2011-12-07: ajout des narrower term "armoiries" et "manifestation publique", et suppression du narrower term "réunion publique", suite à l'appel à commentaires d'octobre 2010-mars 2011
55	1	1	67717/T1-22	vie politique	10	2574	2647	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb13319361h	
56	1	1	67717/T1-33	mouvement d'idées	10	2648	2665	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
57	1	1	67717/T1-804	vie religieuse	10	2666	2751	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb11961511c	
62	1	1	67717/T1-1341	guerre	11	2934	2993	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://dbpedia.org/resource/Category:Wars	2011-12-08 : ajout du narrower term "opération militaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
782	1	1	67717/T1-1210	vie spirituelle	57	2709	2720	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb12286910p	
53	1	1	67717/T1-1242	décision de justice	9	2430	2477	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
1315	1	1	67717/T1-1009	peine capitale	1309	2456	2457	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb126475351	
78	1	1	67717/T1-671	démolition	41	1785	1786	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb119358675	
82	1	1	67717/T1-332	foyer rural	484	1558	1559	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
1421	1	1	67717/T1-924	saisie	53	2471	2472	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb126536199	
61	1	1	67717/T1-974	relations internationales	11	2908	2933	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb11976886h	2011-12-08: suppression du related term "action humanitaire", ajout du narrower term "aide au développement" suite à l'appel à commentaires d'octobre 2010-mars 2011
39	1	1	67717/T1-373	vie quotidienne	6	1648	1663	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://dbpedia.org/resource/Category:Personal_life	
40	1	1	67717/T1-1173	loisir	6	1664	1727	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb13318506p	
41	1	1	67717/T1-556	immobilier	7	1730	1797	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://dbpedia.org/resource/Category:Real_estate	
112	1	1	67717/T1-1007	organisme génétiquement modifié	99	2026	2027	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb123582741	2011-12-06: ajout du related term "production agricole" suite à l'appel à commentaires d'octobre 2010-mars 2011
730	1	1	67717/T1-755	réseau de distribution	43	1869	1882	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1		
510	1	1	67717/T1-722	usine d'incinération	505	1952	1953	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb12062068s	
206	1	1	67717/T1-495	biens départementaux	151	646	647	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
1269	1	1	67717/T1-1313	dénombrement seigneurial	455	772	773	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
45	1	1	67717/T1-3	recherche scientifique	8	2022	2059	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb11938467s	2011-12-07 : ajout du related term "archéologie", suppression des narrower terms "culture expérimentale" et "discipline scientifique", et ajout des narrower terms "expédition scientifique", "recherche historique", "sciences de la vie et de la terre", "sciences humaines" et "sciences physiques", suite à l'appel à commentaires d'octobre 2010-mars 2011
596	1	1	67717/T1-46	contentieux de la sécurité sociale	50	2263	2264	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb12107064j	
286	1	1	67717/T1-1200	distinction honorifique	54	2557	2558	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
642	1	1	67717/T1-50	véhicule automobile	12	33	34	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb11930996t	
227	1	1	67717/T1-1397	base aérienne	155	2848	2849	2013-01-15 16:46:22	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11937704g	
489	1	1	67717/T1-1128	aire de stationnement	483	1848	1849	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb11967295v	
491	1	1	67717/T1-549	aire de jeux	483	1852	1853	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb119360289	
503	1	1	67717/T1-36	déchet radioactif	505	1944	1945	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://dbpedia.org/resource/Category:Radioactive_waste	2011-04-19: Suppression du terme non-descripteurs "énergie atomique" suite à une remarque de l'éditeur GAIA
460	1	1	67717/T1-30	voie ferrée privée	\N	2995	2996	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb11934482q	
458	1	1	67717/T1-688	alleu	26	785	786	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb119829114	
119	1	1	67717/T1-161	repos hebdomadaire	118	1324	1325	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb119824452	
465	1	1	67717/T1-985	voie ferrée d'intérêt national	461	1822	1823	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
411	1	1	67717/T1-1162	libération conditionnelle	408	2438	2439	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119720374	
1278	1	1	67717/T1-245	transport scolaire	728	2170	2171	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb11931336j	
630	1	1	67717/T1-49	viticulture	23	523	524	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1	http://data.bnf.fr/ark:/12148/cb11933804s	
631	1	1	67717/T1-1019	bio-carburant	18	403	404	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1	http://data.bnf.fr/ark:/12148/cb12309118g	
647	1	1	67717/T1-1090	transport maritime	12	39	48	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb13591975x	
1365	1	1	67717/T1-349	limite territoriale	778	1036	1037	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1		
1366	1	1	67717/T1-454	seigneurie	778	1038	1039	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1	http://data.bnf.fr/ark:/12148/cb11935665x	
1367	1	1	67717/T1-1064	arrondissement	778	1040	1041	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1		
593	1	1	67717/T1-790	prestation familiale	32	1217	1218	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb119340353	
390	1	1	67717/T1-809	infraction économique	379	2380	2381	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb133186973	
587	1	1	67717/T1-432	sécurité sociale	32	1157	1162	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1		
1327	1	1	67717/T1-794	hôtel de la sous-préfecture	777	1022	1023	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1		
1320	1	1	67717/T1-652	individu recherché	299	728	729	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1		
1323	1	1	67717/T1-273	hôtel du département	777	1014	1015	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1		
1351	1	1	67717/T1-1318	mer	44	2017	2018	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1	http://data.bnf.fr/ark:/12148/cb119328810	
1126	1	1	67717/T1-152	cancer	1127	1438	1439	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb11931105q	
431	1	1	67717/T1-1283	sûreté mobilière	414	2258	2259	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1		
568	1	1	67717/T1-558	jeune	31	1107	1108	2013-01-15 16:46:25	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb11934260v	2011-12-12 : suppression du related "emploi des jeunes", suite à l'appel à commentaires d'octobre 2010-mars 2011
585	1	1	67717/T1-420	mendicité	583	1154	1155	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb11978307g	
202	1	1	67717/T1-766	bien administré	151	638	639	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
203	1	1	67717/T1-333	acquisition domaniale	151	640	641	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb119707080	
484	1	1	67717/T1-213	équipement socio culturel	38	1557	1580	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb13318356p	
1381	1	1	67717/T1-761	relations avec les usagers	275	2524	2525	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		
927	1	1	67717/T1-459	découpage électoral	58	2807	2808	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
1017	1	1	67717/T1-484	patrimoine scientifique et technique	796	1616	1617	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
864	1	1	67717/T1-739	espace naturel sensible	44	1981	1986	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1		
1322	1	1	67717/T1-272	construction aéronautique	188	190	191	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1	http://dbpedia.org/resource/Category:Aircraft_components	
807	1	1	67717/T1-236	composant électronique	185	172	173	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://dbpedia.org/resource/Category:Electrical_components	
1290	1	1	67717/T1-1363	agent d'assurances	239	252	253	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://data.bnf.fr/ark:/12148/cb11949841n	
155	1	1	67717/T1-912	infrastructure militaire	60	2835	2850	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1		
1380	1	1	67717/T1-308	communication institutionnelle	275	2522	2523	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		
1163	1	1	67717/T1-1220	produit maraîcher	636	580	581	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1		
153	1	1	67717/T1-1135	gendarmerie	303	742	743	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb133408720	
251	1	1	67717/T1-1101	vente au déballage	15	287	290	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1		
479	1	1	67717/T1-181	persécution religieuse	476	2654	2655	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
544	1	1	67717/T1-784	alchimie	542	2816	2817	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb119308569	
1285	1	1	67717/T1-579	demi-solde	160	2886	2887	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1		
84	1	1	67717/T1-908	chemin rural	126	1800	1801	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
562	1	1	67717/T1-514	art dramatique	557	1592	1593	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb11930966w	
264	1	1	67717/T1-221	jeux olympiques	305	1668	1669	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb11939226w	
1173	1	1	67717/T1-500	maladie professionnelle	935	1344	1345	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://dbpedia.org/resource/Category:Occupational_diseases	
490	1	1	67717/T1-465	champ de foire	483	1850	1851	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
532	1	1	67717/T1-1349	orientation scolaire	46	2117	2118	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11935282d	
744	1	1	67717/T1-407	assemblée générale du clergé	743	2686	2687	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1		
573	1	1	67717/T1-269	juif	31	1119	1120	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb11932167h	
574	1	1	67717/T1-312	femme	31	1121	1122	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb119556551	
583	1	1	67717/T1-45	exclusion sociale	32	1151	1156	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb12465297b	
531	1	1	67717/T1-857	enseignement préélémentaire	46	2115	2116	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1		
519	1	1	67717/T1-1456	éducateur sportif	327	2066	2067	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11965236v	
381	1	1	67717/T1-142	agression sexuelle	379	2362	2363	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb13318534x	
537	1	1	67717/T1-1414	fonctionnaire hospitalier	540	932	933	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1		
485	1	1	67717/T1-478	établissement recevant du public	28	879	880	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
686	1	1	67717/T1-626	foyer	79	1788	1789	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb11978351z	
567	1	1	67717/T1-946	enfant	31	1105	1106	2013-01-15 16:46:25	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb119319811	2011-12-12 : suppression du related term "enfant handicapé", suite à l'appel à commentaires d'octobre 2010-mars 2011
87	1	1	67717/T1-472	remembrement rural	65	432	433	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb11948228j	
1434	1	1	67717/T1-1008	servitude radio électrique	1257	136	137	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1		
32	1	1	67717/T1-1299	protection sociale	5	1142	1231	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1		2011-12-12 : suppression des narrower term "enfant handicapé" et "adulte handicapé", suite à l'appel à commentaires d'octobre 2010-mars 2011
395	1	1	67717/T1-751	outrage aux moeurs	379	2390	2391	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1		
641	1	1	67717/T1-653	pêche	23	605	612	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb11944279g	
1438	1	1	67717/T1-1426	matériel informatique	1254	120	121	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1	http://dbpedia.org/resource/Category:Computer_hardware	
415	1	1	67717/T1-189	travail protégé	828	1254	1255	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119678062	
1393	1	1	67717/T1-1163	hygiène sociale	36	1487	1490	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1		
598	1	1	67717/T1-824	protection judiciaire de la jeunesse	50	2271	2280	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb133191253	
1388	1	1	67717/T1-959	blocus	266	2912	2913	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb11978683d	
467	1	1	67717/T1-284	incendie	753	888	889	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb11975823c	
468	1	1	67717/T1-116	économie montagnarde	17	355	356	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb11954237r	
470	1	1	67717/T1-400	sylviculture	22	511	516	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://dbpedia.org/resource/Category:Forestry	
471	1	1	67717/T1-741	forêt privée	22	517	518	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb119403711	
949	1	1	67717/T1-765	législation	30	1073	1074	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb13319331k	
469	1	1	67717/T1-403	espace vert	483	1844	1845	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb11931277v	
113	1	1	67717/T1-68	propriété industrielle	414	2256	2257	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb133187156	
114	1	1	67717/T1-760	association	54	2481	2486	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb13318322c	
115	1	1	67717/T1-287	discipline scientifique	45	2057	2058	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		
1390	1	1	67717/T1-343	amiante	506	1380	1381	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://dbpedia.org/resource/Category:Asbestos	
116	1	1	67717/T1-143	philosophie	56	2651	2652	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb119368422	2011-12-07 : ajout de "sciences humaines" comme related de "sciences humaines"
103	1	1	67717/T1-1005	sciences humaines	45	2035	2036	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb119331908	
1429	1	1	67717/T1-1205	bathymétrie	1348	2006	2007	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1		
108	1	1	67717/T1-1471	recherche historique	45	2045	2052	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		2011-12-07: ajout du concept "recherche historique" avec comme narrower terms "histoire des familles", "histoire locale" et "sciences auxiliaires de l'histoire", suite à l'appel à commentaires d'octobre 2010-mars 2011
109	1	1	67717/T1-1472	sciences de la vie et de la terre	45	2053	2054	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		2011-12-07: ajout du concept "sciences de la vie et de la terre", suite à l'appel à commentaires d'octobre 2010-mars 2011
110	1	1	67717/T1-1473	sciences physiques	45	2055	2056	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		2011-12-07: ajout du concept "sciences physiques", suite à l'appel à commentaires d'octobre 2010-mars 2011
128	1	1	67717/T1-1050	circulation routière	12	3	6	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb119384324	
488	1	1	67717/T1-921	construction scolaire	523	2076	2077	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
97	1	1	67717/T1-896	vie intellectuelle	56	2649	2650	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb11933785b	
466	1	1	67717/T1-31	vacants	22	509	510	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
148	1	1	67717/T1-643	bois	22	493	498	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1	http://data.bnf.fr/ark:/12148/cb11976008h	
369	1	1	67717/T1-687	tourisme de montagne	37	1525	1526	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb11962046w	
68	1	1	67717/T1-41	aide publique au logement	41	1735	1738	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
88	1	1	67717/T1-909	équipement touristique	37	1495	1508	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
464	1	1	67717/T1-443	voie ferrée d'intérêt local	461	1820	1821	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
383	1	1	67717/T1-296	escroquerie	379	2366	2367	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb13318990h	
385	1	1	67717/T1-320	terrorisme	379	2370	2371	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb13340858f	
1284	1	1	67717/T1-247	officier militaire	160	2884	2885	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb119542410	
569	1	1	67717/T1-1450	subsistances	31	1109	1112	2013-01-15 16:46:25	1	2013-01-15 16:47:04	1		
643	1	1	67717/T1-1221	accident des transports	28	883	884	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb124854421	
420	1	1	67717/T1-1158	fonds de commerce	16	311	318	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb119501992	
508	1	1	67717/T1-1129	ordures ménagères	505	1948	1949	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
35	1	1	67717/T1-536	travail	5	1320	1373	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb119715347	2011-12-07 : ajout du narrower term "restauration collective", suite à l'appel à commentaires d'octobre 2010-mars 2011
590	1	1	67717/T1-544	action humanitaire	32	1193	1198	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb13774498v	2011-12-08: suppression du related term "relations internationales" et ajout du related "action humanitaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
520	1	1	67717/T1-614	maître d'apprentissage	327	2068	2069	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
1416	1	1	67717/T1-1244	interne	1099	2202	2203	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
118	1	1	67717/T1-861	conditions du travail	35	1321	1336	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1		
1394	1	1	67717/T1-412	alcoolique	1393	1488	1489	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1		
388	1	1	67717/T1-707	atteinte à l'ordre public	379	2376	2377	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb11982939h	
433	1	1	67717/T1-518	pigeon voyageur	13	99	100	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb12648605m	
637	1	1	67717/T1-202	calamité agricole	23	583	590	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1		2011-12-07: ajout du related term "météorologie", suite à l'appel à commentaires d'octobre 2010-mars 2011
606	1	1	67717/T1-669	fiscalité professionnelle	27	791	802	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
418	1	1	67717/T1-724	entreprise en difficulté	16	307	308	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1		
25	1	1	67717/T1-471	police	4	684	757	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb133183769	2011-12-06: suppression du narrower term "police des frontières" suite à l'appel à commentaires d'octobre 2010-mars 2011
133	1	1	67717/T1-1147	route départementale	126	1812	1813	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1		
134	1	1	67717/T1-1380	voie intercommunale	126	1814	1815	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1		
564	1	1	67717/T1-897	arts plastiques	557	1596	1597	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1		
414	1	1	67717/T1-369	enregistrement judiciaire	50	2255	2262	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1		
138	1	1	67717/T1-1083	décentralisation	705	974	975	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb123083581	
1397	1	1	67717/T1-710	république	947	1066	1067	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb133186466	
1398	1	1	67717/T1-1460	chef d'État	947	1068	1069	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1		2011-12-06: Ajout du concept "chef d'État" suite à l'appel à commentaires d'octobre 2010-mars 2011
1399	1	1	67717/T1-1104	invalide de guerre	595	1228	1229	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb11946883v	2011-12-12 : changement de broader term, suite à l'appel à commentaires d'octobre 2010-mars 2011
673	1	1	67717/T1-650	délégué du personnel	937	1362	1363	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb11971907f	
842	1	1	67717/T1-1407	université populaire	841	1636	1637	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb12009911r	
843	1	1	67717/T1-338	action culturelle	38	1639	1642	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1		
221	1	1	67717/T1-12	génie militaire	155	2836	2837	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://dbpedia.org/resource/Category:Military_engineering	
1157	1	1	67717/T1-769	conseil d'administration	703	966	967	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1		
1321	1	1	67717/T1-1411	arrestation	299	730	731	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1	http://data.bnf.fr/ark:/12148/cb11980238r	
945	1	1	67717/T1-1245	recette fiscale	211	676	677	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb119835536	
565	1	1	67717/T1-704	conservatoire	484	1560	1561	2013-01-15 16:46:25	1	2013-01-15 16:47:04	1		
904	1	1	67717/T1-163	syndic de faillite	554	2328	2329	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
1404	1	1	67717/T1-1484	mort pour la France	997	2960	2961	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1		2011-12-07: ajout du concept "mort pour la France" avec comme broader term "victime de guerre", suite à l'appel à commentaires d'octobre 2010-mars 2011
1409	1	1	67717/T1-780	office d'Ancien Régime	617	860	861	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb12503443d	2011-12-06: modification du preflabel "offices" en "office d'Ancien Régime" suite à l'appel à commentaires d'octobre 2010-mars 2011
635	1	1	67717/T1-603	boisson alcoolisée	23	569	574	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://dbpedia.org/resource/Category:Alcoholic_beverages	
1353	1	1	67717/T1-982	maladie des végétaux	638	592	593	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
1246	1	1	67717/T1-907	travailleur à domicile	936	1356	1357	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1		
844	1	1	67717/T1-963	artiste	38	1643	1644	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb13318455m	
845	1	1	67717/T1-834	manifestation culturelle	38	1645	1646	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb13318614k	
1447	1	1	67717/T1-830	collaboration	1000	2976	2977	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1448	1	1	67717/T1-1202	autorité d'occupation	1000	2978	2979	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1453	1	1	67717/T1-515	costume officiel	278	2534	2535	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1272	1	1	67717/T1-641	droit de place	611	836	837	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
1450	1	1	67717/T1-502	piste cyclable	627	1830	1831	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1451	1	1	67717/T1-754	promenade plantée	627	1832	1833	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1457	1	1	67717/T1-593	trafic de main d'oeuvre	833	1268	1269	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
406	1	1	67717/T1-1421	avortement	379	2412	2413	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb11931009t	2011-12-08 : correction de "loi Veil" au lieu de "loi Weil" dans la scopeNote
407	1	1	67717/T1-26	probation	408	2432	2433	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119709730	
408	1	1	67717/T1-996	application des peines	53	2431	2442	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119507809	
409	1	1	67717/T1-392	réhabilitation	408	2434	2435	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb133190443	
410	1	1	67717/T1-408	amnistie	408	2436	2437	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119374306	
89	1	1	67717/T1-2	visiteur de prison	49	2227	2228	2013-01-15 16:46:22	1	2013-01-15 16:46:38	1		
90	1	1	67717/T1-80	discipline pénitentiaire	49	2229	2232	2013-01-15 16:46:22	1	2013-01-15 16:46:38	1		
1417	1	1	67717/T1-429	acquittement	53	2463	2464	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb12337780p	
1418	1	1	67717/T1-477	extradition	53	2465	2466	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb11975659t	
101	1	1	67717/T1-676	réunion scientifique	45	2031	2032	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		
69	1	1	67717/T1-72	réquisition de logement	41	1739	1740	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb12548263h	
785	1	1	67717/T1-424	ministre du culte	57	2743	2744	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb11931214z	
70	1	1	67717/T1-738	architecte	41	1741	1742	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb11930938n	
71	1	1	67717/T1-396	construction	41	1743	1756	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://dbpedia.org/resource/Category:Construction	
43	1	1	67717/T1-209	urbanisme	7	1840	1915	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://dbpedia.org/resource/Category:Urban_studies_and_planning	
44	1	1	67717/T1-1074	environnement	7	1916	2019	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb11974343c	
77	1	1	67717/T1-1045	syndic de copropriété	41	1783	1784	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb12048179r	
52	1	1	67717/T1-525	justice pénale	9	2354	2429	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
910	1	1	67717/T1-534	expert	554	2340	2341	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb119744200	
95	1	1	67717/T1-1148	travail pénitentiaire	49	2245	2246	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb119768468	
60	1	1	67717/T1-250	défense du territoire	11	2834	2907	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb11967195k	2011-12-08 : suppression du narrower term "opération militaire" et ajout du narrower term "manoeuvre militaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
99	1	1	67717/T1-1413	recherche appliquée	45	2023	2028	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		2011-12-07 : ajout du narrower term "culture expérimentale"
100	1	1	67717/T1-618	découverte scientifique	45	2029	2030	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		
124	1	1	67717/T1-1100	travail des enfants	118	1334	1335	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb119313534	2011-12-13 : modification du prefLabel "emploi des enfants" en "travail des enfants", suite à l'appel à commentaires d'octobre 2010-mars 2011
81	1	1	67717/T1-419	agriculteur	21	435	448	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://dbpedia.org/resource/Category:Farmers	
83	1	1	67717/T1-1454	migration rurale	19	1084	1085	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb11941748v	
614	1	1	67717/T1-306	enregistrement	27	851	854	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1	http://data.bnf.fr/ark:/12148/cb12651477f	
540	1	1	67717/T1-805	personnel	29	929	948	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb119757771	
416	1	1	67717/T1-640	taxe professionnelle	606	792	793	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb11951185m	
49	1	1	67717/T1-1117	condition pénitentiaire	9	2226	2247	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
539	1	1	67717/T1-767	juge	51	2307	2320	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb13318705w	
309	1	1	67717/T1-194	réception officielle	273	2498	2499	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
391	1	1	67717/T1-557	bigamie	379	2382	2383	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb11998639b	
672	1	1	67717/T1-1366	magistrat	539	2318	2319	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1		
1310	1	1	67717/T1-345	bannissement	1309	2446	2447	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb133193307	
1326	1	1	67717/T1-712	hôtel de ville	777	1020	1021	2013-01-15 16:46:30	1	2013-01-15 16:47:56	1		
501	1	1	67717/T1-802	servitude	210	660	661	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb11933263n	
42	1	1	67717/T1-1334	voie de communication	7	1798	1839	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
76	1	1	67717/T1-1133	habitat urbain	41	1777	1782	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb12326736r	
166	1	1	67717/T1-988	aménagement des eaux	179	1918	1919	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1		
592	1	1	67717/T1-591	placement familial	32	1215	1216	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb119760640	
92	1	1	67717/T1-609	enseignement pénitentiaire	49	2235	2236	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		2011-04-18: Suppression des termes non-descripteurs "maison d'arrêt", "maison centrale", "prison" et "centre de détention" suite à une remarque de l'éditeur ANAPHORE
1277	1	1	67717/T1-1063	épicerie	238	240	241	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb12312465b	
594	1	1	67717/T1-987	insertion sociale	32	1219	1220	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
367	1	1	67717/T1-154	association de tourisme	37	1515	1516	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1		
626	1	1	67717/T1-326	voie navigable	42	1827	1828	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1	http://data.bnf.fr/ark:/12148/cb11935887s	
628	1	1	67717/T1-1087	téléphérique	42	1837	1838	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1	http://data.bnf.fr/ark:/12148/cb11991200h	
382	1	1	67717/T1-215	abandon de famille	379	2364	2365	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb119731291	
543	1	1	67717/T1-1111	astrologie	542	2814	2815	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb13318456z	
928	1	1	67717/T1-922	assemblée européenne	703	952	953	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
874	1	1	67717/T1-268	minerai	186	180	181	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb119532125	
850	1	1	67717/T1-752	imprimeur	233	216	217	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb119339251	
818	1	1	67717/T1-1266	aménagement foncier	43	1893	1902	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1		
819	1	1	67717/T1-168	décoration urbaine	43	1903	1904	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1		
1460	1	1	67717/T1-1361	expropriation	1349	2010	2011	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1	http://data.bnf.fr/ark:/12148/cb11950188d	
1461	1	1	67717/T1-540	archéologie aérienne	98	1546	1547	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1	http://data.bnf.fr/ark:/12148/cb119417821	
1462	1	1	67717/T1-664	archéologie subaquatique	98	1548	1549	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1	http://data.bnf.fr/ark:/12148/cb14447093r	
1292	1	1	67717/T1-571	milices bourgeoises	161	2896	2897	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		
552	1	1	67717/T1-920	aide publique aux entreprises	17	357	358	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1		
549	1	1	67717/T1-224	administrateur judiciaire	554	2322	2323	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1		
96	1	1	67717/T1-436	discipline universitaire	47	2125	2126	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		
600	1	1	67717/T1-839	affaire prud'homale	50	2285	2286	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
570	1	1	67717/T1-667	migration saisonnière	31	1113	1114	2013-01-15 16:46:25	1	2013-01-15 16:47:04	1		
644	1	1	67717/T1-126	appareil à pression de vapeur	28	885	886	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1		
104	1	1	67717/T1-319	recherche fondamentale	45	2037	2038	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		
839	1	1	67717/T1-713	population active	33	1283	1284	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb11951055d	
339	1	1	67717/T1-288	anticapitalisme	330	2604	2605	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1		
142	1	1	67717/T1-54	président du conseil régional	693	2758	2759	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1		
435	1	1	67717/T1-418	détention d'armes	294	692	693	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb137709012	
703	1	1	67717/T1-1157	organe délibérant	29	949	972	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
704	1	1	67717/T1-55	régionalisation	705	976	977	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
941	1	1	67717/T1-117	fonds européen	211	668	669	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1		
1289	1	1	67717/T1-1141	école de conduite	239	250	251	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://data.bnf.fr/ark:/12148/cb120504028	
308	1	1	67717/T1-1310	prière publique	273	2496	2497	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
218	1	1	67717/T1-633	congrégation	713	2674	2675	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
1340	1	1	67717/T1-740	catholicisme	784	2732	2733	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb119345700	
685	1	1	67717/T1-53	aide médicale	588	1164	1165	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb11977383b	
607	1	1	67717/T1-398	contributions indirectes	27	803	810	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
1229	1	1	67717/T1-309	sécheresse	637	584	585	2013-01-15 16:46:29	1	2013-01-15 16:47:49	1	http://data.bnf.fr/ark:/12148/cb11937411x	
215	1	1	67717/T1-773	cimetière	483	1842	1843	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11938920h	2011-12-06: Ajout du related term "pompes funèbres" suite à l'appel à commentaires d'octobre 2010-mars 2011
279	1	1	67717/T1-528	censure	54	2543	2544	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://dbpedia.org/resource/Category:Censorship	
280	1	1	67717/T1-681	opinion publique	54	2545	2546	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb13318767f	
751	1	1	67717/T1-1291	missions	743	2700	2701	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb131627441	
702	1	1	67717/T1-893	président d'établissement public de coopération\n\t\t\tintercommunale	693	2776	2777	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
753	1	1	67717/T1-67	sinistre	28	887	896	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb13318743f	
1347	1	1	67717/T1-1429	réformation	457	782	783	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
51	1	1	67717/T1-158	organisation judiciaire	9	2294	2353	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
1391	1	1	67717/T1-956	désinfection	506	1382	1383	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb11976427w	
1392	1	1	67717/T1-523	habitat insalubre	74	1770	1771	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb119753637	
1377	1	1	67717/T1-927	organisme de formation	536	1238	1239	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		
764	1	1	67717/T1-69	impiété	765	2706	2707	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
1389	1	1	67717/T1-577	annexion de territoire	266	2914	2915	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1		
444	1	1	67717/T1-826	débit de boissons	238	234	235	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb11931683w	
1345	1	1	67717/T1-285	reconnaissances	457	778	779	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
1346	1	1	67717/T1-561	agent seigneurial	457	780	781	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
427	1	1	67717/T1-701	société d'économie mixte	16	349	350	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb119736418	
762	1	1	67717/T1-286	pollution	44	1961	1974	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb11932823t	2011-12-13 : ajout du narrower term "pollution du sol", suite à l'appel à commentaires d'octobre 2010-mars 2011
674	1	1	67717/T1-1208	comité d'entreprise	937	1364	1365	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb119341283	
548	1	1	67717/T1-42	redressement judiciaire	230	2250	2251	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11977321s	
440	1	1	67717/T1-576	reporter photographe	274	2508	2509	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1		
237	1	1	67717/T1-220	courtier de marchandises	15	231	232	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1		
690	1	1	67717/T1-1496	allocation logement	588	1172	1173	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		2011-12-13 : ajout du concept "allocation logement" avec comme broader term "aide sociale", suite à l'appel à commentaires d'octobre 2010-mars 2011
463	1	1	67717/T1-1206	voie ferrée des quais	461	1818	1819	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
67	1	1	67717/T1-1438	vente publique immobilière	41	1733	1734	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
866	1	1	67717/T1-92	énergie géothermique	867	406	407	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb119529078	
664	1	1	67717/T1-1348	décès	429	1088	1089	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb119325308	
865	1	1	67717/T1-1185	zone de marais	864	1984	1985	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb11970777t	
892	1	1	67717/T1-103	condition sociale	34	1289	1302	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb11965815j	
893	1	1	67717/T1-446	noblesse	892	1290	1291	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://dbpedia.org/resource/Category:Nobility	
894	1	1	67717/T1-1394	esclavage	892	1292	1293	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://dbpedia.org/resource/Category:Slavery	
895	1	1	67717/T1-723	prolétariat	892	1294	1295	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
1189	1	1	67717/T1-1263	léproserie	800	1406	1407	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb11934274j	
1293	1	1	67717/T1-1280	milices provinciales	161	2898	2899	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		
1343	1	1	67717/T1-1367	protestantisme	784	2738	2739	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1	http://data.bnf.fr/ark:/12148/cb119346212	
4	1	1	67717/T1-1317	administration	0	615	1080	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb120423190	
739	1	1	67717/T1-590	céréale	633	540	541	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://dbpedia.org/resource/Category:Cereals	
1274	1	1	67717/T1-1002	taxe de séjour	611	840	841	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1	http://data.bnf.fr/ark:/12148/cb12341234w	
1402	1	1	67717/T1-680	déporté	997	2956	2957	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb13330183h	
566	1	1	67717/T1-44	émigration	31	1103	1104	2013-01-15 16:46:25	1	2013-01-15 16:47:04	1		
456	1	1	67717/T1-282	droits honorifiques	26	775	776	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
1060	1	1	67717/T1-771	organisme administratif européen	776	996	997	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
48	1	1	67717/T1-1121	vie scolaire	8	2164	2223	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://data.bnf.fr/ark:/12148/cb12653997x	2011-12-07 : ajout du narrower term "internat", suite à l'appel à commentaires d'octobre 2010-mars 2011
1407	1	1	67717/T1-997	association reconnue d'utilité publique	114	2484	2485	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1		
236	1	1	67717/T1-135	poids-et-mesures	15	229	230	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11941221f	
1338	1	1	67717/T1-636	islam	784	2728	2729	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://dbpedia.org/resource/Category:Islam	
1339	1	1	67717/T1-716	chamanisme	784	2730	2731	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb11931141k	
578	1	1	67717/T1-781	recensement de population	31	1129	1130	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1		
18	1	1	67717/T1-166	énergie	2	394	421	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb119314344	
509	1	1	67717/T1-800	déchet hospitalier	505	1950	1951	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb122449331	
1258	1	1	67717/T1-235	criminalité	52	2415	2416	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
776	1	1	67717/T1-435	structure administrative	29	983	1012	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		
1444	1	1	67717/T1-1261	société d'intérêt collectif agricole	174	480	481	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1		
419	1	1	67717/T1-513	compagnie d'assurances	16	309	310	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://dbpedia.org/resource/Category:Insurance_companies	
149	1	1	67717/T1-497	industrie du bois	14	147	150	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1		
896	1	1	67717/T1-1181	servage	892	1296	1297	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://data.bnf.fr/ark:/12148/cb119478388	
851	1	1	67717/T1-1212	éditeur	233	218	219	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb13319412k	
1028	1	1	67717/T1-125	refuge de montagne	88	1500	1501	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1	http://dbpedia.org/resource/Category:Mountain_huts	
1029	1	1	67717/T1-600	résidence de tourisme	88	1502	1503	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1	http://data.bnf.fr/ark:/12148/cb121541226	
249	1	1	67717/T1-932	fraude commerciale	15	281	284	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1		
601	1	1	67717/T1-1176	tutelle judiciaire	50	2287	2292	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
876	1	1	67717/T1-95	télématique	877	102	103	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb11962361m	2011-04-19: Suppression du terme non-descripteurs "messagerie électronique" suite à une remarque de l'éditeur GAIA
878	1	1	67717/T1-413	réseau d'information	877	104	105	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb119327017	2011-04-07: Ajout du terme générique "télécommunications" suite à une remarque de l'éditeur ANAPHORE
652	1	1	67717/T1-1256	bateau de navigation intérieure	12	59	60	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1		
653	1	1	67717/T1-679	transport fluvial	12	61	62	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb13318596g	
646	1	1	67717/T1-745	transport multimodal	12	35	38	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb119753966	
877	1	1	67717/T1-1327	télécommunications	13	101	112	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb119335984	
997	1	1	67717/T1-879	victime de guerre	62	2953	2962	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb119625766	2011-12-08: ajout du narrower term "mort pour la France", suppression du related term "gitan" et ajout du related term "nomade", suite à l'appel à commentaires d'octobre 2010-mars 2011
873	1	1	67717/T1-1441	centrale thermique	513	400	401	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb11977401f	
867	1	1	67717/T1-1315	énergie renouvelable	18	405	416	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb119519625	
1120	1	1	67717/T1-1204	entente	963	382	383	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1		
1317	1	1	67717/T1-1432	assignation à résidence	1309	2460	2461	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb12207021f	
154	1	1	67717/T1-301	atteinte à la sûreté de l'État	379	2356	2357	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb119522645	
232	1	1	67717/T1-65	magasin général	15	213	214	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1		
239	1	1	67717/T1-801	services	15	243	254	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11951149r	
1414	1	1	67717/T1-833	étudiant	1099	2198	2199	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb11949024v	
1415	1	1	67717/T1-1377	apprenti	1099	2200	2201	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb11951682z	
79	1	1	67717/T1-1001	logement social	41	1787	1794	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb11942109j	
1032	1	1	67717/T1-1492	pèlerinage	328	2668	2669	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		2011-12-12: ajout du concept "pèlerinage" avec comme broader term "pratique religieuse", suite à l'appel à commentaires d'octobre 2010-mars 2011
1445	1	1	67717/T1-494	épuration	1000	2972	2973	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1099	1	1	67717/T1-559	population scolaire	48	2195	2204	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb119578165	
1419	1	1	67717/T1-828	appel	53	2467	2468	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb12172974k	
586	1	1	67717/T1-717	tutelle aux prestations sociales	601	2288	2289	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb12404976t	
529	1	1	67717/T1-1077	examen	46	2111	2112	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11976203b	
530	1	1	67717/T1-952	enseignement privé	46	2113	2114	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1		
550	1	1	67717/T1-1232	faillite	230	2252	2253	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11961010n	
1247	1	1	67717/T1-228	télédiffusion	13	113	116	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb119344712	
1426	1	1	67717/T1-911	droits civiques	926	2802	2803	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
1400	1	1	67717/T1-361	franc maçon	276	2528	2529	2013-01-15 16:46:31	1	2013-01-15 16:48:03	1	http://data.bnf.fr/ark:/12148/cb11971434z	
123	1	1	67717/T1-898	horaire de travail	118	1332	1333	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb11950215g	
759	1	1	67717/T1-1000	secours	28	901	912	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		2011-12-06: Ajout du narrower term "équipement de secours" suite à l'appel à commentaires d'octobre 2010-mars 2011
961	1	1	67717/T1-129	planification	17	375	378	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1	http://data.bnf.fr/ark:/12148/cb11972123q	
400	1	1	67717/T1-1048	atteinte à papiers publics	379	2400	2401	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1		
401	1	1	67717/T1-1080	meurtre	379	2402	2403	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb13319142p	
403	1	1	67717/T1-1164	vol	379	2406	2407	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb13318396x	
404	1	1	67717/T1-1224	abus de fonction	379	2408	2409	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb119792826	
1475	1	1	67717/T1-1203	matériel pédagogique	991	2158	2159	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb11977116n	
1279	1	1	67717/T1-256	bourse d'études	728	2172	2173	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb11959600q	
127	1	1	67717/T1-628	sécurité routière	28	877	878	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb119332520	
177	1	1	67717/T1-1463	concours agricole	21	489	490	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb12242245m	2011-12-06: Ajout du concept "concours agricole" suite à l'appel à commentaires d'octobre 2010-mars 2011
1379	1	1	67717/T1-542	droits de succession	614	852	853	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		2011-12-12 : suppression du altLabel "succession", suppression du related term "droits de succession" et ajout du related term "succession", suite à l'appel à commentaires d'octobre 2010-mars 2011
1047	1	1	67717/T1-156	port de pêche	629	18	19	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb12047732h	
1430	1	1	67717/T1-668	police municipale	303	748	749	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1	http://data.bnf.fr/ark:/12148/cb122191980	
1431	1	1	67717/T1-1093	police urbaine	303	750	751	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1		
1432	1	1	67717/T1-1425	maréchaussée	303	752	753	2013-01-15 16:46:31	1	2013-01-15 16:48:05	1		
835	1	1	67717/T1-1419	travailleur frontalier	33	1275	1276	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb11948340p	
1301	1	1	67717/T1-393	tauromachie	40	1723	1724	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1	http://dbpedia.org/resource/Category:Bullfighting	
1041	1	1	67717/T1-233	site	1040	1992	1993	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb12355758f	
1423	1	1	67717/T1-1285	vente judiciaire	53	2475	2476	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1	http://data.bnf.fr/ark:/12148/cb11982847v	
389	1	1	67717/T1-504	association de malfaiteurs	379	2378	2379	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb12374542p	2011-04-07: Ajout du terme générique "crimes et délits" suite à une remarque de l'éditeur ANAPHORE?
907	1	1	67717/T1-1323	conciliateur	554	2334	2335	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
832	1	1	67717/T1-848	insertion professionnelle	33	1265	1266	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb12008653m	
250	1	1	67717/T1-1351	brocanteur	15	285	286	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1		2011-12-13 : suppression du related "objet d'art", suite à l'appel à commentaires d'octobre 2010-mars 2011
883	1	1	67717/T1-99	enfant secouru	589	1182	1183	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1		
1463	1	1	67717/T1-1017	archéologie sous-marine	98	1550	1551	2013-01-15 16:46:31	1	2013-01-15 16:48:08	1	http://data.bnf.fr/ark:/12148/cb11934048f	
180	1	1	67717/T1-10	industrie du cuir	14	151	152	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
994	1	1	67717/T1-146	ravitaillement	62	2941	2946	2013-01-15 16:46:27	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb135052157	
995	1	1	67717/T1-324	camp d'internement	62	2947	2950	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
860	1	1	67717/T1-647	hydrocarbure	186	176	177	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://dbpedia.org/resource/Category:Hydrocarbons	
182	1	1	67717/T1-481	industrie extractive	14	153	164	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
996	1	1	67717/T1-352	ancien combattant	62	2951	2952	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb11946290g	
1396	1	1	67717/T1-346	monarchie	947	1064	1065	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb12650518z	
472	1	1	67717/T1-1036	aménagement forestier	22	519	520	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
981	1	1	67717/T1-1010	officine pharmaceutique	1197	1478	1479	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1		
1024	1	1	67717/T1-1015	croyances populaires	59	2823	2826	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1	http://data.bnf.fr/ark:/12148/cb126474739	
750	1	1	67717/T1-918	papauté	743	2698	2699	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb119523157	
301	1	1	67717/T1-735	police de la pêche	25	737	738	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
1219	1	1	67717/T1-1417	agent de l'administration royale	540	940	941	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1		
1368	1	1	67717/T1-1328	district révolutionnaire	778	1042	1043	2013-01-15 16:46:30	1	2013-01-15 16:48:00	1		
611	1	1	67717/T1-241	impôts locaux	27	831	844	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
1198	1	1	67717/T1-1379	fondation	114	2482	2483	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://dbpedia.org/resource/Category:Foundations	
1199	1	1	67717/T1-186	établissement de formation des maîtres	523	2080	2081	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		
1200	1	1	67717/T1-240	grande école	523	2082	2083	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://dbpedia.org/resource/Category:Grandes_%C3%A9coles	
1201	1	1	67717/T1-304	centre de formation	523	2084	2085	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		
1202	1	1	67717/T1-580	lycée	523	2086	2087	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://data.bnf.fr/ark:/12148/cb11963528k	
1203	1	1	67717/T1-1340	école	523	2088	2089	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://dbpedia.org/resource/Category:Schools	
152	1	1	67717/T1-8	opération militaire	62	2935	2936	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://dbpedia.org/resource/Category:Military_operations	2011-12-08 : modification du broader term de "opération militaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
1344	1	1	67717/T1-1401	taoïsme	784	2740	2741	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1	http://data.bnf.fr/ark:/12148/cb119335922	
698	1	1	67717/T1-1250	président du conseil général	693	2768	2769	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
693	1	1	67717/T1-868	élu	58	2757	2778	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb12116305t	
478	1	1	67717/T1-1247	athéisme	56	2663	2664	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb11937191k	
222	1	1	67717/T1-358	terrain militaire	155	2838	2839	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
1376	1	1	67717/T1-509	formation qualifiante	536	1236	1237	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		
1481	1	1	67717/T1-855	incarcération	94	2240	2241	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1	http://data.bnf.fr/ark:/12148/cb11952257w	
1482	1	1	67717/T1-1275	évasion	94	2242	2243	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1	http://data.bnf.fr/ark:/12148/cb119410943	
3	1	1	67717/T1-246	agriculture	0	423	614	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb11934798x	
197	1	1	67717/T1-1095	forêt domaniale	22	499	502	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb12674074f	
1275	1	1	67717/T1-1298	taxe d'habitation	611	842	843	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1	http://data.bnf.fr/ark:/12148/cb12158481h	
1206	1	1	67717/T1-188	impôt sur la fortune	609	822	823	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://data.bnf.fr/ark:/12148/cb119826208	
483	1	1	67717/T1-508	équipement collectif	43	1841	1868	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		2011-12-07: ajout du narrower term "installation sanitaire publique", suite à l'appel à commentaires d'octobre 2010-mars 2011
181	1	1	67717/T1-442	déchet industriel	505	1942	1943	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb133190234	
1309	1	1	67717/T1-615	peine	53	2443	2462	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11978167s	
902	1	1	67717/T1-104	conseil juridique	554	2324	2325	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://data.bnf.fr/ark:/12148/cb119500911	
1030	1	1	67717/T1-935	restaurant de tourisme	88	1504	1505	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
903	1	1	67717/T1-109	médiateur de justice	554	2326	2327	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
905	1	1	67717/T1-1226	officier de police judiciaire	554	2330	2331	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
906	1	1	67717/T1-629	agent de police judiciaire	554	2332	2333	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
908	1	1	67717/T1-994	avocat	554	2336	2337	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://data.bnf.fr/ark:/12148/cb11935725z	
143	1	1	67717/T1-1081	élection régionale	917	2780	2781	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1		
1288	1	1	67717/T1-949	société de services	425	342	343	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1		
457	1	1	67717/T1-943	administration seigneuriale	26	777	784	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
64	1	1	67717/T1-40	bâtiment agricole	21	425	426	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1	http://dbpedia.org/resource/Category:Agricultural_buildings	
1242	1	1	67717/T1-931	affichage	247	276	277	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb119731427	
186	1	1	67717/T1-1326	matière première	14	175	184	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb11950813f	
731	1	1	67717/T1-937	distribution électrique	730	1872	1873	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb119383545	
371	1	1	67717/T1-1068	guide interprète	37	1529	1530	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb15004343x	
17	1	1	67717/T1-148	action économique	2	354	393	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb119510511	2011-12-07: Ajout du narrower term "coût de la vie" suite à l'appel à commentaires d'octobre 2010-mars 2011
978	1	1	67717/T1-1440	volaille	634	564	565	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb11933813r	
612	1	1	67717/T1-262	corvée royale	27	845	846	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1	http://data.bnf.fr/ark:/12148/cb12106083k	
1357	1	1	67717/T1-656	logement individuel	73	1764	1765	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1		
613	1	1	67717/T1-295	fraude fiscale	27	847	850	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1	http://data.bnf.fr/ark:/12148/cb119829594	
1230	1	1	67717/T1-854	gelée	637	586	587	2013-01-15 16:46:29	1	2013-01-15 16:47:49	1		
1441	1	1	67717/T1-491	coopérative agricole	174	474	475	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1	http://data.bnf.fr/ark:/12148/cb11950101h	
1442	1	1	67717/T1-1235	baux ruraux	174	476	477	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1	http://data.bnf.fr/ark:/12148/cb11950050f	
1443	1	1	67717/T1-875	groupement de producteurs	174	478	479	2013-01-15 16:46:31	1	2013-01-15 16:48:06	1		
911	1	1	67717/T1-1213	reboisement	470	514	515	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb119777498	
294	1	1	67717/T1-28	police administrative	25	687	700	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb119579529	2011-12-06: Ajout du narrower term "pompes funèbres" suite à l'appel à commentaires d'octobre 2010-mars 2011
700	1	1	67717/T1-438	conseiller d'arrondissement	693	2772	2773	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
1016	1	1	67717/T1-1352	armée de l'air	159	2880	2881	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb11954101g	
890	1	1	67717/T1-1132	juridiction	51	2343	2352	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb119774452	2011-12-07 : ajout du altLabel "tribunal de commerce" et du narrower term "justice militaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
728	1	1	67717/T1-1296	oeuvres scolaires	48	2169	2180	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1		
1425	1	1	67717/T1-1255	bien sinistré	999	2968	2969	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
284	1	1	67717/T1-953	commémoration	54	2553	2554	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb133199150	
863	1	1	67717/T1-91	déforestation	864	1982	1983	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb11931844j	
729	1	1	67717/T1-63	éclairage public	730	1870	1871	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb122168130	
538	1	1	67717/T1-672	profession médicale	36	1385	1398	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11932450m	
492	1	1	67717/T1-1216	place publique	483	1854	1855	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb11954989x	
33	1	1	67717/T1-696	emploi	5	1232	1287	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb12024099q	
1245	1	1	67717/T1-517	concierge	936	1354	1355	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb11950083d	
230	1	1	67717/T1-460	affaire commerciale	50	2249	2254	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1		
727	1	1	67717/T1-1404	rythme scolaire	48	2165	2168	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb120668339	
1316	1	1	67717/T1-1447	peine corporelle	1309	2458	2459	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11947440q	
795	1	1	67717/T1-562	langue régionale	796	1612	1613	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		2011-12-07 : le concept "langue régionale" est supprimé des narrower terms de "programme scolaire", le concept "patrimoine culturel" devient broader term de "langue régionale", et "formation musicale" et "patrimoine ethnologique" sont supprimés des related terms de "langue régionale", suite à l'appel à commentaires d'octobre 2010-mars 2011
1035	1	1	67717/T1-130	espèce protégée	1040	1990	1991	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1		
1037	1	1	67717/T1-389	chasse	40	1693	1700	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb119311589	
849	1	1	67717/T1-258	syndicat professionnel	937	1366	1367	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1		
1420	1	1	67717/T1-880	non-lieu	53	2469	2470	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
159	1	1	67717/T1-551	armée	60	2873	2882	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb11976646q	
688	1	1	67717/T1-371	aide judiciaire	588	1168	1169	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb133189341	
968	1	1	67717/T1-546	éleveur	81	438	439	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb12013660w	
687	1	1	67717/T1-357	structure communale d'aide sociale	588	1166	1167	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
1005	1	1	67717/T1-1020	oeuvre de guerre	62	2991	2992	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
1006	1	1	67717/T1-121	ravitaillement militaire	158	2860	2861	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
337	1	1	67717/T1-170	républicanisme	330	2600	2601	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://dbpedia.org/resource/Category:Republicanism	
1271	1	1	67717/T1-1016	impôts provinciaux d'Ancien Régime	611	834	835	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
1328	1	1	67717/T1-1059	hôtel de région	777	1024	1025	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1		
1329	1	1	67717/T1-1160	hôtel communautaire	777	1026	1027	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1		
65	1	1	67717/T1-1047	aménagement rural	21	427	434	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
974	1	1	67717/T1-1410	viande	634	556	557	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://dbpedia.org/resource/Category:Meat	
989	1	1	67717/T1-940	programme scolaire	47	2131	2142	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb119551607	2011-12-07 : suppression du narrower term "langue régionale" suite à l'appel à commentaires d'octobre 2010-mars 2011
1281	1	1	67717/T1-665	fourniture scolaire	728	2176	2177	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1		
988	1	1	67717/T1-1171	collecte publique	590	1196	1197	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1		
888	1	1	67717/T1-1217	orphelinat	589	1188	1189	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb11976462d	
616	1	1	67717/T1-434	exonération fiscale	27	857	858	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
778	1	1	67717/T1-601	circonscription territoriale	29	1031	1058	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		
747	1	1	67717/T1-811	fabrique d'église	743	2692	2693	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb135335276	
826	1	1	67717/T1-862	chômage	33	1245	1250	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb119500644	
752	1	1	67717/T1-1183	concile	743	2702	2703	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
1427	1	1	67717/T1-675	collège électoral	926	2804	2805	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
1446	1	1	67717/T1-1231	résistance	1000	2974	2975	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
220	1	1	67717/T1-96	concession forestière	197	500	501	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
300	1	1	67717/T1-1114	police de la chasse	25	733	736	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
1375	1	1	67717/T1-999	province	778	1056	1057	2013-01-15 16:46:30	1	2013-01-15 16:48:01	1		
1231	1	1	67717/T1-683	grêle	637	588	589	2013-01-15 16:46:29	1	2013-01-15 16:47:49	1	http://data.bnf.fr/ark:/12148/cb119408017	
575	1	1	67717/T1-339	nomade	31	1123	1124	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb13318500m	2011-12-08 : ajout des altLabel "gitan" et "tsigane" et ajout du related term "victime de guerre, suite à l'appel à commentaires d'octobre 2010-mars 2011
1459	1	1	67717/T1-550	entreprise industrielle	425	344	345	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1		
1270	1	1	67717/T1-1316	octroi	611	832	833	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1	http://data.bnf.fr/ark:/12148/cb155070201	
1208	1	1	67717/T1-1375	impôt sur le revenu	609	826	827	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1	http://data.bnf.fr/ark:/12148/cb11934237b	
1385	1	1	67717/T1-1028	substance vénéneuse	1197	1482	1483	2013-01-15 16:46:31	1	2013-01-15 16:48:01	1	http://data.bnf.fr/ark:/12148/cb124663208	
563	1	1	67717/T1-731	musique	557	1594	1595	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb133183660	
9	1	1	67717/T1-483	justice	0	2225	2478	2013-01-15 16:46:21	1	2013-01-15 16:46:21	1	http://data.bnf.fr/ark:/12148/cb133184141	
909	1	1	67717/T1-882	greffier	554	2338	2339	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://data.bnf.fr/ark:/12148/cb119720045	
441	1	1	67717/T1-248	agent de sécurité	239	244	245	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb13319184m	
473	1	1	67717/T1-32	circulation des personnes	295	704	705	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb119512282	
708	1	1	67717/T1-56	médecine vétérinaire	632	526	527	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb119355947	
709	1	1	67717/T1-439	pharmacie vétérinaire	632	528	529	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
312	1	1	67717/T1-727	acte d'opposition	55	2575	2584	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
675	1	1	67717/T1-1214	élection sociale	58	2753	2756	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1		
1456	1	1	67717/T1-1125	fête nationale	278	2540	2541	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1	http://data.bnf.fr/ark:/12148/cb120046827	
1350	1	1	67717/T1-990	lutte contre l'érosion	44	2015	2016	2013-01-15 16:46:30	1	2013-01-15 16:47:58	1		
1232	1	1	67717/T1-1480	canicule	763	1976	1977	2013-01-15 16:46:30	1	2013-01-15 16:47:49	1		2011-12-07: ajout du concept "canicule" avec comme broader term "météorologie", suite à l'appel à commentaires d'octobre 2010-mars 2011
66	1	1	67717/T1-366	gîte rural	88	1496	1497	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
838	1	1	67717/T1-929	retraité	33	1281	1282	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb12107332r	2011-12-12 : ajout du related term "assurance vieillesse", suite à l'appel à commentaires d'octobre 2010-mars 2011
840	1	1	67717/T1-783	atelier de charité	33	1285	1286	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb11978440k	
1042	1	1	67717/T1-1357	faune sauvage	1040	1994	1995	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1		
1043	1	1	67717/T1-1057	parc naturel	1040	1996	1997	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1		
1044	1	1	67717/T1-599	réserve naturelle	1040	1998	1999	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb11935584x	
1045	1	1	67717/T1-1211	flore sauvage	1040	2000	2001	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb11933145f	
1046	1	1	67717/T1-1303	biodiversité	1040	2002	2003	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1		
1031	1	1	67717/T1-1342	terrain de camping	88	1506	1507	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1	http://data.bnf.fr/ark:/12148/cb11959609t	
1265	1	1	67717/T1-237	auberge de jeunesse	368	1520	1521	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1	http://data.bnf.fr/ark:/12148/cb11951804q	
1356	1	1	67717/T1-1130	loyer immobilier	73	1762	1763	2013-01-15 16:46:30	1	2013-01-15 16:47:59	1		
599	1	1	67717/T1-666	affaire civile	50	2281	2284	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
760	1	1	67717/T1-607	cours d'eau	179	1922	1923	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb11952208p	
551	1	1	67717/T1-137	chômage partiel	826	1246	1247	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb119417403	
515	1	1	67717/T1-583	formateur	536	1234	1235	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11931441d	
516	1	1	67717/T1-777	médecin hospitalier	538	1386	1387	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
517	1	1	67717/T1-1056	fonctionnaire de l'État	540	930	931	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11975907d	
847	1	1	67717/T1-458	association de défense de l'environnement	1040	1988	1989	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1		
1235	1	1	67717/T1-934	régime matrimonial	899	1308	1309	2013-01-15 16:46:30	1	2013-01-15 16:47:49	1	http://data.bnf.fr/ark:/12148/cb11982889s	
957	1	1	67717/T1-305	salle des fêtes	484	1572	1573	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1		
1084	1	1	67717/T1-1194	demandeur d'emploi	826	1248	1249	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11977440b	
1212	1	1	67717/T1-291	assurance décès	591	1208	1209	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1	http://data.bnf.fr/ark:/12148/cb11951552r	
1243	1	1	67717/T1-210	employé de maison	936	1352	1353	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb11974329t	
1244	1	1	67717/T1-1346	ouvrier du bâtiment	71	1746	1747	2013-01-15 16:46:30	1	2013-01-15 16:47:50	1	http://data.bnf.fr/ark:/12148/cb119403266	
796	1	1	67717/T1-123	patrimoine culturel	38	1609	1634	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb133190710	2011-12-07 : ajout des narrower terms "langue régionale" et "patrimoine architectural", et suppression des narrower term "objet d'art", "édifice classé" et "monument historique", suite à l'appel à commentaires d'octobre 2010-mars 2011
1484	1	1	67717/T1-1030	dette	901	1316	1317	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1	http://data.bnf.fr/ark:/12148/cb11950122g	
1387	1	1	67717/T1-899	animateur socioculturel	843	1640	1641	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1	http://data.bnf.fr/ark:/12148/cb119595531	
802	1	1	67717/T1-1437	fête	40	1683	1690	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb119344654	2011-12-07 : suppression du related term "langue régionale", suite à l'appel à commentaires d'octobre 2010-mars 2011
1487	1	1	67717/T1-840	commissaire enquêteur	1349	2012	2013	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1		
1452	1	1	67717/T1-1120	voie piétonne	627	1834	1835	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1	http://data.bnf.fr/ark:/12148/cb11932966c	
1188	1	1	67717/T1-1457	établissement sanitaire privé	800	1404	1405	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1		
1190	1	1	67717/T1-1062	hôpital psychiatrique	800	1408	1409	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1		
498	1	1	67717/T1-1482	installation sanitaire publique	483	1866	1867	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		2011-12-07: ajout du concept "installation sanitaire publique" avec comme broader term "équipement collectif", suite à l'appel à commentaires d'octobre 2010-mars 2011
1424	1	1	67717/T1-725	bien spolié	999	2966	2967	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
1204	1	1	67717/T1-715	collège	523	2090	2091	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://data.bnf.fr/ark:/12148/cb11936525n	
846	1	1	67717/T1-532	université	523	2078	2079	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb11933731d	
379	1	1	67717/T1-1403	crimes et délits	52	2355	2414	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1		
1422	1	1	67717/T1-1037	classement sans suite	53	2473	2474	2013-01-15 16:46:31	1	2013-01-15 16:48:04	1		
1336	1	1	67717/T1-1041	enseignement technique	990	2152	2153	2013-01-15 16:46:30	1	2013-01-15 16:47:57	1	http://data.bnf.fr/ark:/12148/cb11933987k	
1260	1	1	67717/T1-331	qualification correctionnelle	52	2419	2420	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1		
1474	1	1	67717/T1-787	bibliothèque scolaire	991	2156	2157	2013-01-15 16:46:31	1	2013-01-15 16:48:09	1	http://data.bnf.fr/ark:/12148/cb11947999r	
1308	1	1	67717/T1-270	réclusion criminelle	1309	2444	2445	2013-01-15 16:46:30	1	2013-01-15 16:47:55	1	http://data.bnf.fr/ark:/12148/cb11952257w	
1294	1	1	67717/T1-294	droits de scolarité	525	2100	2101	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		
1395	1	1	67717/T1-1353	psychopédagogie	1098	2192	2193	2013-01-15 16:46:31	1	2013-01-15 16:48:02	1		
985	1	1	67717/T1-789	parc de loisirs	40	1691	1692	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb11997968h	
986	1	1	67717/T1-933	casino	1296	1712	1713	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb11937726s	
987	1	1	67717/T1-119	alphabétisation	590	1194	1195	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb119308716	
875	1	1	67717/T1-1023	gaz naturel	186	182	183	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://data.bnf.fr/ark:/12148/cb119315274	
743	1	1	67717/T1-793	institution ecclésiastique	57	2683	2704	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb11954011h	
990	1	1	67717/T1-1331	enseignement professionnel	47	2143	2154	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb11935294d	
263	1	1	67717/T1-15	francophonie	61	2909	2910	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://dbpedia.org/resource/Category:La_Francophonie	
1486	1	1	67717/T1-836	sorcellerie	1171	2828	2829	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1	http://data.bnf.fr/ark:/12148/cb11939659z	
1454	1	1	67717/T1-660	hymne national	278	2536	2537	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1	http://data.bnf.fr/ark:/12148/cb11948136w	
1455	1	1	67717/T1-757	drapeau	278	2538	2539	2013-01-15 16:46:31	1	2013-01-15 16:48:07	1	http://data.bnf.fr/ark:/12148/cb11931902w	
1504	2	1	67717/T2-20	conciliation	0	3007	3008	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1505	2	1	67717/T2-53	location	0	3009	3010	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1506	2	1	67717/T2-65	recrutement	0	3011	3012	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1507	2	1	67717/T2-51	informatisation	0	3013	3014	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1508	2	1	67717/T2-31	délégation	0	3015	3016	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1509	2	1	67717/T2-59	programmation	0	3017	3018	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1510	2	1	67717/T2-28	contrôle sanitaire	0	3019	3020	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1511	2	1	67717/T2-43	exploitation commerciale	0	3021	3022	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1512	2	1	67717/T2-68	restauration	0	3023	3024	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1513	2	1	67717/T2-23	contrôle budgétaire	0	3025	3026	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1514	2	1	67717/T2-16	classement	0	3027	3028	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1515	2	1	67717/T2-39	équipement matériel	0	3029	3030	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1516	2	1	67717/T2-1	abolition	0	3031	3032	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1517	2	1	67717/T2-50	indemnisation	0	3033	3034	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1518	2	1	67717/T2-21	construction	0	3035	3036	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1519	2	1	67717/T2-45	fonctionnement	0	3037	3038	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1520	2	1	67717/T2-6	agrément	0	3039	3040	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1521	2	1	67717/T2-19	concession	0	3041	3042	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1522	2	1	67717/T2-4	adhésion	0	3043	3044	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1523	2	1	67717/T2-67	réquisition	0	3045	3046	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1524	2	1	67717/T2-10	assiette	0	3047	3048	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		2011-04-07: Ajout du terme non-descripteur évaluation fiscale suite à une remarque de l'éditeur SICEM
1525	2	1	67717/T2-63	recours hiérarchique	0	3049	3050	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1526	2	1	67717/T2-26	contrôle de sécurité	0	3051	3052	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1527	2	1	67717/T2-33	désignation	0	3053	3054	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1528	2	1	67717/T2-71	tutelle financière	0	3055	3056	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
170	1	1	67717/T1-749	garde particulier	303	744	745	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
705	1	1	67717/T1-347	transfert de compétences	29	973	982	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1		
755	1	1	67717/T1-1055	catastrophe industrielle	753	892	893	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb125613536	
756	1	1	67717/T1-698	catastrophe naturelle	753	894	895	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb11931689z	2011-12-07: ajout du related term "météorologie", suite à l'appel à commentaires d'octobre 2010-mars 2011
757	1	1	67717/T1-1151	transport de matières dangereuses	28	897	898	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
333	1	1	67717/T1-821	dissident	329	2590	2591	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb119318885	
1485	1	1	67717/T1-1442	diplomate	269	2926	2927	2013-01-15 16:46:31	1	2013-01-15 16:48:10	1	http://data.bnf.fr/ark:/12148/cb126501792	
354	1	1	67717/T1-229	royalisme	330	2634	2635	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb12027733f	
1497	1	1	67717/T1-1491	manifestation d'adhésion au régime	288	2570	2571	2013-01-15 16:46:31	1	2013-01-15 16:48:11	1		2011-12-12: ajout du concept "manifestation d'adhésion au régime" avec comme broader term "manifestation publique", suite à l'appel à commentaires d'octobre 2010-mars 2011
1499	2	1	67717/T2-38	entretien	0	2997	2998	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1529	2	1	67717/T2-55	organisation	0	3057	3058	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		2011-04-07: Ajout des termes non-descripteurs réforme administrative et modernisation suite à une remarque de l'éditeur SICEM
1500	2	1	67717/T2-17	commissionnement	0	2999	3000	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1501	2	1	67717/T2-42	exécution budgétaire	0	3001	3002	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1530	2	1	67717/T2-48	gestion du personnel	0	3059	3060	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1531	2	1	67717/T2-2	abstention	0	3061	3062	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1532	2	1	67717/T2-64	recouvrement	0	3063	3064	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1533	2	1	67717/T2-9	arpentage	0	3065	3066	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1502	2	1	67717/T2-40	évacuation	0	3003	3004	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1503	2	1	67717/T2-62	recensement	0	3005	3006	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1534	2	1	67717/T2-69	tarification	0	3067	3068	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1535	2	1	67717/T2-27	contrôle fiscal	0	3069	3070	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1536	2	1	67717/T2-35	dissolution	0	3071	3072	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1537	2	1	67717/T2-24	contrôle de gestion	0	3073	3074	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1538	2	1	67717/T2-57	préparation budgétaire	0	3075	3076	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1539	2	1	67717/T2-5	adjudication	0	3077	3078	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1540	2	1	67717/T2-12	assurance	0	3079	3080	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1541	2	1	67717/T2-46	garantie d'emprunt	0	3081	3082	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1542	2	1	67717/T2-49	immatriculation	0	3083	3084	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1543	2	1	67717/T2-34	destruction	0	3085	3086	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1544	2	1	67717/T2-54	numérisation	0	3087	3088	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1545	2	1	67717/T2-61	protection	0	3089	3090	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1546	2	1	67717/T2-56	placement	0	3091	3092	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1547	2	1	67717/T2-32	dénomination	0	3093	3094	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1548	2	1	67717/T2-66	réglementation	0	3095	3096	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1549	2	1	67717/T2-15	cession	0	3097	3098	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1550	2	1	67717/T2-29	coopération	0	3099	3100	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1551	2	1	67717/T2-44	financement	0	3101	3102	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
803	1	1	67717/T1-991	patrimoine ethnologique	796	1614	1615	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1		2011-12-07 : suppression du related term "langue régionale", suite à l'appel à commentaires d'octobre 2010-mars 2011
34	1	1	67717/T1-942	condition des personnes et des biens	5	1288	1319	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1		
720	1	1	67717/T1-878	greffier de commerce	254	2302	2303	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1		
1552	2	1	67717/T2-22	contrôle	0	3103	3104	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1553	2	1	67717/T2-11	assignation	0	3105	3106	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1554	2	1	67717/T2-60	promotion	0	3107	3108	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1555	2	1	67717/T2-7	aménagement	0	3109	3110	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1556	2	1	67717/T2-37	enquête publique	0	3111	3112	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1557	2	1	67717/T2-18	communication	0	3113	3114	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1558	2	1	67717/T2-58	prévention	0	3115	3116	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1559	2	1	67717/T2-3	acquisition	0	3117	3118	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1560	2	1	67717/T2-14	caution	0	3119	3120	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1561	2	1	67717/T2-41	évaluation	0	3121	3122	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1562	2	1	67717/T2-52	inspection	0	3123	3124	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1563	2	1	67717/T2-70	tutelle administrative	0	3125	3126	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1564	2	1	67717/T2-30	coordination	0	3127	3128	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1565	2	1	67717/T2-13	autorisation	0	3129	3130	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		2011-04-07: Ajout des termes non-descripteurs dérogation, dispense et interdiction suite à une remarque de l'éditeur SICEM
1566	2	1	67717/T2-47	gestion comptable	0	3131	3132	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1567	2	1	67717/T2-8	appel d'offres	0	3133	3134	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1568	2	1	67717/T2-25	contrôle de légalité	0	3135	3136	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1569	2	1	67717/T2-36	enquête	0	3137	3138	2013-01-15 16:48:11	1	2013-01-15 16:48:11	1		
1570	3	1	67717/T4-1	préhistoire	0	3139	3140	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb133183738	
1579	3	1	67717/T4-47	Cinquième République (1958- )	1571	3174	3175	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11936029n	
1580	3	1	67717/T4-46	21e siècle	1571	3176	3177	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11930812p	
1571	3	1	67717/T4-8	époque contemporaine	0	3141	3272	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb119760222	
1581	3	1	67717/T4-45	20e siècle	1571	3178	3263	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11940256v	
1582	3	1	67717/T4-49	Troisième République (1870-1940)	1571	3264	3265	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11941855d	
1576	3	1	67717/T4-4	époque moderne	0	3351	3398	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1		
93	1	1	67717/T1-674	établissement pénitentiaire	49	2237	2238	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb13318425p	2011-04-18: Ajout d'un accent au terme "pénitentiaire" suite à une remarque de l'éditeur ANAPHORE
141	1	1	67717/T1-1138	finances départementales	137	624	625	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1		2011-12-06: suppression du altlabel "comptabilité départementale" suite à l'appel à commentaires d'octobre 2010-mars 2011
111	1	1	67717/T1-1356	culture expérimentale	99	2024	2025	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		2011-12-07 : changement de broader term ("recherche appliquée" et non plus "recherche scientifique"), suite à l'appel à commentaires d'octobre 2010-mars 2011
1635	3	1	67717/T4-86	unité italienne (19e siècle)	1578	3161	3162	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1		
1638	3	1	67717/T4-75	Deuxième République (1848-1852)	1578	3167	3168	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119424306	
1641	3	1	67717/T4-95	conférence de Yalta (1945)	1581	3179	3180	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1		
1626	3	1	67717/T4-80	Restauration (1815-1830)	1578	3143	3144	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11941853q	
1629	3	1	67717/T4-76	guerre de Crimée (1853-1856)	1578	3149	3150	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11939099j	
1632	3	1	67717/T4-82	Révolution de 1848	1578	3155	3156	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119543805	
1605	3	1	67717/T4-37	Directoire (1795-1799)	1574	3322	3323	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11943503t	
1628	3	1	67717/T4-84	Second Empire (1852-1870)	1578	3147	3148	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11936618n	
1643	3	1	67717/T4-96	crise de 1929	1581	3183	3184	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11937041r	
1646	3	1	67717/T4-90	blocus de Berlin (1948-1949)	1581	3189	3190	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11966424n	
1606	3	1	67717/T4-38	guerres de la Révolution (1792-1799)	1574	3324	3325	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1		
1607	3	1	67717/T4-39	guerres de Vendée (1793-1799)	1574	3326	3327	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11940982b	
1627	3	1	67717/T4-72	abolition de l'esclavage (1848)	1578	3145	3146	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1		
1637	3	1	67717/T4-78	guerre de 1870	1578	3165	3166	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11954150p	
1631	3	1	67717/T4-73	Commune de Paris (1871)	1578	3153	3154	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119456147	
1636	3	1	67717/T4-77	guerre de Sécession (1861-1865)	1578	3163	3164	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119795763	
1634	3	1	67717/T4-85	unité allemande (1871)	1578	3159	3160	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1		
1630	3	1	67717/T4-74	coup d'État du 2 décembre 1851	1578	3151	3152	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119418542	
1640	3	1	67717/T4-83	révolution industrielle	1578	3171	3172	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11950231q	
1639	3	1	67717/T4-79	monarchie de Juillet (1830-1848)	1578	3169	3170	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11939017c	
1633	3	1	67717/T4-81	Révolution de 1830	1578	3157	3158	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11938565c	
1642	3	1	67717/T4-94	conférence de Munich (1938)	1581	3181	3182	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1		
1645	3	1	67717/T4-98	Entre-deux-guerres	1581	3187	3188	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb124514622	
1644	3	1	67717/T4-91	Collaboration (1940-1944)	1581	3185	3186	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11952298g	
437	1	1	67717/T1-1300	recherche dans l'intérêt des familles	294	696	697	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1		
725	1	1	67717/T1-684	centre de loisirs	40	1681	1682	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1		
726	1	1	67717/T1-311	centre de vacances	368	1518	1519	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb11954068g	
858	1	1	67717/T1-806	sage femme	538	1396	1397	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb119384115	
724	1	1	67717/T1-62	congés scolaires	727	2166	2167	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb11969152s	
1648	3	1	67717/T4-117	mur de Berlin (1961-1989)	1581	3193	3194	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb119380937	
1649	3	1	67717/T4-106	guerre de Corée (1950-1953)	1581	3195	3196	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11952040m	
1650	3	1	67717/T4-114	guerre 1939-1945	1581	3197	3198	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11996115g	
1651	3	1	67717/T4-125	Révolution russe (1917)	1581	3199	3200	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11965185s	
1652	3	1	67717/T4-99	événements de mai 1968	1581	3201	3202	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11943508j	
1653	3	1	67717/T4-105	guerre d'Indochine (1946-1954)	1581	3203	3204	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11938117d	
1654	3	1	67717/T4-127	traité de l'Atlantique Nord (1949)	1581	3205	3206	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb119483322	
1655	3	1	67717/T4-110	guerre froide (1945-1989)	1581	3207	3208	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb12257064r	
1656	3	1	67717/T4-88	affaire de Suez (1956)	1581	3209	3210	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb119594694	
1657	3	1	67717/T4-121	printemps de Prague (1968)	1581	3211	3212	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb119482919	
1658	3	1	67717/T4-116	Libération (1944-1945)	1581	3213	3214	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11939000n	
1659	3	1	67717/T4-118	occupation allemande (1940-1944)	1581	3215	3216	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11939017c	
1660	3	1	67717/T4-107	guerre des Malouines (1982)	1581	3217	3218	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1		
1661	3	1	67717/T4-111	guerre israélo-arabe (1948- )	1581	3219	3220	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1		
1662	3	1	67717/T4-128	traité de Versailles (1919)	1581	3221	3222	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11954269c	
1663	3	1	67717/T4-87	affaire Dreyfus (1894-1906)	1581	3223	3224	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11942701f	
1664	3	1	67717/T4-109	guerre du Vietnam (1962-1973)	1581	3225	3226	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11937639v	
1665	3	1	67717/T4-92	concile Vatican 2 (1962-1965)	1581	3227	3228	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1		
1666	3	1	67717/T4-108	guerre du Golfe (1990-1991)	1581	3229	3230	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb12181105h	
1667	3	1	67717/T4-119	pacte germano-soviétique (1939)	1581	3231	3232	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11954252n	
1668	3	1	67717/T4-122	Quatrième République (1946-1958)	1581	3233	3234	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11938560n	
1670	3	1	67717/T4-112	guerre russo-japonaise (1904-1905)	1581	3237	3238	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb11954153q	
1671	3	1	67717/T4-97	crise pétrolière (1973-1979)	1581	3239	3240	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1		
1672	3	1	67717/T4-100	Front populaire (1936-1938)	1581	3241	3242	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb126485191	
1673	3	1	67717/T4-93	conférence de Bandung (1955)	1581	3243	3244	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1		
1674	3	1	67717/T4-120	plan Marshall (1947)	1581	3245	3246	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11998785s	
1675	3	1	67717/T4-102	gouvernement provisoire (1944-1946)	1581	3247	3248	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1		
1676	3	1	67717/T4-123	République de Weimar (1919-1933)	1581	3249	3250	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11941200g	
1677	3	1	67717/T4-113	guerre 1914-1918	1581	3251	3252	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11939093g	
1678	3	1	67717/T4-104	guerre d'Espagne (1936-1939)	1581	3253	3254	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11936368c	
1679	3	1	67717/T4-115	insurrection hongroise (1956)	1581	3255	3256	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11943662m	
1680	3	1	67717/T4-126	séparation des Églises et de l'État (1905)	1581	3257	3258	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb13736218g	
1681	3	1	67717/T4-124	réunification allemande (1989-1990)	1581	3259	3260	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb12223796q	
1682	3	1	67717/T4-89	belle époque	1581	3261	3262	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1		
1691	3	1	67717/T4-129	traité de Rome (1957)	1583	3269	3270	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb12017362q	
1683	3	1	67717/T4-57	Renaissance	1617	3367	3368	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb13318531w	
1690	3	1	67717/T4-130	traité sur l'union européenne (1992)	1583	3267	3268	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb12246554c	
1692	3	1	67717/T4-66	guerre de Succession d'Autriche (1740-1748)	1619	3385	3386	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11967021r	
1693	3	1	67717/T4-68	mouvement des Lumières	1619	3387	3388	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119405660	
1694	3	1	67717/T4-67	indépendance des États-Unis (1775-1782)	1619	3389	3390	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11936377b	
1578	3	1	67717/T4-44	19e siècle	1571	3142	3173	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb119731260	
1695	3	1	67717/T4-65	guerre de Sept Ans (1756-1763)	1619	3391	3392	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11941570g	
1696	3	1	67717/T4-62	guerre de Succession d'Espagne (1701-1714)	1619	3393	3394	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119670223	
721	1	1	67717/T1-390	huissier de justice	254	2304	2305	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb119320615	
1700	3	1	67717/T4-50	croisade contre les Albigeois (1208-1229)	1596	3299	3300	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119362594	
859	1	1	67717/T1-90	fiscalité pétrolière	607	804	805	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://dbpedia.org/resource/Category:Hydrocarbons	
861	1	1	67717/T1-975	monopoles fiscaux	607	806	807	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1		
862	1	1	67717/T1-1086	droits de circulation	607	808	809	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb12647453p	
777	1	1	67717/T1-1227	bâtiment administratif	29	1013	1030	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		
1584	3	1	67717/T4-26	guerre de Cent Ans (1337-1453)	1572	3274	3275	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11938577c	
1585	3	1	67717/T4-25	expansion de l'islam (622-1492)	1572	3276	3277	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1		
1586	3	1	67717/T4-24	croisades (1095-1291)	1572	3278	3279	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1		
1587	3	1	67717/T4-27	invasions scandinaves (790-1030)	1572	3280	3281	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1		
1588	3	1	67717/T4-18	10e siècle	1572	3282	3283	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11936234s	
1589	3	1	67717/T4-13	5e siècle	1572	3284	3285	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb134941549	
1590	3	1	67717/T4-19	11e siècle	1572	3286	3287	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11993184k	
1591	3	1	67717/T4-14	6e siècle	1572	3288	3289	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb136163948	
1592	3	1	67717/T4-17	9e siècle	1572	3290	3291	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb145967135	
1593	3	1	67717/T4-22	14e siècle	1572	3292	3293	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb119658026	
1594	3	1	67717/T4-15	7e siècle	1572	3294	3295	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb14596712t	
1595	3	1	67717/T4-20	12e siècle	1572	3296	3297	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb119463061	
434	1	1	67717/T1-962	profession réglementée	294	690	691	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1		2011-12-06: Ajout du related term "boulangerie" suite à l'appel à commentaires d'octobre 2010-mars 2011
638	1	1	67717/T1-289	protection des végétaux	23	591	596	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb119458154	
639	1	1	67717/T1-415	apiculture	23	597	598	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://dbpedia.org/resource/Category:Beekeeping	
746	1	1	67717/T1-539	inquisition	743	2690	2691	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://dbpedia.org/resource/Category:Inquisition	
1596	3	1	67717/T4-21	13e siècle	1572	3298	3301	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11958603h	
1573	3	1	67717/T4-2	Antiquité	0	3307	3316	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11975677r	
1604	3	1	67717/T4-35	campagne d'Italie (1796-1797)	1574	3320	3321	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb122044972	
1618	3	1	67717/T4-31	monarchie d'Ancien Régime	1576	3382	3383	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1		
1597	3	1	67717/T4-16	8e siècle	1572	3302	3303	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb137527038	
1598	3	1	67717/T4-23	15e siècle	1572	3304	3305	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11973624p	
1599	3	1	67717/T4-11	Bas-Empire	1573	3308	3309	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11938653n	
1600	3	1	67717/T4-12	guerre des Gaules	1573	3310	3311	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11951369x	
1619	3	1	67717/T4-30	18e siècle	1576	3384	3397	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11939402f	
1601	3	1	67717/T4-10	Antiquité gauloise	1573	3312	3313	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1		
1602	3	1	67717/T4-9	Antiquité gallo-romaine	1573	3314	3315	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1		
1615	3	1	67717/T4-42	congrès de Vienne (1815)	1575	3348	3349	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1		
770	1	1	67717/T1-1031	taxe d'apprentissage	606	800	801	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb122706853	
195	1	1	67717/T1-1467	artisanat	14	207	208	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb13318591r	2011-12-07: transformation en concept de "artisanat", ancien altLabel de "artisan", avec pour altlabel "métier d'art", auparavant altLabel de "artisan", et pour related term "artisan", suite à l'appel à commentaires d'octobre 2010-mars 2011
461	1	1	67717/T1-736	réseau ferroviaire	42	1817	1824	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb11934482q	
723	1	1	67717/T1-1039	enrôlement	156	2854	2855	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1		
386	1	1	67717/T1-364	faux témoignage	379	2372	2373	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb119835040	
187	1	1	67717/T1-244	industrie agro-alimentaire	14	185	186	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb11949682p	2011-12-07 : modification du prefLabel "industrie agro alimentaire" en "industrie agro-alimentaire"
748	1	1	67717/T1-1053	administration diocésaine	743	2694	2695	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
691	1	1	67717/T1-1497	allocation militaire	588	1174	1175	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		2011-12-13 : ajout du concept "allocation militaire" avec comme broader term "aide sociale", suite à l'appel à commentaires d'octobre 2010-mars 2011
507	1	1	67717/T1-378	déchet animal	505	1946	1947	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11967192j	
1603	3	1	67717/T4-36	Convention nationale (1792-1795)	1574	3318	3319	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb119572364	
1608	3	1	67717/T4-34	campagne d'Égypte (1798-1801)	1574	3328	3329	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11941852c	
1609	3	1	67717/T4-33	Assemblée législative (1791-1792)	1574	3330	3331	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb12009048m	
1610	3	1	67717/T4-32	Assemblée constituante (1789-1791)	1574	3332	3333	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11951371g	
1611	3	1	67717/T4-40	Terreur (1793-1794)	1574	3334	3335	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb11980490d	
1612	3	1	67717/T4-131	Première République (1792-1804)	1574	3336	3337	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1		2011-12-06 : ajout du concept "Première République" suite à l'appel à commentaires d'octobre 2010-mars 2011
1613	3	1	67717/T4-41	Cent jours (1815)	1575	3340	3341	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb119637462	
1614	3	1	67717/T4-43	guerres napoléoniennes (1800-1815)	1575	3342	3347	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb120356943	
1616	3	1	67717/T4-29	17e siècle	1576	3352	3365	2013-01-15 16:48:12	1	2013-01-15 16:48:13	1	http://data.bnf.fr/ark:/12148/cb120419182	
1617	3	1	67717/T4-28	16e siècle	1576	3366	3381	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119789194	
1620	3	1	67717/T4-58	Fronde (1648-1652)	1616	3353	3354	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11943500s	
1621	3	1	67717/T4-59	guerre de Dévolution (1667-1668)	1616	3355	3356	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb120672854	
1622	3	1	67717/T4-64	révocation de l'édit de Nantes (1685)	1616	3357	3358	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb11985657b	
1623	3	1	67717/T4-63	guerre de Trente Ans (1618-1648)	1616	3359	3360	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119385761	
1572	3	1	67717/T4-3	Moyen Âge	0	3273	3306	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb133185191	
423	1	1	67717/T1-817	société coopérative	16	331	332	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb119501005	2011-12-13 : suppression du altLabel "économie sociale", suite à l'appel à commentaires d'octobre 2010-mars 2011
190	1	1	67717/T1-315	bâtiment industriel	14	195	198	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb11931782t	
553	1	1	67717/T1-1277	licenciement économique	834	1272	1273	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb12225367h	
384	1	1	67717/T1-316	corruption	379	2368	2369	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1	http://data.bnf.fr/ark:/12148/cb12011363m	
1624	3	1	67717/T4-61	guerre de la Ligue d'Augsbourg (1688-1697)	1616	3361	3362	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb120048160	
1625	3	1	67717/T4-60	guerre de Hollande (1672-1678)	1616	3363	3364	2013-01-15 16:48:12	1	2013-01-15 16:48:14	1	http://data.bnf.fr/ark:/12148/cb119416388	
1697	3	1	67717/T4-69	Régence (1715-1723)	1619	3395	3396	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb11937062q	
1684	3	1	67717/T4-54	guerres d'Italie (1494-1559)	1617	3369	3370	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119454705	
1669	3	1	67717/T4-101	gouvernement de Vichy (1940-1944)	1581	3235	3236	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb12064507f	
1685	3	1	67717/T4-55	guerres de religion (1562-1598)	1617	3371	3372	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1		
139	1	1	67717/T1-888	finances communales	137	620	621	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb11950195p	2011-12-06: suppression du altlabel "comptabilité communale" suite à l'appel à commentaires d'octobre 2010-mars 2011
640	1	1	67717/T1-986	aquaculture	23	599	604	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb11930924z	
1686	3	1	67717/T4-56	Réforme protestante	1617	3373	3374	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119404792	
1687	3	1	67717/T4-53	Grandes découvertes	1617	3375	3376	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb133189252	
684	1	1	67717/T1-1004	restauration des terrains en montagne	677	1958	1959	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
1688	3	1	67717/T4-52	édit de Nantes (1598)	1617	3377	3378	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1		
1647	3	1	67717/T4-103	guerre d'Algérie (1954-1962)	1581	3191	3192	2013-01-15 16:48:12	1	2013-01-15 16:48:15	1	http://data.bnf.fr/ark:/12148/cb119519776	
1689	3	1	67717/T4-51	Contre-Réforme	1617	3379	3380	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119522049	
1698	3	1	67717/T4-71	campagne de Russie (1812)	1614	3343	3344	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb119587791	
1583	3	1	67717/T4-48	construction européenne	1571	3266	3271	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb119522765	
1699	3	1	67717/T4-70	campagne de France (1814)	1614	3345	3346	2013-01-15 16:48:12	1	2013-01-15 16:48:16	1	http://data.bnf.fr/ark:/12148/cb12124534p	
852	1	1	67717/T1-397	libraire	233	220	221	2013-01-15 16:46:26	1	2013-01-15 16:47:22	1	http://dbpedia.org/resource/Category:Booksellers_%28people%29	
1574	3	1	67717/T4-5	Révolution de 1789	0	3317	3338	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11941849g	2011-12-06 : ajout du concept "Première République" comme narrower term de "Révolution de 1789" suite à l'appel à commentaires d'octobre 2010-mars 2011
1575	3	1	67717/T4-7	Premier Empire (1804-1815)	0	3339	3350	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb119390161	
1577	3	1	67717/T4-6	Consulat (1799-1804)	0	3399	3400	2013-01-15 16:48:12	1	2013-01-15 16:48:12	1	http://data.bnf.fr/ark:/12148/cb11954378m	
1701	4	1	67717/T3-34	bulle pontificale	0	3401	3402	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1702	4	1	67717/T3-53	catalogue	0	3403	3404	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1711	4	1	67717/T3-199	registre matricule	0	3421	3422	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1712	4	1	67717/T3-196	registre des entrées et des sorties	0	3423	3424	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1713	4	1	67717/T3-76	décision budgétaire modificative	0	3425	3426	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1714	4	1	67717/T3-18	atlas	0	3427	3428	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1715	4	1	67717/T3-217	schéma directeur d'aménagement et d'urbanisme	0	3429	3430	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1716	4	1	67717/T3-81	déclaration de succession	0	3431	3432	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1717	4	1	67717/T3-8	agenda	0	3433	3434	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1718	4	1	67717/T3-77	décision de paiement	0	3435	3436	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1703	4	1	67717/T3-198	registre du courrier	0	3405	3406	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1704	4	1	67717/T3-83	déclaration d'intention d'aliéner	0	3407	3408	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1705	4	1	67717/T3-157	permis de recherche	0	3409	3410	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1706	4	1	67717/T3-56	chanson	0	3411	3412	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1707	4	1	67717/T3-118	livre comptable	0	3413	3414	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1708	4	1	67717/T3-185	rapport d'activité	0	3415	3416	2013-01-15 16:48:16	1	2013-01-15 16:48:16	1		
1709	4	1	67717/T3-203	règlement d'entreprise	0	3417	3418	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1710	4	1	67717/T3-32	budget primitif	0	3419	3420	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1719	4	1	67717/T3-64	compte d'exploitation	0	3437	3438	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1720	4	1	67717/T3-99	dossier des ouvrages exécutés	0	3439	3440	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1721	4	1	67717/T3-4	acte de congrès	0	3441	3442	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1722	4	1	67717/T3-116	liste électorale	0	3443	3444	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1723	4	1	67717/T3-74	coupure de presse	0	3445	3446	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1724	4	1	67717/T3-26	bilan comptable	0	3447	3448	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1725	4	1	67717/T3-96	dossier de pension	0	3449	3450	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1726	4	1	67717/T3-197	registre du commerce et des sociétés	0	3451	3452	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1727	4	1	67717/T3-94	dossier de carrière	0	3453	3454	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1728	4	1	67717/T3-168	portrait	0	3455	3456	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1729	4	1	67717/T3-180	protêt	0	3457	3458	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1730	4	1	67717/T3-146	notice individuelle	0	3459	3460	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1731	4	1	67717/T3-23	bail	0	3461	3462	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1732	4	1	67717/T3-13	arrêté d'alignement	0	3463	3464	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1733	4	1	67717/T3-125	main courante	0	3465	3466	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1734	4	1	67717/T3-158	pétition	0	3467	3468	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1735	4	1	67717/T3-112	inventaire	0	3469	3470	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1736	4	1	67717/T3-214	rôle d'imposition	0	3471	3472	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1764	4	1	67717/T3-9	album	0	3527	3528	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1765	4	1	67717/T3-179	programme	0	3529	3530	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1766	4	1	67717/T3-192	registre d'appel	0	3531	3532	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1767	4	1	67717/T3-212	rôle	0	3533	3534	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1768	4	1	67717/T3-129	marché public	0	3535	3536	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1769	4	1	67717/T3-231	tract	0	3537	3538	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
597	1	1	67717/T1-1154	contentieux administratif	50	2265	2270	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1	http://data.bnf.fr/ark:/12148/cb120499138	
885	1	1	67717/T1-336	assistance éducative	598	2274	2275	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb13319494r	
1770	4	1	67717/T3-59	chrono	0	3539	3540	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-18: Ajout du terme non-descriptif "minutier" suite à une remarque de l'éditeur SICEM
1771	4	1	67717/T3-17	arrêté préfectoral	0	3541	3542	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
291	1	1	67717/T1-891	réunion publique	288	2562	2563	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb133408631	2011-12-12 : changement de broader term ("manifestation publique" au lieu de "réunion publique", suppression des narrower "manifestation de protestation" et "mouvement populaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
1737	4	1	67717/T3-151	panneau d'exposition	0	3473	3474	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1738	4	1	67717/T3-183	questionnaire d'enquête	0	3475	3476	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1739	4	1	67717/T3-159	document photographique	0	3477	3478	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-07: Modification des termes "ektachrome" en "ektachrome" et "diapo" en "diapositive" suite à une remarque de l'éditeur SICEM
1740	4	1	67717/T3-60	circulaire	0	3479	3480	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1741	4	1	67717/T3-33	budget supplémentaire	0	3481	3482	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1742	4	1	67717/T3-115	liste d'émargement	0	3483	3484	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1743	4	1	67717/T3-202	règlement	0	3485	3486	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1744	4	1	67717/T3-204	règlement intérieur	0	3487	3488	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1745	4	1	67717/T3-57	charte	0	3489	3490	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1746	4	1	67717/T3-156	permis de démolir	0	3491	3492	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1747	4	1	67717/T3-84	déclaration d'utilité publique	0	3493	3494	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1748	4	1	67717/T3-3	acte civil public	0	3495	3496	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1749	4	1	67717/T3-89	document audiovisuel	0	3497	3498	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-18: Remplacement du terme non-descriptif "CD-audio" par "cd-audio" suite à une remarque de l'éditeur SICEM
1772	4	1	67717/T3-162	plan cadastral	0	3543	3544	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1750	4	1	67717/T3-19	atlas terrier	0	3499	3500	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1751	4	1	67717/T3-111	inscription hypothécaire	0	3501	3502	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1773	4	1	67717/T3-208	répertoire des opérations de douanes	0	3545	3546	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1774	4	1	67717/T3-148	ordre de mission	0	3547	3548	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1752	4	1	67717/T3-25	bibliographie	0	3503	3504	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1753	4	1	67717/T3-122	livre liturgique	0	3505	3506	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1754	4	1	67717/T3-127	manifeste de navire	0	3507	3508	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1755	4	1	67717/T3-52	cartulaire	0	3509	3510	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1756	4	1	67717/T3-132	matrice d'imposition	0	3511	3512	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1757	4	1	67717/T3-216	schéma directeur	0	3513	3514	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1758	4	1	67717/T3-105	étude	0	3515	3516	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1759	4	1	67717/T3-16	arrêté du président du conseil régional	0	3517	3518	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1760	4	1	67717/T3-27	bilan pédagogique	0	3519	3520	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1761	4	1	67717/T3-176	procès-verbal d'examen	0	3521	3522	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1762	4	1	67717/T3-206	répertoire civil	0	3523	3524	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1763	4	1	67717/T3-135	menu	0	3525	3526	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1775	4	1	67717/T3-126	manifeste d'aéronef	0	3549	3550	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1776	4	1	67717/T3-140	modèle de fabrique	0	3551	3552	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1777	4	1	67717/T3-67	contrat	0	3553	3554	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1778	4	1	67717/T3-61	communiqué de presse	0	3555	3556	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1779	4	1	67717/T3-170	procès-verbal	0	3557	3558	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1780	4	1	67717/T3-209	répertoire d'officier public ministériel	0	3559	3560	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1781	4	1	67717/T3-1	accord de travail	0	3561	3562	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1782	4	1	67717/T3-103	état du montant des rôles	0	3563	3564	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1783	4	1	67717/T3-131	matrice cadastrale	0	3565	3566	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1784	4	1	67717/T3-45	carte de membre	0	3567	3568	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1785	4	1	67717/T3-145	note de service	0	3569	3570	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1786	4	1	67717/T3-22	avis	0	3571	3572	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1787	4	1	67717/T3-136	mercuriale	0	3573	3574	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1788	4	1	67717/T3-29	brevet d'invention	0	3575	3576	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1789	4	1	67717/T3-55	certificat d'urbanisme	0	3577	3578	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1790	4	1	67717/T3-2	acte authentique	0	3579	3580	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1791	4	1	67717/T3-137	microforme	0	3581	3582	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1792	4	1	67717/T3-147	objet	0	3583	3584	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1793	4	1	67717/T3-7	affiche	0	3585	3586	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-18: Supréssion de l'association avec "avant-projet" suite à une remarque de l'éditeur SICEM
1794	4	1	67717/T3-167	poème	0	3587	3588	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1824	4	1	67717/T3-182	publication périodique	0	3647	3648	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1825	4	1	67717/T3-161	plan	0	3649	3650	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1826	4	1	67717/T3-62	compte administratif	0	3651	3652	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1827	4	1	67717/T3-186	rapport de gendarmerie	0	3653	3654	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1828	4	1	67717/T3-63	compte de gestion	0	3655	3656	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1829	4	1	67717/T3-141	motion	0	3657	3658	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1830	4	1	67717/T3-143	norme	0	3659	3660	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1831	4	1	67717/T3-171	procès-verbal d'aménagement forestier	0	3661	3662	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1832	4	1	67717/T3-225	télégramme	0	3663	3664	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1833	4	1	67717/T3-69	convention	0	3665	3666	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1834	4	1	67717/T3-121	livre foncier	0	3667	3668	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1835	4	1	67717/T3-48	carte d'identité	0	3669	3670	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1836	4	1	67717/T3-165	plan d'occupation des sols	0	3671	3672	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1837	4	1	67717/T3-85	déclaration fiscale	0	3673	3674	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1838	4	1	67717/T3-124	livret ouvrier	0	3675	3676	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1839	4	1	67717/T3-72	copie d'examen	0	3677	3678	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
276	1	1	67717/T1-370	société secrète	54	2527	2530	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://dbpedia.org/resource/Category:Secret_societies	
340	1	1	67717/T1-634	anarchisme	330	2606	2607	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://dbpedia.org/resource/Category:Anarchism	
1795	4	1	67717/T3-14	arrêté du maire	0	3589	3590	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1796	4	1	67717/T3-21	avant-projet	0	3591	3592	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-07: Supréssion de l'association avec "affiche" suite à une remarque de l'éditeur SICEM
1797	4	1	67717/T3-207	répertoire des métiers	0	3593	3594	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1798	4	1	67717/T3-71	convention de formation	0	3595	3596	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1799	4	1	67717/T3-154	permis	0	3597	3598	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1800	4	1	67717/T3-58	chronique	0	3599	3600	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1801	4	1	67717/T3-194	registre de catholicité	0	3601	3602	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1802	4	1	67717/T3-130	marque de fabrique	0	3603	3604	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1803	4	1	67717/T3-43	carte à jouer	0	3605	3606	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1804	4	1	67717/T3-164	plan de secours	0	3607	3608	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1805	4	1	67717/T3-153	passeport	0	3609	3610	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1806	4	1	67717/T3-30	brochure	0	3611	3612	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1807	4	1	67717/T3-232	transcription hypothécaire	0	3613	3614	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1808	4	1	67717/T3-205	répertoire	0	3615	3616	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1809	4	1	67717/T3-233	warrant	0	3617	3618	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1810	4	1	67717/T3-5	acte de société	0	3619	3620	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1811	4	1	67717/T3-128	maquette	0	3621	3622	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1812	4	1	67717/T3-191	registre	0	3623	3624	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
853	1	1	67717/T1-89	médecin	538	1388	1389	2013-01-15 16:46:26	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb11932462m	
1813	4	1	67717/T3-142	nomenclature	0	3625	3626	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1814	4	1	67717/T3-174	procès-verbal de réunion	0	3627	3628	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1815	4	1	67717/T3-38	cahier des charges	0	3629	3630	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1816	4	1	67717/T3-139	minute notariale	0	3631	3632	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1817	4	1	67717/T3-222	sujet d'examen	0	3633	3634	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1818	4	1	67717/T3-20	autorisation d'urbanisme	0	3635	3636	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
856	1	1	67717/T1-365	pharmacien	538	1392	1393	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://dbpedia.org/resource/Category:Pharmacists	
1819	4	1	67717/T3-113	laissez-passer	0	3637	3638	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
761	1	1	67717/T1-1276	nappe d'eau	179	1924	1925	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
1840	4	1	67717/T3-50	carte postale	0	3679	3680	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1841	4	1	67717/T3-108	fichier	0	3681	3682	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1820	4	1	67717/T3-41	carnet de santé	0	3639	3640	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1821	4	1	67717/T3-227	timbre	0	3641	3642	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1822	4	1	67717/T3-39	calendrier	0	3643	3644	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1823	4	1	67717/T3-28	bilan social	0	3645	3646	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1842	4	1	67717/T3-102	état de section	0	3683	3684	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1843	4	1	67717/T3-134	mémoire	0	3685	3686	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1844	4	1	67717/T3-169	presse institutionnelle	0	3687	3688	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1845	4	1	67717/T3-230	titre de séjour	0	3689	3690	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-07: Ajout du terme non-descriptif "carte de séjour" suite à une remarque de l'éditeur SICEM
1846	4	1	67717/T3-178	profession de foi	0	3691	3692	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1847	4	1	67717/T3-44	carte d'électeur	0	3693	3694	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1848	4	1	67717/T3-226	texte officiel	0	3695	3696	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1849	4	1	67717/T3-35	bulletin de salaire	0	3697	3698	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1850	4	1	67717/T3-114	liste	0	3699	3700	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1851	4	1	67717/T3-73	correspondance	0	3701	3702	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1852	4	1	67717/T3-47	carte de voeu	0	3703	3704	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1853	4	1	67717/T3-155	permis de construire	0	3705	3706	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1854	4	1	67717/T3-166	plan d'urbanisme	0	3707	3708	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1892	4	1	67717/T3-163	plan d'alignement	0	3783	3784	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1893	4	1	67717/T3-86	délibération	0	3785	3786	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1894	4	1	67717/T3-190	rapport d'expertise	0	3787	3788	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1895	4	1	67717/T3-40	caricature	0	3789	3790	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1896	4	1	67717/T3-65	compte financier	0	3791	3792	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1897	4	1	67717/T3-175	procès-verbal de scellés	0	3793	3794	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1898	4	1	67717/T3-201	registre protestant	0	3795	3796	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1899	4	1	67717/T3-188	rapport de police	0	3797	3798	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1900	4	1	67717/T3-144	note	0	3799	3800	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1901	4	1	67717/T3-11	annuaire	0	3801	3802	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
266	1	1	67717/T1-341	frontière	61	2911	2916	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb11954113g	
913	1	1	67717/T1-1243	réfugié de guerre	62	2937	2938	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
1855	4	1	67717/T3-10	annonce officielle et légale	0	3709	3710	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1856	4	1	67717/T3-218	sommier des marchandises	0	3711	3712	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1857	4	1	67717/T3-54	certificat	0	3713	3714	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1858	4	1	67717/T3-88	discours	0	3715	3716	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-04-07: Ajout du terme non-descriptif "allocution" suite à une remarque de l'éditeur SICEM
1859	4	1	67717/T3-210	répertoire général des affaires civiles	0	3717	3718	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1860	4	1	67717/T3-97	dossier de presse	0	3719	3720	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1861	4	1	67717/T3-15	arrêté du président du conseil général	0	3721	3722	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
800	1	1	67717/T1-185	établissement de santé	36	1399	1418	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		2011-12-13 : ajout du narrower term "hôtel-dieu", suite à l'appel à commentaires d'octobre 2010-mars 2011
559	1	1	67717/T1-1110	cinéma	557	1586	1587	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb119361188	
1862	4	1	67717/T3-149	organigramme	0	3723	3724	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1863	4	1	67717/T3-181	publication officielle	0	3725	3726	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1864	4	1	67717/T3-229	titre de propriété	0	3727	3728	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1865	4	1	67717/T3-80	déclaration de revenus	0	3729	3730	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1866	4	1	67717/T3-91	document graphique	0	3731	3732	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		2011-12-06: Transformation du prefLabel "document figuré" en "document graphique", suppression du commentaire et ajout des altlabel "document dessiné", "document peint" et "estampe" suite à l'appel à commentaires d'octobre 2011
1867	4	1	67717/T3-120	livre d'or	0	3733	3734	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1868	4	1	67717/T3-184	rapport	0	3735	3736	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1869	4	1	67717/T3-107	faire-part	0	3737	3738	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1870	4	1	67717/T3-119	livre de raison	0	3739	3740	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1871	4	1	67717/T3-223	table	0	3741	3742	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1872	4	1	67717/T3-100	dossier individuel	0	3743	3744	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1873	4	1	67717/T3-6	acte sous seing privé	0	3745	3746	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1874	4	1	67717/T3-104	état parcellaire	0	3747	3748	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1875	4	1	67717/T3-219	statistique	0	3749	3750	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1876	4	1	67717/T3-42	carte	0	3751	3752	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1877	4	1	67717/T3-31	budget	0	3753	3754	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1878	4	1	67717/T3-150	ouvrage imprimé	0	3755	3756	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1879	4	1	67717/T3-172	procès-verbal d'élection	0	3757	3758	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1902	4	1	67717/T3-68	contrat d'apprentissage	0	3803	3804	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1903	4	1	67717/T3-24	base de données	0	3805	3806	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
924	1	1	67717/T1-169	contentieux électoral	597	2268	2269	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb11954084q	
1880	4	1	67717/T3-187	rapport de mer	0	3759	3760	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1881	4	1	67717/T3-98	dossier de procédure	0	3761	3762	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1882	4	1	67717/T3-79	déclaration de douane	0	3763	3764	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1883	4	1	67717/T3-82	déclaration de travaux	0	3765	3766	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1884	4	1	67717/T3-193	registre d'audience	0	3767	3768	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1885	4	1	67717/T3-92	documentation	0	3769	3770	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1886	4	1	67717/T3-109	fichier immobilier	0	3771	3772	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1887	4	1	67717/T3-70	convention collective	0	3773	3774	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1888	4	1	67717/T3-160	pièce comptable	0	3775	3776	2013-01-15 16:48:17	1	2013-01-15 16:48:17	1		
1889	4	1	67717/T3-220	statut	0	3777	3778	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1890	4	1	67717/T3-138	minute juridictionnelle	0	3779	3780	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1891	4	1	67717/T3-200	registre paroissial	0	3781	3782	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1904	4	1	67717/T3-123	livret militaire	0	3807	3808	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1905	4	1	67717/T3-173	procès-verbal d'enquête	0	3809	3810	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1906	4	1	67717/T3-78	déclaration	0	3811	3812	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1907	4	1	67717/T3-95	dossier de consultation des entreprises	0	3813	3814	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1908	4	1	67717/T3-195	registre d'écrou	0	3815	3816	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1909	4	1	67717/T3-87	diplôme	0	3817	3818	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1910	4	1	67717/T3-46	carte de travail	0	3819	3820	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1911	4	1	67717/T3-215	sceau	0	3821	3822	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1919	4	1	67717/T3-133	médaille	0	3837	3838	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1920	4	1	67717/T3-101	dossier médical	0	3839	3840	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1921	4	1	67717/T3-110	hypothèque maritime	0	3841	3842	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1922	4	1	67717/T3-189	rapport de stage	0	3843	3844	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1923	4	1	67717/T3-211	revue de presse	0	3845	3846	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1924	4	1	67717/T3-66	compte rendu	0	3847	3848	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-04-07: Remplacement de l'association avec "procès-verbal" par celle avec "procès-verbal de réunion" suite à une remarque de l'éditeur SICEM
1925	4	1	67717/T3-90	document de séance	0	3849	3850	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1926	4	1	67717/T3-37	cahier de doléances	0	3851	3852	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-12-01: Modification du terme "cahier de doléance" en "cahier de doléances" suite à l'appel à commentaires d'octobre 2011
1927	4	1	67717/T3-221	statut d'association	0	3853	3854	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1928	4	1	67717/T3-51	carton d'invitation	0	3855	3856	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1929	4	1	67717/T3-49	carte d'identité professionnelle	0	3857	3858	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1930	4	1	67717/T3-152	partition musicale	0	3859	3860	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1931	4	1	67717/T3-106	exploit d'huissier	0	3861	3862	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1932	4	1	67717/T3-117	liste nominative	0	3863	3864	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1933	4	1	67717/T3-235	décision	0	3865	3866	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1934	4	1	67717/T3-236	déclaration féodale	0	3867	3868	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-12-06: Ajout du terme "déclaration féodale" suite à l'appel à commentaires d'octobre 2010-mars 2011
1935	4	1	67717/T3-237	écrit du for privé	0	3869	3870	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-12-06: Ajout du concept "écrit du for privé" suite à l'appel à commentaires d'octobre 2010-mars 2011
1936	4	1	67717/T3-238	curriculum vitae	0	3871	3872	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-12-06: Ajout du concept "curriculum vitae" suite à l'appel à commentaires d'octobre 2010-mars 2011
1937	4	1	67717/T3-239	dictionnaire	0	3873	3874	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-12-06: Ajout du concept "dictionnaire" suite à l'appel à commentaires d'octobre 2010-mars 2011
1938	4	1	67717/T3-240	nécrologie	0	3875	3876	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		2011-12-06: Ajout du concept "nécrologie" suite à l'appel à commentaires d'octobre 2010-mars 2011
841	1	1	67717/T1-86	éducation populaire	38	1635	1638	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb119694549	
816	1	1	67717/T1-124	restauration immobilière	813	1888	1889	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1		
160	1	1	67717/T1-1103	militaire	60	2883	2892	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb12148674k	
920	1	1	67717/T1-1383	élection sénatoriale	917	2790	2791	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
921	1	1	67717/T1-321	référendum	917	2792	2793	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb13319148r	
923	1	1	67717/T1-1254	élection législative	917	2796	2797	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
1912	4	1	67717/T3-213	rôle d'équipage	0	3823	3824	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1913	4	1	67717/T3-228	titre de circulation	0	3825	3826	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1914	4	1	67717/T3-12	armorial	0	3827	3828	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1915	4	1	67717/T3-224	tarif	0	3829	3830	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1916	4	1	67717/T3-93	dossier de candidature	0	3831	3832	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1917	4	1	67717/T3-36	bulletin de vote	0	3833	3834	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
1918	4	1	67717/T3-177	procès-verbal d'infraction	0	3835	3836	2013-01-15 16:48:18	1	2013-01-15 16:48:18	1		
939	1	1	67717/T1-1479	restauration collective	35	1371	1372	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb119364032	2011-12-07: ajout du concept "restauration collective" avec comme broader term "travail" et comme related term "restauration scolaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
310	1	1	67717/T1-1335	inauguration	273	2500	2501	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
615	1	1	67717/T1-410	droits de douane	27	855	856	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1	http://data.bnf.fr/ark:/12148/cb11971959p	
145	1	1	67717/T1-73	collectivité locale	29	915	928	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1	http://data.bnf.fr/ark:/12148/cb14626204j	
765	1	1	67717/T1-485	manifestation antireligieuse	57	2705	2708	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
722	1	1	67717/T1-1408	conscription	156	2852	2853	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb12647843g	
741	1	1	67717/T1-1418	sucre	633	544	545	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://dbpedia.org/resource/Category:Sugar	
937	1	1	67717/T1-847	relations du travail	35	1359	1368	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb119369561	2011-12-13 : modification du prefLabel "relations de travail" en "relations du travail", suite à l'appel à commentaires d'octobre 2010-mars 2011
742	1	1	67717/T1-66	circonscription ecclésiastique	743	2684	2685	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb11948073t	2011-12-08 : suppression du altLabel "prieuré", suite à l'appel à commentaires d'octobre 2010-mars 2011
745	1	1	67717/T1-529	institution monastique	743	2688	2689	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1		2011-12-08 : ajout des altLabel "abbaye" et "prieuré", suite à l'appel à commentaires d'octobre 2010-mars 2011
833	1	1	67717/T1-983	travailleur étranger	33	1267	1270	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
855	1	1	67717/T1-259	chirurgien dentiste	538	1390	1391	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1	http://data.bnf.fr/ark:/12148/cb11949285m	
283	1	1	67717/T1-818	secte	54	2551	2552	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb133185478	
926	1	1	67717/T1-450	électeur	58	2801	2806	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb12302551q	
144	1	1	67717/T1-1382	conseiller régional	693	2760	2761	2013-01-15 16:46:22	1	2013-01-15 16:46:42	1	http://data.bnf.fr/ark:/12148/cb12243012b	
925	1	1	67717/T1-177	campagne électorale	58	2799	2800	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb11936998z	
824	1	1	67717/T1-778	fleurissement	43	1913	1914	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
825	1	1	67717/T1-85	emploi à temps partiel	33	1243	1244	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb11980216f	
588	1	1	67717/T1-1168	aide sociale	32	1163	1178	2013-01-15 16:46:25	1	2013-01-15 16:47:05	1	http://data.bnf.fr/ark:/12148/cb13318536m	2011-12-13 : suppression du narrower term "aide sociale facultative" et transformation des altLabel "allocation logement" et "allocation militaire" en narrower terms, suite à l'appel à commentaires d'octobre 2010-mars 2011
439	1	1	67717/T1-832	exploitant forestier	81	436	437	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb11940361q	
455	1	1	67717/T1-718	féodalité	26	767	774	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb133185598	
617	1	1	67717/T1-462	taxe extraordinaire d'Ancien Régime	27	859	864	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
889	1	1	67717/T1-1281	adoption	589	1190	1191	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb119654937	
857	1	1	67717/T1-1032	auxiliaire médical	538	1394	1395	2013-01-15 16:46:27	1	2013-01-15 16:47:22	1		
954	1	1	67717/T1-693	kiosque à musique	484	1566	1567	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb11991862q	
754	1	1	67717/T1-1393	inondation	753	890	891	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb119713173	
804	1	1	67717/T1-380	bal public	802	1686	1687	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1		
820	1	1	67717/T1-1131	mobilier urbain	43	1905	1906	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb12011170b	
821	1	1	67717/T1-1246	étalement urbain	43	1907	1908	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
822	1	1	67717/T1-444	reconstruction	43	1909	1910	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
823	1	1	67717/T1-1105	secteur sauvegardé	43	1911	1912	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1		
960	1	1	67717/T1-1034	centre de documentation	484	1578	1579	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1	http://data.bnf.fr/ark:/12148/cb13318458n	
868	1	1	67717/T1-430	énergie hydraulique	867	408	409	2013-01-15 16:46:27	1	2013-01-15 16:47:23	1	http://dbpedia.org/resource/Category:Hydropower	
931	1	1	67717/T1-645	canton	778	1032	1033	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb11973332g	
22	1	1	67717/T1-750	forêt	3	492	521	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb119314375	
135	1	1	67717/T1-6	finances régionales	137	618	619	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb12098639x	2011-12-06: suppression du altlabel "comptabilité régionale" suite à l'appel à commentaires d'octobre 2010-mars 2011
224	1	1	67717/T1-799	cimetière militaire	155	2842	2843	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11966824r	
225	1	1	67717/T1-823	caserne	155	2844	2845	2013-01-15 16:46:22	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11948010x	
707	1	1	67717/T1-1452	déconcentration	705	980	981	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb12226310s	
962	1	1	67717/T1-511	développement durable	17	379	380	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1	http://data.bnf.fr/ark:/12148/cb12271499g	
959	1	1	67717/T1-1459	bibliothèque	484	1576	1577	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1	http://dbpedia.org/resource/Category:Libraries	
1094	1	1	67717/T1-141	distribution des prix	48	2183	2184	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1		
1090	1	1	67717/T1-813	dépôt légal	274	2510	2511	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb119787663	
1091	1	1	67717/T1-1186	journaliste	274	2512	2513	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb11934262j	
1092	1	1	67717/T1-968	publication interne	274	2514	2515	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1		
1093	1	1	67717/T1-1358	presse régionale	274	2516	2517	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb11935931f	
482	1	1	67717/T1-34	bâtiment polyvalent	483	1846	1847	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1		
1088	1	1	67717/T1-139	ligne de chemin de fer	462	10	11	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://dbpedia.org/resource/Category:Railway_lines	
1089	1	1	67717/T1-493	liaison ferrée internationale	462	12	13	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1		
610	1	1	67717/T1-1350	redevance parafiscale	27	829	830	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
1104	1	1	67717/T1-904	service éducatif	48	2213	2214	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		
1106	1	1	67717/T1-1142	accident scolaire	48	2217	2218	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		
1197	1	1	67717/T1-337	pharmacie	36	1477	1486	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1	http://dbpedia.org/resource/Category:Pharmacy	
1054	1	1	67717/T1-131	organisme consultatif	776	984	985	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1	http://data.bnf.fr/ark:/12148/cb11972066q	2011-12-06: ajout du altlabel "conseil de quartier" suite à l'appel à commentaires d'octobre 2010-mars 2011
1023	1	1	67717/T1-995	monument historique	796	1628	1629	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
1050	1	1	67717/T1-1091	chasseur	1037	1698	1699	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1	http://data.bnf.fr/ark:/12148/cb11931162j	
1052	1	1	67717/T1-381	pêcheur	1038	1704	1705	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1	http://data.bnf.fr/ark:/12148/cb119416028	
1053	1	1	67717/T1-876	association de pêche	1038	1706	1707	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
1113	1	1	67717/T1-564	famine	569	1110	1111	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1	http://dbpedia.org/resource/Category:Famines	
1116	1	1	67717/T1-646	aide médicale urgente	759	904	905	2013-01-15 16:46:29	1	2013-01-15 16:47:39	1		
1114	1	1	67717/T1-635	transport sanitaire	1196	1464	1465	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1	http://dbpedia.org/resource/Category:Medical_transport_devices	
1112	1	1	67717/T1-756	marché noir	994	2944	2945	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1	http://data.bnf.fr/ark:/12148/cb11937185n	
1169	1	1	67717/T1-1373	énergie nucléaire	18	417	418	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://dbpedia.org/resource/Category:Nuclear_power	
1170	1	1	67717/T1-452	économie d'énergie	18	419	420	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://dbpedia.org/resource/Category:Energy_conservation	
1172	1	1	67717/T1-719	invalide du travail	595	1224	1225	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1		2011-12-12 : changement de broader term, suite à l'appel à commentaires d'octobre 2010-mars 2011
1168	1	1	67717/T1-1305	livre saint	782	2718	2719	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1		
1171	1	1	67717/T1-1364	magie	59	2827	2830	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://dbpedia.org/resource/Category:Magic	
783	1	1	67717/T1-257	presbytère	57	2721	2722	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb12050463g	
784	1	1	67717/T1-1179	culte	57	2723	2742	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb11956969s	
424	1	1	67717/T1-1038	chef d'entreprise	16	333	340	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb11936306t	
589	1	1	67717/T1-852	aide sociale à l'enfance	32	1179	1192	2013-01-15 16:46:25	1	2013-01-15 16:47:06	1		
262	1	1	67717/T1-1268	thermoclimatisme	36	1375	1376	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		
1254	1	1	67717/T1-903	système d'information	13	117	122	2013-01-15 16:46:30	1	2013-01-15 16:47:51	1	http://data.bnf.fr/ark:/12148/cb119419441	
1296	1	1	67717/T1-1060	jeux-et-paris	40	1711	1722	2013-01-15 16:46:30	1	2013-01-15 16:47:54	1		
1287	1	1	67717/T1-1286	gens de guerre	160	2890	2891	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1		
1283	1	1	67717/T1-816	usager des transports	649	52	53	2013-01-15 16:46:30	1	2013-01-15 16:47:53	1		
1273	1	1	67717/T1-298	droit des pauvres	611	838	839	2013-01-15 16:46:30	1	2013-01-15 16:47:52	1		
999	1	1	67717/T1-1453	dommages de guerre	62	2965	2970	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb11976601r	
1001	1	1	67717/T1-699	chantier de la jeunesse	62	2981	2982	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
1002	1	1	67717/T1-563	prisonnier de guerre	62	2983	2984	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://dbpedia.org/resource/Category:Prisoners_of_war	
1003	1	1	67717/T1-979	travail contraint	62	2985	2986	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		2011-12-07 : transformation du prefLabel "service du travail obligatoire" en "travail contraint", suite à l'appel à commentaires d'octobre 2010-mars 2011
1216	1	1	67717/T1-195	fonctionnaire de l'Union européenne	540	934	935	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
1217	1	1	67717/T1-218	agent de droit privé	540	936	937	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
1226	1	1	67717/T1-201	pornographie	297	714	715	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1	http://dbpedia.org/resource/Category:Pornography	
1213	1	1	67717/T1-1354	assurance invalidité	591	1210	1211	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
1214	1	1	67717/T1-673	assurance veuvage	591	1212	1213	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
1215	1	1	67717/T1-355	établissement médico social	1196	1466	1467	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		
1223	1	1	67717/T1-196	ligne maritime	647	42	43	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1		
1224	1	1	67717/T1-642	marin de commerce	647	44	45	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1	http://data.bnf.fr/ark:/12148/cb11944485z	
1225	1	1	67717/T1-1332	armement maritime	647	46	47	2013-01-15 16:46:29	1	2013-01-15 16:47:48	1		
278	1	1	67717/T1-1126	symbolique officielle	54	2533	2542	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		
1071	1	1	67717/T1-807	taxe foncière	608	818	819	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
1072	1	1	67717/T1-134	puéricultrice	810	1420	1421	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1	http://data.bnf.fr/ark:/12148/cb14487700p	
1073	1	1	67717/T1-1012	assurance maternité	591	1200	1201	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1		
1074	1	1	67717/T1-180	halte garderie	810	1422	1423	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1		
1076	1	1	67717/T1-1307	assistante maternelle	810	1426	1427	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11953454n	
1077	1	1	67717/T1-541	mortalité infantile	810	1428	1429	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11961365r	
555	1	1	67717/T1-43	danse	557	1582	1583	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1	http://data.bnf.fr/ark:/12148/cb11931841h	
505	1	1	67717/T1-362	traitement des déchets	44	1941	1956	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb133185656	
506	1	1	67717/T1-748	hygiène	36	1377	1384	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb11944168j	
556	1	1	67717/T1-1228	enseignement artistique	47	2127	2128	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1		
504	1	1	67717/T1-1443	centrale nucléaire	513	396	397	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://dbpedia.org/resource/Category:Nuclear_power_stations	2011-04-19: Suppression du terme non-descripteurs "énergie atomique" suite à une remarque de l'éditeur GAIA
522	1	1	67717/T1-1106	conflit scolaire	46	2073	2074	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
1139	1	1	67717/T1-597	port maritime	629	20	21	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1		
1140	1	1	67717/T1-1434	docker	629	22	23	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1	http://data.bnf.fr/ark:/12148/cb11961308x	
1136	1	1	67717/T1-1189	toxicomanie	1127	1456	1457	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1	http://data.bnf.fr/ark:/12148/cb119336883	2011-12-13 : transformation du prefLabel "toxicomane" en "toxicomanie", suite à l'appel à commentaires d'octobre 2010-mars 2011
1137	1	1	67717/T1-1308	sida	1127	1458	1459	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1	http://data.bnf.fr/ark:/12148/cb11975352k	
1191	1	1	67717/T1-329	établissement public d'hospitalisation	800	1410	1411	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb11940402g	
1182	1	1	67717/T1-782	alimentation	39	1651	1652	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb13318298b	
1183	1	1	67717/T1-1042	animal de compagnie	39	1653	1654	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb119309061	
1184	1	1	67717/T1-1252	vêtement	39	1655	1656	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb11963612g	
1185	1	1	67717/T1-686	comptabilité privée	39	1657	1658	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1		
1186	1	1	67717/T1-416	espace domestique	39	1659	1660	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1		
1187	1	1	67717/T1-900	mobilier	39	1661	1662	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb13162680m	
627	1	1	67717/T1-661	liaison douce	42	1829	1836	2013-01-15 16:46:25	1	2013-01-15 16:47:08	1		
1012	1	1	67717/T1-1465	remonte militaire	158	2870	2871	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		2011-12-07: Ajout du concept "remonte militaire" suite à l'appel à commentaires d'octobre 2010-mars 2011
1013	1	1	67717/T1-122	marine	159	2874	2875	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb121486768	
223	1	1	67717/T1-427	fortification	155	2840	2841	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11931504g	2011-12-13 : ajout du related "patrimoine architectural", suite à l'appel à commentaires d'octobre 2010-mars 2011
678	1	1	67717/T1-554	aménagement du territoire	17	359	374	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb11933911b	
679	1	1	67717/T1-182	décentralisation industrielle	678	364	365	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
680	1	1	67717/T1-1446	ville nouvelle	678	366	367	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb11963619w	
681	1	1	67717/T1-961	zone d'activités	678	368	369	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
682	1	1	67717/T1-1192	politique de la ville	678	370	371	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1	http://data.bnf.fr/ark:/12148/cb11974611k	
683	1	1	67717/T1-1431	zone industrielle	678	372	373	2013-01-15 16:46:25	1	2013-01-15 16:47:12	1		
450	1	1	67717/T1-860	étranger	31	1095	1102	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb13318991v	
451	1	1	67717/T1-29	privilèges des communautés	26	759	764	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
452	1	1	67717/T1-155	coutumes	451	760	761	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb11932512b	
453	1	1	67717/T1-1333	droits d'usage	451	762	763	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
449	1	1	67717/T1-1178	boulangerie	238	238	239	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb11941357q	2011-12-06: Ajout du related term "profession réglementée" suite à l'appel à commentaires d'octobre 2010-mars 2011
462	1	1	67717/T1-164	transport ferroviaire	12	9	14	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1	http://data.bnf.fr/ark:/12148/cb11934482q	
348	1	1	67717/T1-197	cléricalisme	330	2622	2623	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb122371173	
1025	1	1	67717/T1-1046	mythe	1024	2824	2825	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1	http://data.bnf.fr/ark:/12148/cb119325757	
697	1	1	67717/T1-902	parlementaire	693	2766	2767	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb11933028k	
191	1	1	67717/T1-1116	industrie spatiale	14	199	200	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
405	1	1	67717/T1-1392	coups et blessures	379	2410	2411	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1		
881	1	1	67717/T1-1187	téléphone	877	110	111	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb11944593v	
966	1	1	67717/T1-1466	coût de la vie	17	391	392	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1		2011-12-07: Ajout du concept "coût de la vie" suite à l'appel à commentaires d'octobre 2010-mars 2011
975	1	1	67717/T1-655	produit laitier	634	558	559	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://dbpedia.org/resource/Category:Dairy_products	
887	1	1	67717/T1-843	conseil de famille	589	1186	1187	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1		
884	1	1	67717/T1-566	mineur surveillé	598	2272	2273	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1		
882	1	1	67717/T1-98	association de consommateurs	234	224	225	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://dbpedia.org/resource/Category:Consumer_organizations	
331	1	1	67717/T1-78	militant politique	329	2586	2587	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb13321199x	
374	1	1	67717/T1-827	tourisme urbain	37	1535	1536	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb12496446v	
375	1	1	67717/T1-1102	circuit touristique	37	1537	1538	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb11967146c	
376	1	1	67717/T1-1378	organisme local de tourisme	37	1539	1540	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1		
377	1	1	67717/T1-1272	cyclotourisme	37	1541	1542	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb11931835k	
378	1	1	67717/T1-25	incendie volontaire	379	2358	2359	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb119718880	
121	1	1	67717/T1-405	congés payés	118	1328	1329	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb11973438t	
105	1	1	67717/T1-981	organisme de recherche	45	2039	2040	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1		
106	1	1	67717/T1-448	chercheur	45	2041	2042	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb123447898	
107	1	1	67717/T1-1470	expédition scientifique	45	2043	2044	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb119444342	2011-12-07: ajout du concept "expédition scientifique", suite à l'appel à commentaires d'octobre 2010-mars 2011
183	1	1	67717/T1-59	industrie maritime	14	165	168	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
950	1	1	67717/T1-965	droit communautaire	30	1075	1076	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb11950150q	
98	1	1	67717/T1-648	archéologie	38	1545	1554	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://dbpedia.org/resource/Category:Archaeology	2011-12-06: Ajout du altlabel "site archéologique", du narrower term "archéologie du sol" et du related term "recherche scientifique" suite à l'appel à commentaires d'octobre 2010-mars 2011
125	1	1	67717/T1-5	route nationale	126	1802	1803	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1		
126	1	1	67717/T1-1149	réseau routier	42	1799	1816	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1		
120	1	1	67717/T1-374	rémunération	118	1326	1327	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb13162999v	
915	1	1	67717/T1-815	immigration	450	1098	1099	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1		
912	1	1	67717/T1-106	réfugié	450	1096	1097	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb13318513z	
958	1	1	67717/T1-1241	salle de spectacles	484	1574	1575	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1	http://data.bnf.fr/ark:/12148/cb13318892x	
1040	1	1	67717/T1-1365	protection de la nature	44	1987	2004	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb11932598w	
193	1	1	67717/T1-1267	industrie du feu	14	203	204	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
1055	1	1	67717/T1-260	administration départementale	776	986	987	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
1056	1	1	67717/T1-368	administration cantonale d'époque\n\t\t\trévolutionnaire	776	988	989	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
1057	1	1	67717/T1-387	organisme parapublic	776	990	991	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
1058	1	1	67717/T1-422	administration régionale	776	992	993	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1	http://data.bnf.fr/ark:/12148/cb119404808	
951	1	1	67717/T1-1229	démocratie	30	1077	1078	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb133185567	
837	1	1	67717/T1-1146	carrière professionnelle	33	1279	1280	2013-01-15 16:46:26	1	2013-01-15 16:47:21	1	http://data.bnf.fr/ark:/12148/cb119329202	
1048	1	1	67717/T1-637	lot de chasse	1037	1694	1695	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
1049	1	1	67717/T1-1371	association de chasse	1037	1696	1697	2013-01-15 16:46:28	1	2013-01-15 16:47:35	1		
196	1	1	67717/T1-838	dons-et-legs	54	2487	2488	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb120235704	
217	1	1	67717/T1-1193	réserve foncière	818	1894	1895	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
318	1	1	67717/T1-173	recherche minière	182	156	157	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
1026	1	1	67717/T1-658	édifice classé	796	1630	1631	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
314	1	1	67717/T1-249	propos subversif	312	2578	2579	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
315	1	1	67717/T1-382	clandestinité	312	2580	2581	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
316	1	1	67717/T1-729	écrit subversif	312	2582	2583	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
282	1	1	67717/T1-1274	propagande	54	2549	2550	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb135492000	
497	1	1	67717/T1-835	fontaine	483	1864	1865	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb119541004	
952	1	1	67717/T1-114	musée	484	1562	1563	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb11939565m	
496	1	1	67717/T1-570	horloge publique	483	1862	1863	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
671	1	1	67717/T1-795	juge de paix	539	2316	2317	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb13319337n	
695	1	1	67717/T1-1251	maire	693	2762	2763	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb119401390	
1011	1	1	67717/T1-905	service sanitaire militaire	158	2868	2869	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
86	1	1	67717/T1-866	association syndicale de propriétaires	65	430	431	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
947	1	1	67717/T1-476	État	30	1063	1070	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb11937050q	2011-12-06: Ajout du concept "chef d'État" comme narrower term suite à l'appel à commentaires d'octobre 2010-mars 2011
827	1	1	67717/T1-144	emploi aidé	33	1251	1252	2013-01-15 16:46:26	1	2013-01-15 16:47:20	1	http://data.bnf.fr/ark:/12148/cb119385959	2011-12-13 : modification du prefLabel "emploi des jeunes" en "emploi aidé" et suppression des altLabel "chômage des jeunes", "emploi des jeunes" et "travaux d'utilité collective", suite à l'appel à commentaires d'octobre 2010-mars 2011
188	1	1	67717/T1-849	industrie mécanique	14	187	192	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb11953162f	
319	1	1	67717/T1-1052	mineur de fond	182	158	159	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
442	1	1	67717/T1-1078	marchand forain	240	256	257	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1	http://data.bnf.fr/ark:/12148/cb11973954f	
328	1	1	67717/T1-128	pratique religieuse	57	2667	2672	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb119474203	2011-12-12 : ajouty des narrower "pèlerinage" et "service religieux", suite à l'appel à commentaires d'octobre 2010-mars 2011
329	1	1	67717/T1-1169	parti politique	55	2585	2592	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb13318377n	
330	1	1	67717/T1-87	mouvement politique et sociétal	55	2593	2646	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1		2011-12-07 : modification du prefLabel de "mouvement politique" en "mouvement politique et sociétal" et suppression de la scopeNote, suite à l'appel à commentaires d'octobre 2010-mars 2011
322	1	1	67717/T1-20	travailleur social	324	1144	1145	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb119512073	
980	1	1	67717/T1-118	magasin collectif	420	312	313	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb119453790	
817	1	1	67717/T1-84	urbaniste	43	1891	1892	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://dbpedia.org/resource/Category:Urban_planners	
806	1	1	67717/T1-216	mutinerie	90	2230	2231	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb11956310g	
521	1	1	67717/T1-300	professeur	327	2070	2071	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
323	1	1	67717/T1-350	éducateur	327	2062	2063	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb119534252	
287	1	1	67717/T1-1478	armoiries	54	2559	2560	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb11947941f	2011-12-07: ajout du concept "armoiries", suite à l'appel à commentaires d'octobre 2010-mars 2011
288	1	1	67717/T1-1489	manifestation publique	54	2561	2572	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		2011-12-12: ajout du concept "manifestation publique" avec comme broader term "vie publique" et comme narrower "réunion publique", "manifestation de protestation", "mouvement populaire", "manifestation antinationale" et "manifestation d'adhésion au régime", suite à l'appel à commentaires d'octobre 2010-mars 2011
24	1	1	67717/T1-1222	finances publiques	4	616	683	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11950197c	
201	1	1	67717/T1-690	biens communaux	151	636	637	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1		
164	1	1	67717/T1-1483	manoeuvre militaire	60	2905	2906	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb119822503	2011-12-07: ajout du concept "manoeuvre militaire" avec comme broader term "défense du territoire", suite à l'appel à commentaires d'octobre 2010-mars 2011
1176	1	1	67717/T1-178	ferroutage	646	36	37	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://data.bnf.fr/ark:/12148/cb122854807	
1141	1	1	67717/T1-567	port de commerce	629	24	25	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1		
1142	1	1	67717/T1-948	port fluvial	629	26	27	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1		
1143	1	1	67717/T1-1253	port de plaisance	629	28	29	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
231	1	1	67717/T1-280	enseignement commercial	990	2146	2147	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11978259f	
1145	1	1	67717/T1-157	pollution visuelle	762	1962	1963	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
1146	1	1	67717/T1-714	pollution atmosphérique	762	1964	1965	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1	http://data.bnf.fr/ark:/12148/cb119308538	
1147	1	1	67717/T1-1420	pollution des eaux	762	1966	1967	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1	http://data.bnf.fr/ark:/12148/cb11931914w	
1148	1	1	67717/T1-375	pollution sonore	762	1968	1969	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1	http://data.bnf.fr/ark:/12148/cb11931087m	
1149	1	1	67717/T1-327	pollution de la mer	762	1970	1971	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1	http://data.bnf.fr/ark:/12148/cb11952508h	
233	1	1	67717/T1-88	métiers du livre	15	215	222	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1		
1144	1	1	67717/T1-928	officier de port	629	30	31	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
235	1	1	67717/T1-108	armurier	15	227	228	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb119717385	
272	1	1	67717/T1-16	société révolutionnaire	54	2489	2490	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		
273	1	1	67717/T1-21	cérémonie publique	54	2491	2506	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		2011-12-08 : ajout du narrower "cérémonie militaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
274	1	1	67717/T1-140	presse	54	2507	2518	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb133183691	
275	1	1	67717/T1-1388	relations publiques	54	2519	2526	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://dbpedia.org/resource/Category:Public_relations	
94	1	1	67717/T1-737	population pénitentiaire	49	2239	2244	2013-01-15 16:46:22	1	2013-01-15 16:46:39	1	http://data.bnf.fr/ark:/12148/cb133187752	
23	1	1	67717/T1-421	production agricole	3	522	613	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb11932997n	2011-12-06: ajout du related term "organisme génétiquement modifié" et suppression du related term "marché agricole" suite à l'appel à commentaires d'octobre 2010-mars 2011
28	1	1	67717/T1-147	protection civile	4	876	913	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1	http://data.bnf.fr/ark:/12148/cb13516359k	
75	1	1	67717/T1-706	marché immobilier	41	1773	1776	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
1105	1	1	67717/T1-1282	rentrée scolaire	48	2215	2216	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1	http://data.bnf.fr/ark:/12148/cb135073886	
1107	1	1	67717/T1-1225	bataillon scolaire	48	2219	2220	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		
1110	1	1	67717/T1-1072	mineur délinquant	598	2276	2277	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1	http://data.bnf.fr/ark:/12148/cb13318349d	
1111	1	1	67717/T1-1150	rationnement	994	2942	2943	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1	http://data.bnf.fr/ark:/12148/cb119803606	
1087	1	1	67717/T1-858	zone d'aménagement	818	1900	1901	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1		
711	1	1	67717/T1-105	défrichement	470	512	513	2013-01-15 16:46:25	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb119817689	
636	1	1	67717/T1-631	horticulture	23	575	582	2013-01-15 16:46:25	1	2013-01-15 16:47:09	1	http://data.bnf.fr/ark:/12148/cb119320437	
706	1	1	67717/T1-276	délocalisation	705	978	979	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb11954087r	
955	1	1	67717/T1-254	centre d'archives	484	1568	1569	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb13318572g	2011-12-13 : modification du prefLabel "archives" en "centre d'archives", suite à l'appel à commentaires d'octobre 2010-mars 2011
432	1	1	67717/T1-524	identité commerciale	414	2260	2261	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1		
1033	1	1	67717/T1-1493	service religieux	328	2670	2671	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1		2011-12-12: ajout du concept "service religieux" avec comme broader term "pratique religieuse", suite à l'appel à commentaires d'octobre 2010-mars 2011
429	1	1	67717/T1-1139	état civil	31	1087	1094	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb12092345k	
430	1	1	67717/T1-445	commissaire aux comptes	422	328	329	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb11971839s	
281	1	1	67717/T1-753	jumelage	54	2547	2548	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb11977906j	
332	1	1	67717/T1-363	congrès politique	329	2588	2589	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://dbpedia.org/resource/Category:Political_party_assemblies	
1122	1	1	67717/T1-149	eau souterraine	179	1926	1927	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb11931916k	
1123	1	1	67717/T1-243	chenal	179	1928	1929	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb120458418	
1124	1	1	67717/T1-589	traitement des eaux usées	179	1930	1931	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb119319188	
1095	1	1	67717/T1-145	classe de découverte	48	2185	2186	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb11945402p	
254	1	1	67717/T1-101	officier ministériel	51	2295	2306	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb12221709g	
533	1	1	67717/T1-574	enseignement à distance	46	2119	2120	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11951948m	
252	1	1	67717/T1-1384	débit de tabac	15	291	292	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb13169278n	
299	1	1	67717/T1-468	police judiciaire	25	723	732	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
253	1	1	67717/T1-226	oeuvre d'art	796	1610	1611	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1		2011-12-13 : ajout des altLabel "objet d'art" et "mobilier classé et ajout du related "commissaire priseur, suite à l'appel à commentaires d'octobre 2010-mars 2011
165	1	1	67717/T1-9	hydraulique agricole	21	449	456	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb119800177	
163	1	1	67717/T1-966	police militaire	60	2903	2904	2013-01-15 16:46:22	1	2013-01-15 16:46:43	1	http://data.bnf.fr/ark:/12148/cb125053219	
59	1	1	67717/T1-1439	croyances et sciences parallèles	10	2810	2831	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
29	1	1	67717/T1-1345	administration générale	4	914	1059	2013-01-15 16:46:21	1	2013-01-15 16:46:36	1		
343	1	1	67717/T1-538	colonialisme	330	2612	2613	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://dbpedia.org/resource/Category:Colonialism	
179	1	1	67717/T1-695	eau	44	1917	1934	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1	http://data.bnf.fr/ark:/12148/cb11931913j	
246	1	1	67717/T1-851	voyageur représentant placier	15	273	274	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb11970756v	
247	1	1	67717/T1-490	publicité	15	275	278	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://dbpedia.org/resource/Category:Advertising	
696	1	1	67717/T1-1188	conseiller municipal	693	2764	2765	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb11936604z	
425	1	1	67717/T1-1190	société commerciale	16	341	346	2013-01-15 16:46:24	1	2013-01-15 16:46:57	1	http://data.bnf.fr/ark:/12148/cb12009697g	
303	1	1	67717/T1-1172	agent de la force publique	25	741	754	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1		
132	1	1	67717/T1-734	route forestière	126	1810	1811	2013-01-15 16:46:22	1	2013-01-15 16:46:41	1	http://data.bnf.fr/ark:/12148/cb11997839n	
1098	1	1	67717/T1-998	médecine scolaire	48	2191	2194	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb11932339t	
998	1	1	67717/T1-402	forces alliées	62	2963	2964	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
660	1	1	67717/T1-627	pilote	12	89	90	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1		
85	1	1	67717/T1-57	terre inculte	65	428	429	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1	http://data.bnf.fr/ark:/12148/cb12049389v	
436	1	1	67717/T1-204	séjour des étrangers	294	694	695	2013-01-15 16:46:24	1	2013-01-15 16:46:58	1		
886	1	1	67717/T1-100	pupille de l'État	589	1184	1185	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1		
967	1	1	67717/T1-670	pâturage	459	506	507	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb11933041s	
212	1	1	67717/T1-726	monnaie	24	679	680	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb13318359q	
1069	1	1	67717/T1-654	société de construction	71	1744	1745	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1	http://data.bnf.fr/ark:/12148/cb12566294q	
654	1	1	67717/T1-328	navire	12	63	64	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb11950841p	
317	1	1	67717/T1-19	carrière	182	154	155	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1	http://data.bnf.fr/ark:/12148/cb119311182	
320	1	1	67717/T1-535	prospection pétrolière	182	160	161	2013-01-15 16:46:23	1	2013-01-15 16:46:51	1	http://data.bnf.fr/ark:/12148/cb12201699k	
360	1	1	67717/T1-23	compagnonnage	361	296	297	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb119370334	
361	1	1	67717/T1-1025	organisation professionnelle	16	295	304	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
362	1	1	67717/T1-505	délégué consulaire	361	298	299	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
363	1	1	67717/T1-776	chambre consulaire	361	300	301	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1		
758	1	1	67717/T1-1219	immeuble de grande hauteur	28	899	900	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1	http://data.bnf.fr/ark:/12148/cb11946903n	
365	1	1	67717/T1-24	chemin de randonnée	37	1511	1512	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1		
359	1	1	67717/T1-318	mouvement pacifiste	330	2644	2645	2013-01-15 16:46:23	1	2013-01-15 16:46:53	1	http://data.bnf.fr/ark:/12148/cb119542530	
268	1	1	67717/T1-612	organisation internationale	61	2923	2924	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://dbpedia.org/resource/Category:International_organizations	
269	1	1	67717/T1-831	représentation diplomatique	61	2925	2928	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb119478415	
270	1	1	67717/T1-957	relations européennes	61	2929	2930	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		
271	1	1	67717/T1-1485	aide au développement	61	2931	2932	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		2011-12-07: ajout du concept "aide au développement" avec comme broader term "relations internationales" et comme related "action humanitaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
1096	1	1	67717/T1-222	voyage scolaire	48	2187	2188	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1		
1097	1	1	67717/T1-265	échec scolaire	48	2189	2190	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb11931919m	
1100	1	1	67717/T1-474	projet éducatif	48	2205	2206	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb11970705z	
787	1	1	67717/T1-864	temporel ecclésiastique	57	2747	2748	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		
1135	1	1	67717/T1-984	tabagisme	1127	1454	1455	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1	http://data.bnf.fr/ark:/12148/cb11936459p	
1085	1	1	67717/T1-138	association foncière urbaine	818	1896	1897	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1		
514	1	1	67717/T1-255	navigation de plaisance	40	1679	1680	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11933822q	
897	1	1	67717/T1-151	tiers état	892	1298	1299	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://data.bnf.fr/ark:/12148/cb119543898	
898	1	1	67717/T1-184	bourgeoisie	892	1300	1301	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1	http://data.bnf.fr/ark:/12148/cb11940294d	
1131	1	1	67717/T1-700	tuberculose	1127	1446	1447	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb119413462	
854	1	1	67717/T1-651	enseignement hospitalier	990	2148	2149	2013-01-15 16:46:26	1	2013-01-15 16:47:22	1		
1079	1	1	67717/T1-972	contraception	810	1432	1433	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb119413640	
1080	1	1	67717/T1-978	accouchement	810	1434	1435	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11931223x	
1132	1	1	67717/T1-732	maladie mentale	1127	1448	1449	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb119324002	2011-12-13 : transformation du prefLabel "malade mental" en "maladie mentale" et modification du broader term, suite à l'appel à commentaires d'octobre 2010-mars 2011
1009	1	1	67717/T1-1321	réquisitions militaires	158	2864	2865	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb120494921	
1128	1	1	67717/T1-214	maladie sexuellement transmissible	1127	1440	1441	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://dbpedia.org/resource/Category:Sexually_transmitted_diseases_and_infections	
1129	1	1	67717/T1-322	lèpre	1127	1442	1443	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb11938286h	
1130	1	1	67717/T1-501	épidémie	1127	1444	1445	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://dbpedia.org/resource/Category:Epidemics	2011-12-08 : ajout du altLabel "peste", suite à l'appel à commentaires d'octobre 2010-mars 2011
494	1	1	67717/T1-884	puits	483	1858	1859	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://data.bnf.fr/ark:/12148/cb119759000	
1125	1	1	67717/T1-1288	eau pluviale	179	1932	1933	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1	http://data.bnf.fr/ark:/12148/cb12364639v	
1103	1	1	67717/T1-895	accueil périscolaire	48	2211	2212	2013-01-15 16:46:28	1	2013-01-15 16:47:39	1		
1158	1	1	67717/T1-1122	conseil communautaire	703	968	969	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1		
1159	1	1	67717/T1-1451	états provinciaux	703	970	971	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://data.bnf.fr/ark:/12148/cb155228166	
289	1	1	67717/T1-17	maintien de l'ordre	25	685	686	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1		
1063	1	1	67717/T1-1422	établissement public local	776	1002	1003	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
1082	1	1	67717/T1-223	eau-de-vie	635	572	573	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://dbpedia.org/resource/Category:Distilled_beverages	
712	1	1	67717/T1-58	confrérie	713	2676	2677	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb11945760p	
1083	1	1	67717/T1-779	assurance chômage	591	1202	1203	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11967802p	
713	1	1	67717/T1-74	association cultuelle	57	2673	2682	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1		
714	1	1	67717/T1-1391	société de charité	713	2678	2679	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1		
715	1	1	67717/T1-917	tiers-ordre	713	2680	2681	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1		
766	1	1	67717/T1-976	trouble universitaire	525	2098	2099	2013-01-15 16:46:26	1	2013-01-15 16:47:16	1		
767	1	1	67717/T1-71	impôt sur les bénéfices	606	794	795	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb13318714v	
768	1	1	67717/T1-205	taxe sur le chiffre d'affaires	606	796	797	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1		
769	1	1	67717/T1-1136	impôt sur les sociétés	606	798	799	2013-01-15 16:46:26	1	2013-01-15 16:47:17	1	http://data.bnf.fr/ark:/12148/cb131629913	
710	1	1	67717/T1-1362	maladie des animaux	632	530	531	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb11935567b	
808	1	1	67717/T1-82	enfant handicapé	595	1222	1223	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb11931354g	2011-12-12 : changement de broader term, suite à l'appel à commentaires d'octobre 2010-mars 2011
810	1	1	67717/T1-1094	protection maternelle et infantile	36	1419	1436	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb11946186z	
809	1	1	67717/T1-677	éducation spéciale	48	2181	2182	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb12652174t	2011-12-12 : suppression du related term "enfant handicapé", suite à l'appel à commentaires d'octobre 2010-mars 2011
1027	1	1	67717/T1-825	objet d'art	796	1632	1633	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
1038	1	1	67717/T1-1182	pêche à la ligne	40	1701	1708	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb119355827	
1134	1	1	67717/T1-1270	alcoolisme	1127	1452	1453	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1	http://data.bnf.fr/ark:/12148/cb11964742x	
659	1	1	67717/T1-883	transport routier	12	83	88	2013-01-15 16:46:25	1	2013-01-15 16:47:11	1	http://data.bnf.fr/ark:/12148/cb11934481c	
394	1	1	67717/T1-659	délit d'usage	379	2388	2389	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1		
1101	1	1	67717/T1-498	parent d'élève	48	2207	2208	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb119530598	
1000	1	1	67717/T1-788	occupation étrangère	62	2971	2980	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
1081	1	1	67717/T1-136	vin	635	570	571	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11933798p	
290	1	1	67717/T1-926	manifestation sportive	305	1670	1671	2013-01-15 16:46:23	1	2013-01-15 16:46:50	1	http://data.bnf.fr/ark:/12148/cb120987752	
900	1	1	67717/T1-261	patrimoine privé	34	1313	1314	2013-01-15 16:46:27	1	2013-01-15 16:47:25	1		
1007	1	1	67717/T1-1487	justice militaire	890	2344	2345	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb133187067	2011-12-07: ajout du concept "justice militaire" avec comme broader term "juridiction", comme altLabel "conseil de guerre" et "cour martiale" et comme related "organisation de l'armée", suite à l'appel à commentaires d'octobre 2010-mars 2011
1118	1	1	67717/T1-1430	sapeur pompier	759	908	909	2013-01-15 16:46:29	1	2013-01-15 16:47:39	1	http://data.bnf.fr/ark:/12148/cb11933220c	
1119	1	1	67717/T1-1462	équipement de secours	759	910	911	2013-01-15 16:46:29	1	2013-01-15 16:47:40	1		2011-12-06: Ajout du concept "équipement de secours" suite à l'appel à commentaires d'octobre 2010-mars 2011
502	1	1	67717/T1-1258	domaine public fluvial	210	662	663	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
1066	1	1	67717/T1-1088	service déconcentré	776	1008	1009	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
536	1	1	67717/T1-1196	formation professionnelle	33	1233	1242	2013-01-15 16:46:24	1	2013-01-15 16:47:03	1	http://data.bnf.fr/ark:/12148/cb11935376r	
480	1	1	67717/T1-1297	antisémitisme	476	2656	2657	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb11938499d	
1151	1	1	67717/T1-299	ressort judiciaire	778	1034	1035	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
1155	1	1	67717/T1-275	états généraux	703	962	963	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
953	1	1	67717/T1-578	médiathèque	484	1564	1565	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb11975004w	
1175	1	1	67717/T1-682	accident du travail	935	1348	1349	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://data.bnf.fr/ark:/12148/cb119512011	
1152	1	1	67717/T1-212	justice municipale	890	2346	2347	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
1153	1	1	67717/T1-569	justice royale	890	2348	2349	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		
918	1	1	67717/T1-870	élection européenne	917	2786	2787	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb121045557	
979	1	1	67717/T1-1464	équidé	634	566	567	2013-01-15 16:46:27	1	2013-01-15 16:47:31	1	http://data.bnf.fr/ark:/12148/cb12041637h	2011-12-07: Ajout du concept "équidé" suite à l'appel à commentaires d'octobre 2010-mars 2011
1165	1	1	67717/T1-530	mysticisme	782	2712	2713	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://dbpedia.org/resource/Category:Mysticism	
1166	1	1	67717/T1-552	spiritualité	782	2714	2715	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://dbpedia.org/resource/Category:Spirituality	
1161	1	1	67717/T1-1409	jardin familial	79	1790	1791	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://data.bnf.fr/ark:/12148/cb119419937	
1156	1	1	67717/T1-555	assemblée générale	703	964	965	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1		
930	1	1	67717/T1-367	conseil général	703	956	957	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1	http://data.bnf.fr/ark:/12148/cb11950090p	
932	1	1	67717/T1-874	conseil d'arrondissement	703	958	959	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1		
1160	1	1	67717/T1-162	fruit	636	576	577	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://data.bnf.fr/ark:/12148/cb11971232q	
1162	1	1	67717/T1-604	fleur	636	578	579	2013-01-15 16:46:29	1	2013-01-15 16:47:43	1	http://dbpedia.org/resource/Category:Flowers	
1015	1	1	67717/T1-977	armée de terre	159	2878	2879	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1		
1150	1	1	67717/T1-1501	pollution du sol	762	1972	1973	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1		2011-12-13 : ajout du concept "pollution du sol" avec comme broader term "pollution", suite à l'appel à commentaires d'octobre 2010-mars 2011
1133	1	1	67717/T1-797	maladie à déclaration obligatoire	1127	1450	1451	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1		
1154	1	1	67717/T1-970	justice seigneuriale	890	2350	2351	2013-01-15 16:46:29	1	2013-01-15 16:47:42	1	http://data.bnf.fr/ark:/12148/cb13319795x	
1062	1	1	67717/T1-575	service extérieur	776	1000	1001	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
1019	1	1	67717/T1-1343	patrimoine audiovisuel	796	1620	1621	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		
1020	1	1	67717/T1-1337	patrimoine écrit	796	1622	1623	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1	http://data.bnf.fr/ark:/12148/cb136146729	
1022	1	1	67717/T1-1500	patrimoine architectural	796	1626	1627	2013-01-15 16:46:28	1	2013-01-15 16:47:33	1		2011-12-13 : ajout du concept "patrimoine architectural" avec comme broader term "patrimoine culturel", comme altLabel "château", "édifice classé, "monument", "monument historique" et "mur d'enceinte", et comme related terms "fortification" et "édifice cultuel", suite à l'appel à commentaires d'octobre 2010-mars 2011
1194	1	1	67717/T1-1499	hôtel-dieu	800	1416	1417	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		2011-12-13 : ajout du concept "hôtel-dieu" avec comme broader term "établissement de santé", suite à l'appel à commentaires d'octobre 2010-mars 2011
814	1	1	67717/T1-475	lotissement	813	1884	1885	2013-01-15 16:46:26	1	2013-01-15 16:47:19	1	http://data.bnf.fr/ark:/12148/cb126475053	
1086	1	1	67717/T1-469	friche industrielle	818	1898	1899	2013-01-15 16:46:28	1	2013-01-15 16:47:38	1	http://data.bnf.fr/ark:/12148/cb119719977	
956	1	1	67717/T1-871	maison des jeunes	484	1570	1571	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1		
1205	1	1	67717/T1-1287	école centrale	523	2092	2093	2013-01-15 16:46:29	1	2013-01-15 16:47:46	1		
518	1	1	67717/T1-1029	instituteur	327	2064	2065	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1	http://data.bnf.fr/ark:/12148/cb11935237k	
701	1	1	67717/T1-512	échevin	693	2774	2775	2013-01-15 16:46:25	1	2013-01-15 16:47:13	1	http://data.bnf.fr/ark:/12148/cb14616686m	
512	1	1	67717/T1-887	installation classée	28	881	882	2013-01-15 16:46:24	1	2013-01-15 16:47:02	1		
1211	1	1	67717/T1-192	assurance vieillesse	591	1206	1207	2013-01-15 16:46:29	1	2013-01-15 16:47:47	1		2011-12-12 : ajout du related term "retraité", suite à l'appel à commentaires d'octobre 2010-mars 2011
554	1	1	67717/T1-487	auxiliaire de justice	51	2321	2342	2013-01-15 16:46:24	1	2013-01-15 16:47:04	1		
1078	1	1	67717/T1-916	interruption volontaire de grossesse	810	1430	1431	2013-01-15 16:46:28	1	2013-01-15 16:47:37	1	http://data.bnf.fr/ark:/12148/cb11931009t	
938	1	1	67717/T1-1233	entreprise de travail temporaire	35	1369	1370	2013-01-15 16:46:27	1	2013-01-15 16:47:27	1		
1138	1	1	67717/T1-1498	soins médicaux	1127	1460	1461	2013-01-15 16:46:29	1	2013-01-15 16:47:41	1	http://data.bnf.fr/ark:/12148/cb11971647v	2011-12-13 : ajout du concept "soins médicaux" avec comme broader term "action sanitaire", suite à l'appel à commentaires d'octobre 2010-mars 2011
487	1	1	67717/T1-585	installation sportive	305	1672	1673	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1	http://dbpedia.org/resource/Category:Sports_venues	
1181	1	1	67717/T1-183	activité ménagère	39	1649	1650	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1		
1180	1	1	67717/T1-1385	colporteur	240	258	259	2013-01-15 16:46:29	1	2013-01-15 16:47:45	1	http://data.bnf.fr/ark:/12148/cb119419383	
476	1	1	67717/T1-453	tolérance	56	2653	2660	2013-01-15 16:46:24	1	2013-01-15 16:47:00	1	http://data.bnf.fr/ark:/12148/cb119336136	
879	1	1	67717/T1-486	télégraphe	877	106	107	2013-01-15 16:46:27	1	2013-01-15 16:47:24	1	http://data.bnf.fr/ark:/12148/cb11952977f	
1065	1	1	67717/T1-1040	administration locale d'Ancien Régime	776	1006	1007	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1		
1070	1	1	67717/T1-463	cadastre	608	816	817	2013-01-15 16:46:28	1	2013-01-15 16:47:36	1	http://data.bnf.fr/ark:/12148/cb119385254	
1117	1	1	67717/T1-1033	sauvetage en mer	759	906	907	2013-01-15 16:46:29	1	2013-01-15 16:47:39	1	http://data.bnf.fr/ark:/12148/cb11952334h	
976	1	1	67717/T1-1011	oeuf	634	560	561	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb119758837	
1177	1	1	67717/T1-179	salarié agricole	81	442	443	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://data.bnf.fr/ark:/12148/cb119715378	
1178	1	1	67717/T1-449	jeune agriculteur	81	444	445	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://data.bnf.fr/ark:/12148/cb12099036g	
228	1	1	67717/T1-13	vente aux enchères	15	211	212	2013-01-15 16:46:22	1	2013-01-15 16:46:47	1	http://dbpedia.org/resource/Category:Auctioneering	2011-12-13 : ajout du related "commissaire priseur" et suppression du related "objet d'art", suite à l'appel à commentaires d'octobre 2010-mars 2011
658	1	1	67717/T1-1257	véhicule à deux roues	12	81	82	2013-01-15 16:46:25	1	2013-01-15 16:47:10	1	http://data.bnf.fr/ark:/12148/cb13162854m	2011-12-06: modification du preflabel "vehicule à deux roues" en "véhicule à deux roues" et ajout du altlabel "moto" suite à l'appel à commentaires d'octobre 2010-mars 2011
1179	1	1	67717/T1-1209	exploitant agricole	81	446	447	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://data.bnf.fr/ark:/12148/cb119340264	
1174	1	1	67717/T1-841	médecine du travail	935	1346	1347	2013-01-15 16:46:29	1	2013-01-15 16:47:44	1	http://data.bnf.fr/ark:/12148/cb11932453n	
719	1	1	67717/T1-1166	avoué	254	2300	2301	2013-01-15 16:46:26	1	2013-01-15 16:47:14	1	http://data.bnf.fr/ark:/12148/cb11994366f	
338	1	1	67717/T1-1021	socialisme	330	2602	2603	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://data.bnf.fr/ark:/12148/cb12647612n	
1004	1	1	67717/T1-989	défense passive	62	2987	2990	2013-01-15 16:46:28	1	2013-01-15 16:47:32	1	http://data.bnf.fr/ark:/12148/cb133192121	
608	1	1	67717/T1-1058	fiscalité immobilière	27	811	820	2013-01-15 16:46:25	1	2013-01-15 16:47:07	1		
454	1	1	67717/T1-1115	droits seigneuriaux	26	765	766	2013-01-15 16:46:24	1	2013-01-15 16:46:59	1		
1034	1	1	67717/T1-187	contrat de plan	961	376	377	2013-01-15 16:46:28	1	2013-01-15 16:47:34	1	http://data.bnf.fr/ark:/12148/cb119721242	
255	1	1	67717/T1-14	plage	257	1936	1937	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb11944249j	
257	1	1	67717/T1-762	littoral	44	1935	1940	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb11941375n	
260	1	1	67717/T1-1108	défense contre la mer	257	1938	1939	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		
261	1	1	67717/T1-335	discipline sportive	305	1666	1667	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1		
245	1	1	67717/T1-1155	grande surface commerciale	15	271	272	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11937759r	
259	1	1	67717/T1-951	signalisation maritime	647	40	41	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1		
258	1	1	67717/T1-399	aménagement du littoral	678	360	361	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1	http://data.bnf.fr/ark:/12148/cb119562911	
965	1	1	67717/T1-568	prix	17	389	390	2013-01-15 16:46:27	1	2013-01-15 16:47:29	1	http://data.bnf.fr/ark:/12148/cb119709672	
244	1	1	67717/T1-470	manifestation commerciale	15	269	270	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb119314197	
173	1	1	67717/T1-1018	pratiques agraires	21	461	472	2013-01-15 16:46:22	1	2013-01-15 16:46:44	1		
213	1	1	67717/T1-1161	commande publique	24	681	682	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
214	1	1	67717/T1-325	forêt communale	22	503	504	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb12127114p	
72	1	1	67717/T1-441	surveillance des bâtiments	41	1757	1758	2013-01-15 16:46:21	1	2013-01-15 16:46:38	1		
486	1	1	67717/T1-950	construction hospitalière	800	1400	1401	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
234	1	1	67717/T1-581	consommation	15	223	226	2013-01-15 16:46:23	1	2013-01-15 16:46:47	1	http://data.bnf.fr/ark:/12148/cb11934136q	
413	1	1	67717/T1-27	société civile professionnelle	16	305	306	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb12009937f	
499	1	1	67717/T1-35	domaine public maritime	210	656	657	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
946	1	1	67717/T1-113	constitution	30	1061	1062	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1	http://data.bnf.fr/ark:/12148/cb137694876	
948	1	1	67717/T1-385	institutions	30	1071	1072	2013-01-15 16:46:27	1	2013-01-15 16:47:28	1		
412	1	1	67717/T1-822	grâce	408	2440	2441	2013-01-15 16:46:24	1	2013-01-15 16:46:56	1	http://data.bnf.fr/ark:/12148/cb11978966n	
969	1	1	67717/T1-1107	berger	81	440	441	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb11931658p	
970	1	1	67717/T1-238	ver à soie	634	548	549	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb11982081t	
971	1	1	67717/T1-1092	abattoir	634	550	551	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb11936410b	
972	1	1	67717/T1-310	reproduction animale	634	552	553	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb11932756h	
973	1	1	67717/T1-620	bétail	634	554	555	2013-01-15 16:46:27	1	2013-01-15 16:47:30	1	http://data.bnf.fr/ark:/12148/cb11940776v	
916	1	1	67717/T1-1240	naturalisation	450	1100	1101	2013-01-15 16:46:27	1	2013-01-15 16:47:26	1	http://data.bnf.fr/ark:/12148/cb13340916s	
797	1	1	67717/T1-77	naissance	429	1090	1091	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb11958962v	
799	1	1	67717/T1-1201	mariage	429	1092	1093	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1	http://data.bnf.fr/ark:/12148/cb11932417s	
798	1	1	67717/T1-492	population hospitalière	800	1402	1403	2013-01-15 16:46:26	1	2013-01-15 16:47:18	1		
327	1	1	67717/T1-38	enseignant	46	2061	2072	2013-01-15 16:46:23	1	2013-01-15 16:46:52	1	http://dbpedia.org/resource/Category:Teachers	
387	1	1	67717/T1-394	infraction maritime	379	2374	2375	2013-01-15 16:46:23	1	2013-01-15 16:46:55	1		
265	1	1	67717/T1-630	police des frontières	295	702	703	2013-01-15 16:46:23	1	2013-01-15 16:46:49	1	http://data.bnf.fr/ark:/12148/cb12038484z	2011-12-06: remplacement du broader term "police" par "surveillance du territoire" suite à l'appel à commentaires d'octobre 2010-mars 2011
372	1	1	67717/T1-1290	camping caravaning	37	1531	1532	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb11937783m	
373	1	1	67717/T1-1144	tourisme rural	37	1533	1534	2013-01-15 16:46:23	1	2013-01-15 16:46:54	1	http://data.bnf.fr/ark:/12148/cb119483233	
256	1	1	67717/T1-342	tourisme balnéaire	37	1509	1510	2013-01-15 16:46:23	1	2013-01-15 16:46:48	1		
205	1	1	67717/T1-93	concession funéraire	151	644	645	2013-01-15 16:46:22	1	2013-01-15 16:46:45	1	http://data.bnf.fr/ark:/12148/cb13775641p	2011-12-06: Ajout du related term "pompes funèbres" suite à l'appel à commentaires d'octobre 2010-mars 2011
208	1	1	67717/T1-377	aliénation domaniale	151	650	651	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1		
209	1	1	67717/T1-1113	concession domaniale	151	652	653	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb11978416q	
210	1	1	67717/T1-1127	domaine public	24	655	664	2013-01-15 16:46:22	1	2013-01-15 16:46:46	1	http://data.bnf.fr/ark:/12148/cb119719272	
58	1	1	67717/T1-1152	élection	10	2752	2809	2013-01-15 16:46:21	1	2013-01-15 16:46:37	1		
122	1	1	67717/T1-1140	travail de nuit	118	1330	1331	2013-01-15 16:46:22	1	2013-01-15 16:46:40	1	http://data.bnf.fr/ark:/12148/cb119777614	
732	1	1	67717/T1-510	chauffage urbain	730	1874	1875	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb119694084	
733	1	1	67717/T1-537	eau potable	730	1876	1877	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://dbpedia.org/resource/Category:Drinking_water	
734	1	1	67717/T1-856	canalisation	730	1878	1879	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb11975864z	
735	1	1	67717/T1-1295	distribution de gaz	730	1880	1881	2013-01-15 16:46:26	1	2013-01-15 16:47:15	1	http://data.bnf.fr/ark:/12148/cb119821128	
500	1	1	67717/T1-423	occupation temporaire du domaine public	210	658	659	2013-01-15 16:46:24	1	2013-01-15 16:47:01	1		
\.


--
-- Name: adm-keywords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-keywords_id_seq"', 1939, false);


--
-- Data for Name: adm-parapheurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-parapheurs" (id, nom, description, actif, wsto, clientcert_file_uri, passphrase, httpauth, httppasswd, visibilite, typetech, soustype, email_emmeteur, created, created_user_id, modified, modified_user_id) FROM stdin;
\.


--
-- Name: adm-parapheurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-parapheurs_id_seq"', 1, false);


--
-- Data for Name: adm-profils; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-profils" (id, identifiant, nom, description, seda02, seda10, actif, modifiable, created, created_user_id, modified, modified_user_id) FROM stdin;
1	JNLEVT_ASALAE	Journal des évènements as@lae	Profil des fichiers xml d'exportation des événements de as@lae (non modifiable, non supprimable)	\N	\N	t	f	2012-04-03 11:31:00	1	2012-04-03 11:39:11	1
\.


--
-- Name: adm-profils_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-profils_id_seq"', 2, false);


--
-- Data for Name: adm-referentiels; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-referentiels" (id, nom, description) FROM stdin;
1	Accessrestrictioncode	Liste des AccessRestriction, standard d'échange 0.2
2	Appraisalcode	Liste des codes des types Appraisal, standard d'échange 0.2
3	Descriptionlevel	Liste des codes des niveaux de description, standard d'échange 0.2
4	Documenttype	Liste des types de document, standard d'échange 0.2
5	Filetype	Liste des codes des types de fichiers, standard d'échange 0.2
6	Keywordtype	Liste des codes des mots cles, standard d'échange 0.2
7	Languagecode	Liste des codes de langue, ISO-639-1n, standard d'échange 0.2
8	Mimecode	Liste des codes MIME, standard d'échange 0.2
9	Replycode	Liste des codes de réponse, standard d'échange 0.2
\.


--
-- Name: adm-referentiels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-referentiels_id_seq"', 10, false);


--
-- Data for Name: adm-refexterieurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-refexterieurs" (id, type, driver_name, nom, description, actif, parametres, execution_date, update_date, update_report, created, modified, created_user_id, modified_user_id) FROM stdin;
\.


--
-- Name: adm-refexterieurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-refexterieurs_id_seq"', 1, false);


--
-- Data for Name: adm-roles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-roles" (id, collectivite_id, nom, description, created, created_user_id, modified, modified_user_id) FROM stdin;
1	1	Administrateur	Administrateur fonctionnel	2009-02-02 09:35:02	1	2011-07-29 10:33:04	1
\.


--
-- Name: adm-roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-roles_id_seq"', 2, false);


--
-- Data for Name: adm-sequences; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-sequences" (id, nom, description, num_sequence, created, created_user_id, modified, modified_user_id) FROM stdin;
7	DeliveryIdentifier	Utilisée par le compteur ArchiveDelivery	0	2011-01-27 14:21:32	1	2016-03-15 10:58:44	1
11	DestructionNotificationIdentifier	Utilisée par le compteur ArchiveDestructionNotification.	0	2012-04-11 09:20:00	1	2016-02-04 16:57:32	1
8	DestructionRequestIdentifier	Utilisé pour générer le code DestructionRequestIdentifier du bordereau d'élimination d'archive.	0	2011-01-27 14:21:32	1	2016-03-09 14:22:02	1
15	DestructionRequestReplyIdentifier	Utilisée par le compteur Archivedestructionrequestreply.	0	2016-01-19 18:01:07	1	2016-03-09 14:22:47	1
14	DeliveryRequestIdentifier	Utilisé pour générer le code DeliveryRequestIdentifier du bordereau de demande de communication d'archives.	0	2012-04-11 09:20:00	1	2016-03-09 14:42:28	1
12	RestitutionRequestIdentifier	Utilisée par le compteur ArchiveRestitutionRequest.	0	2012-04-11 09:20:00	1	2016-03-02 11:50:05	1
13	RestitutionIdentifier	Utilisée par le compteur ArchiveRestitution.	0	2012-04-11 09:20:00	1	2016-03-02 11:52:41	1
6	TransferIdentifier	Utilisée par le compteur ArchiveTransfert	0	2010-11-06 12:00:00	1	2016-03-15 08:14:37	1
5	AcknowledgementIdentifier	Utilisée par le compteur AcknowledgementIdentifier	0	2014-07-04 11:33:25	1	2016-03-15 08:14:51	1
1	ArchivalAgencyArchiveIdentifier	Utilisée par le compteur ArchivalAgencyArchiveIdentifier.	0	2010-01-06 23:29:01	1	2016-03-15 08:19:55	1
3	TransferReplyIdentifier	Utilisée par le compteur ArchiveTransferReply.	0	2010-01-07 09:18:52	1	2016-03-15 08:20:10	1
16	RestitutionRequestReplyIdentifier	Utilisée par le compteur Archiverestitutionrequestreply.	0	2016-01-20 11:19:23	1	2016-01-29 16:30:57	1
4	TransferAcceptanceIdentifier	Utilisée par le compteur ArchiveTransferAcceptance.	0	2010-01-07 11:29:58	1	2016-03-07 11:27:20	1
2	ArchivalAgencyObjectIdentifier	Utilisée par le compteur ArchivalAgencyObjectIdentifier.	0	2010-04-22 17:20:00	1	2016-03-15 08:19:56	1
9	ArchivalAgencyDocumentIdentifier	Utilisée par les compteurs ArchivalAgencyDocumentArchiveIdentifier et ArchivalAgencyDocumentObjectIdentifier.	0	2012-04-11 09:20:00	1	2016-03-15 08:19:56	1
\.


--
-- Name: adm-sequences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-sequences_id_seq"', 17, false);


--
-- Data for Name: adm-services; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-services" (id, collectivite_id, nom, description, organization_id, acteur_seda, contact_user_id, actif, parent_id, lft, rght, created, created_user_id, modified, modified_user_id) FROM stdin;
1	1	Service d'archives		1	t	1	t	\N	1	2	2009-11-09 08:51:25	1	2013-10-12 09:39:33	1
\.


--
-- Name: adm-services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-services_id_seq"', 2, false);


--
-- Data for Name: adm-typeacteurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-typeacteurs" (id, nom, description, code_type, created, created_user_id, modified, modified_user_id) FROM stdin;
1	Service d'archives	Acteur 'service d'archives' du SEDA	A	2009-10-19 08:50:29	1	2009-10-19 08:50:29	1
2	Service de contrôle	Acteur 'Service de contrôle' du SEDA	C	2009-10-19 08:51:08	1	2009-10-19 08:51:08	1
3	Service versant	Acteur 'Service versant' du SEDA	V	2009-10-19 08:51:31	1	2010-02-24 10:46:03	1
4	Service producteur	Acteur 'Service de producteur' du SEDA	P	2009-10-19 08:51:56	1	2009-10-19 08:51:56	1
5	Service demandeur	Acteur 'Service demandeur' du SEDA	D	2009-10-19 08:52:23	1	2009-10-19 08:52:23	1
\.


--
-- Name: adm-typeacteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-typeacteurs_id_seq"', 6, false);


--
-- Data for Name: adm-typeentites; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-typeentites" (id, code, libelle, created, modified) FROM stdin;
1	1	Collectivite	2009-06-05 14:33:19	2009-06-05 14:33:25
2	2	Famille	2009-06-05 14:34:09	2009-06-05 14:34:13
3	3	Personne	2009-06-05 14:34:34	2009-06-05 14:34:37
\.


--
-- Name: adm-typeentites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-typeentites_id_seq"', 4, false);


--
-- Data for Name: adm-typemessages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-typemessages" (id, classe, code, nom, description) FROM stdin;
1	ArchiveTransferRequest	ArchiveTransferRequest	Demande de transfert d’archives	Demande de transfert d’archives
2	ArchiveTransferRequest	ArchiveTransferRequestReply	Accusé de réception de demande de transfert	Accusé de réception de demande de transfert
3	ArchiveTransferRequest	ArchiveTransferRequestReply	Acceptation de transfert d’archives	Acceptation de transfert d’archives
4	ArchiveTransferRequest	ArchiveTransferRequestReply	Refus de transfert d’archives	Refus de transfert d’archives
5	ArchiveTransferRequest	ArchiveTransferRequestReplyAcknowledgement	Accusé de réception de refus de transfert	Accusé de réception de refus de transfert
6	ArchiveTransfer	ArchiveTransfer	Transfert d’archives	Transfert d’archives
7	ArchiveTransfer	ArchiveTransferReply	Accusé de réception de transfert d’archives	Accusé de réception de transfert d’archives
8	ArchiveTransfer	ArchiveTransferAcceptance	Notification d’acceptation d’archives	Notification d’acceptation d’archives
9	ArchiveTransfer	ArchiveTransferReply	Avis d’anomalie de transfert d’archives	Avis d’anomalie de transfert d’archives
10	ArchiveTransfer	ArchiveTransferReplyAcknowledgement	Accusé de réception d'avis d'anomalie	Accusé de réception d'avis d'anomalie
11	ArchiveDelivery	ArchiveDeliveryRequest	Demande de communication d’archives	Demande de communication d’archives
12	ArchiveDelivery	ArchiveDeliveryRequestReply	Accusé de réception de demande de communication d’archives	Accusé de réception de demande de communication d’archives
13	ArchiveDelivery	ArchiveDeliveryAuthorizationRequest	Demande d'autorisation de communication d’archives	Demande d'autorisation de communication d’archives
14	ArchiveDelivery	ArchiveDeliveryAuthorizationRequestReply	Accusé de réception de demande d'autorisation	Accusé de réception de demande d'autorisation
15	ArchiveDelivery	ArchiveDeliveryAuthorizationRequestReply	Autorisation de communication d'archives	Autorisation de communication d'archives
16	ArchiveDelivery	ArchiveDeliveryAuthorizationRequestReply	Refus d'autorisation de communication d’archives	Refus d'autorisation de communication d’archives
17	ArchiveDelivery	ArchiveDeliveryAuthorizationRequestReplyAcknowledgement	Accusé de réception de refus d'autorisation	Accusé de réception de refus d'autorisation
18	ArchiveDelivery	ArchiveDeliveryRequestReply	Rejet de demande de communication d’archives	Rejet de demande de communication d’archives
19	ArchiveDelivery	ArchiveDelivery	Communication d’archives	Communication d’archives
20	ArchiveDelivery	ArchiveDeliveryAcknowledgement	Accusé de réception de communication d’archives	Accusé de réception de communication d’archives
21	ArchiveDelivery	ArchiveDeliveryAcknowledgement	Avis d’anomalie de réception	Avis d’anomalie de réception
22	ArchiveModification	ArchiveModificationNotification	Avis de modification d’archives	Avis de modification d’archives
23	ArchiveModification	ArchiveModificationNotificationAcknowledgement	Accusé de réception d’avis de modification d’archives	Accusé de réception d’avis de modification d’archives
24	ArchiveDestruction	ArchiveDestructionRequest	Demande d’élimination d’archives	Demande d’élimination d’archives
25	ArchiveDestruction	ArchiveDestructionRequestReply	Accusé de réception de la demande	Accusé de réception de la demande
26	ArchiveDestruction	ArchiveDestructionAcceptance	Acceptation de demande d’élimination d’archives	Acceptation de demande d’élimination d’archives
27	ArchiveDestruction	ArchiveDestructionRequestReply	Rejet de demande d’élimination d’archives	Rejet de demande d’élimination d’archives
28	ArchiveDestruction	ArchiveDestructionRequestReplyAcknowledgement	Accusé de réception de rejet	Accusé de réception de rejet
29	ArchiveDestruction	ArchiveDestructionNotification	Notification d’élimination d’archives	Notification d’élimination d’archives
30	ArchiveRestitution	ArchiveRestitutionRequest	Demande de restitution d’archives	Demande de restitution d’archives
31	ArchiveRestitution	ArchiveRestitutionRequestReply	Accusé de réception de la demande	Accusé de réception de la demande
32	ArchiveRestitution	ArchiveRestitutionRequestReply	Acceptation de demande de restitution d’archives	Acceptation de demande de restitution d’archives
33	ArchiveRestitution	ArchiveRestitutionRequestReply	Rejet de demande de restitution d’archives	Rejet de demande de restitution d’archives
34	ArchiveRestitution	ArchiveRestitution	Restitution d'archives	Restitution d'archives
35	ArchiveRestitution	ArchiveRestitutionAcknowledgement	Accusé de restitution d’archives	Accusé de restitution d’archives
36	ArchiveRestitution	ArchiveRestitutionAcknowledgement	Avis d'anomalie de restitution	Avis d'anomalie de restitution
\.


--
-- Name: adm-typemessages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-typemessages_id_seq"', 37, false);


--
-- Data for Name: adm-users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-users" (id, collectivite_id, service_id, role_id, username, password, cert_subject, need_cert, civilite, nom, prenom, titre, mail, user_delegue_id, actif, affichage_id, created, created_user_id, modified, modified_user_id, last_login, last_logout, password_sha256) FROM stdin;
1	1	1	1	admin	ead104dccfeddb6cd30385ba927896537098ce1b	\\x	f		admin	admin			\N	t	2	2009-11-13 15:49:23	1	2016-03-30 12:13:41	1	2016-03-30 12:13:09	2016-03-30 12:13:41	8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918
\.


--
-- Name: adm-users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-users_id_seq"', 2, false);


--
-- Data for Name: adm-verrous; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "adm-verrous" (id, nom, created, modified) FROM stdin;
1	Archive:create	2013-10-14 07:42:14	2013-10-14 07:42:14
2	Refexterieur:synchronisation	2013-10-14 07:42:14	2013-10-14 07:42:14
3	Archivetransfer:analyse	2015-12-24 16:32:34	2015-12-24 16:32:34
\.


--
-- Name: adm-verrous_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"adm-verrous_id_seq"', 4, false);


--
-- Data for Name: arc-accessrestrictions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-accessrestrictions" (id, code, start_date, accessrestrictioncode_id, end_date, created, modified) FROM stdin;
\.


--
-- Name: arc-accessrestrictions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-accessrestrictions_id_seq"', 1, false);


--
-- Data for Name: arc-appraisals; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-appraisals" (id, code, duration, start_date, appraisalcode_id, end_date, created, modified) FROM stdin;
\.


--
-- Name: arc-appraisals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-appraisals_id_seq"', 1, false);


--
-- Data for Name: arc-archivefiles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-archivefiles" (id, archive_id, type, version, identifier, filename, pronom_unique_id, mime_code, size_bytes, hash_type, hash_value, integrity, created, modified) FROM stdin;
\.


--
-- Name: arc-archivefiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-archivefiles_id_seq"', 1, false);


--
-- Data for Name: arc-archivefilestorages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-archivefilestorages" (id, archivefile_id, volume_identifier, storef, created, modified) FROM stdin;
\.


--
-- Name: arc-archivefilestorages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-archivefilestorages_id_seq"', 1, false);


--
-- Data for Name: arc-archiveobjects; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-archiveobjects" (id, archival_agency_object_identifier, description_level, name, transferring_agency_object_identifier, archive_id, contentdescription_id, accessrestriction_id, appraisal_id, descriptionlevel_id, parent_id, lft, rght, created, modified, taille_documents_octets, eliminated, transfered, restitued, originating_agency_object_identifier) FROM stdin;
\.


--
-- Name: arc-archiveobjects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-archiveobjects_id_seq"', 1, false);


--
-- Data for Name: arc-archives; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-archives" (id, archival_agency_archive_identifier, archival_agreement, archival_profile, name, service_level, transferring_agency_archive_identifier, accord_id, profil_id, archivetransfer_id, transferring_agency_id, archival_agency_id, originating_agency_id, contentdescription_id, accessrestriction_id, appraisal_id, rangement, taille_documents_octets, eliminated, transfered, volume_identifiers_json, fichier_archive_nom, fichier_archive_storef, fichier_cyclevie_nom, fichier_cyclevie_storef, created, modified, integrite_fichiers_statut, integrite_fichiers_process, integrite_fichiers_date, freezed, restitued, description_language_json, originating_agency_archive_identifier) FROM stdin;
\.


--
-- Name: arc-archives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-archives_id_seq"', 1, false);


--
-- Data for Name: arc-contentdescriptions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-contentdescriptions" (id, custodial_history_json, description, file_plan_position, format, latest_date, oldest_date, other_descriptive_data, related_object_reference, size, size_unit_code, other_metadata_json, originating_agency_id, repository_id, accessrestriction_id, created, modified, description_level, descriptionlevel_id, language_json) FROM stdin;
\.


--
-- Name: arc-contentdescriptions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-contentdescriptions_id_seq"', 1, false);


--
-- Data for Name: arc-contentdescriptives; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-contentdescriptives" (id, keyword_content, keyword_reference, keyword_type, contentdescription_id, accessrestriction_id, keywordtype_id, created, modified) FROM stdin;
\.


--
-- Name: arc-contentdescriptives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-contentdescriptives_id_seq"', 1, false);


--
-- Data for Name: arc-documentfiles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-documentfiles" (id, document_id, document_type, document_order, identifier, filename, pronom_unique_id, mime_code, size_bytes, hash_type, hash_value, deleted, json_metadata, integrity, created, modified) FROM stdin;
\.


--
-- Name: arc-documentfiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-documentfiles_id_seq"', 1, false);


--
-- Data for Name: arc-documentfilestorages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-documentfilestorages" (id, documentfile_id, volume_identifier, storef, created, modified) FROM stdin;
\.


--
-- Name: arc-documentfilestorages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-documentfilestorages_id_seq"', 1, false);


--
-- Data for Name: arc-documents; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-documents" (id, attachment, attachment_filename, attachment_format, attachment_mime_code, control, copy, creation, description, identification, issue, item_identifier, purpose, receipt, response, status, submission, type, other_metadata_json, model, foreign_key, documenttype_id, created, modified, converted, conversion_format, archival_agency_document_identifier, originating_agency_document_identifier, transferring_agency_document_identifier, language_json) FROM stdin;
\.


--
-- Name: arc-documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-documents_id_seq"', 1, false);


--
-- Data for Name: arc-organizations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "arc-organizations" (id, identification, nom, prive, parent_id, lft, rght, typeentite_id, date_existence_debut, date_existence_fin, seda02, seda10, active, modifiable, created, created_user_id, modified, modified_user_id, collectivite_id) FROM stdin;
1	FRAD000	Service d'archives	f	\N	1	2	1	\N	\N	<?xml version="1.0" encoding="UTF-8"?>\n<Organization>\n  <Identification>FRAD000</Identification>\n  <Name>Service d'archives départemental</Name>\n</Organization>\n	<?xml version="1.0" encoding="UTF-8"?>\n<Organization>\n  <Identification>FRAD000</Identification>\n  <Name>Service d'archives départemental</Name>\n</Organization>\n	t	t	2011-01-25 16:25:42	1	2016-01-29 15:00:22	1	1
\.


--
-- Name: arc-organizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"arc-organizations_id_seq"', 2, false);


--
-- Data for Name: archivedeliveries_archiveobjects; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archivedeliveries_archiveobjects (id, archiveobject_id, archivedelivery_id) FROM stdin;
\.


--
-- Name: archivedeliveries_archiveobjects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archivedeliveries_archiveobjects_id_seq', 1, false);


--
-- Data for Name: archivedeliveries_archives; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archivedeliveries_archives (id, archive_id, archivedelivery_id) FROM stdin;
\.


--
-- Name: archivedeliveries_archives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archivedeliveries_archives_id_seq', 1, false);


--
-- Data for Name: archivedeliveryrequests_archives; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archivedeliveryrequests_archives (id, archive_id, archivedeliveryrequest_id) FROM stdin;
\.


--
-- Name: archivedeliveryrequests_archives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archivedeliveryrequests_archives_id_seq', 1, false);


--
-- Data for Name: archivedestructionrequests_archives; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archivedestructionrequests_archives (id, archive_id, archivedestructionrequest_id) FROM stdin;
\.


--
-- Name: archivedestructionrequests_archives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archivedestructionrequests_archives_id_seq', 1, false);


--
-- Data for Name: archivedestructionrequests_arcobjs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archivedestructionrequests_arcobjs (id, archiveobject_id, archivedestructionrequest_id) FROM stdin;
\.


--
-- Name: archivedestructionrequests_arcobjs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archivedestructionrequests_arcobjs_id_seq', 1, false);


--
-- Data for Name: archiverestitutionrequests_archives; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archiverestitutionrequests_archives (id, archive_id, archiverestitutionrequest_id) FROM stdin;
\.


--
-- Name: archiverestitutionrequests_archives_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archiverestitutionrequests_archives_id_seq', 1, false);


--
-- Data for Name: archives_archivetransfers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY archives_archivetransfers (id, archive_id, archivetransfer_id) FROM stdin;
\.


--
-- Name: archives_archivetransfers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('archives_archivetransfers_id_seq', 1, false);


--
-- Data for Name: aros; Type: TABLE DATA; Schema: public; Owner: -
--

COPY aros (id, parent_id, model, foreign_key, alias, lft, rght) FROM stdin;
3	2	User	1	admin	3	4
1	\N	Collectivite	1	Conseil Général	1	6
2	1	Role	1	Administrateur	2	5
\.


--
-- Data for Name: aros_acos; Type: TABLE DATA; Schema: public; Owner: -
--

COPY aros_acos (id, aro_id, aco_id, _create, _read, _update, _delete) FROM stdin;
13	1	13	1 	1 	1 	1 
14	1	14	1 	1 	1 	1 
15	1	15	1 	1 	1 	1 
58	3	2	1 	1 	1 	1 
59	3	3	1 	1 	1 	1 
60	3	4	1 	1 	1 	1 
16	1	16	1 	1 	1 	1 
62	3	6	1 	1 	1 	1 
63	3	7	1 	1 	1 	1 
64	3	8	1 	1 	1 	1 
65	3	9	1 	1 	1 	1 
66	3	10	1 	1 	1 	1 
67	3	11	1 	1 	1 	1 
68	3	12	1 	1 	1 	1 
69	3	13	1 	1 	1 	1 
70	3	14	1 	1 	1 	1 
71	3	15	1 	1 	1 	1 
72	3	16	1 	1 	1 	1 
73	3	17	1 	1 	1 	1 
74	3	18	1 	1 	1 	1 
75	3	19	1 	1 	1 	1 
76	3	20	1 	1 	1 	1 
77	3	21	1 	1 	1 	1 
78	3	22	1 	1 	1 	1 
79	3	23	1 	1 	1 	1 
80	3	24	1 	1 	1 	1 
81	3	25	1 	1 	1 	1 
82	3	26	1 	1 	1 	1 
83	3	27	1 	1 	1 	1 
84	3	28	1 	1 	1 	1 
85	3	29	1 	1 	1 	1 
86	3	30	1 	1 	1 	1 
87	3	31	1 	1 	1 	1 
88	3	32	1 	1 	1 	1 
89	3	33	1 	1 	1 	1 
90	3	34	1 	1 	1 	1 
91	3	35	1 	1 	1 	1 
92	3	36	1 	1 	1 	1 
93	3	37	1 	1 	1 	1 
94	3	38	1 	1 	1 	1 
95	3	39	1 	1 	1 	1 
96	3	40	1 	1 	1 	1 
97	3	41	1 	1 	1 	1 
98	3	42	1 	1 	1 	1 
99	3	43	1 	1 	1 	1 
100	3	44	1 	1 	1 	1 
101	3	45	1 	1 	1 	1 
102	3	46	1 	1 	1 	1 
103	3	47	1 	1 	1 	1 
104	3	48	1 	1 	1 	1 
105	3	49	1 	1 	1 	1 
106	3	50	1 	1 	1 	1 
107	3	51	1 	1 	1 	1 
108	3	52	1 	1 	1 	1 
109	3	53	1 	1 	1 	1 
110	3	54	1 	1 	1 	1 
111	3	55	1 	1 	1 	1 
112	3	56	1 	1 	1 	1 
17	1	17	1 	1 	1 	1 
18	1	18	1 	1 	1 	1 
19	1	19	1 	1 	1 	1 
20	1	20	1 	1 	1 	1 
21	1	21	1 	1 	1 	1 
22	1	22	1 	1 	1 	1 
23	1	23	1 	1 	1 	1 
24	1	24	1 	1 	1 	1 
25	1	25	1 	1 	1 	1 
26	1	26	1 	1 	1 	1 
27	1	27	1 	1 	1 	1 
28	1	28	1 	1 	1 	1 
30	1	30	1 	1 	1 	1 
31	1	31	1 	1 	1 	1 
32	1	32	1 	1 	1 	1 
33	1	33	1 	1 	1 	1 
34	1	34	1 	1 	1 	1 
35	1	35	1 	1 	1 	1 
36	1	36	1 	1 	1 	1 
37	1	37	1 	1 	1 	1 
38	1	38	1 	1 	1 	1 
39	1	39	1 	1 	1 	1 
40	1	40	1 	1 	1 	1 
41	1	41	1 	1 	1 	1 
42	1	42	1 	1 	1 	1 
43	1	43	1 	1 	1 	1 
44	1	44	1 	1 	1 	1 
45	1	45	1 	1 	1 	1 
47	1	47	1 	1 	1 	1 
48	1	48	1 	1 	1 	1 
49	1	49	1 	1 	1 	1 
50	1	50	1 	1 	1 	1 
51	1	51	1 	1 	1 	1 
52	1	52	1 	1 	1 	1 
53	1	53	1 	1 	1 	1 
54	1	54	1 	1 	1 	1 
55	1	55	1 	1 	1 	1 
56	1	56	1 	1 	1 	1 
1	1	1	1 	1 	1 	1 
2	1	2	1 	1 	1 	1 
3	1	3	1 	1 	1 	1 
4	1	4	1 	1 	1 	1 
5	1	5	1 	1 	1 	1 
6	1	6	1 	1 	1 	1 
7	1	7	1 	1 	1 	1 
8	1	8	1 	1 	1 	1 
9	1	9	1 	1 	1 	1 
10	1	10	1 	1 	1 	1 
11	1	11	1 	1 	1 	1 
57	3	1	1 	1 	1 	1 
61	3	5	1 	1 	1 	1 
12	1	12	1 	1 	1 	1 
29	1	29	1 	1 	1 	1 
46	1	46	1 	1 	1 	1 
282	1	58	1 	1 	1 	1 
341	3	58	1 	1 	1 	1 
\.


--
-- Name: aros_acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('aros_acos_id_seq', 342, false);


--
-- Name: aros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('aros_id_seq', 4, false);


--
-- Data for Name: connecteurs_organizations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY connecteurs_organizations (id, connecteur_id, organization_id) FROM stdin;
1	1	1
\.


--
-- Name: connecteurs_organizations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('connecteurs_organizations_id_seq', 15, false);


--
-- Data for Name: io-acknowledgements; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-acknowledgements" (id, sens, version_seda, fichier_message, foreign_model, foreign_key, comment, date, acknowledgement_identifier, acknowledgement_identifier_jsattr, message_received_identifier, receiver_id, sender_id, created, created_user_id, modified, modified_user_id) FROM stdin;
\.


--
-- Name: io-acknowledgements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-acknowledgements_id_seq"', 1, false);


--
-- Data for Name: io-alertes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-alertes" (id, model, foreign_key, code, commentaire, note, created) FROM stdin;
\.


--
-- Name: io-alertes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-alertes_id_seq"', 1, false);


--
-- Data for Name: io-archivedeliveries; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivedeliveries" (id, sens, comment, date, delivery_authorization_identifier, delivery_identifier, delivery_request_identifier, unit_identifier, requester_id, archival_agency_id, fichier_message, fichier_documents, statut, created, created_user_id, modified, modified_user_id, type_demandeur, archivedeliveryrequest_id, comfiles_nb, comfiles_size_bytes, version_seda) FROM stdin;
\.


--
-- Name: io-archivedeliveries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivedeliveries_id_seq"', 1, false);


--
-- Data for Name: io-archivedeliveryrequests; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivedeliveryrequests" (id, sens, comment, date, delivery_request_identifier, derogation, unit_identifier, access_requester_id, archival_agency_id, fichier_message, statut, decision, created, created_user_id, modified, modified_user_id, version_seda) FROM stdin;
\.


--
-- Name: io-archivedeliveryrequests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivedeliveryrequests_id_seq"', 1, false);


--
-- Data for Name: io-archivedestructionnotifications; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivedestructionnotifications" (id, sens, comment, date, destruct_acceptance_identifier, destruct_date, destruct_notif_identifier, destruct_request_identifier, unit_identifier, originating_agency_id, archival_agency_id, archivedestructionrequest_id, fichier_message, envoi_date, envoi_destinataire, created, created_user_id, modified, modified_user_id, version_seda) FROM stdin;
\.


--
-- Name: io-archivedestructionnotifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivedestructionnotifications_id_seq"', 1, false);


--
-- Data for Name: io-archivedestructionrequestreplies; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivedestructionrequestreplies" (id, sens, version_seda, comment, date, des_req_identifier, des_req_rep_identifier, reply_code, unit_identifiers_json, archival_agency_id, originating_agency_id, archivedestructionrequest_id, fichier_message, created, created_user_id, modified, modified_user_id) FROM stdin;
\.


--
-- Name: io-archivedestructionrequestreplies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivedestructionrequestreplies_id_seq"', 1, false);


--
-- Data for Name: io-archivedestructionrequests; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivedestructionrequests" (id, sens, comment, date, destruction_request_identifier, unit_identifier, originating_agency_id, archival_agency_id, control_authority_id, fichier_message, statut, created, created_user_id, modified, modified_user_id, fichier_message_pdf, fichier_message_signe, fichier_message_pdf_signe, decision, signature_message_pdf, desfiles_nb, desfiles_size_bytes, version_seda) FROM stdin;
\.


--
-- Name: io-archivedestructionrequests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivedestructionrequests_id_seq"', 1, false);


--
-- Data for Name: io-archiverestitutionrequestreplies; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archiverestitutionrequestreplies" (id, sens, version_seda, comment, date, reply_code, res_req_identifier, res_req_rep_identifier, unit_identifiers_json, archival_agency_id, originating_agency_id, archiverestitutionrequest_id, fichier_message, created, created_user_id, modified, modified_user_id) FROM stdin;
\.


--
-- Name: io-archiverestitutionrequestreplies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archiverestitutionrequestreplies_id_seq"', 1, false);


--
-- Data for Name: io-archiverestitutionrequests; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archiverestitutionrequests" (id, sens, comment, date, restitution_request_identifier, unit_identifier, originating_agency_id, archival_agency_id, fichier_message, statut, decision, created, created_user_id, modified, modified_user_id, version_seda) FROM stdin;
\.


--
-- Name: io-archiverestitutionrequests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archiverestitutionrequests_id_seq"', 1, false);


--
-- Data for Name: io-archiverestitutions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archiverestitutions" (id, sens, comment, date, restitution_identifier, restitution_request_reply_identifier, originating_agency_id, archival_agency_id, archiverestitutionrequestreply_id, archiverestitutionrequest_id, fichier_message, fichier_documents, envoi_date, envoi_destinataire, created, created_user_id, modified, modified_user_id, statut, version_seda) FROM stdin;
\.


--
-- Name: io-archiverestitutions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archiverestitutions_id_seq"', 1, false);


--
-- Data for Name: io-archivetransferacceptances; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivetransferacceptances" (id, sens, comment, date, reply_code, transfer_acceptance_identifier, transfer_identifier, archivetransfer_id, transferring_agency_id, archival_agency_id, fichier_message, created, created_user_id, modified, modified_user_id, version_seda, transfer_acceptance_ident_jsattr) FROM stdin;
\.


--
-- Name: io-archivetransferacceptances_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivetransferacceptances_id_seq"', 1, false);


--
-- Data for Name: io-archivetransferreplies; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivetransferreplies" (id, sens, comment, date, reply_code, transfer_identifier, transfer_reply_identifier, archivetransfer_id, transferring_agency_id, archival_agency_id, fichier_message, created, created_user_id, modified, modified_user_id, version_seda, grant_date) FROM stdin;
\.


--
-- Name: io-archivetransferreplies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivetransferreplies_id_seq"', 1, false);


--
-- Data for Name: io-archivetransfers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-archivetransfers" (id, sens, comment, date, related_transfer_reference, transfer_identifier, transfer_request_reply_identifier, transferring_agency_id, archival_agency_id, originating_agency_id, accord_id, contrat_id, fichier_message, repertoire_documents, taille_documents_octets, statut, conforme, decision, created, created_user_id, modified, modified_user_id, origine, fichier_documents, alerte, infected, fichier_message_orig, repondre, repondre_via, repondre_destinataire, version_seda) FROM stdin;
\.


--
-- Name: io-archivetransfers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-archivetransfers_id_seq"', 1, false);


--
-- Data for Name: io-avscans; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-avscans" (id, model, foreign_key, antivirus_type, date, scan_summary, scan_details) FROM stdin;
\.


--
-- Name: io-avscans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-avscans_id_seq"', 1, false);


--
-- Data for Name: io-erreurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-erreurs" (id, model, foreign_key, code, commentaire, note, created) FROM stdin;
\.


--
-- Name: io-erreurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-erreurs_id_seq"', 1, false);


--
-- Data for Name: io-messages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-messages" (id, collectivite_id, service_id, comment, date, typemessage_id, fichier_message, expediteur_organization_id, destinataire_organization_id, sens, statut, message_id, created, created_user_id, modified, modified_user_id) FROM stdin;
\.


--
-- Name: io-messages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-messages_id_seq"', 1, false);


--
-- Data for Name: io-piecejointes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "io-piecejointes" (id, archivetransfer_id, filename, taille_octets, bdx_fmt, bdx_mimecode, val_info, val_fmt, val_mimecode, val_format, val_version, val_bien_forme, val_valide, val_archivable, val_date, infected, virusname, fmt_puid, fmt_formatname, fmt_version, fmt_signaturename, fmt_mimetype, fmt_info, fmt_date) FROM stdin;
\.


--
-- Name: io-piecejointes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"io-piecejointes_id_seq"', 1, false);


--
-- Data for Name: keywords_keywords; Type: TABLE DATA; Schema: public; Owner: -
--

COPY keywords_keywords (id, keyword_id, related_id) FROM stdin;
1	3	19
2	3	20
3	12	296
4	12	370
5	14	37
6	14	181
7	15	230
8	15	231
9	16	415
10	16	416
11	16	417
12	18	186
13	19	63
14	19	81
15	19	82
16	20	81
17	21	83
18	21	170
19	22	467
20	22	468
21	22	469
22	23	111
23	23	112
24	23	631
25	27	605
26	32	586
27	33	552
28	35	361
29	36	91
30	36	1098
31	36	1174
32	36	1195
33	37	14
34	45	96
35	45	97
36	45	98
37	47	987
38	55	291
39	55	293
40	56	330
41	57	781
42	58	924
43	59	283
44	60	153
45	60	154
46	61	264
47	61	265
48	62	279
49	62	282
50	62	393
51	63	19
52	63	64
53	63	65
54	63	66
55	64	63
56	65	63
57	65	84
58	66	63
59	67	228
60	70	558
61	71	758
62	72	441
63	74	1390
64	77	434
65	80	540
66	81	19
67	81	20
68	82	19
69	83	21
70	84	65
71	84	365
72	85	711
73	86	1085
74	88	483
75	88	485
76	91	36
77	92	536
78	94	408
79	96	45
80	97	45
81	98	45
82	100	113
83	102	114
84	103	116
85	111	23
86	112	23
87	113	100
88	114	102
89	114	1057
90	114	1156
91	116	103
92	120	540
93	124	790
94	126	127
95	126	128
96	127	126
97	127	128
98	127	642
99	127	650
100	128	126
101	128	127
102	128	489
103	135	136
104	136	135
105	136	142
106	136	143
107	136	144
108	137	138
109	138	137
110	139	775
111	140	774
112	141	772
113	142	136
114	142	692
115	143	136
116	143	692
117	144	136
118	144	692
119	146	147
120	147	146
121	148	149
122	149	148
123	151	196
124	151	197
125	153	60
126	154	60
127	156	380
128	158	311
129	158	1007
130	165	166
131	166	165
132	168	178
133	170	21
134	171	641
135	171	1036
136	171	1047
137	172	453
138	172	459
139	175	634
140	175	967
141	176	188
142	178	168
143	179	304
144	179	733
145	181	14
146	184	736
147	184	737
148	186	18
149	187	238
150	187	633
151	187	971
152	188	176
153	189	762
154	189	1352
155	190	512
156	195	535
157	196	151
158	196	218
159	197	151
160	197	219
161	201	214
162	205	215
163	205	216
164	207	217
165	214	201
166	215	205
167	215	216
168	215	224
169	216	205
170	216	215
171	217	207
172	218	196
173	218	530
174	219	197
175	220	439
176	223	1022
177	224	215
178	227	661
179	228	67
180	228	229
181	229	228
182	229	253
183	230	15
184	231	15
185	235	434
186	235	435
187	238	187
188	238	1276
189	239	1288
190	241	615
191	241	1364
192	245	485
193	246	434
194	246	936
195	249	390
196	250	434
197	252	434
198	252	740
199	253	229
200	255	256
201	256	255
202	256	261
203	256	262
204	257	258
205	257	259
206	258	257
207	259	257
208	261	256
209	261	369
210	262	256
211	262	369
212	264	61
213	265	61
214	265	266
215	265	915
216	266	265
217	267	343
218	271	590
219	273	289
220	277	693
221	279	62
222	279	1000
223	282	62
224	282	1000
225	283	59
226	283	784
227	287	1493
228	289	273
229	289	290
230	289	291
231	289	292
232	289	293
233	290	289
234	291	55
235	291	289
236	291	306
237	292	289
238	293	55
239	293	289
240	293	306
241	293	312
242	294	379
243	294	433
244	296	12
245	297	395
246	298	1296
247	299	411
248	299	905
249	299	906
250	300	1037
251	301	1038
252	301	1412
253	304	179
254	305	992
255	306	291
256	306	293
257	306	349
258	306	600
259	308	328
260	311	158
261	312	293
262	319	936
263	322	323
264	323	322
265	326	576
266	327	515
267	327	516
268	327	517
269	328	308
270	330	56
271	334	522
272	342	847
273	343	267
274	349	306
275	349	848
276	349	849
277	356	574
278	361	35
279	362	667
280	363	667
281	365	84
282	366	514
283	369	261
284	369	262
285	369	1028
286	370	12
287	370	434
288	371	434
289	372	489
290	379	294
291	380	156
292	380	157
293	387	647
294	388	435
295	390	249
296	390	831
297	390	836
298	390	1120
299	390	1364
300	390	1457
301	393	62
302	395	297
303	398	997
304	398	1448
305	401	664
306	408	94
307	411	299
308	413	414
309	414	413
310	414	422
311	414	429
312	414	430
313	415	16
314	416	16
315	417	16
316	418	548
317	418	550
318	418	551
319	418	552
320	418	553
321	419	753
322	420	981
323	422	414
324	423	1441
325	424	1179
326	425	432
327	425	769
328	428	552
329	429	414
330	430	414
331	432	425
332	433	294
333	434	77
334	434	235
335	434	246
336	434	250
337	434	252
338	434	370
339	434	371
340	434	439
341	434	440
342	434	441
343	434	442
344	434	443
345	434	444
346	434	445
347	434	446
348	434	447
349	434	448
350	434	449
351	435	235
352	435	388
353	436	450
354	439	220
355	439	434
356	439	470
357	439	471
358	440	434
359	441	72
360	441	434
361	442	434
362	443	434
363	444	434
364	444	485
365	444	635
366	444	1030
367	444	1134
368	444	1228
369	445	434
370	446	434
371	447	434
372	447	974
373	448	434
374	449	434
375	450	436
376	450	686
377	450	833
378	450	914
379	453	172
380	453	459
381	454	1152
382	454	1154
383	459	172
384	459	453
385	459	1048
386	461	462
387	462	461
388	462	643
389	463	629
390	467	22
391	468	22
392	468	634
393	469	22
394	469	1043
395	470	439
396	471	439
397	480	573
398	483	88
399	483	484
400	483	485
401	483	486
402	483	487
403	483	488
404	484	483
405	484	485
406	485	88
407	485	245
408	485	444
409	485	483
410	485	484
411	485	487
412	485	523
413	485	777
414	485	786
415	485	800
416	485	980
417	485	984
418	485	985
419	485	986
420	486	483
421	487	483
422	487	485
423	487	661
424	488	483
425	489	128
426	489	372
427	489	575
428	491	985
429	499	14
430	502	14
431	503	504
432	504	503
433	504	512
434	505	506
435	506	505
436	506	1124
437	510	512
438	512	190
439	512	504
440	512	510
441	512	873
442	513	731
443	514	366
444	515	327
445	516	327
446	516	537
447	517	327
448	517	539
449	520	535
450	522	334
451	522	766
452	523	485
453	523	1063
454	530	218
455	535	195
456	535	520
457	536	92
458	536	990
459	536	1201
460	537	516
461	537	800
462	537	857
463	538	854
464	539	517
465	540	80
466	540	120
467	540	831
468	548	418
469	548	549
470	549	548
471	550	418
472	550	904
473	551	418
474	552	33
475	552	418
476	552	428
477	553	418
478	555	556
479	556	555
480	556	558
481	556	559
482	556	562
483	556	563
484	556	564
485	556	565
486	558	70
487	558	556
488	559	556
489	559	1226
490	562	556
491	563	556
492	564	556
493	565	556
494	567	589
495	567	810
496	567	811
497	567	812
498	568	598
499	568	832
500	568	956
501	568	1001
502	568	1109
503	568	1110
504	569	994
505	570	1177
506	570	1180
507	572	588
508	572	592
509	572	686
510	572	1211
511	572	1215
512	573	480
513	573	1307
514	574	356
515	574	810
516	575	489
517	575	997
518	576	326
519	576	588
520	576	593
521	576	899
522	576	1266
523	576	1408
524	586	32
525	587	596
526	588	572
527	588	576
528	588	592
529	588	686
530	589	567
531	589	592
532	589	686
533	589	885
534	590	271
535	592	572
536	592	588
537	592	589
538	593	576
539	594	832
540	596	587
541	597	668
542	598	568
543	599	983
544	599	1356
545	599	1401
546	599	1442
547	599	1460
548	600	306
549	600	834
550	601	887
551	603	675
552	605	27
553	608	1069
554	610	1247
555	615	241
556	623	624
557	623	625
558	624	623
559	625	623
560	626	652
561	626	653
562	626	1142
563	629	463
564	631	23
565	633	187
566	634	175
567	634	468
568	634	967
569	634	968
570	634	969
571	635	444
572	635	862
573	636	1161
574	637	756
575	637	763
576	640	15
577	641	171
578	642	127
579	642	643
580	642	644
581	642	645
582	643	462
583	643	642
584	643	647
585	643	649
586	643	650
587	643	651
588	643	653
589	643	654
590	643	656
591	643	658
592	643	659
593	643	664
594	644	642
595	644	665
596	645	642
597	647	387
598	647	643
599	647	1117
600	649	643
601	649	1278
602	650	127
603	650	643
604	650	1289
605	651	643
606	651	1322
607	652	626
608	653	626
609	653	643
610	654	643
611	654	716
612	655	1180
613	656	643
614	656	1434
615	657	979
616	658	643
617	658	1450
618	659	643
619	661	227
620	661	487
621	664	401
622	664	643
623	664	798
624	664	1077
625	664	1175
626	665	644
627	666	667
628	667	362
629	667	363
630	667	666
631	667	669
632	667	673
633	667	674
634	668	597
635	669	667
636	670	1219
637	670	1409
638	673	667
639	674	667
640	675	603
641	676	677
642	677	676
643	686	450
644	686	572
645	686	588
646	686	589
647	692	142
648	692	143
649	692	144
650	693	277
651	693	694
652	694	693
653	695	775
654	695	933
655	696	775
656	696	933
657	697	928
658	697	929
659	698	772
660	698	930
661	698	931
662	699	772
663	699	930
664	699	931
665	700	932
666	702	774
667	711	85
668	716	654
669	717	718
670	718	717
671	721	891
672	724	725
673	724	726
674	725	724
675	725	726
676	725	727
677	725	728
678	726	724
679	726	725
680	727	725
681	728	725
682	731	513
683	733	179
684	733	1276
685	736	184
686	737	184
687	740	252
688	740	861
689	753	419
690	754	760
691	754	761
692	756	637
693	756	762
694	756	763
695	758	71
696	759	1114
697	760	754
698	761	754
699	762	189
700	762	756
701	763	637
702	763	756
703	766	522
704	769	425
705	770	1335
706	772	141
707	772	698
708	772	699
709	772	779
710	774	140
711	774	702
712	775	139
713	775	695
714	775	696
715	775	780
716	777	485
717	779	772
718	779	930
719	779	931
720	780	775
721	780	933
722	781	57
723	784	283
724	786	485
725	786	1022
726	790	124
727	790	791
728	791	790
729	797	798
730	798	664
731	798	797
732	799	1235
733	799	1408
734	800	485
735	800	537
736	800	854
737	802	803
738	803	802
739	803	1024
740	803	1025
741	808	567
742	808	809
743	810	567
744	810	574
745	810	1073
746	811	567
747	812	567
748	822	999
749	823	1242
750	826	1083
751	827	568
752	828	1209
753	831	390
754	831	540
755	832	568
756	832	594
757	833	450
758	834	600
759	836	390
760	838	1211
761	842	846
762	844	936
763	846	842
764	847	342
765	848	349
766	849	349
767	854	538
768	854	800
769	857	537
770	859	860
771	860	859
772	861	740
773	862	635
774	872	873
775	873	512
776	873	872
777	873	875
778	875	873
779	883	884
780	884	883
781	885	589
782	887	601
783	890	1151
784	891	721
785	899	576
786	901	983
787	901	1356
788	901	1442
789	904	550
790	905	299
791	906	299
792	912	913
793	913	912
794	914	450
795	915	265
796	918	928
797	920	929
798	922	932
799	923	929
800	924	58
801	927	931
802	928	697
803	928	918
804	929	697
805	929	920
806	929	923
807	930	698
808	930	699
809	930	779
810	931	698
811	931	699
812	931	779
813	931	927
814	932	700
815	932	922
816	933	695
817	933	696
818	933	780
819	935	1172
820	936	246
821	936	319
822	936	844
823	936	1076
824	936	1091
825	936	1140
826	936	1224
827	936	1244
828	939	1280
829	956	568
830	958	1228
831	967	175
832	967	634
833	968	634
834	969	634
835	971	187
836	974	447
837	979	657
838	979	1012
839	979	1298
840	980	485
841	981	420
842	983	599
843	983	901
844	984	485
845	985	485
846	985	491
847	986	485
848	987	47
849	990	536
850	992	305
851	994	569
852	996	1210
853	997	398
854	997	575
855	997	1399
856	997	1400
857	999	822
858	1000	279
859	1000	282
860	1001	568
861	1007	158
862	1012	979
863	1022	223
864	1022	786
865	1024	803
866	1025	803
867	1030	444
868	1035	1036
869	1035	1037
870	1035	1038
871	1035	1039
872	1036	171
873	1036	1035
874	1036	1047
875	1037	300
876	1037	1035
877	1037	1042
878	1038	301
879	1038	1035
880	1038	1039
881	1039	1035
882	1039	1038
883	1042	1037
884	1043	469
885	1047	171
886	1047	1036
887	1048	459
888	1057	114
889	1063	523
890	1069	608
891	1070	1348
892	1073	810
893	1076	936
894	1077	664
895	1083	826
896	1085	86
897	1091	936
898	1098	36
899	1109	568
900	1110	568
901	1114	759
902	1116	1196
903	1117	647
904	1120	390
905	1124	506
906	1134	444
907	1140	936
908	1142	626
909	1151	890
910	1152	454
911	1154	454
912	1156	114
913	1161	636
914	1172	935
915	1173	1390
916	1174	36
917	1175	664
918	1177	570
919	1179	424
920	1180	570
921	1180	655
922	1188	1198
923	1195	36
924	1196	1116
925	1198	1188
926	1201	536
927	1209	828
928	1210	996
929	1210	1399
930	1211	572
931	1211	838
932	1215	572
933	1219	670
934	1224	936
935	1226	559
936	1228	444
937	1228	958
938	1235	799
939	1236	1379
940	1242	823
941	1244	936
942	1247	610
943	1266	576
944	1276	238
945	1276	733
946	1276	1280
947	1278	649
948	1280	939
949	1280	1276
950	1288	239
951	1289	650
952	1296	298
953	1298	979
954	1303	1304
955	1304	1303
956	1307	573
957	1322	651
958	1335	770
959	1348	1070
960	1352	189
961	1354	1355
962	1355	1354
963	1356	599
964	1356	901
965	1364	241
966	1364	390
967	1374	27
968	1379	1236
969	1390	74
970	1390	1173
971	1399	997
972	1399	1210
973	1400	997
974	1401	599
975	1408	576
976	1408	799
977	1409	670
978	1412	301
979	1433	1434
980	1434	656
981	1441	423
982	1442	599
983	1442	901
984	1448	398
985	1450	658
986	1457	390
987	1460	599
988	1493	287
989	1506	1530
990	1507	1515
991	1515	1507
992	1518	1555
993	1530	1506
994	1532	1566
995	1555	1518
996	1566	1532
997	1730	1872
998	1738	1875
999	1740	1848
1000	1759	1848
1001	1768	1815
1002	1768	1907
1003	1771	1848
1004	1790	1816
1005	1795	1848
1006	1814	1924
1007	1815	1768
1008	1816	1790
1009	1818	1853
1010	1837	1865
1011	1848	1740
1012	1848	1759
1013	1848	1771
1014	1848	1795
1015	1848	1861
1016	1848	1863
1017	1853	1818
1018	1861	1848
1019	1863	1848
1020	1865	1837
1021	1868	1900
1022	1868	1924
1023	1872	1730
1024	1875	1738
1025	1900	1868
1026	1900	1924
1027	1907	1768
1028	1924	1814
1029	1924	1868
1030	1924	1900
\.


--
-- Name: keywords_keywords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('keywords_keywords_id_seq', 1031, false);


--
-- Data for Name: oaipmh-resumptiontoken_records; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "oaipmh-resumptiontoken_records" (id, resumptiontoken_id, archive_id, identifier, datestamp, deleted) FROM stdin;
\.


--
-- Name: oaipmh-resumptiontoken_records_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"oaipmh-resumptiontoken_records_id_seq"', 1, false);


--
-- Data for Name: oaipmh-resumptiontokens; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "oaipmh-resumptiontokens" (id, resumption_token, verb, metadataprefix, expiration_date, complete_list_size, cursor) FROM stdin;
\.


--
-- Name: oaipmh-resumptiontokens_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"oaipmh-resumptiontokens_id_seq"', 1, false);


--
-- Data for Name: organizations_typeacteurs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY organizations_typeacteurs (id, organization_id, typeacteur_id) FROM stdin;
1	1	1
2	1	3
3	1	4
4	1	5
\.


--
-- Name: organizations_typeacteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('organizations_typeacteurs_id_seq', 394, false);


--
-- Data for Name: ref-accessrestrictioncodes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-accessrestrictioncodes" (id, code, name, description) FROM stdin;
1	AR038	0 an	Documents administratifs librement communicables. (Code du Patrimoine, art. L. 213-1)
2	AR039	25 ans	Documents dont la communication porte atteinte au secret des délibérations du Gouvernement et des autorités responsables relevant du pouvoir exécutif, à la conduite des relations extérieures, à la monnaie et au crédit public, au secret en matière commerciale et industrielle, à la recherche par les services compétents des infractions fiscales et douanières. (Code du Patrimoine, art. L. 213-2, I, 1, a)
3	AR040	25 ans	Documents qui ne sont pas considérés comme administratifs et mentionnés au dernier alinéa de l'article 1er de la loi n° 78-753 du 17 juillet 1978 : actes et documents élaborés ou détenus par les assemblées parlementaires, avis du Conseil d'Etat et des juridictions administratives, documents de la Cour des comptes mentionnés à l'article L. 140-9 du code des juridictions financières et les documents des chambres régionales des comptes mentionnés à l'article L. 241-6 du même code, documents d'instruction des réclamations adressées au Médiateur de la République, documents préalables à l'élaboration du rapport d'accréditation des établissements de santé prévu à l'article L. 6113-6 du code de la santé publique, rapports d'audit des établissements de santé mentionnés à l'article 40 de la loi de financement de la sécurité sociale pour 2001 (n° 2000-1257 du 23 décembre 2000). (Code du Patrimoine, art. L. 213-2, I, 1, b)
4	AR041	25 ans	Secret statistique. (Code du Patrimoine, art. L. 213-2, I, 1, a)
5	AR042	25 ans	Documents élaborés dans le cadre d'un contrat de prestation de services exécuté pour le compte d'une ou de plusieurs personnes déterminées. (Code du Patrimoine, art. L. 213-2, I, 1, c)
6	AR043	25 ans à compter de la date de décès de l'intéressé	Documents dont la communication est susceptible de porter atteinte au secret médical. (Code du Patrimoine, art. L. 213-2, I, 2)
7	AR044	25 ans à compter de la date de décès de l'intéressé si ce délai est plus court que le délai ordinaire de 75 ans	Etat civil (actes de naissance ou de mariage). (Code du Patrimoine, art. L. 213-2, I, 4, e)
8	AR045	25 ans à compter de la date de décès de l'intéressé si ce délai est plus court que le délai ordinaire de 75 ans	Minutes et répertoires des officiers publics et ministériels. (Code du Patrimoine, art. L. 213-2, I, 4, d)
9	AR046	25 ans à compter de la date de décès de l'intéressé si ce délai est plus court que le délai ordinaire de 75 ans	Documents relatifs aux enquêtes réalisées par les services de la police judiciaire. (Code du Patrimoine, art. L. 213-2, I, 4, b)
10	AR047	25 ans à compter de la date de décès de l'intéressé si ce délai est plus court que le délai ordinaire de 75 ans	Documents relatifs aux affaires portées devant les juridictions, sous réserve des dispositions particulières relatives aux jugements, et à l'exécution des décisions de justice. (Code du Patrimoine, art. L. 213-2, I, 4, c)
11	AR048	50 ans	Documents dont la communication porte atteinte à la protection de la vie privée ou portant appréciation ou jugement de valeur sur une personne physique nommément désignée, ou facilement identifiable, ou qui font apparaître le comportement d'une personne dans des conditions susceptibles de lui porter préjudice. (Code du Patrimoine, art. L. 213-2, I, 3)
12	AR049	50 ans	Documents dont la communication porte atteinte au secret de la défense nationale, aux intérêts fondamentaux de l'État dans la conduite de la politique extérieure, à la sûreté de l'État, à la sécurité publique. (Code du Patrimoine, art. L. 213-2, I, 3)
13	AR050	50 ans	Documents relatifs à la construction, à l'équipement et au fonctionnement des ouvrages, bâtiments ou parties de bâtiments utilisés pour la détention de personnes ou recevant habituellement des personnes détenues. (Code du Patrimoine, art. L. 213-2, I, 3)
14	AR051	50 ans	Documents élaborés dans le cadre d'un contrat de prestation de services dont la communication porte atteinte au secret de la défense nationale, aux intérêts fondamentaux de l'État dans la conduite de la politique extérieure, à la sûreté de l'État, à la sécurité publique, à la protection de la vie privée, ou concernant des bâtiments employés pour la détention de personnes ou recevant habituellement des personnes détenues. (Code du Patrimoine, art. L. 213-2, I, 1, c)
15	AR052	75 ans	Secret statistique : données collectées au moyen de questionnaires ayant trait aux faits et aux comportements d'ordre privé. (Code du Patrimoine, art. L. 213-2, I, 4, a)
16	AR053	75 ans	Documents élaborés dans le cadre d'un contrat de prestation de services et pouvant être liés aux services de la police judiciaire ou aux affaires portées devant les juridictions. (Code du Patrimoine, art. L 213-2, I, 1, b)
17	AR054	75 ans	Etat civil (actes de naissance ou de mariage). (Code du Patrimoine, art. L. 213-2, I, 4, e)
18	AR055	75 ans	Minutes et répertoires des officiers publics et ministériels. (Code du Patrimoine, art. L. 213-2, I, 4, d)
19	AR056	75 ans	Documents relatifs aux enquêtes réalisées par les services de la police judiciaire. (Code du Patrimoine, art. L. 213-2, I, 4, b)
20	AR057	75 ans	Documents relatifs aux affaires portées devant les juridictions. (Code du Patrimoine, art. L. 213-2, I, 4, c)
21	AR058	100 ans	Documents évoquant des personnes mineures : statistiques, enquêtes de la police judiciaire, documents relatifs aux affaires portées devant les juridictions et à l'exécution des décisions de justice. (Code du Patrimoine, art. L. 213-2, I, 5)
22	AR059	100 ans	Secret de la Défense nationale : documents couverts ou ayant été couverts par le secret de la défense nationale dont la communication est de nature à porter atteinte à la sécurité de personnes nommément désignées ou facilement identifiables (agents spéciaux, agents de renseignements...). (Code du Patrimoine, art. L. 213-2, I, 3)
23	AR060	100 ans	Documents dont la communication est de nature à porter atteinte à l'intimité de la vie sexuelle des personnes : enquêtes de la police judiciaire, documents relatifs aux affaires portées devant les juridictions. (Code du Patrimoine, art. L. 213-2, I, 5)
24	AR061	120 ans à compter de la date de naissance si la date de décès de l'intéressé n'est pas connue	Documents dont la communication est susceptible de porter atteinte au secret médical. (Code du Patrimoine, art. L. 213-2, I, 2)
25	AR062	Illimitée	Archives publiques dont la communication est susceptible d'entraîner la diffusion d'informations permettant de concevoir, fabriquer, utiliser ou localiser des armes nucléaires, biologiques, chimiques ou toutes autres armes ayant des effets directs ou indirects de destruction d'un niveau analogue. (Code du Patrimoine, art. L. 213-2, II)
\.


--
-- Name: ref-accessrestrictioncodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-accessrestrictioncodes_id_seq"', 26, false);


--
-- Data for Name: ref-appraisalcodes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-appraisalcodes" (id, code, name) FROM stdin;
1	conserver	Conserver
2	detruire	Détruire
\.


--
-- Name: ref-appraisalcodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-appraisalcodes_id_seq"', 3, false);


--
-- Data for Name: ref-descriptionlevels; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-descriptionlevels" (id, code, name, description) FROM stdin;
1	class	Classe	Cette valeur, issue de la tradition archivistique allemande, ne correspond pas pour l'instant aux pratiques archivistiques françaises
2	collection	Collection	Réunion artificielle de documents en fonction de critères communs liés à leur contenu ou à leur support, sans considération de leur provenance, par opposition au fonds d'archives constitué de façon organique
3	file	Dossier	ensemble de documents regroupés, soit par le producteur pour son usage courant, soit dans le processus du classement d'archives, parce qu'ils concernent un même sujet ou une même affaire; le dossier est ordinairement l'unité de base à l'intérieur d'une série organique
4	fonds	Fonds	ensemble de documents quels que soit leur type et leur support, créé ou reçu de manière organique et utilisé par une personne physique ou morale dans l'exercice de ses activités
5	item	Item	plus petite unité documentaire, par exemple une lettre, un mémoire, un rapport, une photographie, un enregistrement sonore
6	recordgrp	Groupe de documents	niveau de description intermédiaire qui ne correspond pas à une division organique (sous-fonds, série ou sous-série organique); parties au sein d'une collection, versements,  épaves d'un fonds, subdivisions de fonds dont on ne connait pas la nature exacte, sous-ensemble classés thématiquement
7	series	Serie organique	division organique d'un fonds, correspondant à un ensemble de dossiers maintenus groupés parce qu'ils résultent d'une même activité, se rapportent à une même fonction ou à un même sujet ou revêtent une même forme
8	subfonds	Sous fonds	division organique d'un fonds correspondant aux divisions administratives de l'institution ou de l'organisme producteur, ou, à défaut,  à un regroupement géographique, chronologique, fonctionnel ou autre des documents; quand le producteur a une structure hiérarchique complexe , chaque sous-fonds est lui-même subdivisé, autant que nécessaire pour refléter les niveaux hiérarchiques
9	subgrp	Sous-groupe de documents	subdivision du groupe de documents
10	subseries	Sous-série organique	subdivision de la série organique
\.


--
-- Name: ref-descriptionlevels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-descriptionlevels_id_seq"', 11, false);


--
-- Data for Name: ref-documenttypes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-documenttypes" (id, code, name, description) FROM stdin;
1	CDO	Contenu de données	Objet numérique ou physique qui est l'objet principal de\r\n\t\t\t\t\t\t\tla pérennisation. ISO 14721:2003(Space data and information transfer\r\n\t\t\t\t\t\t\tsystems -- Open archival information system --Reference\r\n\t\t\t\t\t\tmodel)
2	RI	Information de représentation	Information qui traduit un contenu de données en des\r\n\t\t\t\t\t\t\tconcepts plus explicites. Par exemple, la définition du code ASCII\r\n\t\t\t\t\t\t\tdécrit comment une séquence de bits (un contenu de données) est\r\n\t\t\t\t\t\t\tconvertie en caractères. L'information de représentation peut être de\r\n\t\t\t\t\t\t\tstructure ou sémantique. ISO 14721:2003 (Space data and information\r\n\t\t\t\t\t\t\ttransfer systems -- Open archival information system -- Reference\r\n\t\t\t\t\t\tmodel
3	RISTR	Information de structure	Information de représentation qui explique la façon dont\r\n\t\t\t\t\t\t\td'autres informations sont organisées. Elle établit par exemple une\r\n\t\t\t\t\t\t\tcorrespondance entre les trains de bits et les types de données courants\r\n\t\t\t\t\t\t\tsur ordinateurs (tels que caractères, nombres, pixels ou agrégats de ces\r\n\t\t\t\t\t\t\ttypes tels que chaînes de caractères et tableaux). ISO 14721:2003 (Space\r\n\t\t\t\t\t\t\tdata and information transfer systems -- Open archival information\r\n\t\t\t\t\t\t\tsystem -- Reference model)
4	RISEM	Information sémantique	Information de représentation qui complète l'information\r\n\t\t\t\t\t\t\tde structure pour donner par exemple la signification particulière\r\n\t\t\t\t\t\t\tassociée à chacun des éléments de la structure, les opérations\r\n\t\t\t\t\t\t\tréalisables sur chaque type de données, leurs corrélations... ISO\r\n\t\t\t\t\t\t\t14721:2003 (Space data and information systemstransfe -- Open archival\r\n\t\t\t\t\t\t\tinformation system -- Reference model)
5	PDI	Information de pérennisation	Information nécessaire à une bonne conservation du contenu\r\n\t\t\t\t\t\t\td'information, et qui peut être décomposée en informations de\r\n\t\t\t\t\t\t\tprovenance, d'identification, d'intégrité et de contexte. ISO 14721:2003\r\n\t\t\t\t\t\t\t(Space data and information transfer systems -- Open archival\r\n\t\t\t\t\t\t\tinformation system -- Reference model)
6	PDIPRO	Information de provenance	Information de pérennisation qui documente l'historique du\r\n\t\t\t\t\t\t\tcontenu d'information. Cette information renseigne sur l'origine ou la\r\n\t\t\t\t\t\t\tsource du contenu d'information, sur toute modification intervenue\r\n\t\t\t\t\t\t\tdepuis sa création et sur ceux qui en ont eu la responsabilité. Exemple\r\n\t\t\t\t\t\t\t: nom du principal responsable de l'enregistrement des données,\r\n\t\t\t\t\t\t\tinformations relatives au stockage, à la manipulation et à la migration\r\n\t\t\t\t\t\t\tdes données. ISO 14721:2003 (Space data and information transfer systems\r\n\t\t\t\t\t\t\t-- Open archival information system -- Reference\r\n\t\t\t\t\t\tmodel)
7	PDIREF	Information d'identification	Information de pérennisation qui identifie, et si\r\n\t\t\t\t\t\t\tnécessaire décrit, le ou les mécanismes d'attribution des\r\n\t\t\t\t\t\t\tidentificateurs au contenu d'information. Elle inclut aussi les\r\n\t\t\t\t\t\t\tidentificateurs qui permettent à un système externe de se référer sans\r\n\t\t\t\t\t\t\téquivoque à un contenu d'information particulier. ISO 14721:2003 (Space\r\n\t\t\t\t\t\t\tdata and information transfer systems -- Open archival information\r\n\t\t\t\t\t\t\tsystem -- Reference model)
8	PDIFIX	Information d'intégrité	Information de pérennisation qui décrit les mécanismes et\r\n\t\t\t\t\t\t\tdes clés d'authentification garantissant que le contenu d'information\r\n\t\t\t\t\t\t\tn'a pas subi de modification sans que celle-ci ait été tracée. Par\r\n\t\t\t\t\t\t\texemple, le code CRC (contrôl de redondance cyclique) pour un fichier.\r\n\t\t\t\t\t\t\tISO 14721:2003 (Space data and information transfer systems -- Open\r\n\t\t\t\t\t\t\tarchival information system -- Reference model)
9	PDICTX	Information de contexte	Information de pérennisation qui décrit les liens entre un\r\n\t\t\t\t\t\t\tcontenu d'information et son environnement. Elle inclut entre autres les\r\n\t\t\t\t\t\t\traisons de la création de ce contenu d'information et son rapport avec\r\n\t\t\t\t\t\t\td'autres Objets-contenu d'information. ISO 14721:2003 (Space data and\r\n\t\t\t\t\t\t\tinformation transfer systems -- Open archival information system --\r\n\t\t\t\t\t\t\tReference model)
\.


--
-- Name: ref-documenttypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-documenttypes_id_seq"', 10, false);


--
-- Data for Name: ref-filetypes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-filetypes" (id, code, name, description) FROM stdin;
1	fmt/1	Broadcast WAVE	Broadcast WAVE (Version=0)
2	fmt/2	Broadcast WAVE	Broadcast WAVE (Version=1)
3	fmt/3	Graphics Interchange Format	Graphics Interchange Format (Version=1987a)
4	fmt/4	Graphics Interchange Format	Graphics Interchange Format (Version=1989a)
5	fmt/5	Audio/Video Interleaved Format	Audio/Video Interleaved Format
6	fmt/6	Waveform Audio	Waveform Audio
7	fmt/7	Tagged Image File Format	Tagged Image File Format (Version=3)
8	fmt/8	Tagged Image File Format	Tagged Image File Format (Version=4)
9	fmt/9	Tagged Image File Format	Tagged Image File Format (Version=5)
10	fmt/10	Tagged Image File Format	Tagged Image File Format (Version=6)
11	fmt/11	Portable Network Graphics	Portable Network Graphics (Version=1.0)
12	fmt/12	Portable Network Graphics	Portable Network Graphics (Version=1.1)
13	fmt/13	Portable Network Graphics	Portable Network Graphics (Version=1.2)
14	fmt/14	Portable Document Format	Portable Document Format (Version=1.0)
15	fmt/15	Portable Document Format	Portable Document Format (Version=1.1)
16	fmt/16	Portable Document Format	Portable Document Format (Version=1.2)
17	fmt/17	Portable Document Format	Portable Document Format (Version=1.3)
18	fmt/18	Portable Document Format	Portable Document Format (Version=1.4)
19	fmt/19	Portable Document Format	Portable Document Format (Version=1.5)
20	fmt/20	Portable Document Format	Portable Document Format (Version=1.6)
21	fmt/21	AutoCAD Drawing	AutoCAD Drawing (Version=1.0)
22	fmt/22	AutoCAD Drawing	AutoCAD Drawing (Version=1.2)
23	fmt/23	AutoCAD Drawing	AutoCAD Drawing (Version=1.3)
24	fmt/24	AutoCAD Drawing	AutoCAD Drawing (Version=1.4)
25	fmt/25	AutoCAD Drawing	AutoCAD Drawing (Version=2.0)
26	fmt/26	AutoCAD Drawing	AutoCAD Drawing (Version=2.1)
27	fmt/27	AutoCAD Drawing	AutoCAD Drawing (Version=2.2)
28	fmt/28	AutoCAD Drawing	AutoCAD Drawing (Version=2.5)
29	fmt/29	AutoCAD Drawing	AutoCAD Drawing (Version=2.6)
30	fmt/30	AutoCAD Drawing	AutoCAD Drawing (Version=R9)
31	fmt/31	AutoCAD Drawing	AutoCAD Drawing (Version=R10)
32	fmt/32	AutoCAD Drawing	AutoCAD Drawing (Version=R11/12)
33	fmt/33	AutoCAD Drawing	AutoCAD Drawing (Version=R13)
34	fmt/34	AutoCAD Drawing	AutoCAD Drawing (Version=R14)
35	fmt/35	AutoCAD Drawing	AutoCAD Drawing (Version=2000-2002)
36	fmt/36	AutoCAD Drawing	AutoCAD Drawing (Version=2004-2005)
37	fmt/37	Microsoft Word for Windows Document	Microsoft Word for Windows Document (Version=1.0)
38	fmt/38	Microsoft Word for Windows Document	Microsoft Word for Windows Document (Version=2.0)
39	fmt/39	Microsoft Word for Windows Document	Microsoft Word for Windows Document (Version=6.0/95)
40	fmt/40	Microsoft Word for Windows Document	Microsoft Word for Windows Document (Version=97-2003)
41	fmt/41	Raw JPEG Stream	Raw JPEG Stream
42	fmt/42	JPEG File Interchange Format	JPEG File Interchange Format (Version=1.00)
43	fmt/43	JPEG File Interchange Format	JPEG File Interchange Format (Version=1.01)
44	fmt/44	JPEG File Interchange Format	JPEG File Interchange Format (Version=1.02)
45	fmt/45	Rich Text Format	Rich Text Format (Version=1.0)
46	fmt/46	Rich Text Format	Rich Text Format (Version=1.1)
47	fmt/47	Rich Text Format	Rich Text Format (Version=1.2)
48	fmt/48	Rich Text Format	Rich Text Format (Version=1.3)
49	fmt/49	Rich Text Format	Rich Text Format (Version=1.4)
50	fmt/50	Rich Text Format	Rich Text Format (Version=1.5)
51	fmt/51	Rich Text Format	Rich Text Format (Version=1.6)
52	fmt/52	Rich Text Format	Rich Text Format (Version=1.7)
53	fmt/53	Rich Text Format	Rich Text Format (Version=1.8)
54	fmt/54	Drawing Interchange Binary Format	Drawing Interchange Binary Format (Version=1.0)
55	fmt/55	Binary Interchange File Format (BIFF) Worksheet	Binary Interchange File Format (BIFF) Worksheet (Version=2)
56	fmt/56	Binary Interchange File Format (BIFF) Worksheet	Binary Interchange File Format (BIFF) Worksheet (Version=3)
57	fmt/57	Binary Interchange File Format (BIFF) Worksheet	Binary Interchange File Format (BIFF) Worksheet (Version=4S)
58	fmt/58	Binary Interchange File Format (BIFF) Workbook	Binary Interchange File Format (BIFF) Workbook (Version=4W)
59	fmt/59	Binary Interchange File Format (BIFF) Workbook	Binary Interchange File Format (BIFF) Workbook (Version=5)
60	fmt/60	Binary Interchange File Format (BIFF) Workbook	Binary Interchange File Format (BIFF) Workbook (Version=7)
61	fmt/61	Binary Interchange File Format (BIFF) Workbook	Binary Interchange File Format (BIFF) Workbook (Version=8)
62	fmt/62	Binary Interchange File Format (BIFF) Workbook	Binary Interchange File Format (BIFF) Workbook (Version=8X)
63	fmt/63	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=Generic)
64	fmt/64	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=1.0)
65	fmt/65	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=1.2)
66	fmt/66	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=1.3)
67	fmt/67	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=1.4)
68	fmt/68	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2.0)
69	fmt/69	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2.1)
70	fmt/70	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2.2)
71	fmt/71	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2.5)
72	fmt/72	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2.6)
73	fmt/73	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=R9)
74	fmt/74	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=R10)
75	fmt/75	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=R11/12)
76	fmt/76	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=R13)
77	fmt/77	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=R14)
78	fmt/78	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2000-2002)
79	fmt/79	Drawing Interchange File Format (ASCII)	Drawing Interchange File Format (ASCII) (Version=2004-2005)
80	fmt/80	Drawing Interchange File Format (Binary)	Drawing Interchange File Format (Binary) (Version=R10)
81	fmt/81	Drawing Interchange File Format (Binary)	Drawing Interchange File Format (Binary) (Version=R11/12)
82	fmt/82	Drawing Interchange File Format (Binary)	Drawing Interchange File Format (Binary) (Version=R13)
83	fmt/83	Drawing Interchange File Format (Binary)	Drawing Interchange File Format (Binary) (Version=R14)
84	fmt/84	Drawing Interchange File Format (Binary)	Drawing Interchange File Format (Binary) (Version=2000-2002)
85	fmt/85	Drawing Interchange File Format (Binary)	Drawing Interchange File Format (Binary) (Version=2004-2005)
86	fmt/86	PCX	PCX (Version=0)
87	fmt/87	PCX	PCX (Version=2)
88	fmt/88	PCX	PCX (Version=3)
89	fmt/89	PCX	PCX (Version=4)
90	fmt/90	PCX	PCX (Version=5)
91	fmt/91	Scalable Vector Graphics	Scalable Vector Graphics (Version=1.0)
92	fmt/92	Scalable Vector Graphics	Scalable Vector Graphics (Version=1.1)
93	fmt/93	Virtual Reality Modeling Language	Virtual Reality Modeling Language (Version=1.0)
94	fmt/94	Virtual Reality Modeling Language	Virtual Reality Modeling Language (Version=97)
95	fmt/95	Portable Document Format - Archival	Portable Document Format - Archival (Version=1)
96	fmt/96	Hypertext Markup Language	Hypertext Markup Language
97	fmt/97	Hypertext Markup Language	Hypertext Markup Language (Version=2.0)
98	fmt/98	Hypertext Markup Language	Hypertext Markup Language (Version=3.2)
99	fmt/99	Hypertext Markup Language	Hypertext Markup Language (Version=4.0)
100	fmt/100	Hypertext Markup Language	Hypertext Markup Language (Version=4.01)
101	fmt/101	Extensible Markup Language	Extensible Markup Language (Version=1.0)
102	fmt/102	Extensible Hypertext Markup Language	Extensible Hypertext Markup Language (Version=1.0)
103	fmt/103	Extensible Hypertext Markup Language	Extensible Hypertext Markup Language (Version=1.1)
104	fmt/104	Macromedia Flash	Macromedia Flash (Version=1)
105	fmt/105	Macromedia Flash	Macromedia Flash (Version=2)
106	fmt/106	Macromedia Flash	Macromedia Flash (Version=3)
107	fmt/107	Macromedia Flash	Macromedia Flash (Version=4)
108	fmt/108	Macromedia Flash	Macromedia Flash (Version=5)
109	fmt/109	Macromedia Flash	Macromedia Flash (Version=6)
110	fmt/110	Macromedia Flash	Macromedia Flash (Version=7)
111	fmt/111	OLE2 Compound Document Format	OLE2 Compound Document Format
112	fmt/112	Still Picture Interchange File Format	Still Picture Interchange File Format (Version=1.0)
113	fmt/113	Still Picture Interchange File Format	Still Picture Interchange File Format (Version=2.0)
114	fmt/114	Windows Bitmap	Windows Bitmap (Version=1.0)
115	fmt/115	Windows Bitmap	Windows Bitmap (Version=2.0)
116	fmt/116	Windows Bitmap	Windows Bitmap (Version=3.0)
117	fmt/117	Windows Bitmap	Windows Bitmap (Version=3.0 NT)
118	fmt/118	Windows Bitmap	Windows Bitmap (Version=4.0)
119	fmt/119	Windows Bitmap	Windows Bitmap (Version=5.0)
120	fmt/120	DROID File Collection File Format	DROID File Collection File Format (Version=1.0)
121	fmt/121	DROID Signature File Format	DROID Signature File Format (Version=1.0)
122	fmt/122	Encapsulated PostScript File Format	Encapsulated PostScript File Format (Version=1.2)
123	fmt/123	Encapsulated PostScript File Format	Encapsulated PostScript File Format (Version=2.0)
124	fmt/124	Encapsulated PostScript File Format	Encapsulated PostScript File Format (Version=3.0)
125	fmt/125	Microsoft Powerpoint Presentation	Microsoft Powerpoint Presentation (Version=95)
126	fmt/126	Microsoft Powerpoint Presentation	Microsoft Powerpoint Presentation (Version=97-2002)
127	fmt/127	OpenOffice Draw	OpenOffice Draw (Version=1.0)
128	fmt/128	OpenOffice Writer	OpenOffice Writer (Version=1.0)
129	fmt/129	OpenOffice Calc	OpenOffice Calc (Version=1.0)
130	fmt/130	OpenOffice Impress	OpenOffice Impress (Version=1.0)
131	fmt/131	Advanced Systems Format	Advanced Systems Format
132	fmt/132	Windows Media Audio	Windows Media Audio
133	fmt/133	Windows Media Video	Windows Media Video
134	fmt/134	MPEG 1/2 Audio Layer 3	MPEG 1/2 Audio Layer 3
135	fmt/135	OpenDocument Format	OpenDocument Format (Version=1.0)
136	fmt/136	OpenDocument Text Format	OpenDocument Text Format (Version=1.0)
137	fmt/137	OpenDocument Spreadsheet Format	OpenDocument Spreadsheet Format (Version=1.0)
138	fmt/138	OpenDocument Presentation Format	OpenDocument Presentation Format (Version=1.0)
139	fmt/139	OpenDocument Drawing Format	OpenDocument Drawing Format (Version=1.0)
140	fmt/140	OpenDocument Database Format	OpenDocument Database Format (Version=1.0)
141	fmt/141	Waveform Audio (PCMWAVEFORMAT)	Waveform Audio (PCMWAVEFORMAT)
142	fmt/142	Waveform Audio (WAVEFORMATEX)	Waveform Audio (WAVEFORMATEX)
143	fmt/143	Waveform Audio (WAVEFORMATEXTENSIBLE)	Waveform Audio (WAVEFORMATEXTENSIBLE)
144	fmt/144	Portable Document Format - Exchange 1:1999	Portable Document Format - Exchange 1:1999
145	fmt/145	Portable Document Format - Exchange 1:2001	Portable Document Format - Exchange 1:2001
146	fmt/146	Portable Document Format - Exchange 1a:2003	Portable Document Format - Exchange 1a:2003
147	fmt/147	Portable Document Format - Exchange 2:2003	Portable Document Format - Exchange 2:2003
148	fmt/148	Portable Document Format - Exchange 3:2003	Portable Document Format - Exchange 3:2003
149	fmt/149	JTIP (JPEG Tiled Image Pyramid)	JTIP (JPEG Tiled Image Pyramid)
150	fmt/150	JPEG-LS	JPEG-LS
151	fmt/151	JPX (JPEG 2000 Extended)	JPX (JPEG 2000 Extended)
152	fmt/152	Digital Negative Format (DNG)	Digital Negative Format (DNG) (Version=1.1)
153	fmt/153	Tagged Image File Format for Image Technology (TIFF/IT)	Tagged Image File Format for Image Technology (TIFF/IT)
154	fmt/154	Tagged Image File Format for Electronic Still Picture Imaging (TIFF/EP)	Tagged Image File Format for Electronic Still Picture Imaging (TIFF/EP)
155	fmt/155	GeoTIFF	GeoTIFF
156	fmt/156	Tagged Image File Format for Internet Fax (TIFF-FX)	Tagged Image File Format for Internet Fax (TIFF-FX)
157	fmt/157	Portable Document Format - Exchange 1a:2001	Portable Document Format - Exchange 1a:2001
158	fmt/158	Portable Document Format - Exchange 3:2002	Portable Document Format - Exchange 3:2002
\.


--
-- Name: ref-filetypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-filetypes_id_seq"', 159, false);


--
-- Data for Name: ref-keywordtypes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-keywordtypes" (id, code, documentation) FROM stdin;
1	corpname	Collectivité
2	famname	Nom de famille
3	geogname	Nom géographique
4	name	Nom
5	occupation	Fonction
6	persname	Nom de personne
7	subject	Mot-matière
8	genreform	Type de document
9	function	Activité
\.


--
-- Name: ref-keywordtypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-keywordtypes_id_seq"', 10, false);


--
-- Data for Name: ref-languagecodes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-languagecodes" (id, code, documentation) FROM stdin;
1	aa	afar
2	ab	abkhaze
3	af	afrikaans
4	ak	akan
5	sq	albanais
6	am	amharique
7	ar	arabe
8	an	aragonais
9	hy	arménien
10	as	assamais
11	av	avar
12	ae	avestique
13	ay	aymara
14	az	azéri
15	ba	bachkir
16	bm	bambara
17	eu	basque
18	be	biélorusse
19	bn	bengali
20	bh	langues biharis
21	bi	bichlamar
22	bo	tibétain
23	bs	bosniaque
24	br	breton
25	bg	bulgare
26	my	birman
27	ca	catalan; valencien
28	cs	tchèque
29	ch	chamorro
30	ce	tchétchène
31	zh	chinois
32	cu	slavon d'église; vieux slave; slavon liturgique; vieux bulgare
33	cv	tchouvache
34	kw	cornique
35	co	corse
36	cr	cree
37	cy	gallois
38	da	danois
39	de	allemand
40	dv	maldivien
41	nl	néerlandais; flamand
42	dz	dzongkha
43	el	grec moderne (après 1453)
44	en	anglais
45	eo	espéranto
46	et	estonien
47	ee	éwé
48	fo	féroïen
49	fa	persan
50	fj	fidjien
51	fi	finnois
52	fr	français
53	fy	frison occidental
54	ff	peul
55	ka	géorgien
56	gd	gaélique; gaélique écossais
57	ga	irlandais
58	gl	galicien
59	gv	manx; mannois
60	gn	guarani
61	gu	goudjrati
62	ht	haïtien; créole haïtien
63	ha	haoussa
64	he	hébreu
65	hz	herero
66	hi	hindi
67	ho	hiri motu
68	hr	croate
69	hu	hongrois
70	ig	igbo
71	is	islandais
72	io	ido
73	ii	yi de Sichuan
74	iu	inuktitut
75	ie	interlingue
76	ia	interlingua (langue auxiliaire internationale)
77	id	indonésien
78	ik	inupiaq
79	it	italien
80	jv	javanais
81	ja	japonais
82	kl	groenlandais
83	kn	kannada
84	ks	kashmiri
85	kr	kanouri
86	kk	kazakh
87	km	khmer central
88	ki	kikuyu
89	rw	rwanda
90	ky	kirghiz
91	kv	kom
92	kg	kongo
93	ko	coréen
94	kj	kuanyama; kwanyama
95	ku	kurde
96	lo	lao
97	la	latin
98	lv	letton
99	li	limbourgeois
100	ln	lingala
101	lt	lituanien
102	lb	luxembourgeois
103	lu	luba-katanga
104	lg	ganda
105	mk	macédonien
106	mh	marshall
107	ml	malayalam
108	mi	maori
109	mr	marathe
110	ms	malais
111	mg	malgache
112	mt	maltais
113	mn	mongol
114	na	nauruan
115	nv	navaho
116	nr	ndébélé du Sud
117	nd	ndébélé du Nord
118	ng	ndonga
119	ne	népalais
120	nn	norvégien nynorsk; nynorsk, norvégien
121	nb	norvégien bokmål
122	no	norvégien
123	ny	chichewa; chewa; nyanja
124	oc	occitan (après 1500)
125	oj	ojibwa
126	or	oriya
127	om	galla
128	os	ossète
129	pa	pendjabi
130	pi	pali
131	pl	polonais
132	pt	portugais
133	ps	pachto
134	qu	quechua
135	rm	romanche
136	ro	roumain; moldave
137	rn	rundi
138	ru	russe
139	sg	sango
140	sa	sanskrit
141	si	singhalais
142	sk	slovaque
143	sl	slovène
144	se	sami du Nord
145	sm	samoan
146	sn	shona
147	sd	sindhi
148	so	somali
149	st	sotho du Sud
150	es	espagnol; castillan
151	sc	sarde
152	sr	serbe
153	ss	swati
154	su	soundanais
155	sw	swahili
156	sv	suédois
157	ty	tahitien
158	ta	tamoul
159	tt	tatar
160	te	télougou
161	tg	tadjik
162	tl	tagalog
163	th	thaï
164	ti	tigrigna
165	to	tongan (Îles Tonga)
166	tn	tswana
167	ts	tsonga
168	tk	turkmène
169	tr	turc
170	tw	twi
171	ug	ouïgour
172	uk	ukrainien
173	ur	ourdou
174	uz	ouszbek
175	ve	venda
176	vi	vietnamien
177	vo	volapük
178	wa	wallon
179	wo	wolof
180	xh	xhosa
181	yi	yiddish
182	yo	yoruba
183	za	zhuang; chuang
184	zu	zoulou
\.


--
-- Name: ref-languagecodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-languagecodes_id_seq"', 185, false);


--
-- Data for Name: ref-mimecodes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-mimecodes" (id, code, name, definition) FROM stdin;
1	application/activemessage	activemessage	Shapiro
2	application/andrew-inset	andrew-inset	Borenstein
3	application/applefile	applefile	Faltstrom
4	application/atom+xml	atom+xml	RFC4287,RFC5023
5	application/atomicmail	atomicmail	Borenstein
6	application/atomcat+xml	atomcat+xml	RFC5023
7	application/atomsvc+xml	atomsvc+xml	RFC5023
8	application/auth-policy+xml	auth-policy+xml	RFC4745
9	application/batch-SMTP	batch-SMTP	RFC2442
10	application/beep+xml	beep+xml	RFC3080
11	application/cals-1840	cals-1840	RFC1895
12	application/ccxml+xml	ccxml+xml	RFC4267
13	application/cellml+xml	cellml+xml	RFC4708
14	application/cnrp+xml	cnrp+xml	RFC3367
15	application/commonground	commonground	Glazer
16	application/conference-info+xml	conference-info+xml	RFC4575
17	application/cpl+xml	cpl+xml	RFC3880
18	application/csta+xml	csta+xml	Ecma International Helpdesk
19	application/CSTAdata+xml	CSTAdata+xml	Ecma International Helpdesk
20	application/cybercash	cybercash	Eastlake
21	application/davmount+xml	davmount+xml	RFC4709
22	application/dca-rft	dca-rft	Campbell
23	application/dec-dx	dec-dx	Campbell
24	application/dialog-info+xml	dialog-info+xml	RFC4235
25	application/dicom	dicom	RFC3240
26	application/dns	dns	RFC4027
27	application/dvcs	dvcs	RFC3029
28	application/ecmascript	ecmascript	RFC4329
29	application/EDI-Consent	EDI-Consent	RFC1767
30	application/EDIFACT	EDIFACT	RFC1767
31	application/EDI-X12	EDI-X12	RFC1767
32	application/epp+xml	epp+xml	RFC3730
33	application/eshop	eshop	Katz
34	application/example	example	RFC4735
35	application/fastinfoset	fastinfoset	ITU-T ASN.1 Rapporteur
36	application/fastsoap	fastsoap	ITU-T ASN.1 Rapporteur
37	application/fits	fits	RFC4047
38	application/font-tdpfr	font-tdpfr	RFC3073
39	application/H224	H224	RFC4573
40	application/http	http	RFC2616
41	application/hyperstudio	hyperstudio	Domino
42	application/iges	iges	Parks
43	application/im-iscomposing+xml	im-iscomposing+xml	RFC3994
44	application/index	index	RFC2652
45	application/index.cmd	index.cmd	RFC2652
46	application/index.obj	index.obj	RFC2652
47	application/index.response	index.response	RFC2652
48	application/index.vnd	index.vnd	RFC2652
49	application/iotp	iotp	RFC2935
50	application/ipp	ipp	RFC2910
51	application/isup	isup	RFC3204
52	application/javascript	javascript	RFC4329
53	application/json	json	RFC4627
54	application/kpml-request+xml	kpml-request+xml	RFC4730
55	application/kpml-response+xml	kpml-response+xml	RFC4730
56	application/mac-binhex40	mac-binhex40	Faltstrom
57	application/macwriteii	macwriteii	Lindner
58	application/marc	marc	RFC2220
59	application/mathematica	mathematica	Wolfram
60	application/mbms-associated-procedure-description+xml	mbms-associated-procedure-description+xml	3GPP
61	application/mbms-deregister+xml	mbms-deregister+xml	3GPP
62	application/mbms-envelope+xml	mbms-envelope+xml	3GPP
63	application/mbms-msk-response+xml	mbms-msk-response+xml	3GPP
64	application/mbms-msk+xml	mbms-msk+xml	3GPP
65	application/mbms-protection-description+xml	mbms-protection-description+xml	3GPP
66	application/mbms-reception-report+xml	mbms-reception-report+xml	3GPP
67	application/mbms-register-response+xml	mbms-register-response+xml	3GPP
68	application/mbms-register+xml	mbms-register+xml	3GPP
69	application/mbms-user-service-description+xml	mbms-user-service-description+xml	3GPP
70	application/mbox	mbox	RFC4155
71	application/media_control+xml	media_control+xml	RFC5168
72	application/mediaservercontrol+xml	mediaservercontrol+xml	RFC5022
73	application/mikey	mikey	RFC3830
74	application/moss-keys	moss-keys	RFC1848
75	application/moss-signature	moss-signature	RFC1848
76	application/mosskey-data	mosskey-data	RFC1848
77	application/mosskey-request	mosskey-request	RFC1848
78	application/mpeg4-generic	mpeg4-generic	RFC3640
79	application/mpeg4-iod	mpeg4-iod	RFC4337
80	application/mpeg4-iod-xmt	mpeg4-iod-xmt	RFC4337
81	application/mp4	mp4	RFC4337
82	application/msword	msword	Lindner
83	application/mxf	mxf	RFC4539
84	application/nasdata	nasdata	RFC4707
85	application/news-message-id	news-message-id	RFC1036Spencer
86	application/news-transmission	news-transmission	RFC1036 Spencer
87	application/nss	nss	Hammer
88	application/ocsp-request	ocsp-request	RFC2560
89	application/ocsp-response	ocsp-response	RFC2560
90	application/octet-stream	octet-stream	RFC2045,RFC2046
91	application/oda	oda	RFC2045,RFC2046
92	application/oebps-package+xml	oebps-package+xml	RFC4839
93	application/ogg	ogg	RFC3534
94	application/parityfec	parityfec	RFC5109
95	application/pdf	pdf	RFC3778
96	application/pgp-encrypted	pgp-encrypted	RFC3156
97	application/pgp-keys	pgp-keys	RFC3156
98	application/pgp-signature	pgp-signature	RFC3156
99	application/pidf+xml	pidf+xml	RFC3863
100	application/pkcs10	pkcs10	RFC2311
101	application/pkcs7-mime	pkcs7-mime	RFC2311
102	application/pkcs7-signature	pkcs7-signature	RFC2311
103	application/pkix-cert	pkix-cert	RFC2585
104	application/pkixcmp	pkixcmp	RFC2510
105	application/pkix-crl	pkix-crl	RFC2585
106	application/pkix-pkipath	pkix-pkipath	RFC4366
107	application/pls+xml	pls+xml	RFC4267
108	application/poc-settings+xml	poc-settings+xml	RFC4354
109	application/postscript	postscript	RFC2045,RFC2046
110	application/prs.alvestrand.titrax-sheet	prs.alvestrand.titrax-sheet	Alvestrand
111	application/prs.cww	prs.cww	Rungchavalnont
112	application/prs.nprend	prs.nprend	Doggett
113	application/prs.plucker	prs.plucker	Janssen
114	application/rdf+xml	rdf+xml	RFC3870
115	application/qsig	qsig	RFC3204
116	application/reginfo+xml	reginfo+xml	RFC3680
117	application/relax-ng-compact-syntax	relax-ng-compact-syntax	ISO/IEC19757-2:2003/FDAM-1
118	application/remote-printing	remote-printing	RFC1486Rose
119	application/resource-lists+xml	resource-lists+xml	RFC4826
120	application/riscos	riscos	Smith
121	application/rlmi+xml	rlmi+xml	RFC4662
122	application/rls-services+xml	rls-services+xml	RFC4826
123	application/rtf	rtf	Lindner
124	application/rtx	rtx	RFC4588
125	application/samlassertion+xml	samlassertion+xml	OASIS Security Services Technical Committee (SSTC)
126	application/samlmetadata+xml	samlmetadata+xml	OASIS Security Services Technical Committee (SSTC)
127	application/sbml+xml	sbml+xml	RFC3823
128	application/scvp-cv-request	scvp-cv-request	RFC5055
129	application/scvp-cv-response	scvp-cv-response	RFC5055
130	application/scvp-vp-request	scvp-vp-request	RFC5055
131	application/scvp-vp-response	scvp-vp-response	RFC5055
132	application/sdp	sdp	RFC4566
133	application/set-payment	set-payment	Korver
134	application/set-payment-initiation	set-payment-initiation	Korver
135	application/set-registration	set-registration	Korver
136	application/set-registration-initiation	set-registration-initiation	Korver
137	application/sgml	sgml	RFC1874
138	application/sgml-open-catalog	sgml-open-catalog	Grosso
139	application/shf+xml	shf+xml	RFC4194
140	application/sieve	sieve	RFC5228
141	application/simple-filter+xml	simple-filter+xml	RFC4661
142	application/simple-message-summary	simple-message-summary	RFC3842
143	application/simpleSymbolContainer	simpleSymbolContainer	3GPP
144	application/slate	slate	Crowley
145	application/smil (OBSOLETE)	smil (OBSOLETE)	RFC4536
146	application/smil+xml	smil+xml	RFC4536
147	application/soap+fastinfoset	soap+fastinfoset	ITU-T ASN.1 Rapporteur
148	application/soap+xml	soap+xml	RFC3902
149	application/sparql-query	sparql-query	W3C
150	application/sparql-results+xml	sparql-results+xml	W3C
151	application/spirits-event+xml	spirits-event+xml	RFC3910
152	application/srgs	srgs	RFC4267
153	application/srgs+xml	srgs+xml	RFC4267
154	application/ssml+xml	ssml+xml	RFC4267
155	application/timestamp-query	timestamp-query	RFC3161
156	application/timestamp-reply	timestamp-reply	RFC3161
157	application/tve-trigger	tve-trigger	Welsh
158	application/ulpfec	ulpfec	RFC5109
159	application/vemmi	vemmi	RFC2122
160	application/vnd.3gpp.bsf+xml	vnd.3gpp.bsf+xml	Meredith
161	application/vnd.3gpp.pic-bw-large	vnd.3gpp.pic-bw-large	Meredith
162	application/vnd.3gpp.pic-bw-small	vnd.3gpp.pic-bw-small	Meredith
163	application/vnd.3gpp.pic-bw-var	vnd.3gpp.pic-bw-var	Meredith
164	application/vnd.3gpp.sms	vnd.3gpp.sms	Meredith
165	application/vnd.3gpp2.bcmcsinfo+xml	vnd.3gpp2.bcmcsinfo+xml	Dryden
166	application/vnd.3gpp2.sms	vnd.3gpp2.sms	Mahendran
167	application/vnd.3gpp2.tcap	vnd.3gpp2.tcap	Mahendran
168	application/vnd.3M.Post-it-Notes	vnd.3M.Post-it-Notes	O'Brien
169	application/vnd.accpac.simply.aso	vnd.accpac.simply.aso	Leow
170	application/vnd.accpac.simply.imp	vnd.accpac.simply.imp	Leow
171	application/vnd.acucobol	vnd.acucobol	Lubin
172	application/vnd.acucorp	vnd.acucorp	Lubin
173	application/vnd.adobe.xdp+xml	vnd.adobe.xdp+xml	Brinkman
174	application/vnd.adobe.xfdf	vnd.adobe.xfdf	Perelman
175	application/vnd.aether.imp	vnd.aether.imp	Moskowitz
176	application/vnd.americandynamics.acc	vnd.americandynamics.acc	Sands
177	application/vnd.amiga.ami	vnd.amiga.ami	Blumberg
178	application/vnd.anser-web-certificate-issue-initiation	vnd.anser-web-certificate-issue-initiation	Mori
179	application/vnd.antix.game-component	vnd.antix.game-component	Shelton
180	application/vnd.apple.installer+xml	vnd.apple.installer+xml	Bierman
181	application/vnd.audiograph	vnd.audiograph	Slusanschi
182	application/vnd.autopackage	vnd.autopackage	Hearn
183	application/vnd.avistar+xml	vnd.avistar+xml	Vysotsky
184	application/vnd.blueice.multipass	vnd.blueice.multipass	Holmstrom
185	application/vnd.bmi	vnd.bmi	Gotoh
186	application/vnd.businessobjects	vnd.businessobjects	Imoucha
187	application/vnd.cab-jscript	vnd.cab-jscript	Falkenberg
188	application/vnd.canon-cpdl	vnd.canon-cpdl	Muto
189	application/vnd.canon-lips	vnd.canon-lips	Muto
190	application/vnd.cendio.thinlinc.clientconf	vnd.cendio.thinlinc.clientconf	Åstrand
191	application/vnd.chemdraw+xml	vnd.chemdraw+xml	Howes
192	application/vnd.chipnuts.karaoke-mmd	vnd.chipnuts.karaoke-mmd	Xiong
193	application/vnd.cinderella	vnd.cinderella	Kortenkamp
194	application/vnd.cirpack.isdn-ext	vnd.cirpack.isdn-ext	Mayeux
195	application/vnd.claymore	vnd.claymore	Simpson
196	application/vnd.clonk.c4group	vnd.clonk.c4group	Brammer
197	application/vnd.commerce-battelle	vnd.commerce-battelle	Applebaum
198	application/vnd.commonspace	vnd.commonspace	Chandhok
199	application/vnd.cosmocaller	vnd.cosmocaller	Dellutri
200	application/vnd.contact.cmsg	vnd.contact.cmsg	Patz
201	application/vnd.crick.clicker	vnd.crick.clicker	Burt
202	application/vnd.crick.clicker.keyboard	vnd.crick.clicker.keyboard	Burt
203	application/vnd.crick.clicker.palette	vnd.crick.clicker.palette	Burt
204	application/vnd.crick.clicker.template	vnd.crick.clicker.template	Burt
205	application/vnd.crick.clicker.wordbank	vnd.crick.clicker.wordbank	Burt
206	application/vnd.criticaltools.wbs+xml	vnd.criticaltools.wbs+xml	Spiller
207	application/vnd.ctc-posml	vnd.ctc-posml	Kohlhepp
208	application/vnd.ctct.ws+xml	vnd.ctct.ws+xml	Ancona
209	application/vnd.cups-pdf	vnd.cups-pdf	Sweet
210	application/vnd.cups-postscript	vnd.cups-postscript	Sweet
211	application/vnd.cups-ppd	vnd.cups-ppd	Sweet
212	application/vnd.cups-raster	vnd.cups-raster	Sweet
213	application/vnd.cups-raw	vnd.cups-raw	Sweet
214	application/vnd.curl	vnd.curl	Byrnes
215	application/vnd.cybank	vnd.cybank	Helmee
216	application/vnd.data-vision.rdz	vnd.data-vision.rdz	Fields
217	application/vnd.denovo.fcselayout-link	vnd.denovo.fcselayout-link	Dixon
218	application/vnd.dna	vnd.dna	Searcy
219	application/vnd.dpgraph	vnd.dpgraph	Parker
220	application/vnd.dreamfactory	vnd.dreamfactory	Appleton
221	application/vnd.dvb.esgcontainer	vnd.dvb.esgcontainer	Heuer
222	application/vnd.dvb.ipdcesgaccess	vnd.dvb.ipdcesgaccess	Heuer
223	application/vnd.dxr	vnd.dxr	Duffy
224	application/vnd.ecdis-update	vnd.ecdis-update	Buettgenbach
225	application/vnd.ecowin.chart	vnd.ecowin.chart	Olsson
226	application/vnd.ecowin.filerequest	vnd.ecowin.filerequest	Olsson
227	application/vnd.ecowin.fileupdate	vnd.ecowin.fileupdate	Olsson
228	application/vnd.ecowin.series	vnd.ecowin.series	Olsson
229	application/vnd.ecowin.seriesrequest	vnd.ecowin.seriesrequest	Olsson
230	application/vnd.ecowin.seriesupdate	vnd.ecowin.seriesupdate	Olsson
231	application/vnd.enliven	vnd.enliven	Santinelli
232	application/vnd.epson.esf	vnd.epson.esf	Hoshina
233	application/vnd.epson.msf	vnd.epson.msf	Hoshina
234	application/vnd.epson.quickanime	vnd.epson.quickanime	Gu
235	application/vnd.epson.salt	vnd.epson.salt	Nagatomo
236	application/vnd.epson.ssf	vnd.epson.ssf	Hoshina
237	application/vnd.ericsson.quickcall	vnd.ericsson.quickcall	Tidwell
238	application/vnd.eszigno3+xml	vnd.eszigno3+xml	Tóth
239	application/vnd.eudora.data	vnd.eudora.data	Resnick
240	application/vnd.ezpix-album	vnd.ezpix-album	Electronic Zombie, Corp.
241	application/vnd.ezpix-package	vnd.ezpix-package	Electronic Zombie, Corp.
242	application/vnd.fdf	vnd.fdf	Zilles
243	application/vnd.ffsns	vnd.ffsns	Holstage
244	application/vnd.fints	vnd.fints	Hammann
245	application/vnd.FloGraphIt	vnd.FloGraphIt	Floersch
246	application/vnd.fluxtime.clip	vnd.fluxtime.clip	Winter
247	application/vnd.framemaker	vnd.framemaker	Wexler
248	application/vnd.frogans.fnc	vnd.frogans.fnc	Tamas
249	application/vnd.frogans.ltf	vnd.frogans.ltf	Tamas
250	application/vnd.fsc.weblaunch	vnd.fsc.weblaunch	D.Smith
251	application/vnd.fujitsu.oasys	vnd.fujitsu.oasys	Togashi
252	application/vnd.fujitsu.oasys2	vnd.fujitsu.oasys2	Togashi
253	application/vnd.fujitsu.oasys3	vnd.fujitsu.oasys3	Okudaira
254	application/vnd.fujitsu.oasysgp	vnd.fujitsu.oasysgp	Sugimoto
255	application/vnd.fujitsu.oasysprs	vnd.fujitsu.oasysprs	Ogita
256	application/vnd.fujixerox.ART4	vnd.fujixerox.ART4	Tanabe
257	application/vnd.fujixerox.ART-EX	vnd.fujixerox.ART-EX	Tanabe
258	application/vnd.fujixerox.ddd	vnd.fujixerox.ddd	Onda
259	application/vnd.fujixerox.docuworks	vnd.fujixerox.docuworks	Taguchi
260	application/vnd.fujixerox.docuworks.binder	vnd.fujixerox.docuworks.binder	Matsumoto
261	application/vnd.fujixerox.HBPL	vnd.fujixerox.HBPL	Tanabe
262	application/vnd.fut-misnet	vnd.fut-misnet	Pruulmann
263	application/vnd.fuzzysheet	vnd.fuzzysheet	Birtwistle
264	application/vnd.genomatix.tuxedo	vnd.genomatix.tuxedo	Frey
265	application/vnd.google-earth.kml+xml	vnd.google-earth.kml+xml	Ashbridge
266	application/vnd.google-earth.kmz	vnd.google-earth.kmz	Ashbridge
267	application/vnd.grafeq	vnd.grafeq	Tupper
268	application/vnd.gridmp	vnd.gridmp	Lawson
269	application/vnd.groove-account	vnd.groove-account	Joseph
270	application/vnd.groove-help	vnd.groove-help	Joseph
271	application/vnd.groove-identity-message	vnd.groove-identity-message	Joseph
272	application/vnd.groove-injector	vnd.groove-injector	Joseph
273	application/vnd.groove-tool-message	vnd.groove-tool-message	Joseph
274	application/vnd.groove-tool-template	vnd.groove-tool-template	Joseph
275	application/vnd.groove-vcard	vnd.groove-vcard	Joseph
276	application/vnd.HandHeld-Entertainment+xml	vnd.HandHeld-Entertainment+xml	Hamilton
277	application/vnd.hbci	vnd.hbci	Hammann
278	application/vnd.hcl-bireports	vnd.hcl-bireports	Serres
279	application/vnd.hhe.lesson-player	vnd.hhe.lesson-player	Jones
280	application/vnd.hp-HPGL	vnd.hp-HPGL	Pentecost
281	application/vnd.hp-hpid	vnd.hp-hpid	Gupta
282	application/vnd.hp-hps	vnd.hp-hps	Aubrey
283	application/vnd.hp-jlyt	vnd.hp-jlyt	Gaash
284	application/vnd.hp-PCL	vnd.hp-PCL	Pentecost
285	application/vnd.hp-PCLXL	vnd.hp-PCLXL	Pentecost
286	application/vnd.httphone	vnd.httphone	Lefevre
287	application/vnd.hzn-3d-crossword	vnd.hzn-3d-crossword	Minnis
288	application/vnd.ibm.afplinedata	vnd.ibm.afplinedata	Buis
289	application/vnd.ibm.electronic-media	vnd.ibm.electronic-media	Tantlinger
290	application/vnd.ibm.MiniPay	vnd.ibm.MiniPay	Herzberg
291	application/vnd.ibm.modcap	vnd.ibm.modcap	Hohensee
292	application/vnd.ibm.rights-management	vnd.ibm.rights-management	Tantlinger
293	application/vnd.ibm.secure-container	vnd.ibm.secure-container	Tantlinger
294	application/vnd.iccprofile	vnd.iccprofile	Green
295	application/vnd.igloader	vnd.igloader	Fisher
296	application/vnd.immervision-ivp	vnd.immervision-ivp	Villegas
297	application/vnd.immervision-ivu	vnd.immervision-ivu	Villegas
298	application/vnd.informedcontrol.rms+xml	vnd.informedcontrol.rms+xml	Wahl
299	application/vnd.informix-visionary	vnd.informix-visionary	Gales
300	application/vnd.intercon.formnet	vnd.intercon.formnet	Gurak
301	application/vnd.intertrust.digibox	vnd.intertrust.digibox	Tomasello
302	application/vnd.intertrust.nncp	vnd.intertrust.nncp	Tomasello
303	application/vnd.intu.qbo	vnd.intu.qbo	Scratchley
304	application/vnd.intu.qfx	vnd.intu.qfx	Scratchley
305	application/vnd.ipunplugged.rcprofile	vnd.ipunplugged.rcprofile	Ersson
306	application/vnd.irepository.package+xml	vnd.irepository.package+xml	Knowles
307	application/vnd.is-xpr	vnd.is-xpr	Natarajan
308	application/vnd.jam	vnd.jam	B.Kumar
309	application/vnd.japannet-directory-service	vnd.japannet-directory-service	Fujii
310	application/vnd.japannet-jpnstore-wakeup	vnd.japannet-jpnstore-wakeup	Yoshitake
311	application/vnd.japannet-payment-wakeup	vnd.japannet-payment-wakeup	Fujii
312	application/vnd.japannet-registration	vnd.japannet-registration	Yoshitake
313	application/vnd.japannet-registration-wakeup	vnd.japannet-registration-wakeup	Fujii
743	image/jpeg	jpeg	RFC2045,RFC2046
314	application/vnd.japannet-setstore-wakeup	vnd.japannet-setstore-wakeup	Yoshitake
315	application/vnd.japannet-verification	vnd.japannet-verification	Yoshitake
316	application/vnd.japannet-verification-wakeup	vnd.japannet-verification-wakeup	Fujii
317	application/vnd.jcp.javame.midlet-rms	vnd.jcp.javame.midlet-rms	Gorshenev
318	application/vnd.jisp	vnd.jisp	Deckers
319	application/vnd.joost.joda-archive	vnd.joost.joda-archive	Joost
320	application/vnd.kahootz	vnd.kahootz	Macdonald
321	application/vnd.kde.karbon	vnd.kde.karbon	Faure
322	application/vnd.kde.kchart	vnd.kde.kchart	Faure
323	application/vnd.kde.kformula	vnd.kde.kformula	Faure
324	application/vnd.kde.kivio	vnd.kde.kivio	Faure
325	application/vnd.kde.kontour	vnd.kde.kontour	Faure
326	application/vnd.kde.kpresenter	vnd.kde.kpresenter	Faure
327	application/vnd.kde.kspread	vnd.kde.kspread	Faure
328	application/vnd.kde.kword	vnd.kde.kword	Faure
329	application/vnd.kenameaapp	vnd.kenameaapp	DiGiorgio-Haag
330	application/vnd.kidspiration	vnd.kidspiration	Bennett
331	application/vnd.Kinar	vnd.Kinar	Thakkar
332	application/vnd.koan	vnd.koan	Cole
333	application/vnd.kodak-descriptor	vnd.kodak-descriptor	Donahue
334	application/vnd.liberty-request+xml	vnd.liberty-request+xml	McDowell
335	application/vnd.llamagraphics.life-balance.desktop	vnd.llamagraphics.life-balance.desktop	White
336	application/vnd.llamagraphics.life-balance.exchange+xml	vnd.llamagraphics.life-balance.exchange+xml	White
337	application/vnd.lotus-1-2-3	vnd.lotus-1-2-3	Wattenberger
338	application/vnd.lotus-approach	vnd.lotus-approach	Wattenberger
339	application/vnd.lotus-freelance	vnd.lotus-freelance	Wattenberger
340	application/vnd.lotus-notes	vnd.lotus-notes	Laramie
341	application/vnd.lotus-organizer	vnd.lotus-organizer	Wattenberger
342	application/vnd.lotus-screencam	vnd.lotus-screencam	Wattenberger
343	application/vnd.lotus-wordpro	vnd.lotus-wordpro	Wattenberger
344	application/vnd.macports.portpkg	vnd.macports.portpkg	Berry
345	application/vnd.marlin.drm.actiontoken+xml	vnd.marlin.drm.actiontoken+xml	Ellison
346	application/vnd.marlin.drm.conftoken+xml	vnd.marlin.drm.conftoken+xml	Ellison
347	application/vnd.marlin.drm.mdcf	vnd.marlin.drm.mdcf	Ellison
348	application/vnd.mcd	vnd.mcd	Gotoh
349	application/vnd.medcalcdata	vnd.medcalcdata	Schoonjans
350	application/vnd.mediastation.cdkey	vnd.mediastation.cdkey	Flurry
351	application/vnd.meridian-slingshot	vnd.meridian-slingshot	Wedel
352	application/vnd.MFER	vnd.MFER	Hirai
353	application/vnd.mfmp	vnd.mfmp	Ikeda
690	audio/mpeg	mpeg	RFC3003
354	application/vnd.micrografx.flo	vnd.micrografx.flo	Prevo
355	application/vnd.micrografx.igx	vnd.micrografx.igx	Prevo
356	application/vnd.mif	vnd.mif	Wexler
357	application/vnd.minisoft-hp3000-save	vnd.minisoft-hp3000-save	Bartram
358	application/vnd.mitsubishi.misty-guard.trustweb	vnd.mitsubishi.misty-guard.trustweb	Tanaka
359	application/vnd.Mobius.DAF	vnd.Mobius.DAF	Kabayama
360	application/vnd.Mobius.DIS	vnd.Mobius.DIS	Kabayama
361	application/vnd.Mobius.MBK	vnd.Mobius.MBK	Devasia
362	application/vnd.Mobius.MQY	vnd.Mobius.MQY	Devasia
363	application/vnd.Mobius.MSL	vnd.Mobius.MSL	Kabayama
364	application/vnd.Mobius.PLC	vnd.Mobius.PLC	Kabayama
365	application/vnd.Mobius.TXF	vnd.Mobius.TXF	Kabayama
366	application/vnd.mophun.application	vnd.mophun.application	Wennerstrom
367	application/vnd.mophun.certificate	vnd.mophun.certificate	Wennerstrom
368	application/vnd.motorola.flexsuite	vnd.motorola.flexsuite	Patton
369	application/vnd.motorola.flexsuite.adsi	vnd.motorola.flexsuite.adsi	Patton
370	application/vnd.motorola.flexsuite.fis	vnd.motorola.flexsuite.fis	Patton
371	application/vnd.motorola.flexsuite.gotap	vnd.motorola.flexsuite.gotap	Patton
372	application/vnd.motorola.flexsuite.kmr	vnd.motorola.flexsuite.kmr	Patton
373	application/vnd.motorola.flexsuite.ttc	vnd.motorola.flexsuite.ttc	Patton
374	application/vnd.motorola.flexsuite.wem	vnd.motorola.flexsuite.wem	Patton
375	application/vnd.mozilla.xul+xml	vnd.mozilla.xul+xml	McDaniel
376	application/vnd.ms-artgalry	vnd.ms-artgalry	Slawson
377	application/vnd.ms-asf	vnd.ms-asf	Fleischman
378	application/vnd.ms-cab-compressed	vnd.ms-cab-compressed	Scarborough
379	application/vnd.mseq	vnd.mseq	Le Bodic
380	application/vnd.ms-excel	vnd.ms-excel	Gill
381	application/vnd.ms-fontobject	vnd.ms-fontobject	Scarborough
382	application/vnd.ms-htmlhelp	vnd.ms-htmlhelp	Techtonik
383	application/vnd.msign	vnd.msign	Borcherding
384	application/vnd.ms-ims	vnd.ms-ims	Ledoux
385	application/vnd.ms-lrm	vnd.ms-lrm	Ledoux
386	application/vnd.ms-playready.initiator+xml	vnd.ms-playready.initiator+xml	Schneider
387	application/vnd.ms-powerpoint	vnd.ms-powerpoint	Gill
388	application/vnd.ms-project	vnd.ms-project	Gill
389	application/vnd.ms-tnef	vnd.ms-tnef	Gill
390	application/vnd.ms-wmdrm.lic-chlg-req	vnd.ms-wmdrm.lic-chlg-req	Lau
391	application/vnd.ms-wmdrm.lic-resp	vnd.ms-wmdrm.lic-resp	Lau
392	application/vnd.ms-wmdrm.meter-chlg-req	vnd.ms-wmdrm.meter-chlg-req	Lau
393	application/vnd.ms-wmdrm.meter-resp	vnd.ms-wmdrm.meter-resp	Lau
394	application/vnd.ms-works	vnd.ms-works	Gill
395	application/vnd.ms-wpl	vnd.ms-wpl	Plastina
744	image/jpm	jpm	RFC3745
396	application/vnd.ms-xpsdocument	vnd.ms-xpsdocument	McGatha
397	application/vnd.multiad.creator	vnd.multiad.creator	Mills
398	application/vnd.multiad.creator.cif	vnd.multiad.creator.cif	Mills
399	application/vnd.musician	vnd.musician	Adams
400	application/vnd.music-niff	vnd.music-niff	Butler
401	application/vnd.muvee.style	vnd.muvee.style	Anantharamu
402	application/vnd.ncd.control	vnd.ncd.control	Tarkkala
403	application/vnd.ncd.reference	vnd.ncd.reference	Tarkkala
404	application/vnd.nervana	vnd.nervana	Judkins
405	application/vnd.netfpx	vnd.netfpx	Mutz
406	application/vnd.neurolanguage.nlu	vnd.neurolanguage.nlu	DuFeu
407	application/vnd.noblenet-directory	vnd.noblenet-directory	Solomon
408	application/vnd.noblenet-sealer	vnd.noblenet-sealer	Solomon
409	application/vnd.noblenet-web	vnd.noblenet-web	Solomon
410	application/vnd.nokia.catalogs	vnd.nokia.catalogs	Nokia
411	application/vnd.nokia.conml+wbxml	vnd.nokia.conml+wbxml	Nokia
412	application/vnd.nokia.conml+xml	vnd.nokia.conml+xml	Nokia
413	application/vnd.nokia.iptv.config+xml	vnd.nokia.iptv.config+xml	Nokia
414	application/vnd.nokia.iSDS-radio-presets	vnd.nokia.iSDS-radio-presets	Nokia
415	application/vnd.nokia.landmark+wbxml	vnd.nokia.landmark+wbxml	Nokia
416	application/vnd.nokia.landmark+xml	vnd.nokia.landmark+xml	Nokia
417	application/vnd.nokia.landmarkcollection+xml	vnd.nokia.landmarkcollection+xml	Nokia
418	application/vnd.nokia.ncd	vnd.nokia.ncd	Nokia
419	application/vnd.nokia.n-gage.ac+xml	vnd.nokia.n-gage.ac+xml	Nokia
420	application/vnd.nokia.n-gage.data	vnd.nokia.n-gage.data	Nokia
421	application/vnd.nokia.n-gage.symbian.install	vnd.nokia.n-gage.symbian.install	Nokia
422	application/vnd.nokia.pcd+wbxml	vnd.nokia.pcd+wbxml	Nokia
423	application/vnd.nokia.pcd+xml	vnd.nokia.pcd+xml	Nokia
424	application/vnd.nokia.radio-preset	vnd.nokia.radio-preset	Nokia
425	application/vnd.nokia.radio-presets	vnd.nokia.radio-presets	Nokia
426	application/vnd.novadigm.EDM	vnd.novadigm.EDM	Swenson
427	application/vnd.novadigm.EDX	vnd.novadigm.EDX	Swenson
428	application/vnd.novadigm.EXT	vnd.novadigm.EXT	Swenson
429	application/vnd.oasis.opendocument.chart	vnd.oasis.opendocument.chart	Oppermann
430	application/vnd.oasis.opendocument.chart-template	vnd.oasis.opendocument.chart-template	Oppermann
431	application/vnd.oasis.opendocument.formula	vnd.oasis.opendocument.formula	Oppermann
432	application/vnd.oasis.opendocument.formula-template	vnd.oasis.opendocument.formula-template	Oppermann
433	application/vnd.oasis.opendocument.graphics	vnd.oasis.opendocument.graphics	Oppermann
434	application/vnd.oasis.opendocument.graphics-template	vnd.oasis.opendocument.graphics-template	Oppermann
435	application/vnd.oasis.opendocument.image	vnd.oasis.opendocument.image	Oppermann
436	application/vnd.oasis.opendocument.image-template	vnd.oasis.opendocument.image-template	Oppermann
437	application/vnd.oasis.opendocument.presentation	vnd.oasis.opendocument.presentation	Oppermann
438	application/vnd.oasis.opendocument.presentation-template	vnd.oasis.opendocument.presentation-template	Oppermann
439	application/vnd.oasis.opendocument.spreadsheet	vnd.oasis.opendocument.spreadsheet	Oppermann
440	application/vnd.oasis.opendocument.spreadsheet-template	vnd.oasis.opendocument.spreadsheet-template	Oppermann
441	application/vnd.oasis.opendocument.text	vnd.oasis.opendocument.text	Oppermann
442	application/vnd.oasis.opendocument.text-master	vnd.oasis.opendocument.text-master	Oppermann
443	application/vnd.oasis.opendocument.text-template	vnd.oasis.opendocument.text-template	Oppermann
444	application/vnd.oasis.opendocument.text-web	vnd.oasis.opendocument.text-web	Oppermann
445	application/vnd.obn	vnd.obn	Hessling
446	application/vnd.olpc-sugar	vnd.olpc-sugar	Palmieri
447	application/vnd.oma.bcast.associated-procedure-parameter+xml	vnd.oma.bcast.associated-procedure-parameter+xml	Rauschenbach, OMNA - Open Mobile Naming Authority
448	application/vnd.oma.bcast.drm-trigger+xml	vnd.oma.bcast.drm-trigger+xml	Rauschenbach, OMNA - Open Mobile Naming Authority
449	application/vnd.oma.bcast.imd+xml	vnd.oma.bcast.imd+xml	RauschenbachOMNA - Open Mobile Naming Authority
450	application/vnd.oma.bcast.ltkm	vnd.oma.bcast.ltkm	RauschenbachOMNA - Open Mobile Naming Authority
451	application/vnd.oma.bcast.notification+xml	vnd.oma.bcast.notification+xml	RauschenbachOMNA - Open Mobile Naming Authority
452	application/vnd.oma.bcast.sgboot	vnd.oma.bcast.sgboot	RauschenbachOMNA - Open Mobile Naming Authority
453	application/vnd.oma.bcast.sgdd+xml	vnd.oma.bcast.sgdd+xml	RauschenbachOMNA - Open Mobile Naming Authority
454	application/vnd.oma.bcast.sgdu	vnd.oma.bcast.sgdu	RauschenbachOMNA - Open Mobile Naming Authority
455	application/vnd.oma.bcast.simple-symbol-container	vnd.oma.bcast.simple-symbol-container	RauschenbachOMNA - Open Mobile Naming Authority
456	application/vnd.oma.bcast.smartcard-trigger+xml	vnd.oma.bcast.smartcard-trigger+xml	RauschenbachOMNA - Open Mobile Naming Authority
457	application/vnd.oma.bcast.sprov+xml	vnd.oma.bcast.sprov+xml	RauschenbachOMNA - Open Mobile Naming Authority
458	application/vnd.oma.bcast.stkm	vnd.oma.bcast.stkm	RauschenbachOMNA - Open Mobile Naming Authority
459	application/vnd.oma.dd2+xml	vnd.oma.dd2+xml	SatoOpen Mobile Alliance's BAC DLDRM Working Group
460	application/vnd.oma.drm.risd+xml	vnd.oma.drm.risd+xml	RauschenbachOMNA - Open Mobile Naming Authority
461	application/vnd.oma.group-usage-list+xml	vnd.oma.group-usage-list+xml	KelleyOMA Presence and Availability (PAG) Working Group
462	application/vnd.oma.poc.detailed-progress-report+xml	vnd.oma.poc.detailed-progress-report+xml	OMA Push to Talk over Cellular (POC) Working Group
542	application/vnd.swiftview-ics	vnd.swiftview-ics	Widener
463	application/vnd.oma.poc.final-report+xml	vnd.oma.poc.final-report+xml	OMA Push to Talk over Cellular (POC) Working Group
464	application/vnd.oma.poc.groups+xml	vnd.oma.poc.groups+xml	KelleyOMA Push to Talk over Cellular (POC) Working Group
465	application/vnd.oma.poc.invocation-descriptor+xml	vnd.oma.poc.invocation-descriptor+xml	OMA Push to Talk over Cellular (POC) Working Group
466	application/vnd.oma.poc.optimized-progress-report+xml	vnd.oma.poc.optimized-progress-report+xml	OMA Push to Talk over Cellular (POC) Working Group
467	application/vnd.oma.xcap-directory+xml	vnd.oma.xcap-directory+xml	KelleyOMA Presence and Availability (PAG) Working Group
468	application/vnd.omads-email+xml	vnd.omads-email+xml	OMA Data Synchronization Working Group
469	application/vnd.omads-file+xml	vnd.omads-file+xml	OMA Data Synchronization Working Group
470	application/vnd.omads-folder+xml	vnd.omads-folder+xml	OMA Data Synchronization Working Group
471	application/vnd.omaloc-supl-init	vnd.omaloc-supl-init	Grange
472	application/vnd.oma-scws-config	vnd.oma-scws-config	Mahalal
473	application/vnd.oma-scws-http-request	vnd.oma-scws-http-request	Mahalal
474	application/vnd.oma-scws-http-response	vnd.oma-scws-http-response	Mahalal
475	application/vnd.openofficeorg.extension	vnd.openofficeorg.extension	Lingner
476	application/vnd.osa.netdeploy	vnd.osa.netdeploy	Klos
477	application/vnd.osgi.bundle	vnd.osgi.bundle	Kriens
478	application/vnd.osgi.dp	vnd.osgi.dp	Kriens
479	application/vnd.otps.ct-kip+xml	vnd.otps.ct-kip+xml	Nyström
480	application/vnd.palm	vnd.palm	Peacock
481	application/vnd.paos.xml	vnd.paos.xml	Kemp
482	application/vnd.pg.format	vnd.pg.format	Gandert
483	application/vnd.pg.osasli	vnd.pg.osasli	Gandert
484	application/vnd.piaccess.application-licence	vnd.piaccess.application-licence	Maneos
485	application/vnd.picsel	vnd.picsel	Naccarato
486	application/vnd.poc.group-advertisement+xml	vnd.poc.group-advertisement+xml	KelleyOMA Push to Talk over Cellular (POC) Working Group
487	application/vnd.pocketlearn	vnd.pocketlearn	Pando
488	application/vnd.powerbuilder6	vnd.powerbuilder6	Guy
489	application/vnd.powerbuilder6-s	vnd.powerbuilder6-s	Guy
490	application/vnd.powerbuilder7	vnd.powerbuilder7	Shilts
491	application/vnd.powerbuilder75	vnd.powerbuilder75	Shilts
492	application/vnd.powerbuilder75-s	vnd.powerbuilder75-s	Shilts
493	application/vnd.powerbuilder7-s	vnd.powerbuilder7-s	Shilts
494	application/vnd.preminet	vnd.preminet	Tenhunen
495	application/vnd.previewsystems.box	vnd.previewsystems.box	Smolgovsky
496	application/vnd.proteus.magazine	vnd.proteus.magazine	Hoch
497	application/vnd.publishare-delta-tree	vnd.publishare-delta-tree	Ben-Kiki
498	application/vnd.pvi.ptid1	vnd.pvi.ptid1	Lamb
499	application/vnd.pwg-multiplexed	vnd.pwg-multiplexed	RFC3391
500	application/vnd.pwg-xhtml-print+xml	vnd.pwg-xhtml-print+xml	Wright
501	application/vnd.qualcomm.brew-app-res	vnd.qualcomm.brew-app-res	Forrester
502	application/vnd.Quark.QuarkXPress	vnd.Quark.QuarkXPress	Scheidler
503	application/vnd.rapid	vnd.rapid	Szekely
504	application/vnd.recordare.musicxml	vnd.recordare.musicxml	Good
505	application/vnd.recordare.musicxml+xml	vnd.recordare.musicxml+xml	Good
506	application/vnd.RenLearn.rlprint	vnd.RenLearn.rlprint	Wick
507	application/vnd.ruckus.download	vnd.ruckus.download	Harris
508	application/vnd.s3sms	vnd.s3sms	Tarkkala
509	application/vnd.sbm.mid2	vnd.sbm.mid2	Murai
510	application/vnd.scribus	vnd.scribus	Bradney
511	application/vnd.sealed.3df	vnd.sealed.3df	Kwan
512	application/vnd.sealed.csf	vnd.sealed.csf	Kwan
513	application/vnd.sealed.doc	vnd.sealed.doc	Petersen
514	application/vnd.sealed.eml	vnd.sealed.eml	Petersen
515	application/vnd.sealed.mht	vnd.sealed.mht	Petersen
516	application/vnd.sealed.net	vnd.sealed.net	Lambert
517	application/vnd.sealed.ppt	vnd.sealed.ppt	Petersen
518	application/vnd.sealed.tiff	vnd.sealed.tiff	KwanLambert
519	application/vnd.sealed.xls	vnd.sealed.xls	Petersen
520	application/vnd.sealedmedia.softseal.html	vnd.sealedmedia.softseal.html	Petersen
521	application/vnd.sealedmedia.softseal.pdf	vnd.sealedmedia.softseal.pdf	Petersen
522	application/vnd.seemail	vnd.seemail	Webb
523	application/vnd.sema	vnd.sema	Hansson
524	application/vnd.semd	vnd.semd	Hansson
525	application/vnd.semf	vnd.semf	Hansson
526	application/vnd.shana.informed.formdata	vnd.shana.informed.formdata	Selzler
527	application/vnd.shana.informed.formtemplate	vnd.shana.informed.formtemplate	Selzler
528	application/vnd.shana.informed.interchange	vnd.shana.informed.interchange	Selzler
529	application/vnd.shana.informed.package	vnd.shana.informed.package	Selzler
530	application/vnd.SimTech-MindMapper	vnd.SimTech-MindMapper	Koh
531	application/vnd.smaf	vnd.smaf	Takahashi
532	application/vnd.solent.sdkm+xml	vnd.solent.sdkm+xml	Gauntlett
533	application/vnd.spotfire.dxp	vnd.spotfire.dxp	Jernberg
534	application/vnd.spotfire.sfs	vnd.spotfire.sfs	Jernberg
535	application/vnd.sss-cod	vnd.sss-cod	Dani
536	application/vnd.sss-dtf	vnd.sss-dtf	Bruno
537	application/vnd.sss-ntf	vnd.sss-ntf	Bruno
538	application/vnd.street-stream	vnd.street-stream	Levitt
539	application/vnd.sun.wadl+xml	vnd.sun.wadl+xml	Hadley
540	application/vnd.sus-calendar	vnd.sus-calendar	Niedfeldt
541	application/vnd.svd	vnd.svd	Becker
742	image/jp2	jp2	RFC3745
543	application/vnd.syncml.dm+wbxml	vnd.syncml.dm+wbxml	OMA-DM Work Group
544	application/vnd.syncml.dm+xml	vnd.syncml.dm+xml	RaoOMA-DM Work Group
545	application/vnd.syncml.ds.notification	vnd.syncml.ds.notification	OMA Data Synchronization Working Group
546	application/vnd.syncml+xml	vnd.syncml+xml	OMA Data Synchronization Working Group
547	application/vnd.tao.intent-module-archive	vnd.tao.intent-module-archive	Shelton
548	application/vnd.tmobile-livetv	vnd.tmobile-livetv	Helin
549	application/vnd.trid.tpt	vnd.trid.tpt	Cusack
550	application/vnd.triscape.mxs	vnd.triscape.mxs	Simonoff
551	application/vnd.trueapp	vnd.trueapp	Hepler
552	application/vnd.truedoc	vnd.truedoc	Chase
553	application/vnd.ufdl	vnd.ufdl	Manning
554	application/vnd.uiq.theme	vnd.uiq.theme	Ocock
555	application/vnd.umajin	vnd.umajin	Riden
556	application/vnd.unity	vnd.unity	Unity3d
557	application/vnd.uoml+xml	vnd.uoml+xml	Gerdes
558	application/vnd.uplanet.alert	vnd.uplanet.alert	Martin
559	application/vnd.uplanet.alert-wbxml	vnd.uplanet.alert-wbxml	Martin
560	application/vnd.uplanet.bearer-choice	vnd.uplanet.bearer-choice	Martin
561	application/vnd.uplanet.bearer-choice-wbxml	vnd.uplanet.bearer-choice-wbxml	Martin
562	application/vnd.uplanet.cacheop	vnd.uplanet.cacheop	Martin
563	application/vnd.uplanet.cacheop-wbxml	vnd.uplanet.cacheop-wbxml	Martin
564	application/vnd.uplanet.channel	vnd.uplanet.channel	Martin
565	application/vnd.uplanet.channel-wbxml	vnd.uplanet.channel-wbxml	Martin
566	application/vnd.uplanet.list	vnd.uplanet.list	Martin
567	application/vnd.uplanet.listcmd	vnd.uplanet.listcmd	Martin
568	application/vnd.uplanet.listcmd-wbxml	vnd.uplanet.listcmd-wbxml	Martin
569	application/vnd.uplanet.list-wbxml	vnd.uplanet.list-wbxml	Martin
570	application/vnd.uplanet.signal	vnd.uplanet.signal	Martin
571	application/vnd.vcx	vnd.vcx	T.Sugimoto
572	application/vnd.vectorworks	vnd.vectorworks	Pharr
573	application/vnd.vd-study	vnd.vd-study	Rogge
574	application/vnd.vidsoft.vidconference	vnd.vidsoft.vidconference	Hess
575	application/vnd.visio	vnd.visio	Sandal
576	application/vnd.visionary	vnd.visionary	Aravindakumar
577	application/vnd.vividence.scriptfile	vnd.vividence.scriptfile	Risher
578	application/vnd.vsf	vnd.vsf	Rowe
579	application/vnd.wap.sic	vnd.wap.sic	WAP-Forum
580	application/vnd.wap.slc	vnd.wap.slc	WAP-Forum
581	application/vnd.wap.wbxml	vnd.wap.wbxml	Stark
582	application/vnd.wap.wmlc	vnd.wap.wmlc	Stark
583	application/vnd.wap.wmlscriptc	vnd.wap.wmlscriptc	Stark
584	application/vnd.webturbo	vnd.webturbo	Rehem
585	application/vnd.wfa.wsc	vnd.wfa.wsc	Wi-Fi Alliance
586	application/vnd.wmc	vnd.wmc	Kjørnes
587	application/vnd.wmf.bootstrap	vnd.wmf.bootstrap	Nguyenphu Iyer
588	application/vnd.wordperfect	vnd.wordperfect	Scarborough
589	application/vnd.wqd	vnd.wqd	Bostrom
590	application/vnd.wrq-hp3000-labelled	vnd.wrq-hp3000-labelled	Bartram
591	application/vnd.wt.stf	vnd.wt.stf	Wohler
592	application/vnd.wv.csp+xml	vnd.wv.csp+xml	Ingimundarson
593	application/vnd.wv.csp+wbxml	vnd.wv.csp+wbxml	Salmi
594	application/vnd.wv.ssp+xml	vnd.wv.ssp+xml	Ingimundarson
595	application/vnd.xara	vnd.xara	Matthewman
596	application/vnd.xfdl	vnd.xfdl	Manning
597	application/vnd.xmpie.cpkg	vnd.xmpie.cpkg	Sherwin
598	application/vnd.xmpie.dpkg	vnd.xmpie.dpkg	Sherwin
599	application/vnd.xmpie.plan	vnd.xmpie.plan	Sherwin
600	application/vnd.xmpie.ppkg	vnd.xmpie.ppkg	Sherwin
601	application/vnd.xmpie.xlim	vnd.xmpie.xlim	Sherwin
602	application/vnd.yamaha.hv-dic	vnd.yamaha.hv-dic	Yamamoto
603	application/vnd.yamaha.hv-script	vnd.yamaha.hv-script	Yamamoto
604	application/vnd.yamaha.hv-voice	vnd.yamaha.hv-voice	Yamamoto
605	application/vnd.yamaha.smaf-audio	vnd.yamaha.smaf-audio	Shinoda
606	application/vnd.yamaha.smaf-phrase	vnd.yamaha.smaf-phrase	Shinoda
607	application/vnd.yellowriver-custom-menu	vnd.yellowriver-custom-menu	Yellow
608	application/vnd.zzazz.deck+xml	vnd.zzazz.deck+xml	Hewett
609	application/voicexml+xml	voicexml+xml	RFC4267
610	application/watcherinfo+xml	watcherinfo+xml	RFC3858
611	application/whoispp-query	whoispp-query	RFC2957
612	application/whoispp-response	whoispp-response	RFC2958
613	application/wita	wita	Campbell
614	application/wordperfect5.1	wordperfect5.1	Lindner
615	application/wsdl+xml	wsdl+xml	W3C
616	application/wspolicy+xml	wspolicy+xml	W3C
617	application/x400-bp	x400-bp	RFC1494
618	application/xcap-att+xml	xcap-att+xml	RFC4825
619	application/xcap-caps+xml	xcap-caps+xml	RFC4825
620	application/xcap-el+xml	xcap-el+xml	RFC4825
621	application/xcap-error+xml	xcap-error+xml	RFC4825
622	application/xcap-ns+xml	xcap-ns+xml	RFC4825
623	application/xenc+xml	xenc+xml	ReagleXENC Working Group
624	application/xhtml-voice+xml (Obsolete)	xhtml-voice+xml (Obsolete)	RFC-mccobb-xplusv-media-type-04.txt
625	application/xhtml+xml	xhtml+xml	RFC3236
626	application/xml	xml	RFC3023
627	application/xml-dtd	xml-dtd	RFC3023
628	application/xml-external-parsed-entity	xml-external-parsed-entity	RFC3023
629	application/xmpp+xml	xmpp+xml	RFC3923
630	application/xop+xml	xop+xml	Nottingham
631	application/xv+xml	xv+xml	RFC4374
632	application/zip	zip	Lindner
633	audio/32kadpcm	32kadpcm	RFC2421,RFC2422
634	audio/3gpp	3gpp	RFC3839,RFC4281
635	audio/3gpp2	3gpp2	RFC4393
636	audio/ac3	ac3	RFC4184
637	audio/AMR	AMR	RFC4867
638	audio/AMR-WB	AMR-WB	RFC4867
639	audio/amr-wb+	amr-wb+	RFC4352
640	audio/asc	asc	RFC4695
641	audio/basic	basic	RFC2045,RFC2046
642	audio/BV16	BV16	RFC4298
643	audio/BV32	BV32	RFC4298
644	audio/clearmode	clearmode	RFC4040
645	audio/CN	CN	RFC3389
646	audio/DAT12	DAT12	RFC3190
647	audio/dls	dls	RFC4613
648	audio/dsr-es201108	dsr-es201108	RFC3557
649	audio/dsr-es202050	dsr-es202050	RFC4060
650	audio/dsr-es202211	dsr-es202211	RFC4060
651	audio/dsr-es202212	dsr-es202212	RFC4060
652	audio/eac3	eac3	RFC4598
653	audio/DVI4	DVI4	RFC4856
654	audio/EVRC	EVRC	RFC4788
655	audio/EVRC0	EVRC0	RFC4788
656	audio/EVRC1	EVRC1	RFC4788
657	audio/EVRCB	EVRCB	RFC5188
658	audio/EVRCB0	EVRCB0	RFC5188
659	audio/EVRCB1	EVRCB1	RFC4788
660	audio/EVRC-QCP	EVRC-QCP	RFC3625
661	audio/EVRCWB	EVRCWB	RFC5188
662	audio/EVRCWB0	EVRCWB0	RFC5188
663	audio/EVRCWB1	EVRCWB1	RFC5188
664	audio/example	example	RFC4735
665	audio/G722	G722	RFC4856
666	audio/G7221	G7221	RFC3047
667	audio/G723	G723	RFC4856
668	audio/G726-16	G726-16	RFC4856
669	audio/G726-24	G726-24	RFC4856
670	audio/G726-32	G726-32	RFC4856
671	audio/G726-40	G726-40	RFC4856
672	audio/G728	G728	RFC4856
673	audio/G729	G729	RFC4856
674	audio/G7291	G7291	RFC4749
675	audio/G729D	G729D	RFC4856
676	audio/G729E	G729E	RFC4856
677	audio/GSM	GSM	RFC4856
678	audio/GSM-EFR	GSM-EFR	RFC4856
679	audio/iLBC	iLBC	RFC3952
680	audio/L8	L8	RFC4856
681	audio/L16	L16	RFC4856
682	audio/L20	L20	RFC3190
683	audio/L24	L24	RFC3190
684	audio/LPC	LPC	RFC4856
685	audio/mobile-xmf	mobile-xmf	RFC4723
686	audio/MPA	MPA	RFC3555
687	audio/mp4	mp4	RFC4337
688	audio/MP4A-LATM	MP4A-LATM	RFC3016
689	audio/mpa-robust	mpa-robust	RFC5219
691	audio/mpeg4-generic	mpeg4-generic	RFC3640
692	audio/parityfec	parityfec	RFC5109
693	audio/PCMA	PCMA	RFC4856
694	audio/PCMU	PCMU	RFC4856
695	audio/prs.sid	prs.sid	Walleij RFC4735
696	audio/QCELP	QCELP	RFC3555,RFC3625,RFC4047
697	audio/RED	RED	RFC3555,RFC1494
698	audio/rtp-enc-aescm128	rtp-enc-aescm128	3GPP,RFC2046
699	audio/rRFC2045tp-midi	rtp-midi	RFC4695,RFC1314
700	audio/rtx	rtx	RFC4588,RFC3745
701	audio/SMV	SMV	RFC3558,RFC2045,RFC2046
702	audio/SMV0	SMV0	RFC3558,RFC3745
703	audio/SMV-QCP	SMV-QCP	RFC3625,RFC3745
704	audio/sp-midi	sp-midi	KosonenT. White
705	audio/t140c	t140c	RFC4351
706	audio/t38	t38	RFC4612
707	audio/telephone-event	telephone-event	RFC4733
708	audio/tone	tone	RFC4733,RFC3362
709	audio/ulpfec	ulpfec	RFC5109,RFC3302
710	audio/VDVI	VDVI	RFC4856,RFC3950
711	audio/VMR-WB	VMR-WB	RFC4348,RFC4424
712	audio/vnd.3gpp.iufp	vnd.3gpp.iufp	Belling
713	audio/vnd.4SB	vnd.4SB	De Jaham
714	audio/vnd.audiokoz	vnd.audiokoz	DeBarros
715	audio/vnd.CELP	vnd.CELP	De Jaham
716	audio/vnd.cisco.nse	vnd.cisco.nse	Kumar
717	audio/vnd.cmles.radio-events	vnd.cmles.radio-events	Goulet
718	audio/vnd.cns.anp1	vnd.cns.anp1	McLaughlin
719	audio/vnd.cns.inf1	vnd.cns.inf1	McLaughlin
720	audio/vnd.digital-winds	vnd.digital-winds	Strazds
721	audio/vnd.dlna.adts	vnd.dlna.adts	Heredia
722	audio/vnd.dolby.mlp	vnd.dolby.mlp	Ward
723	audio/vnd.everad.plj	vnd.everad.plj	Cicelsky
724	audio/vnd.hns.audio	vnd.hns.audio	Swaminathan
725	audio/vnd.lucent.voice	vnd.lucent.voice	Vaudreuil
726	audio/vnd.nokia.mobile-xmf	vnd.nokia.mobile-xmf	Nokia Corporation
727	audio/vnd.nortel.vbk	vnd.nortel.vbk	Parsons
728	audio/vnd.nuera.ecelp4800	vnd.nuera.ecelp4800	Fox
729	audio/vnd.nuera.ecelp7470	vnd.nuera.ecelp7470	Fox
730	audio/vnd.nuera.ecelp9600	vnd.nuera.ecelp9600	Fox
731	audio/vnd.octel.sbc	vnd.octel.sbc	Vaudreuil
732	audio/vnd.qcelp - DEPRECATED - Please use audio/qcelp	vnd.qcelp - DEPRECATED - Please use audio/qcelp	RFC3625
733	audio/vnd.rhetorex.32kadpcm	vnd.rhetorex.32kadpcm	Vaudreuil
734	audio/vnd.sealedmedia.softseal.mpeg	vnd.sealedmedia.softseal.mpeg	Petersen
735	audio/vnd.vmx.cvsd	vnd.vmx.cvsd	Vaudreuil
736	image/cgm	cgm	Computer Graphics Metafile Francis
737	image/example	example	RFC4735
738	image/fits	fits	RFC4047
739	image/g3fax	g3fax	RFC1494
740	image/gif	gif	RFC2045,RFC2046
741	image/ief	ief	Image Exchange Format RFC1314
745	image/jpx	jpx	RFC3745
746	image/naplps	naplps	Ferber
747	image/png	png	Randers-Pehrson
748	image/prs.btif	prs.btif	Simon
749	image/prs.pti	prs.pti	Laun
750	image/t38	t38	RFC3362
751	image/tiff	tiff	Tag Image File Format RFC3302
752	image/tiff-fx	tiff-fx	Tag Image File Format Fax eXtended RFC3950
753	image/vnd.adobe.photoshop	vnd.adobe.photoshop	Scarborough
754	image/vnd.cns.inf2	vnd.cns.inf2	McLaughlin
755	image/vnd.djvu	vnd.djvu	Bottou
756	image/vnd.dwg	vnd.dwg	Moline
757	image/vnd.dxf	vnd.dxf	Moline
758	image/vnd.fastbidsheet	vnd.fastbidsheet	Becker
759	image/vnd.fpx	vnd.fpx	Spencer
760	image/vnd.fst	vnd.fst	Fuldseth
761	image/vnd.fujixerox.edmics-mmr	vnd.fujixerox.edmics-mmr	Onda
762	image/vnd.fujixerox.edmics-rlc	vnd.fujixerox.edmics-rlc	Onda
763	image/vnd.globalgraphics.pgb	vnd.globalgraphics.pgb	Bailey
764	image/vnd.microsoft.icon	vnd.microsoft.icon	Butcher
765	image/vnd.mix	vnd.mix	Reddy
766	image/vnd.ms-modi	vnd.ms-modi	Vaughan
767	image/vnd.net-fpx	vnd.net-fpx	Spencer
768	image/vnd.sealed.png	vnd.sealed.png	Petersen
769	image/vnd.sealedmedia.softseal.gif	vnd.sealedmedia.softseal.gif	Petersen
770	image/vnd.sealedmedia.softseal.jpg	vnd.sealedmedia.softseal.jpg	Petersen
771	image/vnd.svf	vnd.svf	Moline
772	image/vnd.wap.wbmp	vnd.wap.wbmp	Stark
773	image/vnd.xiff	vnd.xiff	S.Martin
774	message/CPIM	CPIM	RFC3862
775	message/delivery-status	delivery-status	RFC1894
776	message/disposition-notification	disposition-notification	RFC2298
777	message/example	example	RFC4735
778	message/external-body	external-body	RFC2045,RFC2046
779	message/http	http	RFC2616
780	message/news	news	RFC1036, H.Spencer
781	message/partial	partial	RFC2045,RFC2046
782	message/rfc822	rfc822	RFC2045,RFC2046
783	message/s-http	s-http	RFC2660
784	message/sip	sip	RFC3261
785	message/sipfrag	sipfrag	RFC3420
786	message/tracking-status	tracking-status	RFC3886
787	message/vnd.si.simp	vnd.si.simp	Parks Young
788	model/example	example	RFC4735
789	model/iges	iges	Parks
790	model/mesh	mesh	RFC2077
791	model/vnd.dwf	vnd.dwf	Pratt
792	model/vnd.flatland.3dml	vnd.flatland.3dml	Powers
793	model/vnd.gdl	vnd.gdl	Babits
794	model/vnd.gs-gdl	vnd.gs-gdl	Babits
795	model/vnd.gtw	vnd.gtw	Ozaki
796	model/vnd.moml+xml	vnd.moml+xml	Brooks
797	model/vnd.mts	vnd.mts	Rabinovitch
798	model/vnd.parasolid.transmit.binary	vnd.parasolid.transmit.binary	Parasolid
799	model/vnd.parasolid.transmit.text	vnd.parasolid.transmit.text	Parasolid
800	model/vnd.vtu	vnd.vtu	Rabinovitch
801	model/vrml	vrml	RFC2077
802	multipart/alternative	alternative	RFC2045,RFC2046
803	multipart/appledouble	appledouble	Faltstrom
804	multipart/byteranges	byteranges	RFC2068
805	multipart/digest	digest	RFC2045,RFC2046
806	multipart/encrypted	encrypted	RFC1847
807	multipart/example	example	RFC4735
808	multipart/form-data	form-data	RFC2388
809	multipart/header-set	header-set	Crocker
810	multipart/mixed	mixed	RFC2045,RFC2046
811	multipart/parallel	parallel	RFC2045,RFC2046
812	multipart/related	related	RFC2387
813	multipart/report	report	RFC1892
814	multipart/signed	signed	RFC1847
815	multipart/voice-message	voice-message	RFC2421,RFC2423
816	text/calendar	calendar	RFC2445
817	text/css	css	RFC2318
818	text/csv	csv	RFC4180
819	text/directory	directory	RFC2425
820	text/dns	dns	RFC4027
821	text/ecmascript (obsolete)	ecmascript (obsolete)	RFC4329
822	text/enriched	enriched	RFC1896
823	text/example	example	RFC4735
824	text/html	html	RFC2854
825	text/javascript (obsolete)	javascript (obsolete)	RFC4329
826	text/parityfec	parityfec	RFC5109
827	text/plain	plain	RFC2046,RFC3676,RFC-wilde-text-fragment-09.txt
828	text/prs.fallenstein.rst	prs.fallenstein.rst	Fallenstein
829	text/prs.lines.tag	prs.lines.tag	Lines
830	text/RED	RED	RFC4102
831	text/rfc822-headers	rfc822-headers	RFC1892
832	text/richtext	richtext	RFC2045,RFC2046
833	text/rtf	rtf	Lindner
834	text/rtp-enc-aescm128	rtp-enc-aescm128	3GPP
835	text/rtx	rtx	RFC4588
836	text/sgml	sgml	RFC1874
837	text/t140	t140	RFC4103
838	text/tab-separated-values	tab-separated-values	Lindner
839	text/troff	troff	RFC4263
840	text/ulpfec	ulpfec	RFC5109
841	text/uri-list	uri-list	RFC2483
842	text/vnd.abc	vnd.abc	Allen
843	text/vnd.curl	vnd.curl	Byrnes
844	text/vnd.DMClientScript	vnd.DMClientScript	Bradley
845	text/vnd.esmertec.theme-descriptor	vnd.esmertec.theme-descriptor	Eilemann
846	text/vnd.fly	vnd.fly	Gurney
847	text/vnd.fmi.flexstor	vnd.fmi.flexstor	Hurtta
848	text/vnd.in3d.3dml	vnd.in3d.3dml	Powers
849	text/vnd.in3d.spot	vnd.in3d.spot	Powers
850	text/vnd.IPTC.NewsML	vnd.IPTC.NewsML	IPTC
851	text/vnd.IPTC.NITF	vnd.IPTC.NITF	IPTC
852	text/vnd.latex-z	vnd.latex-z	Lubos
853	text/vnd.motorola.reflex	vnd.motorola.reflex	Patton
854	text/vnd.ms-mediapackage	vnd.ms-mediapackage	Nelson
855	text/vnd.net2phone.commcenter.command	vnd.net2phone.commcenter.command	Xie
856	text/vnd.si.uricatalogue	vnd.si.uricatalogue	Parks Young
857	text/vnd.sun.j2me.app-descriptor	vnd.sun.j2me.app-descriptor	G.Adams
858	text/vnd.trolltech.linguist	vnd.trolltech.linguist	D.Lambert
859	text/vnd.wap.si	vnd.wap.si	WAP-Forum
860	text/vnd.wap.sl	vnd.wap.sl	WAP-Forum
861	text/vnd.wap.wml	vnd.wap.wml	Stark
862	text/vnd.wap.wmlscript	vnd.wap.wmlscript	Stark
863	text/xml	xml	RFC3023
864	text/xml-external-parsed-entity	xml-external-parsed-entity	RFC3023
865	video/3gpp	3gpp	RFC3839, RFC4281
866	video/3gpp2	3gpp2	RFC4393
867	video/3gpp-tt	3gpp-tt	RFC4396
868	video/BMPEG	BMPEG	RFC3555
869	video/BT656	BT656	RFC3555
870	video/CelB	CelB	RFC3555
871	video/DV	DV	RFC3189
872	video/example	example	RFC4735
873	video/H261	H261	RFC4587
874	video/H263	H263	RFC3555
875	video/H263-1998	H263-1998	RFC4629
876	video/H263-2000	H263-2000	RFC4629
877	video/H264	H264	RFC3984
878	video/JPEG	JPEG	RFC3555
879	video/MJ2	MJ2	RFC3745
880	video/MP1S	MP1S	RFC3555
881	video/MP2P	MP2P	RFC3555
882	video/MP2T	MP2T	RFC3555
883	video/mp4	mp4	RFC4337
884	video/MP4V-ES	MP4V-ES	RFC3016
885	video/MPV	MPV	RFC3555
886	video/mpeg	mpeg	RFC2045,RFC2046
887	video/mpeg4-generic	mpeg4-generic	RFC3640
888	video/nv	nv	RFC4856
889	video/parityfec	parityfec	RFC5109
890	video/pointer	pointer	RFC2862
891	video/quicktime	quicktime	Lindner
892	video/raw	raw	RFC4175
893	video/rtp-enc-aescm128	rtp-enc-aescm128	3GPP
894	video/rtx	rtx	RFC4588
895	video/SMPTE292M	SMPTE292M	RFC3497
896	video/ulpfec	ulpfec	RFC5109
897	video/vc1	vc1	RFC4425
898	video/vnd.dlna.mpeg-tts	vnd.dlna.mpeg-tts	Heredia
899	video/vnd.fvt	vnd.fvt	Fuldseth
900	video/vnd.hns.video	vnd.hns.video	Swaminathan
901	video/vnd.iptvforum.1dparityfec-1010	vnd.iptvforum.1dparityfec-1010	Nakamura
902	video/vnd.iptvforum.1dparityfec-2005	vnd.iptvforum.1dparityfec-2005	Nakamura
903	video/vnd.iptvforum.2dparityfec-1010	vnd.iptvforum.2dparityfec-1010	Nakamura
904	video/vnd.iptvforum.2dparityfec-2005	vnd.iptvforum.2dparityfec-2005	Nakamura
905	video/vnd.iptvforum.ttsavc	vnd.iptvforum.ttsavc	Nakamura
906	video/vnd.iptvforum.ttsmpeg2	vnd.iptvforum.ttsmpeg2	Nakamura
907	video/vnd.motorola.video	vnd.motorola.video	McGinty
908	video/vnd.motorola.videop	vnd.motorola.videop	McGinty
909	video/vnd.mpegurl	vnd.mpegurl	Recktenwald
910	video/vnd.nokia.interleaved-multimedia	vnd.nokia.interleaved-multimedia	Kangaslampi
911	video/vnd.nokia.videovoip	vnd.nokia.videovoip	Nokia
912	video/vnd.objectvideo	vnd.objectvideo	Clark
913	video/vnd.sealed.mpeg1	vnd.sealed.mpeg1	Petersen
914	video/vnd.sealed.mpeg4	vnd.sealed.mpeg4	Petersen
915	video/vnd.sealed.swf	vnd.sealed.swf	Petersen
916	video/vnd.sealedmedia.softseal.mov	vnd.sealedmedia.softseal.mov	Petersen
917	video/vnd.vivo	vnd.vivo	Wolfe
\.


--
-- Name: ref-mimecodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-mimecodes_id_seq"', 918, false);


--
-- Data for Name: ref-replycodes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "ref-replycodes" (id, code, documentation) FROM stdin;
1	000	OK
2	001	OK (consulter les commentaires pour information)
3	002	OK (Demande aux autorités de contrôle effectuée)
4	101	Message mal formé.
5	102	Système momentanément indisponible.
6	200	Service producteur non reconnu.
7	201	Service d'archive non reconnu.
8	202	Service versant non reconnu.
9	203	Dépôt non conforme au profil de données.
10	204	Format de document non géré.
11	205	Format de document non conforme au format déclaré.
12	206	Signature du message invalide.
13	207	Empreinte(s) invalide(s).
14	208	Archive indisponible. Délai de communication non écoulé.
15	209	Archive absente (élimination, restitution, transfert)
16	210	Archive inconnue
17	211	Pièce attachée absente.
18	212	Dérogation refusée.
19	300	Convention invalide.
20	301	Dépôt non conforme à la convention. Quota des versements dépassé.
21	302	Dépôt non conforme à la convention. Identifiant du producteur non conforme.
22	303	Dépôt non conforme à la convention. Identifiant du service versant non conforme.
23	304	Dépôt non conforme à la convention. Identifiant du service d'archives non conforme.
24	305	Dépôt non conforme à la convention. Signature(s) de document(s) absente(s).
25	306	Dépôt non conforme à la convention. Volume non conforme.
26	307	Dépôt non conforme à la convention. Format non conforme.
27	308	Dépôt non conforme à la convention. Empreinte(s) non transmise(s).
28	309	Dépôt non conforme à la convention. Absence de signature du message.
29	310	Dépôt non conforme à la convention. Signature(s) de document(s) non valide(s).
30	311	Dépôt non conforme à la convention. Signature(s) de document(s) non vérifiée(s).
31	312	Dépôt non conforme à la convention. Dates de début ou de fin non respectées.
\.


--
-- Name: ref-replycodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"ref-replycodes_id_seq"', 32, false);


--
-- Data for Name: services_users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY services_users (id, service_id, user_id) FROM stdin;
1	1	1
\.


--
-- Name: services_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('services_users_id_seq', 13, false);


--
-- Data for Name: versions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY versions (version, date) FROM stdin;
\.


--
-- Data for Name: wkf-circuits; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-circuits" (id, nom, description, actif, defaut, created_user_id, modified_user_id, created, modified, type) FROM stdin;
3	Traitement des demandes de restitution	Traitement des demandes de restitution à la demande du service producteur.	t	t	1	\N	2013-03-01 11:50:01	2013-03-01 11:50:01	ARCHIVE_RESTITUTION_REQUEST_FROM_ORIGINATING_AGENCY
2	Traitement des demandes d'élimination	Traitement des demandes d'élimination à la demande du service d'archives.	t	t	1	1	2013-03-01 11:47:04	2013-03-01 11:50:17	ARCHIVE_DESTRUCTION_REQUEST_FROM_ARCHIVAL_AGENCY
1	Traitement des transferts d'archives	Traitement des transferts d'archives en entrée.	t	t	1	1	2013-03-01 11:44:04	2013-03-01 11:50:24	ARCHIVE_TRANSFER
4	Traitements des accusés de restitution d’archives	Traitements des accusés de restitution d’archives.	t	t	1	\N	2013-03-01 11:52:08	2013-03-01 11:52:08	ARCHIVE_RESTITUTION_ACKNOWLEDGEMENT
5	Traitement des demandes de communication	Traitement des demandes de communication d'archives.	t	t	1	\N	2013-03-01 11:55:19	2013-03-01 11:55:19	ARCHIVE_DELIVERY_REQUEST
\.


--
-- Name: wkf-circuits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-circuits_id_seq"', 6, false);


--
-- Data for Name: wkf-compositions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-compositions" (id, type_validation, created, created_user_id, modified, modified_user_id, type, mail_contact, mail, model, foreign_key, traiteur_model, traiteur_foreign_key) FROM stdin;
1	V	2013-03-01 11:44:39	1	2013-03-01 11:44:39	1	USER	\N	\N	Etape	1	User	1
2	V	2013-03-01 11:45:16	1	2013-03-01 11:45:16	1	USER	\N	\N	Etape	2	User	1
3	\N	2013-03-01 11:48:07	1	2013-03-01 11:48:07	1	SERVICE_PRODUCTEUR	\N	\N	Etape	3		\N
4	V	2013-03-01 11:49:23	1	2013-03-01 11:49:23	1	USER	\N	\N	Etape	4	User	1
5	V	2013-03-01 11:50:59	1	2013-03-01 11:50:59	1	USER	\N	\N	Etape	5	User	1
6	V	2013-03-01 11:51:32	1	2013-03-01 11:51:32	1	USER	\N	\N	Etape	6	User	1
7	\N	2013-03-01 11:54:09	1	2013-03-01 11:54:09	1	SERVICE_PRODUCTEUR	\N	\N	Etape	7		\N
8	V	2013-03-01 11:55:55	1	2013-03-01 11:55:55	1	USER	\N	\N	Etape	8	User	1
11	V	2013-03-12 15:27:31	1	2013-04-19 10:47:37	1	USER	\N	\N	Organization	1	User	1
\.


--
-- Name: wkf-compositions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-compositions_id_seq"', 12, false);


--
-- Data for Name: wkf-etapes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-etapes" (id, circuit_id, nom, description, type, ordre, created_user_id, modified_user_id, created, modified) FROM stdin;
1	1	Contrôle		2	1	1	1	2013-03-01 11:44:27	2013-03-01 11:44:27
2	1	Validation		2	2	1	1	2013-03-01 11:44:58	2013-03-01 11:45:04
3	2	Accord du service producteur	Le service producteur doit donner sons accord pour l'élimination des ses archives.	2	1	1	1	2013-03-01 11:47:56	2013-03-01 11:47:56
4	2	Validation	L'archiviste valide la demande d'élimination.	2	2	1	1	2013-03-01 11:49:11	2013-03-01 11:49:11
5	3	Contrôle		2	1	1	1	2013-03-01 11:50:49	2013-03-01 11:50:49
6	3	Validation		2	2	1	1	2013-03-01 11:51:22	2013-03-01 11:51:22
7	4	Acquittement de restitution	Le service producteur atteste être en possession des archives restituées et donne son accord pour la destruction des archives restituées.	2	1	1	1	2013-03-01 11:53:58	2013-03-01 11:53:58
8	5	Accord du service d'archives		2	1	1	1	2013-03-01 11:55:45	2013-03-01 11:55:45
\.


--
-- Name: wkf-etapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-etapes_id_seq"', 9, false);


--
-- Data for Name: wkf-signatures; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-signatures" (id, type_signature, signature, created) FROM stdin;
\.


--
-- Name: wkf-signatures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-signatures_id_seq"', 1, false);


--
-- Data for Name: wkf-traitementpjs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-traitementpjs" (id, traitement_id, nom, filename, mime_code, size_bytes, contents, created) FROM stdin;
\.


--
-- Name: wkf-traitementpjs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-traitementpjs_id_seq"', 1, false);


--
-- Data for Name: wkf-traitements; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-traitements" (id, circuit_id, foreign_key, numero_traitement, created, model, commentaire, statut, resultat) FROM stdin;
\.


--
-- Data for Name: wkf-traitements_compositions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-traitements_compositions" (id, traitementetape_id, composition_id, traiteur_type, traiteur_model, traiteur_foreign_key, action, treated, traiteur_notified, traiteur_date_notification, type_validation, mail_contact, mail, traite_par, commentaire, signature_id, created, modified) FROM stdin;
\.


--
-- Name: wkf-traitements_compositions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-traitements_compositions_id_seq"', 1, false);


--
-- Data for Name: wkf-traitements_etapes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "wkf-traitements_etapes" (id, traitement_id, etape_id, nom, type, ordre, treated, created, modified) FROM stdin;
\.


--
-- Name: wkf-traitements_etapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-traitements_etapes_id_seq"', 1, false);


--
-- Name: wkf-traitements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('"wkf-traitements_id_seq"', 1, false);


--
-- Name: accords_filetypes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY accords_filetypes
    ADD CONSTRAINT accords_filetypes_pkey PRIMARY KEY (id);


--
-- Name: accords_versants_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY accords_versants
    ADD CONSTRAINT accords_versants_pkey PRIMARY KEY (id);


--
-- Name: acos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY acos
    ADD CONSTRAINT acos_pkey PRIMARY KEY (id);


--
-- Name: adm-accords_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-accords"
    ADD CONSTRAINT "adm-accords_pkey" PRIMARY KEY (id);


--
-- Name: adm-affichages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-affichages"
    ADD CONSTRAINT "adm-affichages_pkey" PRIMARY KEY (id);


--
-- Name: adm-altkeywords_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-altkeywords"
    ADD CONSTRAINT "adm-altkeywords_pkey" PRIMARY KEY (id);


--
-- Name: adm-collectivites_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-collectivites"
    ADD CONSTRAINT "adm-collectivites_pkey" PRIMARY KEY (id);


--
-- Name: adm-compteurs_nom_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-compteurs"
    ADD CONSTRAINT "adm-compteurs_nom_key" UNIQUE (nom);


--
-- Name: adm-compteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-compteurs"
    ADD CONSTRAINT "adm-compteurs_pkey" PRIMARY KEY (id);


--
-- Name: adm-connecteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-connecteurs"
    ADD CONSTRAINT "adm-connecteurs_pkey" PRIMARY KEY (id);


--
-- Name: adm-contrats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-contrats"
    ADD CONSTRAINT "adm-contrats_pkey" PRIMARY KEY (id);


--
-- Name: adm-fichiers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-fichiers"
    ADD CONSTRAINT "adm-fichiers_pkey" PRIMARY KEY (id);


--
-- Name: adm-journaux_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-journaux"
    ADD CONSTRAINT "adm-journaux_pkey" PRIMARY KEY (id);


--
-- Name: adm-keywordlists_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-keywordlists"
    ADD CONSTRAINT "adm-keywordlists_pkey" PRIMARY KEY (id);


--
-- Name: adm-keywords_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-keywords"
    ADD CONSTRAINT "adm-keywords_pkey" PRIMARY KEY (id);


--
-- Name: adm-parapheurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-parapheurs"
    ADD CONSTRAINT "adm-parapheurs_pkey" PRIMARY KEY (id);


--
-- Name: adm-profils_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-profils"
    ADD CONSTRAINT "adm-profils_pkey" PRIMARY KEY (id);


--
-- Name: adm-referentiels_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-referentiels"
    ADD CONSTRAINT "adm-referentiels_pkey" PRIMARY KEY (id);


--
-- Name: adm-refexterieurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-refexterieurs"
    ADD CONSTRAINT "adm-refexterieurs_pkey" PRIMARY KEY (id);


--
-- Name: adm-roles_collectivite_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-roles"
    ADD CONSTRAINT "adm-roles_collectivite_id_key" UNIQUE (collectivite_id, nom);


--
-- Name: adm-roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-roles"
    ADD CONSTRAINT "adm-roles_pkey" PRIMARY KEY (id);


--
-- Name: adm-sequences_nom_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-sequences"
    ADD CONSTRAINT "adm-sequences_nom_key" UNIQUE (nom);


--
-- Name: adm-sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-sequences"
    ADD CONSTRAINT "adm-sequences_pkey" PRIMARY KEY (id);


--
-- Name: adm-services_collectivite_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-services"
    ADD CONSTRAINT "adm-services_collectivite_id_key" UNIQUE (collectivite_id, nom);


--
-- Name: adm-services_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-services"
    ADD CONSTRAINT "adm-services_pkey" PRIMARY KEY (id);


--
-- Name: adm-typeacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-typeacteurs"
    ADD CONSTRAINT "adm-typeacteurs_pkey" PRIMARY KEY (id);


--
-- Name: adm-typeentites_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-typeentites"
    ADD CONSTRAINT "adm-typeentites_pkey" PRIMARY KEY (id);


--
-- Name: adm-typemessages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-typemessages"
    ADD CONSTRAINT "adm-typemessages_pkey" PRIMARY KEY (id);


--
-- Name: adm-users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-users"
    ADD CONSTRAINT "adm-users_pkey" PRIMARY KEY (id);


--
-- Name: adm-verrous_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "adm-verrous"
    ADD CONSTRAINT "adm-verrous_pkey" PRIMARY KEY (id);


--
-- Name: arc-accessrestrictions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-accessrestrictions"
    ADD CONSTRAINT "arc-accessrestrictions_pkey" PRIMARY KEY (id);


--
-- Name: arc-appraisals_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-appraisals"
    ADD CONSTRAINT "arc-appraisals_pkey" PRIMARY KEY (id);


--
-- Name: arc-archivefiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-archivefiles"
    ADD CONSTRAINT "arc-archivefiles_pkey" PRIMARY KEY (id);


--
-- Name: arc-archivefilestorages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-archivefilestorages"
    ADD CONSTRAINT "arc-archivefilestorages_pkey" PRIMARY KEY (id);


--
-- Name: arc-archiveobjects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-archiveobjects"
    ADD CONSTRAINT "arc-archiveobjects_pkey" PRIMARY KEY (id);


--
-- Name: arc-archives_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-archives"
    ADD CONSTRAINT "arc-archives_pkey" PRIMARY KEY (id);


--
-- Name: arc-contentdescriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-contentdescriptions"
    ADD CONSTRAINT "arc-contentdescriptions_pkey" PRIMARY KEY (id);


--
-- Name: arc-contentdescriptives_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-contentdescriptives"
    ADD CONSTRAINT "arc-contentdescriptives_pkey" PRIMARY KEY (id);


--
-- Name: arc-documentfiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-documentfiles"
    ADD CONSTRAINT "arc-documentfiles_pkey" PRIMARY KEY (id);


--
-- Name: arc-documentfilestorages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-documentfilestorages"
    ADD CONSTRAINT "arc-documentfilestorages_pkey" PRIMARY KEY (id);


--
-- Name: arc-documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-documents"
    ADD CONSTRAINT "arc-documents_pkey" PRIMARY KEY (id);


--
-- Name: arc-organizations_nom_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-organizations"
    ADD CONSTRAINT "arc-organizations_nom_key" UNIQUE (nom);


--
-- Name: arc-organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "arc-organizations"
    ADD CONSTRAINT "arc-organizations_pkey" PRIMARY KEY (id);


--
-- Name: archivedeliveries_archiveobjects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archivedeliveries_archiveobjects
    ADD CONSTRAINT archivedeliveries_archiveobjects_pkey PRIMARY KEY (id);


--
-- Name: archivedeliveries_archives_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archivedeliveries_archives
    ADD CONSTRAINT archivedeliveries_archives_pkey PRIMARY KEY (id);


--
-- Name: archivedeliveryrequests_archives_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archivedeliveryrequests_archives
    ADD CONSTRAINT archivedeliveryrequests_archives_pkey PRIMARY KEY (id);


--
-- Name: archivedestructionrequests_archives_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archivedestructionrequests_archives
    ADD CONSTRAINT archivedestructionrequests_archives_pkey PRIMARY KEY (id);


--
-- Name: archivedestructionrequests_arcobjs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archivedestructionrequests_arcobjs
    ADD CONSTRAINT archivedestructionrequests_arcobjs_pkey PRIMARY KEY (id);


--
-- Name: archiverestitutionrequests_archives_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archiverestitutionrequests_archives
    ADD CONSTRAINT archiverestitutionrequests_archives_pkey PRIMARY KEY (id);


--
-- Name: archives_archivetransfers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY archives_archivetransfers
    ADD CONSTRAINT archives_archivetransfers_pkey PRIMARY KEY (id);


--
-- Name: aros_acos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY aros_acos
    ADD CONSTRAINT aros_acos_pkey PRIMARY KEY (id);


--
-- Name: aros_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY aros
    ADD CONSTRAINT aros_pkey PRIMARY KEY (id);


--
-- Name: connecteurs_organizations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY connecteurs_organizations
    ADD CONSTRAINT connecteurs_organizations_pkey PRIMARY KEY (id);


--
-- Name: io-acknowledgements_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-acknowledgements"
    ADD CONSTRAINT "io-acknowledgements_pkey" PRIMARY KEY (id);


--
-- Name: io-alertes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-alertes"
    ADD CONSTRAINT "io-alertes_pkey" PRIMARY KEY (id);


--
-- Name: io-archivedeliveries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivedeliveries"
    ADD CONSTRAINT "io-archivedeliveries_pkey" PRIMARY KEY (id);


--
-- Name: io-archivedeliveryrequests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivedeliveryrequests"
    ADD CONSTRAINT "io-archivedeliveryrequests_pkey" PRIMARY KEY (id);


--
-- Name: io-archivedestructionnotifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivedestructionnotifications"
    ADD CONSTRAINT "io-archivedestructionnotifications_pkey" PRIMARY KEY (id);


--
-- Name: io-archivedestructionrequestreplies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivedestructionrequestreplies"
    ADD CONSTRAINT "io-archivedestructionrequestreplies_pkey" PRIMARY KEY (id);


--
-- Name: io-archivedestructionrequests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivedestructionrequests"
    ADD CONSTRAINT "io-archivedestructionrequests_pkey" PRIMARY KEY (id);


--
-- Name: io-archiverestitutionrequestreplies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archiverestitutionrequestreplies"
    ADD CONSTRAINT "io-archiverestitutionrequestreplies_pkey" PRIMARY KEY (id);


--
-- Name: io-archiverestitutionrequests_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archiverestitutionrequests"
    ADD CONSTRAINT "io-archiverestitutionrequests_pkey" PRIMARY KEY (id);


--
-- Name: io-archiverestitutions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archiverestitutions"
    ADD CONSTRAINT "io-archiverestitutions_pkey" PRIMARY KEY (id);


--
-- Name: io-archivetransferacceptances_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivetransferacceptances"
    ADD CONSTRAINT "io-archivetransferacceptances_pkey" PRIMARY KEY (id);


--
-- Name: io-archivetransferreplies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivetransferreplies"
    ADD CONSTRAINT "io-archivetransferreplies_pkey" PRIMARY KEY (id);


--
-- Name: io-archivetransfers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-archivetransfers"
    ADD CONSTRAINT "io-archivetransfers_pkey" PRIMARY KEY (id);


--
-- Name: io-avscans_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-avscans"
    ADD CONSTRAINT "io-avscans_pkey" PRIMARY KEY (id);


--
-- Name: io-erreurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-erreurs"
    ADD CONSTRAINT "io-erreurs_pkey" PRIMARY KEY (id);


--
-- Name: io-messages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-messages"
    ADD CONSTRAINT "io-messages_pkey" PRIMARY KEY (id);


--
-- Name: io-piecejointes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "io-piecejointes"
    ADD CONSTRAINT "io-piecejointes_pkey" PRIMARY KEY (id);


--
-- Name: keywords_keywords_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keywords_keywords
    ADD CONSTRAINT keywords_keywords_pkey PRIMARY KEY (id);


--
-- Name: oaipmh-resumptiontoken_records_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "oaipmh-resumptiontoken_records"
    ADD CONSTRAINT "oaipmh-resumptiontoken_records_pkey" PRIMARY KEY (id);


--
-- Name: oaipmh-resumptiontokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "oaipmh-resumptiontokens"
    ADD CONSTRAINT "oaipmh-resumptiontokens_pkey" PRIMARY KEY (id);


--
-- Name: organizations_typeacteurs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY organizations_typeacteurs
    ADD CONSTRAINT organizations_typeacteurs_pkey PRIMARY KEY (id);


--
-- Name: ref-accessrestrictioncodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-accessrestrictioncodes"
    ADD CONSTRAINT "ref-accessrestrictioncodes_pkey" PRIMARY KEY (id);


--
-- Name: ref-appraisalcodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-appraisalcodes"
    ADD CONSTRAINT "ref-appraisalcodes_pkey" PRIMARY KEY (id);


--
-- Name: ref-descriptionlevels_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-descriptionlevels"
    ADD CONSTRAINT "ref-descriptionlevels_pkey" PRIMARY KEY (id);


--
-- Name: ref-documenttypes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-documenttypes"
    ADD CONSTRAINT "ref-documenttypes_pkey" PRIMARY KEY (id);


--
-- Name: ref-filetypes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-filetypes"
    ADD CONSTRAINT "ref-filetypes_pkey" PRIMARY KEY (id);


--
-- Name: ref-keywordtypes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-keywordtypes"
    ADD CONSTRAINT "ref-keywordtypes_pkey" PRIMARY KEY (id);


--
-- Name: ref-languagecodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-languagecodes"
    ADD CONSTRAINT "ref-languagecodes_pkey" PRIMARY KEY (id);


--
-- Name: ref-mimecodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-mimecodes"
    ADD CONSTRAINT "ref-mimecodes_pkey" PRIMARY KEY (id);


--
-- Name: ref-replycodes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "ref-replycodes"
    ADD CONSTRAINT "ref-replycodes_pkey" PRIMARY KEY (id);


--
-- Name: services_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY services_users
    ADD CONSTRAINT services_users_pkey PRIMARY KEY (id);


--
-- Name: services_users_service_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY services_users
    ADD CONSTRAINT services_users_service_id_key UNIQUE (service_id, user_id);


--
-- Name: wkf-circuits_nom_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-circuits"
    ADD CONSTRAINT "wkf-circuits_nom_key" UNIQUE (nom);


--
-- Name: wkf-circuits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-circuits"
    ADD CONSTRAINT "wkf-circuits_pkey" PRIMARY KEY (id);


--
-- Name: wkf-compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-compositions"
    ADD CONSTRAINT "wkf-compositions_pkey" PRIMARY KEY (id);


--
-- Name: wkf-etapes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-etapes"
    ADD CONSTRAINT "wkf-etapes_pkey" PRIMARY KEY (id);


--
-- Name: wkf-signatures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-signatures"
    ADD CONSTRAINT "wkf-signatures_pkey" PRIMARY KEY (id);


--
-- Name: wkf-traitementpjs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-traitementpjs"
    ADD CONSTRAINT "wkf-traitementpjs_pkey" PRIMARY KEY (id);


--
-- Name: wkf-traitements_compositions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-traitements_compositions"
    ADD CONSTRAINT "wkf-traitements_compositions_pkey" PRIMARY KEY (id);


--
-- Name: wkf-traitements_etapes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-traitements_etapes"
    ADD CONSTRAINT "wkf-traitements_etapes_pkey" PRIMARY KEY (id);


--
-- Name: wkf-traitements_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY "wkf-traitements"
    ADD CONSTRAINT "wkf-traitements_pkey" PRIMARY KEY (id);


--
-- Name: acos_acos_acos_idx1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX acos_acos_acos_idx1 ON acos USING btree (lft, rght);


--
-- Name: acos_acos_acos_idx2; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX acos_acos_acos_idx2 ON acos USING btree (alias);


--
-- Name: acos_acos_acos_idx3; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX acos_acos_acos_idx3 ON acos USING btree (model, foreign_key);


--
-- Name: adm-altkeywords_keyword_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-altkeywords_keyword_id" ON "adm-altkeywords" USING btree (keyword_id);


--
-- Name: adm-crons_pkey; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX "adm-crons_pkey" ON "adm-crons" USING btree (id);


--
-- Name: adm-fichiers_foreign_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-fichiers_foreign_idx" ON "adm-fichiers" USING btree (foreign_model, foreign_key);


--
-- Name: adm-journaux_date_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-journaux_date_key" ON "adm-journaux" USING btree (date);


--
-- Name: adm-journaux_evenement_nom_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-journaux_evenement_nom_key" ON "adm-journaux" USING btree (evenement_nom);


--
-- Name: adm-journaux_origine_nom_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-journaux_origine_nom_key" ON "adm-journaux" USING btree (origine_nom);


--
-- Name: adm-keywords_kwlidcode; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-keywords_kwlidcode" ON "adm-keywords" USING btree (keywordlist_id, code);


--
-- Name: adm-users_adm-users_USERNAME; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-users_adm-users_USERNAME" ON "adm-users" USING btree (username);


--
-- Name: adm-verrous_nom_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "adm-verrous_nom_idx" ON "adm-verrous" USING btree (nom);


--
-- Name: arc-archivefiles_archive_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX "arc-archivefiles_archive_idx" ON "arc-archivefiles" USING btree (archive_id, type, version);


--
-- Name: arc-archivefiles_archivetype_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-archivefiles_archivetype_idx" ON "arc-archivefiles" USING btree (archive_id, type);


--
-- Name: arc-archivefilestorages_archivefile_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-archivefilestorages_archivefile_id" ON "arc-archivefilestorages" USING btree (archivefile_id);


--
-- Name: arc-archivefilestorages_storef_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX "arc-archivefilestorages_storef_idx" ON "arc-archivefilestorages" USING btree (volume_identifier, storef);


--
-- Name: arc-archiveobjects_archive_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-archiveobjects_archive_id" ON "arc-archiveobjects" USING btree (archive_id);


--
-- Name: arc-archiveobjects_parent_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-archiveobjects_parent_id" ON "arc-archiveobjects" USING btree (parent_id);


--
-- Name: arc-documentfiles_document_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX "arc-documentfiles_document_idx" ON "arc-documentfiles" USING btree (document_id, document_type, document_order);


--
-- Name: arc-documentfilestorages_documentfile_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-documentfilestorages_documentfile_id" ON "arc-documentfilestorages" USING btree (documentfile_id);


--
-- Name: arc-documentfilestorages_storef_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX "arc-documentfilestorages_storef_idx" ON "arc-documentfilestorages" USING btree (volume_identifier, storef);


--
-- Name: arc-documents_model_foreign_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-documents_model_foreign_key" ON "arc-documents" USING btree (model, foreign_key);


--
-- Name: arc-organizations_arc-organizations_identification; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "arc-organizations_arc-organizations_identification" ON "arc-organizations" USING btree (identification);


--
-- Name: aros_acos_aros_acos_aco_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX aros_acos_aros_acos_aco_id ON aros_acos USING btree (aco_id);


--
-- Name: aros_acos_aros_acos_aro_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX aros_acos_aros_acos_aro_id ON aros_acos USING btree (aro_id);


--
-- Name: aros_aros_aros_idx1; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX aros_aros_aros_idx1 ON aros USING btree (lft, rght);


--
-- Name: aros_aros_aros_idx2; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX aros_aros_aros_idx2 ON aros USING btree (alias);


--
-- Name: aros_aros_aros_idx3; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX aros_aros_aros_idx3 ON aros USING btree (model, foreign_key);


--
-- Name: connecteurs_organizations_connecteur_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX connecteurs_organizations_connecteur_id ON connecteurs_organizations USING btree (connecteur_id);


--
-- Name: connecteurs_organizations_organization_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX connecteurs_organizations_organization_id ON connecteurs_organizations USING btree (organization_id);


--
-- Name: io-alertes_io-alertes_model_foreign_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-alertes_io-alertes_model_foreign_key" ON "io-alertes" USING btree (model, foreign_key);


--
-- Name: io-archivedeliveries_io-archivedeliveries_delivery_identifier; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivedeliveries_io-archivedeliveries_delivery_identifier" ON "io-archivedeliveries" USING btree (delivery_identifier);


--
-- Name: io-archivedeliveries_io-archivedeliveries_sens; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivedeliveries_io-archivedeliveries_sens" ON "io-archivedeliveries" USING btree (sens);


--
-- Name: io-archivedeliveryrequests_delivery_request_identifier; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivedeliveryrequests_delivery_request_identifier" ON "io-archivedeliveryrequests" USING btree (delivery_request_identifier);


--
-- Name: io-archivedeliveryrequests_sens; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivedeliveryrequests_sens" ON "io-archivedeliveryrequests" USING btree (sens);


--
-- Name: io-archivedestructionrequests_io-archivedestructionrequests_des; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivedestructionrequests_io-archivedestructionrequests_des" ON "io-archivedestructionrequests" USING btree (destruction_request_identifier);


--
-- Name: io-archivedestructionrequests_io-archivedestructionrequests_sen; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivedestructionrequests_io-archivedestructionrequests_sen" ON "io-archivedestructionrequests" USING btree (sens);


--
-- Name: io-archiverestitutionrequests_restitution_request_identifier; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archiverestitutionrequests_restitution_request_identifier" ON "io-archiverestitutionrequests" USING btree (restitution_request_identifier);


--
-- Name: io-archiverestitutionrequests_sens; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archiverestitutionrequests_sens" ON "io-archiverestitutionrequests" USING btree (sens);


--
-- Name: io-archivetransferacceptances_io-archivetransferacceptances_sen; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivetransferacceptances_io-archivetransferacceptances_sen" ON "io-archivetransferacceptances" USING btree (sens);


--
-- Name: io-archivetransferreplies_io-archivetransferreplies_sens; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivetransferreplies_io-archivetransferreplies_sens" ON "io-archivetransferreplies" USING btree (sens);


--
-- Name: io-archivetransfers_io-archivetransfers_sens; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivetransfers_io-archivetransfers_sens" ON "io-archivetransfers" USING btree (sens);


--
-- Name: io-archivetransfers_io-archivetransfers_transfer_identifier; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-archivetransfers_io-archivetransfers_transfer_identifier" ON "io-archivetransfers" USING btree (transfer_identifier);


--
-- Name: io-avscans_io-avscans_model_foreign_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-avscans_io-avscans_model_foreign_key" ON "io-avscans" USING btree (model, foreign_key);


--
-- Name: io-erreurs_io-erreurs_model_foreign_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-erreurs_io-erreurs_model_foreign_key" ON "io-erreurs" USING btree (model, foreign_key);


--
-- Name: io-piecejointes_io-piecejointes_archivetransfer_id_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "io-piecejointes_io-piecejointes_archivetransfer_id_key" ON "io-piecejointes" USING btree (archivetransfer_id);


--
-- Name: keywords_keywords_keyword_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX keywords_keywords_keyword_id ON keywords_keywords USING btree (keyword_id);


--
-- Name: organizations_typeacteurs_organizations_typeacteurs_fk_organiza; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX organizations_typeacteurs_organizations_typeacteurs_fk_organiza ON organizations_typeacteurs USING btree (organization_id);


--
-- Name: organizations_typeacteurs_organizations_typeacteurs_fk_typeacte; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX organizations_typeacteurs_organizations_typeacteurs_fk_typeacte ON organizations_typeacteurs USING btree (typeacteur_id);


--
-- Name: wkf-circuits_wkf-circuits_created_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-circuits_wkf-circuits_created_user_id" ON "wkf-circuits" USING btree (created_user_id);


--
-- Name: wkf-circuits_wkf-circuits_modified_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-circuits_wkf-circuits_modified_user_id" ON "wkf-circuits" USING btree (modified_user_id);


--
-- Name: wkf-compositions_belongs_to; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-compositions_belongs_to" ON "wkf-compositions" USING btree (model, foreign_key);


--
-- Name: wkf-compositions_traiteur; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-compositions_traiteur" ON "wkf-compositions" USING btree (traiteur_model, traiteur_foreign_key);


--
-- Name: wkf-etapes_wkf-etapes_circuit_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-etapes_wkf-etapes_circuit_id" ON "wkf-etapes" USING btree (circuit_id);


--
-- Name: wkf-etapes_wkf-etapes_nom; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-etapes_wkf-etapes_nom" ON "wkf-etapes" USING btree (nom);


--
-- Name: wkf-traitements_compositions_composition_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-traitements_compositions_composition_id" ON "wkf-traitements_compositions" USING btree (composition_id);


--
-- Name: wkf-traitements_compositions_traitementetape_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-traitements_compositions_traitementetape_id" ON "wkf-traitements_compositions" USING btree (traitementetape_id);


--
-- Name: wkf-traitements_compositions_traiteur; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-traitements_compositions_traiteur" ON "wkf-traitements_compositions" USING btree (traiteur_model, traiteur_foreign_key);


--
-- Name: wkf-traitements_etapes_traitement_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-traitements_etapes_traitement_id" ON "wkf-traitements_etapes" USING btree (traitement_id);


--
-- Name: wkf-traitements_wkf-traitements_model_foreign_key; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX "wkf-traitements_wkf-traitements_model_foreign_key" ON "wkf-traitements" USING btree (model, foreign_key);






--
-- Mise à jour des valeurs des séquences pour le champ id de chaque table
--

SELECT pg_catalog.setval('accords_filetypes_id_seq', ( SELECT coalesce(max("accords_filetypes".id), 0) + 1 FROM "accords_filetypes" ), false);
SELECT pg_catalog.setval('accords_versants_id_seq', ( SELECT coalesce(max("accords_versants".id), 0) + 1 FROM "accords_versants" ), false);
SELECT pg_catalog.setval('acos_id_seq', ( SELECT coalesce(max("acos".id), 0) + 1 FROM "acos" ), false);
SELECT pg_catalog.setval('adm-accords_id_seq', ( SELECT coalesce(max("adm-accords".id), 0) + 1 FROM "adm-accords" ), false);
SELECT pg_catalog.setval('adm-affichages_id_seq', ( SELECT coalesce(max("adm-affichages".id), 0) + 1 FROM "adm-affichages" ), false);
SELECT pg_catalog.setval('adm-altkeywords_id_seq', ( SELECT coalesce(max("adm-altkeywords".id), 0) + 1 FROM "adm-altkeywords" ), false);
SELECT pg_catalog.setval('adm-collectivites_id_seq', ( SELECT coalesce(max("adm-collectivites".id), 0) + 1 FROM "adm-collectivites" ), false);
SELECT pg_catalog.setval('adm-compteurs_id_seq', ( SELECT coalesce(max("adm-compteurs".id), 0) + 1 FROM "adm-compteurs" ), false);
SELECT pg_catalog.setval('adm-connecteurs_id_seq', ( SELECT coalesce(max("adm-connecteurs".id), 0) + 1 FROM "adm-connecteurs" ), false);
SELECT pg_catalog.setval('adm-contrats_id_seq', ( SELECT coalesce(max("adm-contrats".id), 0) + 1 FROM "adm-contrats" ), false);
SELECT pg_catalog.setval('adm-crons_id_seq', ( SELECT coalesce(max("adm-crons".id), 0) + 1 FROM "adm-crons" ), false);
SELECT pg_catalog.setval('adm-fichiers_id_seq', ( SELECT coalesce(max("adm-fichiers".id), 0) + 1 FROM "adm-fichiers" ), false);
SELECT pg_catalog.setval('adm-journaux_id_seq', ( SELECT coalesce(max("adm-journaux".id), 0) + 1 FROM "adm-journaux" ), false);
SELECT pg_catalog.setval('adm-keywordlists_id_seq', ( SELECT coalesce(max("adm-keywordlists".id), 0) + 1 FROM "adm-keywordlists" ), false);
SELECT pg_catalog.setval('adm-keywords_id_seq', ( SELECT coalesce(max("adm-keywords".id), 0) + 1 FROM "adm-keywords" ), false);
SELECT pg_catalog.setval('adm-parapheurs_id_seq', ( SELECT coalesce(max("adm-parapheurs".id), 0) + 1 FROM "adm-parapheurs" ), false);
SELECT pg_catalog.setval('adm-profils_id_seq', ( SELECT coalesce(max("adm-profils".id), 0) + 1 FROM "adm-profils" ), false);
SELECT pg_catalog.setval('adm-referentiels_id_seq', ( SELECT coalesce(max("adm-referentiels".id), 0) + 1 FROM "adm-referentiels" ), false);
SELECT pg_catalog.setval('adm-refexterieurs_id_seq', ( SELECT coalesce(max("adm-refexterieurs".id), 0) + 1 FROM "adm-refexterieurs" ), false);
SELECT pg_catalog.setval('adm-roles_id_seq', ( SELECT coalesce(max("adm-roles".id), 0) + 1 FROM "adm-roles" ), false);
SELECT pg_catalog.setval('adm-sequences_id_seq', ( SELECT coalesce(max("adm-sequences".id), 0) + 1 FROM "adm-sequences" ), false);
SELECT pg_catalog.setval('adm-services_id_seq', ( SELECT coalesce(max("adm-services".id), 0) + 1 FROM "adm-services" ), false);
SELECT pg_catalog.setval('adm-typeacteurs_id_seq', ( SELECT coalesce(max("adm-typeacteurs".id), 0) + 1 FROM "adm-typeacteurs" ), false);
SELECT pg_catalog.setval('adm-typeentites_id_seq', ( SELECT coalesce(max("adm-typeentites".id), 0) + 1 FROM "adm-typeentites" ), false);
SELECT pg_catalog.setval('adm-typemessages_id_seq', ( SELECT coalesce(max("adm-typemessages".id), 0) + 1 FROM "adm-typemessages" ), false);
SELECT pg_catalog.setval('adm-users_id_seq', ( SELECT coalesce(max("adm-users".id), 0) + 1 FROM "adm-users" ), false);
SELECT pg_catalog.setval('adm-verrous_id_seq', ( SELECT coalesce(max("adm-verrous".id), 0) + 1 FROM "adm-verrous" ), false);
SELECT pg_catalog.setval('arc-accessrestrictions_id_seq', ( SELECT coalesce(max("arc-accessrestrictions".id), 0) + 1 FROM "arc-accessrestrictions" ), false);
SELECT pg_catalog.setval('arc-appraisals_id_seq', ( SELECT coalesce(max("arc-appraisals".id), 0) + 1 FROM "arc-appraisals" ), false);
SELECT pg_catalog.setval('arc-archivefiles_id_seq', ( SELECT coalesce(max("arc-archivefiles".id), 0) + 1 FROM "arc-archivefiles" ), false);
SELECT pg_catalog.setval('arc-archivefilestorages_id_seq', ( SELECT coalesce(max("arc-archivefilestorages".id), 0) + 1 FROM "arc-archivefilestorages" ), false);
SELECT pg_catalog.setval('arc-archiveobjects_id_seq', ( SELECT coalesce(max("arc-archiveobjects".id), 0) + 1 FROM "arc-archiveobjects" ), false);
SELECT pg_catalog.setval('arc-archives_id_seq', ( SELECT coalesce(max("arc-archives".id), 0) + 1 FROM "arc-archives" ), false);
SELECT pg_catalog.setval('arc-contentdescriptions_id_seq', ( SELECT coalesce(max("arc-contentdescriptions".id), 0) + 1 FROM "arc-contentdescriptions" ), false);
SELECT pg_catalog.setval('arc-contentdescriptives_id_seq', ( SELECT coalesce(max("arc-contentdescriptives".id), 0) + 1 FROM "arc-contentdescriptives" ), false);
SELECT pg_catalog.setval('arc-documentfiles_id_seq', ( SELECT coalesce(max("arc-documentfiles".id), 0) + 1 FROM "arc-documentfiles" ), false);
SELECT pg_catalog.setval('arc-documentfilestorages_id_seq', ( SELECT coalesce(max("arc-documentfilestorages".id), 0) + 1 FROM "arc-documentfilestorages" ), false);
SELECT pg_catalog.setval('arc-documents_id_seq', ( SELECT coalesce(max("arc-documents".id), 0) + 1 FROM "arc-documents" ), false);
SELECT pg_catalog.setval('arc-organizations_id_seq', ( SELECT coalesce(max("arc-organizations".id), 0) + 1 FROM "arc-organizations" ), false);
SELECT pg_catalog.setval('archivedeliveries_archiveobjects_id_seq', ( SELECT coalesce(max("archivedeliveries_archiveobjects".id), 0) + 1 FROM "archivedeliveries_archiveobjects" ), false);
SELECT pg_catalog.setval('archivedeliveries_archives_id_seq', ( SELECT coalesce(max("archivedeliveries_archives".id), 0) + 1 FROM "archivedeliveries_archives" ), false);
SELECT pg_catalog.setval('archivedeliveryrequests_archives_id_seq', ( SELECT coalesce(max("archivedeliveryrequests_archives".id), 0) + 1 FROM "archivedeliveryrequests_archives" ), false);
SELECT pg_catalog.setval('archivedestructionrequests_archives_id_seq', ( SELECT coalesce(max("archivedestructionrequests_archives".id), 0) + 1 FROM "archivedestructionrequests_archives" ), false);
SELECT pg_catalog.setval('archivedestructionrequests_arcobjs_id_seq', ( SELECT coalesce(max("archivedestructionrequests_arcobjs".id), 0) + 1 FROM "archivedestructionrequests_arcobjs" ), false);
SELECT pg_catalog.setval('archiverestitutionrequests_archives_id_seq', ( SELECT coalesce(max("archiverestitutionrequests_archives".id), 0) + 1 FROM "archiverestitutionrequests_archives" ), false);
SELECT pg_catalog.setval('archives_archivetransfers_id_seq', ( SELECT coalesce(max("archives_archivetransfers".id), 0) + 1 FROM "archives_archivetransfers" ), false);
SELECT pg_catalog.setval('aros_id_seq', ( SELECT coalesce(max("aros".id), 0) + 1 FROM "aros" ), false);
SELECT pg_catalog.setval('aros_acos_id_seq', ( SELECT coalesce(max("aros_acos".id), 0) + 1 FROM "aros_acos" ), false);
SELECT pg_catalog.setval('connecteurs_organizations_id_seq', ( SELECT coalesce(max("connecteurs_organizations".id), 0) + 1 FROM "connecteurs_organizations" ), false);
SELECT pg_catalog.setval('io-acknowledgements_id_seq', ( SELECT coalesce(max("io-acknowledgements".id), 0) + 1 FROM "io-acknowledgements" ), false);
SELECT pg_catalog.setval('io-alertes_id_seq', ( SELECT coalesce(max("io-alertes".id), 0) + 1 FROM "io-alertes" ), false);
SELECT pg_catalog.setval('io-archivedeliveries_id_seq', ( SELECT coalesce(max("io-archivedeliveries".id), 0) + 1 FROM "io-archivedeliveries" ), false);
SELECT pg_catalog.setval('io-archivedeliveryrequests_id_seq', ( SELECT coalesce(max("io-archivedeliveryrequests".id), 0) + 1 FROM "io-archivedeliveryrequests" ), false);
SELECT pg_catalog.setval('io-archivedestructionnotifications_id_seq', ( SELECT coalesce(max("io-archivedestructionnotifications".id), 0) + 1 FROM "io-archivedestructionnotifications" ), false);
SELECT pg_catalog.setval('io-archivedestructionrequestreplies_id_seq', ( SELECT coalesce(max("io-archivedestructionrequestreplies".id), 0) + 1 FROM "io-archivedestructionrequestreplies" ), false);
SELECT pg_catalog.setval('io-archivedestructionrequests_id_seq', ( SELECT coalesce(max("io-archivedestructionrequests".id), 0) + 1 FROM "io-archivedestructionrequests" ), false);
SELECT pg_catalog.setval('io-archiverestitutionrequestreplies_id_seq', ( SELECT coalesce(max("io-archiverestitutionrequestreplies".id), 0) + 1 FROM "io-archiverestitutionrequestreplies" ), false);
SELECT pg_catalog.setval('io-archiverestitutionrequests_id_seq', ( SELECT coalesce(max("io-archiverestitutionrequests".id), 0) + 1 FROM "io-archiverestitutionrequests" ), false);
SELECT pg_catalog.setval('io-archiverestitutions_id_seq', ( SELECT coalesce(max("io-archiverestitutions".id), 0) + 1 FROM "io-archiverestitutions" ), false);
SELECT pg_catalog.setval('io-archivetransferacceptances_id_seq', ( SELECT coalesce(max("io-archivetransferacceptances".id), 0) + 1 FROM "io-archivetransferacceptances" ), false);
SELECT pg_catalog.setval('io-archivetransferreplies_id_seq', ( SELECT coalesce(max("io-archivetransferreplies".id), 0) + 1 FROM "io-archivetransferreplies" ), false);
SELECT pg_catalog.setval('io-archivetransfers_id_seq', ( SELECT coalesce(max("io-archivetransfers".id), 0) + 1 FROM "io-archivetransfers" ), false);
SELECT pg_catalog.setval('io-avscans_id_seq', ( SELECT coalesce(max("io-avscans".id), 0) + 1 FROM "io-avscans" ), false);
SELECT pg_catalog.setval('io-erreurs_id_seq', ( SELECT coalesce(max("io-erreurs".id), 0) + 1 FROM "io-erreurs" ), false);
SELECT pg_catalog.setval('io-messages_id_seq', ( SELECT coalesce(max("io-messages".id), 0) + 1 FROM "io-messages" ), false);
SELECT pg_catalog.setval('io-piecejointes_id_seq', ( SELECT coalesce(max("io-piecejointes".id), 0) + 1 FROM "io-piecejointes" ), false);
SELECT pg_catalog.setval('keywords_keywords_id_seq', ( SELECT coalesce(max("keywords_keywords".id), 0) + 1 FROM "keywords_keywords" ), false);
SELECT pg_catalog.setval('oaipmh-resumptiontoken_records_id_seq', ( SELECT coalesce(max("oaipmh-resumptiontoken_records".id), 0) + 1 FROM "oaipmh-resumptiontoken_records" ), false);
SELECT pg_catalog.setval('oaipmh-resumptiontokens_id_seq', ( SELECT coalesce(max("oaipmh-resumptiontokens".id), 0) + 1 FROM "oaipmh-resumptiontokens" ), false);
SELECT pg_catalog.setval('organizations_typeacteurs_id_seq', ( SELECT coalesce(max("organizations_typeacteurs".id), 0) + 1 FROM "organizations_typeacteurs" ), false);
SELECT pg_catalog.setval('ref-accessrestrictioncodes_id_seq', ( SELECT coalesce(max("ref-accessrestrictioncodes".id), 0) + 1 FROM "ref-accessrestrictioncodes" ), false);
SELECT pg_catalog.setval('ref-appraisalcodes_id_seq', ( SELECT coalesce(max("ref-appraisalcodes".id), 0) + 1 FROM "ref-appraisalcodes" ), false);
SELECT pg_catalog.setval('ref-descriptionlevels_id_seq', ( SELECT coalesce(max("ref-descriptionlevels".id), 0) + 1 FROM "ref-descriptionlevels" ), false);
SELECT pg_catalog.setval('ref-documenttypes_id_seq', ( SELECT coalesce(max("ref-documenttypes".id), 0) + 1 FROM "ref-documenttypes" ), false);
SELECT pg_catalog.setval('ref-filetypes_id_seq', ( SELECT coalesce(max("ref-filetypes".id), 0) + 1 FROM "ref-filetypes" ), false);
SELECT pg_catalog.setval('ref-keywordtypes_id_seq', ( SELECT coalesce(max("ref-keywordtypes".id), 0) + 1 FROM "ref-keywordtypes" ), false);
SELECT pg_catalog.setval('ref-languagecodes_id_seq', ( SELECT coalesce(max("ref-languagecodes".id), 0) + 1 FROM "ref-languagecodes" ), false);
SELECT pg_catalog.setval('ref-mimecodes_id_seq', ( SELECT coalesce(max("ref-mimecodes".id), 0) + 1 FROM "ref-mimecodes" ), false);
SELECT pg_catalog.setval('ref-replycodes_id_seq', ( SELECT coalesce(max("ref-replycodes".id), 0) + 1 FROM "ref-replycodes" ), false);
SELECT pg_catalog.setval('services_users_id_seq', ( SELECT coalesce(max("services_users".id), 0) + 1 FROM "services_users" ), false);
SELECT pg_catalog.setval('wkf-circuits_id_seq', ( SELECT coalesce(max("wkf-circuits".id), 0) + 1 FROM "wkf-circuits" ), false);
SELECT pg_catalog.setval('wkf-compositions_id_seq', ( SELECT coalesce(max("wkf-compositions".id), 0) + 1 FROM "wkf-compositions" ), false);
SELECT pg_catalog.setval('wkf-etapes_id_seq', ( SELECT coalesce(max("wkf-etapes".id), 0) + 1 FROM "wkf-etapes" ), false);
SELECT pg_catalog.setval('wkf-signatures_id_seq', ( SELECT coalesce(max("wkf-signatures".id), 0) + 1 FROM "wkf-signatures" ), false);
SELECT pg_catalog.setval('wkf-traitementpjs_id_seq', ( SELECT coalesce(max("wkf-traitementpjs".id), 0) + 1 FROM "wkf-traitementpjs" ), false);
SELECT pg_catalog.setval('wkf-traitements_id_seq', ( SELECT coalesce(max("wkf-traitements".id), 0) + 1 FROM "wkf-traitements" ), false);
SELECT pg_catalog.setval('wkf-traitements_compositions_id_seq', ( SELECT coalesce(max("wkf-traitements_compositions".id), 0) + 1 FROM "wkf-traitements_compositions" ), false);
SELECT pg_catalog.setval('wkf-traitements_etapes_id_seq', ( SELECT coalesce(max("wkf-traitements_etapes".id), 0) + 1 FROM "wkf-traitements_etapes" ), false);

--
-- Fin mise à jour des valeurs des séquences pour le champ id de chaque table
--

--
-- Insertion de la version du schéma de la base
--
INSERT INTO versions (date, version)
VALUES (current_timestamp, 'V1.6.0');

--
-- Validation de la transaction
--
COMMIT;
