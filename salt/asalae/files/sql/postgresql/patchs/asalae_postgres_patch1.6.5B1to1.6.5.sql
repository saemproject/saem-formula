--
-- PostgreSQL database dump : patch 1.6.5 de la base de données de l'application as@lae
--
-- Note : patch a appliquer sur une version 1.6.4
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

-- CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- Début du patch
--
BEGIN;

--
-- Name: adm-crons; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
INSERT INTO "adm-crons" (id, nom, description, controller, action, has_params, next_execution_time, execution_duration, last_execution_start_time, last_execution_end_time, last_execution_report, last_execution_status, active, created, created_user_id, modified, modified_user_id)
VALUES
  (22, 'Mise à jour Libersign', 'Met à jour le logiciel de signature Libersign pour l''utilsation de  l''extension de signature des navigateurs Firefox et Chrome.',
   'traitements', 'updateLibersign', FALSE, now(), 'P1D', now(), now(), 'Création de la tâche planifiée', 'SUCCES',
   TRUE, now(), '1', now(), '1');

--
-- Name: adm-users; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "adm-users" ADD COLUMN refexterieur_id integer DEFAULT null;
ALTER TABLE "adm-users" ADD COLUMN reflogin VARCHAR(255) DEFAULT null;
ALTER TABLE "adm-users" ALTER COLUMN created_user_id DROP NOT NULL;
ALTER TABLE "adm-users" ALTER COLUMN modified_user_id DROP NOT NULL;

--
-- io-archiverestitutionrequests
--
ALTER TABLE "io-archiverestitutionrequests" ADD COLUMN resfiles_nb integer DEFAULT 0;
ALTER TABLE "io-archiverestitutionrequests" ADD COLUMN resfiles_size_bytes bigint DEFAULT 0;

--
-- Fin du patch
--
COMMIT;
