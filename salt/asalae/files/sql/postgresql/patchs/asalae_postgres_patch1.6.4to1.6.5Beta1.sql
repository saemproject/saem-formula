--
-- PostgreSQL database dump : patch 1.6.5 de la base de données de l'application as@lae
--
-- Note : patch a appliquer sur une version 1.6.4
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

-- CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- Début du patch
--
BEGIN;

--
-- Insertion de la version du schéma de la base
--
INSERT INTO versions (date, version)
VALUES (current_timestamp, 'V1.6.5');

--
-- Fin du patch
--
COMMIT;
