--
-- PostgreSQL database dump : patch 1.6.3 de la base de données de l'application as@lae
--
-- Note : patch a appliquer sur une version 1.6.2
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

-- CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- Début du patch
--
BEGIN;

--
-- Name: io-archivedeliveryrequestreplies; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
CREATE TABLE "io-archivedeliveryrequestreplies" (
  id serial primary key,
  sens character varying(1),
  version_seda character varying(128),
  comment text,
  date timestamp without time zone,
  dlv_req_identifier character varying(256) DEFAULT NULL::character varying,
  dlv_req_rep_identifier character varying(256) DEFAULT NULL::character varying,
  reply_code character varying(128) DEFAULT NULL::character varying,
  unit_identifiers_json text,
  access_requester_id integer,
  archival_agency_id integer,
  archivedeliveryrequest_id integer not null,
  fichier_message character varying(1024) DEFAULT NULL::character varying,
  created timestamp without time zone,
  created_user_id integer,
  modified timestamp without time zone,
  modified_user_id integer
);

--
-- Name: adm-compteurs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
COPY "adm-compteurs" (id, nom, description, def_compteur, sequence_id, def_reinit, val_reinit, created, modified, created_user_id, modified_user_id, seda02, seda10, type) FROM stdin;
17	Archivedeliveryrequestreply	Utilisé pour générer le code DeliveryRequestReplyIdentifier ainsi que le nom du fichier d''échange XML.	ADLRR_#s#	17			now	now	1	1	\N	\N	interne
\.

--
-- Data for Name: adm-sequences; Type: TABLE DATA; Schema: public; Owner: asalae
--
COPY "adm-sequences" (id, nom, description, num_sequence, created, created_user_id, modified, modified_user_id) FROM stdin;
17	DeliveryRequestReplyIdentifier	Utilisée par le compteur Archivedeliveryrequestreply.	0	now	1	now	1
\.

--
-- Name: adm-refexterieurs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
CREATE TABLE archivedeliveryrequests_arcobjs (
  id SERIAL PRIMARY KEY,
  archiveobject_id integer NOT NULL,
  archivedeliveryrequest_id integer NOT NULL
);

--
-- Name: adm-crons; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
INSERT INTO "adm-crons" (id, nom, description, controller, action, has_params, next_execution_time, execution_duration, last_execution_start_time, last_execution_end_time, last_execution_report, last_execution_status, active, created, created_user_id, modified, modified_user_id)
VALUES
  (21, 'Vérification des volumes de stockage', 'Vérification des volumes de stockage actifs : accessibilité, taux d''occupation des volumes de type FS, contrôles sur des dates de validité',
   'volumes', 'checkVolumes', FALSE, now(), 'P1D', now(), now(), 'Création de la tâche planifiée', 'SUCCES',
   TRUE, now(), '1', now(), '1');

--
-- Insertion de la version du schéma de la base
--
INSERT INTO versions (date, version)
VALUES (current_timestamp, 'V1.6.3');

--
-- Fin du patch
--
COMMIT;
