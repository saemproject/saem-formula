--
-- PostgreSQL database dump : patch 1.6.4 de la base de données de l'application as@lae
--
-- Note : patch a appliquer sur une version 1.6.3
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

-- CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- Début du patch
--
BEGIN;

--
-- Name: adm-refexterieurs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "adm-refexterieurs" DROP COLUMN IF EXISTS ordre;
ALTER TABLE "adm-refexterieurs" ADD COLUMN ordre INTEGER;
UPDATE "adm-refexterieurs" ref1
SET ordre = (
  SELECT rnum FROM (
    SELECT id, row_number() over (ORDER BY created) rnum
    FROM "adm-refexterieurs") ref2
  WHERE ref2.id = ref1.id);
ALTER TABLE "adm-refexterieurs" ALTER COLUMN ordre SET NOT NULL;

--
-- Name: io-piecejointes; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "io-piecejointes" DROP COLUMN IF EXISTS hash_type;
ALTER TABLE "io-piecejointes" DROP COLUMN IF EXISTS hash_value;
ALTER TABLE "io-piecejointes" ADD COLUMN hash_type VARCHAR(256) DEFAULT NULL;
ALTER TABLE "io-piecejointes" ADD COLUMN hash_value VARCHAR(1024) DEFAULT NULL;

--
-- Insertion de la version du schéma de la base
--
INSERT INTO versions (date, version)
VALUES (current_timestamp, 'V1.6.4');

--
-- Fin du patch
--
COMMIT;
