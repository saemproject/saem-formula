--
-- PostgreSQL database dump : patch 1.6 de la base de données de l'application as@lae
--
-- Note : patch a appliquer sur une version 1.5.03
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

-- CREATE OR REPLACE PROCEDURAL LANGUAGE plpgsql;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- Début du patch
--
BEGIN;

--
-- Name: adm-accords; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "adm-accords" ADD COLUMN tout_producteur boolean DEFAULT false;

--
-- Name: adm-compteurs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "adm-compteurs" ADD COLUMN type VARCHAR(128) DEFAULT 'interne';
COPY "adm-compteurs" (id, nom, description, def_compteur, sequence_id, def_reinit, val_reinit, created, modified, created_user_id, modified_user_id, seda02, seda10, type) FROM stdin;
15	Archivedestructionrequestreply	Utilisé pour générer le code DestructionRequestReplyIdentifier ainsi que le nom du fichier d''échange XML.	ADRR_#s#	15			now	now	1	1	\N	\N	interne
16	Archiverestitutionrequestreply	Utilisé pour générer le code RestitutionRequestReplyIdentifier ainsi que le nom du fichier d'échange XML.	ARRR_#s#	16			now	now	1	1	\N	\N	interne
\.

--
-- Name: adm-crons; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
INSERT INTO "adm-crons" (id, nom, description, controller, action, has_params, next_execution_time, execution_duration, last_execution_start_time, last_execution_end_time, last_execution_report, last_execution_status, active, created, created_user_id, modified, modified_user_id)
VALUES
  (19, 'Référentiels extérieurs : synchronisation', 'Création ou mise à jour des éléments liés aux référentiels extérieurs',
   'refexterieurs', 'synchronisation', FALSE, now(), 'PT1H', now(), now(), 'Création de la tâche planifiée', 'SUCCES',
   FALSE, now(), '1', now(), '1');
INSERT INTO "adm-crons" (id, nom, description, controller, action, has_params, params, next_execution_time, execution_duration, last_execution_start_time, last_execution_end_time, last_execution_report, last_execution_status, active, created, created_user_id, modified, modified_user_id)
VALUES
  (20, 'Rétention des transferts : suppression des fichiers', 'Suppression des fichiers des transferts acceptés une fois le délai de rétention passé. Paramètre 1 : [true, false], optionnel, défaut = true, les archives doivent être intègres avant de pouvoir supprimer les transferts (true).',
   'archivetransfers', 'supprimerFichiersTransfertsAcceptes', TRUE, 'true', now(), 'P1D', now(), now(), 'Création de la tâche planifiée', 'SUCCES',
   FALSE, now(), '1', now(), '1');


--
-- adm-journaux
--
ALTER TABLE "adm-journaux" ALTER COLUMN message TYPE TEXT;
ALTER TABLE "adm-journaux" ADD cycle_vie_type VARCHAR(1) DEFAULT 'N' NOT NULL;
UPDATE "adm-journaux" SET cycle_vie_type = 'A' WHERE cycle_vie = true;
UPDATE "adm-journaux" SET cycle_vie_type = 'I' WHERE cycle_vie = true AND message like 'Vérification de l''intégrité des fichiers de l''archive%';

--
-- Name: adm-keywordlists; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "adm-keywordlists" ADD COLUMN modifiable boolean DEFAULT true;
ALTER TABLE "adm-keywordlists" ADD COLUMN identifiant VARCHAR(256) DEFAULT NULL;
UPDATE  "adm-keywordlists" SET identifiant = '67717/Matiere' WHERE id = 1;
UPDATE  "adm-keywordlists" SET identifiant = '67717/Actions' WHERE id = 2;
UPDATE  "adm-keywordlists" SET identifiant = '67717/Contexte' WHERE id = 3;
UPDATE  "adm-keywordlists" SET identifiant = '67717/Typologie' WHERE id = 4;
UPDATE  "adm-keywordlists" SET identifiant = nom WHERE id > 4;
ALTER TABLE "adm-keywordlists" ALTER COLUMN identifiant SET NOT NULL;

--
-- Name: adm-keywords; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
UPDATE "adm-keywords" SET code = substr(code, 7) WHERE substr(code, 0, 7) = '67717/';
UPDATE "adm-keywords" SET code='67717/'||code WHERE keywordlist_id IN (1, 2, 3, 4);
CREATE INDEX "adm-keywords_kwlidcode" ON "adm-keywords" USING btree (keywordlist_id, code);

--
-- Name: adm-profils; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "adm-profils" ADD COLUMN modifiable boolean DEFAULT true;
UPDATE "adm-profils" SET modifiable=FALSE WHERE identifiant = 'JNLEVT_ASALAE';
ALTER TABLE "adm-profils" DROP COLUMN IF EXISTS schema_xml CASCADE;

--
-- Data for Name: adm-sequences; Type: TABLE DATA; Schema: public; Owner: asalae
--
COPY "adm-sequences" (id, nom, description, num_sequence, created, created_user_id, modified, modified_user_id) FROM stdin;
15	DestructionRequestReplyIdentifier	Utilisée par le compteur Archivedestructionrequestreply.	0	now	1	now	1
16	RestitutionRequestReplyIdentifier	Utilisée par le compteur Archiverestitutionrequestreply.	0	now	1	now	1
\.

--
-- Name: adm-refexterieurs; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
CREATE TABLE "adm-refexterieurs" (
  id SERIAL PRIMARY KEY,
  type VARCHAR(255) NOT NULL,
  driver_name VARCHAR(255) NOT NULL,
  nom VARCHAR(255) NOT NULL,
  description TEXT,
  actif BOOLEAN DEFAULT TRUE,
  parametres TEXT NOT NULL,
  execution_date TIMESTAMP WITHOUT TIME ZONE,
  update_date TIMESTAMP WITHOUT TIME ZONE,
  update_report TEXT,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  modified TIMESTAMP WITHOUT TIME ZONE,
  created_user_id INTEGER,
  modified_user_id INTEGER
);

--
-- Data for Name: adm-verrous; Type: TABLE DATA; Schema: public; Owner: asalae
--
INSERT INTO "adm-verrous" (nom, created, modified)
  VALUES ('Refexterieur:synchronisation', current_timestamp, current_timestamp);

--
-- Name: arc-organizations; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
ALTER TABLE "arc-organizations" ADD COLUMN modifiable boolean DEFAULT true;

--
-- Name: io-archivedestructionrequestreplies; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
CREATE TABLE "io-archivedestructionrequestreplies" (
  id serial primary key,
  sens character varying(1),
  version_seda character varying(128),
  comment text,
  date timestamp without time zone,
  des_req_identifier character varying(256) DEFAULT NULL::character varying,
  des_req_rep_identifier character varying(256) DEFAULT NULL::character varying,
  reply_code character varying(128) DEFAULT NULL::character varying,
  unit_identifiers_json text,
  archival_agency_id integer,
  originating_agency_id integer,
  archivedestructionrequest_id integer not null,
  fichier_message character varying(1024) DEFAULT NULL::character varying,
  created timestamp without time zone,
  created_user_id integer,
  modified timestamp without time zone,
  modified_user_id integer
);

--
-- Name: io-archiverestitutionrequestreplies; Type: TABLE; Schema: public; Owner: -; Tablespace:
--
CREATE TABLE "io-archiverestitutionrequestreplies" (
  id serial primary key,
  sens character varying(1),
  version_seda character varying(128),
  comment text,
  date timestamp without time zone,
  reply_code character varying(128) DEFAULT NULL::character varying,
  res_req_identifier character varying(256) DEFAULT NULL::character varying,
  res_req_rep_identifier character varying(256) DEFAULT NULL::character varying,
  unit_identifiers_json text,
  archival_agency_id integer,
  originating_agency_id integer,
  archiverestitutionrequest_id integer not null,
  fichier_message character varying(1024) DEFAULT NULL::character varying,
  created timestamp without time zone,
  created_user_id integer,
  modified timestamp without time zone,
  modified_user_id integer
);





--
-- Insertion de la version du schéma de la base 
--
INSERT INTO versions (date, version)
VALUES (current_timestamp, 'V1.6.0');

--
-- Fin du patch
--
COMMIT;
