{% set p  = salt['pillar.get']('java', {}) %}
{% set g  = salt['grains.get']('java', {}) %}

{%- set java_home            = salt['grains.get']('java_home', salt['pillar.get']('java_home', '/opt/jre')) %}

{%- set default_version_name = 'jre1.6.0_45' %}
{%- set default_prefix       = '/opt' %}
{%- set default_filename   = 'jre-6u45-linux-x64.tgz' %}
{%- set default_source_hash  = '5a912f027461157dc488f21228a9efc34a72c059645334e106368347e410aa83' %}

{%- set version_name         = g.get('version_name', p.get('version_name', default_version_name)) %}
{%- set filename           = g.get('filename', p.get('filename', default_filename)) %}


{%- set prefix               = g.get('prefix', p.get('prefix', default_prefix)) %}
{%- set java_real_home       = prefix + '/' + version_name %}
{%- set jre_lib_sec          = java_real_home + '/jre/lib/security' %}

{%- set java = {} %}
{%- do java.update( { 'version_name'   : version_name,

                      'source_hash'    : source_hash,
                      'filename'      : filename,
                      'java_home'      : java_home,
                      'prefix'         : prefix,
                      'java_real_home' : java_real_home,
                      'jre_lib_sec'    : jre_lib_sec,
                    } ) %}
