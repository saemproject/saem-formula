# -*- coding: utf-8 -*-
# vim: ft=sls
{%- from 'asalae/cines/map.jinja' import cines with context %}

cines:
  pkg.installed:
    - name: ImageMagick

mediainfo:
  pkg:
    - installed

cines_dist:
  archive.extracted:
    - name: /opt/
    - source: {{ cines.dist.url }}
    - archive_format: tar
    - source_hash: {{ cines.dist.hash }}
    - keep: True  # XXX
    - if_missing: /opt/formatValidator2/

/opt/formatValidator2/launch_asalae.sh:
  file.managed:
    - source: salt://asalae/cines/launch_asalae.sh
    - hash: md5=f772a1226ba2ce3bb0d7809c1091a91b
    - mode: 750
    - if_missing: /tmp/formatValidator2/

/opt/formatValidator2/etc/jhove.conf:
  file.managed:
    - source: salt://asalae/cines/jhove.conf
