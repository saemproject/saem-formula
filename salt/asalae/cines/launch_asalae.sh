#!/bin/sh

########################################################################
# script de lancement du validateur de format
# auteur prat@cines.fr
# last maj : 2010/08/02
# Usage: [-path] repertoire ou fichier pour lancer la validation
########################################################################

# load envionnement vars
.	 /opt/formatValidator2/load_validator.sh

# Java JRE directory -- change to your local java home
export JAVA_HOME=/opt/jre
export JAVA=$JAVA_HOME/bin/java

# Retrieve a copy of all command line arguments to pass to the application.

# Set the CLASSPATH and invoke the Java loader.
${JAVA}  -classpath $VALIDATOR_JARS -Dfvhome=${FV_HOME} $LIB_JNI_OPTION  fr.cines.pac.format.CmdMgr -path "$*"
