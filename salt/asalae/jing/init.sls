# -*- coding: utf-8 -*-
# vim: ft=sls
{%- from 'asalae/jing/map.jinja' import jing with context %}

unzip:
  pkg:
    - installed

jing_dist:
  archive:
    - name: /opt
    - extracted
    - source: {{ jing.dist.url }}
    - archive_format: zip
    - source_hash: {{ jing.dist.hash }}
    - user: apache
    - if_missing: /opt/jing-20091111
    - keep: True
    - require:
      - pkg: unzip

/opt/jing:
  file.symlink:
    - target: /opt/jing-20091111
    - user: apache
    - group: apache
