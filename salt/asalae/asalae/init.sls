{% from "asalae/asalae/map.jinja" import asalae with context %}

include:
    - common.epel
    - common.postgres.client
    - asalae.apache
    - asalae.apache.mod_ssl
    - asalae.apache.mod_php5
    - asalae.apache.mod_rewrite
    - asalae.php
    - asalae.php.pear
    - asalae.php.pgsql
    - asalae.php.soap
    - asalae.php.xml
    - asalae.php.mbstring
    - asalae.php.intl

packages:
  pkg.installed:
     - pkgs:
       - rsync
       - mailx
       - wget
       - ntp
       - unzip
       - clamav
       - clamav-db
       - clamd

clamav-service:
 cmd.run:
  - name: |
     setenforce 0
     chmod +x /etc/init.d/clamd
     /sbin/chkconfig --add clamd
     /sbin/chkconfig  clamd on
     /etc/init.d/clamd restart

asalae_dist:
  archive:
    - name: /opt
    - extracted
    - source: {{ asalae.dist.url }}
    - archive_format: zip
    - keep: True
    - if_missing: /opt/{{ asalae.dist.name }}

/var/www/asalae:
  file.symlink:
    - target: /opt/{{ asalae.dist.name }}
    - require:
      - pkg: apache
      - archive: asalae_dist

rightcorrection:
 cmd.run:
 - name: chmod -R 757 /opt/{{ asalae.dist.name }}
{% for cfg in ('asalae.ini.php', 'database.php', 'types_connecteurs.ini.php') %}
/var/www/asalae/app/config/{{ cfg }}:
  file.managed:
    - source: salt://asalae/asalae/{{ cfg }}
    - template: jinja
    - require:
      - file: /var/www/asalae
{% endfor %}


XML_RPC:
  pecl.installed:
    - require:
      - pkg: php-pear

/var/www/asalae/cake/console/cake:
 file.managed:
 - mode: 0755

models:
 cmd.run:
  - name: cd /opt/{{ asalae.dist.name }}/app/webroot/files/models && rename _default.odt .odt *.odt
/data/echanges/entree:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/echanges/sortie:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/archives:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/messages:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/root/.pgpass:
  file.managed:
    - contents: {{ asalae.db.host }}:{{ asalae.db.port }}:{{ asalae.db.database }}:{{ asalae.db.user }}:{{ asalae.db.password }}
    - mode: 0600

/var/www/asalae/app/webroot/check/.htpasswd:
  file.managed:
    - source: salt://asalae/asalae/htpasswd

/etc/httpd/conf.d/asalae.conf:
  file.managed:
    - source: salt://asalae/asalae/apache-vhost.conf
    - template: jinja

/etc/php.ini:
  file.managed:
    - source: salt://asalae/asalae/php.ini

/etc/sysconfig/selinux:
  file.managed:
    - source: salt://asalae/asalae/selinux

/var/www/asalae/app/webroot/files/jhoveConfig.xsd:
  file.managed:
    - source: salt://asalae/asalae/jhoveConfig.xsd

inject_schema:
  cmd.run:
    - name: psql -h {{ asalae.db.host }} -p {{ asalae.db.port }} -U {{ asalae.db.user }} {{ asalae.db.database }} < /var/www/asalae/app/config/sql/postgresql/asalae_postgres_1.6.0.sql
    - unless: psql -h {{ asalae.db.host }} -p {{ asalae.db.port }}  -U {{ asalae.db.user }} {{ asalae.db.database }} -c "SELECT * FROM acos ;"

majschema:
 cmd.run:
  - name: |
     cake/console/cake majschema160_161
     cake/console/cake majschema161_162
     cake/console/cake majschema162_163
     cake/console/cake majschema163_164
  - cwd : /var/www/asalae/

del_pgpass:
  file.absent:
   - name: /root/.pgpass
