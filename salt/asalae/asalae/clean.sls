rename_data_dir:
 cmd.run:
 - name: mv /data /data"-`date +%Y-%m-%d`"

/data/echanges/entree:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/echanges/sortie:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/archives:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

/data/messages:
  file.directory:
    - user: apache
    - group: apache
    - makedirs: True
    - recurse:
      - user
      - group
      - mode

