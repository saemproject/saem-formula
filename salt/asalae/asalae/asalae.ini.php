<?php
/**
 * Fichier de configuration de as@lae
 */

/**
 * Nom de l'instance, du service d'archives ou de la collectivité administratrice de as@lae
 */
$config['Denomination'] = '';

/**
 * Options générales d'affichage des pages
 */
$config['Affichage']['message'] = '';
$config['Affichage']['nblignes'] = 15;
$config['Affichage']['pied_page'] = '';
$config['Affichage']['titre_login'] = '';
$config['Affichage']['masque_logo_asalae'] = false;
$config['Affichage']['logo_name'] = '';

/**
 * Configuration des répertoires
 */
$config['Repertoire']['entree'] = '/datatemp/echanges/entree';
$config['Repertoire']['sortie'] = '/datatemp/echanges/sortie';
$config['Repertoire']['stockageMessages'] = '/datatemp/messages';
$config['Repertoire']['planRangement'] = '#OriginatingAgencyIdentification#/#AAAA#/#ArchivalAgreement#/#ArchivalAgencyArchiveIdentifier#';

/**
 * Option pour la gestion des droits
 */
$config['Droits']['heritageRestrictif'] = true;
/**
 Gestion fichiers volumineux
*/
$config['Flowjs']['enabled'] = true;
$config['Flowjs']['chunkSize'] = (1024 * 1024) * 100;

/**
 * Envoi des mails
 */
$config['Mail']['envoi_par_smtp'] = false;
$config['Mail']['smtp_port'] = 25;
$config['Mail']['smtp_timeout'] = 30;
$config['Mail']['smtp_host'] = 'your.smtp.server';
$config['Mail']['smtp_username'] = 'your_smtp_username';
$config['Mail']['smtp_password'] = 'your_smtp_password';
$config['Mail']['smtp_client'] = 'smtp_helo_hostname';

$config['Mail']['from'] = 'asalae <noReply@exemple.com>';
$config['Mail']['replyto'] = 'asalae <noReply@exemple.com>';
$config['Mail']['subject_prefix'] = '[SAE]';

$config['Mail']['administrateur'] = 'admin@exemple.com';

/**
 * Configuration de l'horodatage
 *  $config['Horodatage']['type'] : type du service d'horodatage utilisé pour l'horodatage des messages SEDA, des pièces jointes et du journal (caractères, 'OPENSIGN'|'IAIK'|'CRYPTOLOG' optionnel)
 *  $config['Horodatage']['msg']['type'] : type du service d'horodatage utilisé pour l'horodatage des messages SEDA (caractères, 'OPENSIGN'|'IAIK'|'CRYPTOLOG' optionnel)
 *  $config['Horodatage']['pjs']['type'] : type du service d'horodatage utilisé pour l'horodatage des pièces jointes (caractères, 'OPENSIGN'|'IAIK'|'CRYPTOLOG' optionnel)
 *  $config['Horodatage']['jnl']['type'] : type du service d'horodatage utilisé pour l'horodatage des fichiers d'eport du journal des événements (caractères, 'OPENSIGN'|'IAIK'|'CRYPTOLOG' optionnel)
*/
$config['Horodatage']['type'] = 'OPENSIGN';
$config['Opensign']['host'] = 'http://horodatage.local/opensign.wsdl';

/**
 * Configuration des interruptions de service (en cas de maintenance, sauvegarde, réplication ...)
 */
// Doit-on prévenir le service versant de l'interruption de service suite à un transfert (true|false)
$config['InterruptionService']['prevenirVersant'] = true;
// Doit-on prévenir l'administrateur qu'un transfert a été effectué pendant une interruption du service (true|false)
$config['InterruptionService']['prevenirAdministrateur'] = true;
// définition des plages horaires d'interruption du service
// heures sous la forme hh:mm:ss (exemple : '08:00:00' pour 8 heures du matin)
$config['InterruptionService']['Plages'][1]['debut'] = '23:00:00';
$config['InterruptionService']['Plages'][1]['fin'] = '23:30:00';
$config['InterruptionService']['Plages'][1]['message'] = '';
/**
*$config['Proxy']['host'] = ''; // fqdn ou ip du proxy
*$config['Proxy']['port'] = ''; // port du proxy
*$config['Proxy']['userpwd'] = ''; // nom d'utilisateur et mot de passe formatés sous la forme "[username]  *[password]" pour la connexion avec le proxy
*/
/**
 * Configuration pour le traitement des transferts
 */
$config['TraitementTransferts']['traitementAsynchrone'] = false;
$config['TraitementTransferts']['deplaceRefuses'] = true;
$config['TraitementTransferts']['defaultCommentATROk'] = 'Votre transfert d\'archive a été pris en compte par la plate-forme as@lae';
$config['TraitementTransferts']['defaultCommentATA'] = 'Votre transfert d\'archive a été accepté par la plate-forme as@lae';
$config['TraitementTransferts']['defaultCommentATRKo'] = 'Votre transfert d\'archive a été rejeté par la plate-forme as@lae';
$config['TraitementTransferts']['autoriseValidationAutomatique'] = true;
$config['TraitementTransferts']['autoriseSansAccord'] = true;
$config['TraitementTransferts']['nbJoursRetention'] = 0;

/**
 * Configuration pour le traitement des archives
 */
$config['Archives']['elimine_acceptee_archivage_definitif'] = true;

/**
 * Configuration de la méthode de validation des relaxng
 * $config['RelaxngValidation']['type'] : type de validation (caractères 'DOM'|'JING', 'DOM' par défaut)
 * Paramètre 'DOM' : validation par le DOMDocument de PHP : pas de paramètre
 * Paramètre 'JING' : validation par l'outil JING
 * $config['RelaxngValidation']['jing']['java'] : ligne de commande pour execution java (caractères, optionnel, 'java' par défaut)
 * $config['RelaxngValidation']['jing']['jar'] : uri vers le fichier jar de jing (caractères, optionnel, '/opt/jing/bin/jing.jar' par défaut)
 * $config['RelaxngValidation']['jing']['options'] : options de jing (caractères, optionnel, '' par défaut)
 */
$config['RelaxngValidation']['type'] = 'JING';

/**
 * Configuration pour la validation des pièces jointes
 */
$config['FormatValidator']['type'] = 'CINES';
$config['FormatValidator']['home'] = '/opt/formatValidator2/';

/**
 * Configuration de l'outil de detection du format des fichiers (pièces jointes, ...)
 * $config['FormatDetector']['type'] : type de validation (caractères 'FIDO', vide par défaut)
 */
$config['FormatDetector']['type'] = 'FIDO';

/**
 * Configuration de l'antivirus
 */
$config['Antivirus']['type'] = 'CLAMAV-DAEMON';
$config['Antivirus']['exec'] = '/usr/bin/clamscan';

/**
 * Configuration des éditions
 */
$config['Edition']['type'] = 'GEDOOO';
$config['Edition']['repertoireModeles'] = WWW_ROOT.'files'.DS.'models'.DS;
$config['Edition']['GEDOOO_WSDL'] = "http://127.0.0.1:8880/ODFgedooo/OfficeService?wsdl";

/**
 * Configuration pour la conversion de format des documents
 * Paramètres communs :
 *  $config['Conversion']['type'] : type de l'outil de conversion (caractères 'UNOCONV'|'CLOUDOOO')
 * Paramètre 'UNOCONV' : unoconv
 *  $config['Conversion']['exec'] : uri vers le fichier exécutable de unoconv (caractères, obligatoire)
 * Paramètre 'CLOUDOOO' : serveur de conversion CloudOOO
 *  $config['Conversion']['cloudoooUrl'] : url du serveur CloudOOO (caractères, obligatoire)
 *  $config['Conversion']['cloudoooInfo'] : information de version de Cloudooo (caractères, obligatoire)
 */
$config['Conversion']['type'] = 'CLOUDOOO';
$config['Conversion']['cloudoooUrl'] = '127.0.0.1:8011';
$config['Conversion']['cloudoooInfo'] = 'CLOUDOOO Version 1.2.2 utilisant LIBREOFFICE version 3.5.4';

/**
 * Configuration pour l'exportation du journal des évènements
 * Les paramètres suivants sont optionnels :
 * $config['ExportJournal']['ArchivalAgency'] = 'FRAD000';
 * $config['ExportJournal']['VolumeIdentifier'] = 'Volume01';
 * $config['ExportJournal']['Rangement'] = 'journal_evenement_asalae';
 */

/**
 * Configuration des volumes de stockage
 * Paramètres communs :
 *  $config['Volume']['volumeIdentifier']['nom'] : nom du volume (caractères, optionnel)
 *  $config['Volume']['volumeIdentifier']['description'] : description (caractères, optionnel)
 *  $config['Volume']['volumeIdentifier']['type'] : type du volume (caractères 'FS'|'STS-PEA'|'DX6000'|'HCP'|'EMC-ATMOS', optionnel, 'FS' par défaut)
 *  $config['Volume']['volumeIdentifier']['actif'] : indique si le volume est utilisable (booléen true|false, optionnel, true par défaut)
 *  $config['Volume']['volumeIdentifier']['date_debut'] : date de début de validité du volume (caractères format date 'AAAA-MM-JJ', optionnel)
 *  $config['Volume']['volumeIdentifier']['date_fin'] : date de fin de validité du volume (caractères format date 'AAAA-MM-JJ', optionnel)
 *  $config['Volume']['volumeIdentifier']['alerte_delais_date_fin'] : délais d'alerte avant la date de fin de valididté (caractères format xsd:duration 'PnYnMnDTnHnMnS', optionnel)
 *  $config['Volume']['volumeIdentifier']['alerte_taux_occupation'] : taux d'alerte d'occupation du volume en % (numérique de 0 à 100, optionnel)
 *  $config['Volume']['volumeIdentifier']['secondaire'] : identifiant du volume de stockage secondaire (écriture en Y)
 * Paramètre type='FS' : File System
 *  $config['Volume']['volumeIdentifier']['type'] = 'FS';
 *  $config['Volume']['volumeIdentifier']['repertoire'] = ''; // chemin vers le répertoire du volume (caractères, obligatoire)
 * Paramètres type=STS-PEA : coffre fort numérique STS-PEA
 *  $config['Volume']['volumeIdentifier']['type'] = 'STS-PEA';
 *  $config['Volume']['volumeIdentifier']['urlWsdl'] = ''; // url wsdl des webservice du coffre (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['userLogin'] = ''; // identifiant de connexion (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['userPassword'] = ''; // mot de passe de connexion (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['vaultId'] = ''; // identifiant coffre (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['containerId'] = ''; // identifiant conteneur (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['depositMethod'] = 'uri'; // méthode de dépôt dans le coffre (caractères 'uri'|'stream', optionnel, 'uri' par défaut)
 *  $config['Volume']['volumeIdentifier']['depositHashAlgorithm'] = 'SHA256'; fonction de hachage à utiliser pour le dépôt (caractères 'MD5'|'SHA1'|'SHA256'|'SHA384'|'SHA512'|'WHIRPOOL', optionnel, 'SHA256' par défaut)
 * Paramètres type=DX6000 : baie de stockage DELL DX6000 CAStor
 *  $config['Volume']['volumeIdentifier']['type'] = 'DX6000';
 *  $config['Volume']['volumeIdentifier']['url'] = ''; // url csn (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['port'] = 80; // port (catactères, optionnel, '80' par défaut)
 *  $config['Volume']['volumeIdentifier']['reps'] = 2; // nombre de réplications des fichiers (numérique, optionnel, 2 par défaut)
 * Paramètres type=HCP : Hitachi Content Platform
 *  $config['Volume']['volumeIdentifier']['type'] = 'HCP';
 *  $config['Volume']['volumeIdentifier']['login'] = ''; // login (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['password'] = ''; // mot de passe (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['https'] = true; // force le https (booléen true|false, optionnel, true par défaut)
 *  $config['Volume']['volumeIdentifier']['host'] = ''; // nom d'hote (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['tenant'] = ''; // nom du conteneur (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['nameSpace'] = ''; // nom du namespace (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['port'] = 0; // numéro du port (caractères, optionnel)
 * Paramètres type='EMC-ATMOS' : EMC Atmos Cloud Storage
 *  $config['Volume']['volumeIdentifier']['type'] = 'EMC-ATMOS';
 *  $config['Volume']['volumeIdentifier']['host'] = ''; // url ou ip de la baie (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['port'] = 80; // numéro du port (caractères, optionnel, '80' par défaut)
 *  $config['Volume']['volumeIdentifier']['fullTokenId'] = ''; // identifiant de connexion (caractères, obligatoire)
 *  $config['Volume']['volumeIdentifier']['sharedSecret'] = ''; // secret (caractères, obligatoire)
 *
$config['Volume']['Volume01']['nom'] = 'Volume de stockage principal';
$config['Volume']['Volume01']['repertoire'] = '/data/archives/';
*/


/** Configuration des Volumes pour une écriture en Y () */

$config['Volume']['Volume01']['nom'] = 'Stockage Principal';
$config['Volume']['Volume01']['repertoire'] = '/data_cd33_YA/archives/';
$config['Volume']['Volume01']['secondaire'] = 'Volume02';
$config['Volume']['Volume02']['nom'] = 'Stockage Secondaire';
$config['Volume']['Volume02']['repertoire'] = '/data_cd33_YB/archives/';

/**
 * Gestion resumptionToken (OAI-PMH)
 */
$config['OAIPMH']['maxListSize'] = 500;
$config['OAIPMH']['expiration'] = 1; //Nombre d'heures avant expiration du token

/**
 * Gestion multi-collectivités : ne rien ajouter sous ces lignes
 * Décommenter la ligne ci-dessous pour activer la gestion multi services d'archives
 */
//include(CONFIGS.'loadMulti.php');
?>
