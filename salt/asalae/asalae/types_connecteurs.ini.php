<?php
{% from "asalae/asalae/map.jinja" import asalae with context %}
/**
 * Configuration des types de connecteurs utilisés par as@lae
 */

// LOCAL
$config['Connecteur1']['actif'] = true;
$config['Connecteur1']['type'] = 'LOCAL';
$config['Connecteur1']['nom'] = 'Instance locale as@lae';
$config['Connecteur1']['description'] = 'Les messages sont envoyés sur l\'instance elle même';

// Mail
$config['Connecteur2']['actif'] = true;
$config['Connecteur2']['type'] = 'MAIL';
$config['Connecteur2']['nom'] = 'Par courriel';
$config['Connecteur2']['decription'] = 'Asalae envoie les messages du SEDA (AR, ATR, ATA, ...) par mail';
$config['Connecteur2']['param1_nom'] = 'Courriel(s) des destinataires';
$config['Connecteur2']['param1_info'] = 'Courrier électronique du ou des destinataires (mail1@mail.fr, mail2@mail.fr, ...)';

// ASALAE
$config['Connecteur3']['actif'] = true;
$config['Connecteur3']['type'] = 'ASALAE';
$config['Connecteur3']['nom'] = 'Instance distante as@lae';
$config['Connecteur3']['description'] = 'Asalae envoie les messages du SEDA (AR, ATR, ATA, ...) à une instance as@lae';
$config['Connecteur3']['param1_nom'] = 'Url de l\'instance as@lae destinatrice des messages';
$config['Connecteur3']['param1_info'] = '';
$config['Connecteur3']['param2_nom'] = 'Identifiant de connexion';
$config['Connecteur3']['param2_info'] = '';
$config['Connecteur3']['param3_nom'] = 'Mot de passe de connexion';
$config['Connecteur3']['param3_info'] = '';

// WEBDELIB
$config['Connecteur4']['actif'] = false;
$config['Connecteur4']['type'] = 'WEBDELIB';
$config['Connecteur4']['nom'] = 'Application WebDelib 3';
$config['Connecteur4']['description'] = 'Asalae envoie les messages du SEDA (AR, ATR, ATA, ...) à WebDelib v3';
$config['Connecteur4']['param1_nom'] = 'Url de WebDelib';
$config['Connecteur4']['param1_info'] = '';

// S2LOW
$config['Connecteur5']['actif'] = false;
$config['Connecteur5']['type'] = 'S2LOW';
$config['Connecteur5']['nom'] = 'Application S2LOW';
$config['Connecteur5']['description'] = 'Asalae envoie les messages du SEDA (AR, ATR, ATA, ...) à S2LOW';
$config['Connecteur5']['param1_nom'] = 'Url de S2LOW';
$config['Connecteur5']['param1_info'] = '';

// ARCHILAND
$config['Connecteur6']['actif'] = false;
$config['Connecteur6']['type'] = 'ARCHILAND';
$config['Connecteur6']['nom'] = 'GED Alfresco projet Archiland';
$config['Connecteur6']['description'] = 'Asalae envoie les messages du SEDA (AR, ATR, ATA, ...) à la GED Alfresco Archiland';
$config['Connecteur6']['param1_nom'] = 'Url de la GED Alfresco Archiland';
$config['Connecteur6']['param1_info'] = '';
$config['Connecteur6']['param2_nom'] = 'Identifiant de connexion à la GED Alfresco Archiland';
$config['Connecteur6']['param2_info'] = '';
$config['Connecteur6']['param3_nom'] = 'Mot de passe de connexion à la GED Alfresco Archiland';
$config['Connecteur6']['param3_info'] = '';

// ASALAE RESTFUL
$config['Connecteur7']['actif'] = true;
$config['Connecteur7']['type'] = 'ASALAE RESTFUL';
$config['Connecteur7']['nom'] = 'Instance distante as@lae via webservice REST';
$config['Connecteur7']['description'] = 'Asalae envoie les messages du SEDA (AR, ATR, ATA, ...) à une instance as@lae (RESTFUL)';
$config['Connecteur7']['param1_nom'] = 'Url de l\'instance as@lae destinatrice des messages';
$config['Connecteur7']['param1_info'] = '';
$config['Connecteur7']['param2_nom'] = 'Identifiant de connexion';
$config['Connecteur7']['param2_info'] = '';
$config['Connecteur7']['param3_nom'] = 'SHA 256 du mot de passe de connexion';
$config['Connecteur7']['param3_info'] = '';
?>