{% from "asalae/asalae/map.jinja" import asalae with context %}

{% if salt['file.directory_exists']('/var/www/asalae/app') %}
include:
    - asalae.proxy
    - asalae.asalae-update.update
{% else %}
include:
    - asalae.proxy
    - asalae.asalae
    - asalae.asalae-install.cron
    - asalae.asalae-install.drivers
    - asalae.oracle-java
    - asalae.cines
    - asalae.cloudooo
    - asalae.gedooo
    - asalae.jing
    - asalae.fido
    - asalae.liberhorodatage
{% endif %}
