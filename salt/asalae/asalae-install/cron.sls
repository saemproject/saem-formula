asalae_cron:
  cmd.run: 
   - name: |
      crontab -r
      (crontab -l ; echo "*/30 * * * * /var/www/asalae/cake/console/cake -app /var/www/asalae/app cron http://`hostname`/ > /dev/null 2>&1") | crontab
