==============
saem-formula
==============

A saltstack formula handling installation and configuration of `saem`_.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``SAEM``
------------

Installation de l'instance de demo.



