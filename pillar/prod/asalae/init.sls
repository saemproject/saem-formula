asalae:
  lookup:
   dist:
    url: salt://asalae/files/asalae1.6.6.zip
    hash: md5=74749892104e9797342f68d98953f7487ce6e329
    name: asalae1.6.6
	proxy: http://srvmv-wpad.cg33.fr:80/
   version:
    actuelle: 165
    cible: 166
   db:
    host: '10.255.234.5'
    database: 'saem_asa'
    user: 'saem_asa'
    login: 'saem_asa'
    password: 'As@la3'
    port: '5432'
   referentiel:
    url: http://10.255.231.26:18080
    token:
     id: 8bda30737fc7464bb4daee3ff4fb7c85
     secret: 8380a12f41eb4128b0e6e6001fb674f4f53946f877c4454786ed0c394357cf6ba3f6f717f0704c19b6161a668a318e922f4bc885b48849ceaf20a1db89a42a2a
