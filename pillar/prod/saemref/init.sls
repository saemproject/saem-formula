saemref:
  lookup:
    versions:
      saemref: 0.22.1
    instance:
      # nom instance referentiel
      name: saemref
      # nom utilisateur systeme
      user: saemref
      # http port
      port: 18080
      oai_port: 18081
      oai_threads: 8
      # Anonymous user
      anonymous_user: anon
      anonymous_password: anon
      # default ARK NAA
      default_ark_naa_who: ADGIRONDE
      default_ark_naa_what: 25651
	  proxy: http://srvmv-wpad.cg33.fr:80/
      # wsgi settings
      # 2 * 8 = can handle 16 concurrent request
      # and will use 16 database connections.
      wsgi: true
      wsgi_workers: 2
      wsgi_threads: 8

      # Secret keys for encrypting session/cookies data and authentication They
      # are MANDATORY and should not be the same.
      sessions_secret: SETME
      authtk_session_secret: SETME1
      authtk_persistent_secret: SETME2

      # Pool size must be equal to wsgi_threads when using wsgi
      pool_size: 8

      # Run in test mode: will replace some data files to create the instance faster
      test_mode: true
    db:
      driver: postgres
      host: "10.255.231.32"
      port: "5432"
      name: saem_ref
      user: saem_ref
      pass: Cub1cw$b
    admin:
      login: admin
      pass: admin

postgres:
  version: 9.4
