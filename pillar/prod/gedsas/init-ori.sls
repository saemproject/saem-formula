alfresco:
  # Emplacement du dossier data
  data: /opt/alfresco-5.0.d/alf_data

  # Nom DNS du serveur ou est installe Alfresco
  host: srvmv-julienne.cg33.fr

  # Parametres de la base de donnees
  db:
    # Machine et port du serveur de base de donnees
    host: srvmv-solange.cg33.fr
    port: 5432
    # Nom de la base
    name: saem_alf_va
    # Identifiants pour acceder a la base
    username: saem_alf
    password: Alfr$sc033

  mail:
    host: hermes.cg33.fr
    port: 25
    username: XXXX
    password: YYYY
    from:
      default: no-reply@ged.cg33.fr
    auth: false

  ldap:
    enabled: true
    authentication:
      active: true
      allowGuestLogin: false
      userNameFormat: '%s@cg33.fr'
      url: ldap://xxx.cg33.fr:389
      defaultAdministratorUserNames: AZY
      principal: admin
      credentials: secret
  synchronization:
    groupDifferentialQuery: (&(objectclass\=group)(!(modifyTimestamp<\={0})))
    personDifferentialQuery: (&(objectclass\=user)(userAccountControl\:1.2.840.113556.1.4.803\:\=512)(!(modifyTimestamp<\={0})))
    groupSearchBase: ou\=GED-CG33,ou\=Groupes Applications,ou\=Groupes,ou\=CGG,dc=cg33,dc=fr
    userSearchBase: ou\=Utilisateurs,ou\=CGG,dc=cg33,dc=fr
    modifyTimestampAttributeName: modifyTimestamp
    syncOnStartup: false

# Nom DNS du serveur ou est installe la webapp share
  share:
    host: srvmv-julienne.cg33.fr
  version:
    alfresco: 1.2
    share: 1.2
  # Parametres du referentiel de logilab
  referentiel:
  # URL du referentiel
    url: http://srvmv-julienne.cg33.fr:18080
  # Token et secret pour authentification HMAC
    token:
      id: f620748d1b4d4d2f8e89615d331049b8
      secret: 76058420d4e8464996eeebcc517890b6869fc39fee214df1ac7486502d4c0eb5c92bd9c8dd794d15b892df6b1b78c505bc489a004f9648118a55e35fc6b7ace9
  # Parametres du job de synchro avec le referentiel
    synchro:
    # cron de lancement de la synchro , chaque trois heures ci-dessous
      cronExpression: 0 30 */3 * * ?

# Parametres de asalae
  asalae:
    url: http://srvmv-claude.cg33.fr
    login: admin
    password: admin
    bigfilesize: 10000000
    cronBigfileTimer: 0 0/3 * * * ?
  # Parametres du job de recuperation des messages seda d asalae
    poller:
    # cron de lancement du job , chaque deux minutes ci-dessous
      cronExpression: 0 */2 * * * ?
  #Parametres du flux PES V2
  pes:
    organization: 
      ark: ark:/25654/o000206493
    profil:
      ark: ark:/25651/p000261736
    vidoc:
      url: http://frmp0179.frml.bull.fr:8080
      username: administrator
      password: bull
