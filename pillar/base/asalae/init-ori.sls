asalae:
  lookup:
   dist:
    url: salt://asalae/files/asalae1.6.6.zip
    hash: md5=74749892104e9797342f68d98953f7487ce6e329
    name: asalae1.6.6
   version:
    actuelle: 165
    cible: 166
   db:
    host: 'srvmv-solange.cg33.fr'
    database: 'saem_asa_va'
    user: 'saem_asa'
    login: 'saem_asa'
    password: 'Asal@3'
    port: '5432'
   proxy:
    http: ''
    https: ''
   referentiel:
    # URL du referentiel
    url: srvmv-claude.cg33.fr:18080
    # Token et secret pour authentification HMAC
    token:
     id: 3ddfe77d331d48ed9e803b3c2f8a81ce 
     secret: a0380072300843368177c9833dc38be6a6931a23e9e74c1eb3ba8bff91512642f84c8a2529384fcc983993e957f5fb0aa7e3707b0d18472db13d29fea96ff3aa
