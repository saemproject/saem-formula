alfresco:
  asalae:
   url:  http://10.255.234.42
   login: admin
   password: admin
   bigfilesize: 10000000
   cronBigfileTimer: 0 0/3 * * * ?
   poller:
    cronExpression: 0 */2 * * * ?
  branch: Preprod
  proxy: http://srvmv-wpad.cg33.fr:80/
  pes:
   organization:
    ark: ark:/25654/o000206493
   profil:
    ark: ark:/25651/p000261736
   vidoc:
    url: http://frmp0179.frml.bull.fr:8080
    username: administrator
    password: bull
  # Emplacement du dossier data
  data: /opt/alfresco-5.0.d/alf_data

  # Nom DNS du serveur ou est installe Alfresco
  host: srvmv-web030.cg33.dmz2

  # Parametres de la base de donnees
  db:
    # Machine et port du serveur de base de donnees
    host: 10.255.234.5
    port: 5432
    # Nom de la base
    name: saem_alf_pp
    # Identifiants pour acceder a la base
    username: saem_alf
    password: Alfr$sc0

  mail:
    host: hermes.cg33.fr
    port: 25
    username: XXXX
    password: YYYY
    from:
      default: no-reply@ged.cg33.fr
    auth: false

  ldap:
    enabled: true
    authentication:
      active: true
      allowGuestLogin: false
      userNameFormat: '%s@cg33.fr'
      url: ldap://xxx.cg33.fr:389
      defaultAdministratorUserNames: AZY
      principal: admin
      credentials: secret
  synchronization:
    groupDifferentialQuery: (&(objectclass\=group)(!(modifyTimestamp<\={0})))
    personDifferentialQuery: (&(objectclass\=user)(userAccountControl\:1.2.840.113556.1.4.803\:\=512)(!(modifyTimestamp<\={0})))
    groupSearchBase: ou\=GED-CG33,ou\=Groupes Applications,ou\=Groupes,ou\=CGG,dc=cg33,dc=fr
    userSearchBase: ou\=Utilisateurs,ou\=CGG,dc=cg33,dc=fr
    modifyTimestampAttributeName: modifyTimestamp
    syncOnStartup: false

  # Nom DNS du serveur ou est installe la webapp share
  share:
    host: srvmv-web030.cg33.dmz2
	login: admin
	password: admin
  startDelay: 
    synchro: 600000
    poller: 600000
    pesv2: 600000
    communication: 600000
    unlock: 600000
    flux: 600000
    transfert: 600000
  cronExpression:
    pesv2: 0 35 7 ? * *
    communication: 0 30 7 ? * *
    unlock: 0 0 7 ? * *
    flux: 0 0/15 * ? * *
    transfert: 0 30 2,13 ? * *
  size: 
    php: 100000000
  duration:
    communication: 14400
    unlock: 43200
  manager:
    site: admin
  vocabulary:
    categoryFile: 
    dua: 
	encode: 
  # Parametres du referentiel de logilab
  referentiel:
  # URL du referentiel
    url: http://10.255.231.23:18080
  # Token et secret pour authentification HMAC
  token:
    id: 8bda30737fc7464bb4daee3ff4fb7c85
    secret: 8380a12f41eb4128b0e6e6001fb674f4f53946f877c4454786ed0c394357cf6ba3f6f717f0704c19b6161a668a318e922f4bc885b48849ceaf20a1db89a42a2a
  # Parametres du job de synchro avec le referentiel
  synchro:
    # cron de lancement de la synchro , chaque trois heures ci-dessous
    cronExpression: 0 30 */3 * * ?                 
